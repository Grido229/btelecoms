@extends('dashboard.layouts.app', ['title' => 'Dashboard'])

@section('content')
<div class="content container">

    <!-- Page Header -->
	<section class="comp-section">
		<nav aria-label="breadcrumb">
			<ol class="breadcrumb">
				<li class="breadcrumb-item"><a href="{{ url('dashboard/home') }}">Home</a></li>
				<li class="breadcrumb-item"><a href="{{ url('dashboard/validations/dcm') }}">Dcm</a></li>
				<li class="breadcrumb-item active" aria-current="page">Détails Dcm</li>
			</ol>
		</nav>
	</section>
    <!-- /Page Header -->
    <div class="row">
        <div class="col-lg-12">
            <div class="card">
                <div class="card-header">
                    <h4 class="card-title">Informations DCM </h4>

                </div>

                <div class="card-body">

                    <div class="row">
                        <p class="col-sm-3 text-muted mb-0 mb-sm-3">Numero Bon Commande</p>
                        <p class="col-sm-9 text-right font-weight-bold"> {{$bon->num_bon}} </p>
                    </div>

                    <div class="row">
                        <p class="col-sm-3 text-muted mb-0 mb-sm-3">Numero Reference</p>
                        <p class="col-sm-9 text-right font-weight-bold"> {{$bon->num_ref}} </p>
                    </div>

                    <div class="row">
                        <p class="col-sm-3 text-muted mb-0 mb-sm-3">Objet</p>
                        <p class="col-sm-9 text-right font-weight-bold">{{$bon->object}}</p>
                    </div>

                    <div class="row">
                        <p class="col-sm-3 text-muted mb-0 mb-sm-3">Date</p>
                        <p class="col-sm-9 text-right font-weight-bold">{{$bon->date}}</p>
                    </div>

                    <div class="row">
                        <p class="col-sm-3 text-muted mb-0 mb-sm-3">Nom operateur</p>
                        <p class="col-sm-9 text-right font-weight-bold"> {{ $bon->client->enterprise_name }} </p>
                    </div>

                    <div class="row">
                        <p class="col-sm-3 text-muted mb-0 mb-sm-3">Adress </p>
                        <p class="col-sm-9 text-right font-weight-bold"> {{ $bon->client->adress }} </p>
                    </div>

                    <div class="row">
                        <p class="col-sm-3 text-muted mb-0 mb-sm-3">Telephone</p>
                        <p class="col-sm-9 text-right font-weight-bold"> {{ $bon->client->tel }} </p>
                    </div>

                    <div class="row">
                        <p class="col-sm-3 text-muted mb-0 mb-sm-3">Capcité</p>
                        @if ($bon->capacite_id != NULL)
                        <p class="col-sm-9 text-right font-weight-bold"> {{ $bon->capacite->capacite }} </p>
                        @else
                        <p class="col-sm-9 text-right font-weight-bold" style="color:red;"> pas de capacite associée à cette demande</p>
                        @endif
                    </div>

                    <div class="row">
                        <p class="col-sm-3 text-muted mb-0 mb-sm-3">Montant</p>
                        <p class="col-sm-9 text-right font-weight-bold"> {{ $bon->total_price_ht }} </p>
                    </div>

                </div>
            </div>
        </div>
    </div>
</div>

@endsection


