@extends('dashboard.layouts.app', ['title' => 'Dashboard'])

@section('content')
<div class="content container-fluid">
	<section class="comp-section">
		<nav aria-label="breadcrumb">
			<ol class="breadcrumb">
				<li class="breadcrumb-item"><a href="{{ url('dashboard/home') }}">Home</a></li>
				<li class="breadcrumb-item"><a href="{{ url('dashboard/bons') }}">Demandes de services</a></li>
				<li class="breadcrumb-item active" aria-current="page">Validations</li>
			</ol>
		</nav>
	</section>
<!--reseach--->

<!--end-reseach--->
	<div class="row">
		<div class="col-sm-12">
			<div class="card">
				<div class="card-header">
                    <h4 class="card-title">Demande en cours de traitement</h4>
                </div>
				<div class=pull-right>
				    <div class="input-group-prepend"></div>
                </div>
                <div class=pull-left>
                    <div class="input-group-prepend"></div>
                </div>


				<div class="card-body">
                    <div class="container">
                        <div class="input-group">
                            <input class="form-control" id="myInput" type="text" placeholder="Recherche..">
                        </div>
                    </div>

                    <div class="row my-3">
                        <form action="{{ url('dashboard/validations/dcm/filter') }}" method="POST">
                            @csrf

                            <div class="form-row">
                                <div class="col-lg-9">
                                    <select class="form-control" name="filter">
                                        @if ($type == 'non_validate')
                                            <option selected value="non_validate">Demandes non validées</option>
                                        @else
                                            <option value="non_validate">Demandes non validées</option>
                                        @endif

                                        @if ($type == 'already_validate')
                                            <option selected value="already_validate">Demandes validées</option>
                                        @else
                                            <option value="already_validate">Demandes validées</option>
                                        @endif
                                    </select>
                                </div>

                                <div class="col-lg-3">
                                    <button type="submit" class="btn btn-info">Voir</button>
                                </div>
                            </div>
                        </form>
                    </div>


					<div class="table-responsive">
                        <table class="datatable table table-stripped">
                            <thead>
                                <tr>
									<th>Numéro</th>
                                    <th>Entreprise</th>
                                    <th>Objet</th>
                                    <th>Date</th>
                                    <th class="text-center">Action</th>
                                </tr>
                            </thead>
                            <tbody id="myTable">
                                @foreach ($bons as $bon)
                                    <tr>
                                        <td> {{ $bon->num_bon }}</td>
                                        <td> {{ $bon->client->enterprise_name }}</td>
                                        <td>
                                            <?php
                                                echo substr($bon->object, 0 , 36);
                                                if (strlen($bon->object) > 36) {
                                                    echo "...";
                                                }
                                            ?>
                                        </td>

                                        <td>{{ $bon->date }}</td>

                                        <td class="text-center">
                                            <a href="{{ route('dcm.datails',$bon->id) }}" title="Afficher les details" class="btn btn-sm btn-info px-2" style="border-radius: .5rem;">
                                                <i class="fa fa-eye"></i>
                                            </a>
                                        </td>


                                        @if ($type == 'non_validate')
                                            <td>
                                                <form action="{{ url('dashboard/validations/dcm/validate/' . $bon->id) }}" method="POST">
                                                    @csrf
                                                    <button class="btn btn-success">
                                                        Valider
                                                    </button>
                                                </form>



                                            </td>
                                        @endif
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                        <!--reseach--->
                    <script>
                        $(document).ready(function(){
                            $("#myInput").on("keyup", function() {
                                var value = $(this).val().toLowerCase();
                                $("#myTable tr").filter(function() {
                                $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
                                });
                            });
                        });
                    </script>
                    <!--end-reseach--->
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection
