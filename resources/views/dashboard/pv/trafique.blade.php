@extends('dashboard.layouts.app', ['title' => 'Dashboard'])

@section('content')
<div class="content container-fluid">

					<!-- Page Header -->
	<section class="comp-section">
		<nav aria-label="breadcrumb">
			<ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="{{ url('dashboard/home') }}">Home</a></li>
				<li class="breadcrumb-item"><a href="{{ url('dashboard/pv') }}">Procès verbaux</a></li>
				<li class="breadcrumb-item active" aria-current="page">Ajouter un procès verbal de trafic</li>
			</ol>
		</nav>
	</section>
	<!-- /Page Header -->

	<!-- Row -->
	<div class="row">
		<div class="col-sm-12">

			<!-- Custom Boostrap Validation -->
			<div class="card">
				<div class="card-header">
					<h4 class="card-title">Ajouter un procès verbal de trafic</h4>
				</div>
				<div class="card-body">

                    <!--debut formulaire-->
                    <form method="POST" action="{{ url('dashboard/pv/create') }}">
                        @csrf


                        <div>
                            <a href="{{ url('dashboard/pv') }}" style="margin-right: 10px" class="btn btn-primary">Annuler</a>
                            <button type="submit" class="btn btn-success">
                                Ajouter
                            </button>
                        </div>
                    </form>

                    <!--fin formulaire-->

				</div>
			</div>


		</div>
	</div>
	<!-- /Row -->

				</div>
@endsection
