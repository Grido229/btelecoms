@extends('dashboard.layouts.app', ['title' => 'Dashboard'])

@section('content')
<div class="content container">

    <!-- Page Header -->
	<section class="comp-section">
		<nav aria-label="breadcrumb">
			<ol class="breadcrumb">
				<li class="breadcrumb-item"><a href="{{ url('dashboard/home') }}">Home</a></li>
				<li class="breadcrumb-item"><a href="{{ url('dashboard/pv') }}">Procès verbaux</a></li>
				<li class="breadcrumb-item active" aria-current="page">Détails</li>
			</ol>
		</nav>
	</section>
    <!-- /Page Header -->
    <div class="row">
        <div class="col-lg-12">
            <div class="card">
                <div class="card-header">
                    <h4 class="card-title">Informations du procès verbal</h4>
                    <a href="{{ url('dashboard/pv/' . $pv->id . '/edit') }}" class="btn btn-success mt-4 float-right" style="margin-top: -2rem !important;">Modifier</a>
                    <a href="{{ url('dashboard/pv/' . $pv->id . '/pdfDetail') }}" class="btn btn-primary mt-4 float-right" style="margin-top: -2rem !important;">Imprimer</a>

                </div>
                <div class="card-body">
                    <div class="row">
                        <p class="col-sm-3 text-muted mb-0 mb-sm-3">Numéro de reférence</p>
                        <p class="col-sm-9 text-right font-weight-bold">{{ $pv->bon->num_ref }}</p>
                    </div>

                    <div class="row">
                        <p class="col-sm-3 text-muted mb-0 mb-sm-3">Numero pv</p>
                        <p class="col-sm-9 text-right font-weight-bold">{{ $pv->num_commande_protocoles }}</p>
                    </div>

                    <div class="row">
                        <p class="col-sm-3 text-muted mb-0 mb-sm-3">Type pv</p>
                        <p class="col-sm-9 text-right font-weight-bold">{{ $pv->type }}</p>
                    </div>

                    <div class="row">
                        <p class="col-sm-3 text-muted mb-0 mb-sm-3">Statut</p>
                        <p class="col-sm-9 text-right font-weight-bold">{{$pv->status}}</p>
                    </div>
                {{--
                    <div class="row">
                        <p class="col-sm-3 text-muted mb-0 mb-sm-3">Date status</p>
                        <p class="col-sm-9 text-right font-weight-bold">{{$pv->status_date}}</p>
                    </div>

                    <div class="row">
                        <p class="col-sm-3 text-muted mb-0 mb-sm-3">Objet status</p>
                        <p class="col-sm-9 text-right font-weight-bold">{{$pv->status_object}}</p>
                    </div>
                --}}
                    <div class="row">
                        <p class="col-sm-3 text-muted mb-0 mb-sm-3">Objet</p>
                        <p class="col-sm-9 text-right font-weight-bold">{{ $pv->bon->object }}</p>
                    </div>
                    <div class="row">
                        <p class="col-sm-3 text-muted mb-0 mb-sm-3">Entreprise</p>
                        <p class="col-sm-9 text-right font-weight-bold"> {{ $pv->client->enterprise_name }}</p>
                    </div>

                    <div class="row">
                        <p class="col-sm-3 text-muted mb-0 mb-sm-3">Salle d'acceuil</p>
                        <p class="col-sm-9 text-right font-weight-bold">{{$pv->reception_room}}</p>
                    </div>

                    <div class="row">
                        <p class="col-sm-3 text-muted mb-0 mb-sm-3">Condition du local</p>
                        <p class="col-sm-9 text-right font-weight-bold">{{$pv->local_conditions}}</p>
                    </div>

                    <div class="row">
                        <p class="col-sm-3 text-muted mb-0 mb-sm-3">Date de mise en service</p>
                        <p class="col-sm-9 text-right font-weight-bold">{{$pv->commissioningDate}}</p>
                    </div>

                    <div class="row">
                        <p class="col-sm-3 text-muted mb-0 mb-sm-3">Type offre</p>
                        <p class="col-sm-9 text-right font-weight-bold">{{$pv->type_offre}}</p>
                    </div>

                    @if ($pv->type == 'capacite')

                        <h5 class="font-weight-bold my-2 py-2">Equipements</h5>

                        <div class="row">
                            <p class="col-sm-3 text-muted  mb-3">Support</p>
                            <p class="col-sm-9 text-right font-weight-bold mb-0">{{$pv->support ? 'Oui' : 'Non'}}</p>
                        </div>

                        <div class="row">
                            <p class="col-sm-3 text-muted mb-3">Type d'equipements</p>
                            <p class="col-sm-9 text-right font-weight-bold mb-3">{{$pv->type_equipment}}</p>
                        </div>



                        <div class="row">
                            <p class="col-sm-3 text-muted mb-3">Longueur guide</p>
                            <p class="col-sm-9 text-right font-weight-bold mb-3">{{$pv->guide_length}} m</p>
                        </div>

                        <div class="row">
                            <p class="col-sm-3 text-muted mb-3">Nombre d'antennes</p>
                            <p class="col-sm-9 text-right font-weight-bold mb-3">{{$pv->nbr_antenna}}</p>
                        </div>

                        <div class="row">
                            <p class="col-sm-3 text-muted mb-3">Type d'antennes</p>
                            <p class="col-sm-9 text-right font-weight-bold mb-3">{{$pv->type_antenna}}</p>
                        </div>

                        <div class="row">
                            <p class="col-sm-3 text-muted mb-3">Position TMA depuis le sol</p>
                            <p class="col-sm-9 text-right font-weight-bold mb-3">{{$pv->postion_tma}}m</p>
                        </div>

                        <div class="row">
                            <p class="col-sm-3 text-muted  mb-3">Travaux effectués</p>
                            <p class="col-sm-9 text-right font-weight-bold mb-0">{{$pv->work_done}}</p>
                        </div>

                        <h5 class="font-weight-bold my-2 py-2">Liens numériques/internet</h5>

                        <div class="row">
                            <p class="col-sm-3 text-muted  mb-3">Nombre de liens attribués</p>
                            <p class="col-sm-9 text-right font-weight-bold mb-0">{{$pv->number_links_attributed}} Mb/s</p>
                        </div>
                        @if ($pv->distance != null)
                            <div class="row">
                                <p class="col-sm-3 text-muted  mb-3">Distance Cotonou-station</p>
                                <p class="col-sm-9 text-right font-weight-bold mb-0">{{$pv->distance->distance}}</p>
                            </div>
                        @endif
                    @endif

                    @if ($pv->type == 'colocalisation')
                        <h5 class="font-weight-bold my-3 my-2 py-2">Energie</h5>

                        <div class="row">
                            <p class="col-sm-3 text-muted mb-3">Source 220 AC</p>
                            <p class="col-sm-9 text-right font-weight-bold mb-3">{{$pv->source_ac}}</p>
                        </div>

                        <div class="row">
                            <p class="col-sm-3 text-muted mb-3">Source 48 CC</p>
                            <p class="col-sm-9 text-right font-weight-bold mb-3">{{$pv->sorce_cc}}</p>
                        </div>

                        <div class="row">
                            <p class="col-sm-3 text-muted mb-3">Fournie par</p>
                            <p class="col-sm-9 text-right font-weight-bold mb-3">{{$pv->provider}}</p>
                        </div>

                        <div class="row">
                            <p class="col-sm-3 text-muted mb-3">Intensité consommée par l'équipements</p>
                            <p class="col-sm-9 text-right font-weight-bold mb-3">{{$pv->intensity_consomme}}</p>
                        </div>

                        <div class="row">
                            <p class="col-sm-3 text-muted mb-3">Tension d'alimentation de l'équipements</p>
                            <p class="col-sm-9 text-right font-weight-bold mb-3">{{$pv->tension}}</p>
                        </div>

                        <div class="row">
                            <p class="col-sm-3 text-muted mb-3">Energie consommée</p>
                            <p class="col-sm-9 text-right font-weight-bold mb-3">{{$pv->energy_consomme}}</p>
                        </div>

                        <div class="row">
                            <p class="col-sm-3 text-muted mb-3">Energie consommée par mois</p>
                            <p class="col-sm-9 text-right font-weight-bold mb-3">{{$pv->energy_consomme_per_month}}</p>
                        </div>

                        <h5 class="font-weight-bold my-3 my-2 py-2">Equipements</h5>

                        <div class="row">
                            <p class="col-sm-3 text-muted mb-3">Type d'equipements</p>
                            <p class="col-sm-9 text-right font-weight-bold mb-3">{{$pv->type_equipment}}</p>
                        </div>

                        <div class="row">
                            <p class="col-sm-3 text-muted  mb-3">Support</p>
                            <p class="col-sm-9 text-right font-weight-bold mb-0">{{$pv->support ? 'Oui' : 'Non'}}</p>
                        </div>

                        <div class="row">
                            <p class="col-sm-3 text-muted mb-3">Longueur guide</p>
                            <p class="col-sm-9 text-right font-weight-bold mb-3">{{$pv->guide_length}}</p>
                        </div>

                        <div class="row">
                            <p class="col-sm-3 text-muted mb-3">Nombre d'antennes</p>
                            <p class="col-sm-9 text-right font-weight-bold mb-3">{{$pv->nbr_antenna}}</p>
                        </div>

                        <div class="row">
                            <p class="col-sm-3 text-muted mb-3">Type d'antennes</p>
                            <p class="col-sm-9 text-right font-weight-bold mb-3">{{$pv->type_antenna}}</p>
                        </div>

                        <div class="row">
                            <p class="col-sm-3 text-muted mb-3">Position TMA depuis le sol</p>
                            <p class="col-sm-9 text-right font-weight-bold mb-3">{{$pv->postion_tma}}</p>
                        </div>

                        <div class="row">
                            <p class="col-sm-3 text-muted mb-3">Volume occupé au sol</p>
                            <p class="col-sm-9 text-right font-weight-bold mb-3">{{$pv->sol_capacity}}</p>
                        </div>

                        <h5 class="font-weight-bold my-3 my-2 py-2">Dimensions physiques</h5>

                        <div class="row">
                            <p class="col-sm-3 text-muted mb-3">Longeur</p>
                            <p class="col-sm-9 text-right font-weight-bold mb-3">{{$pv->length}}</p>
                        </div>

                        <div class="row">
                            <p class="col-sm-3 text-muted mb-3">Largeur</p>
                            <p class="col-sm-9 text-right font-weight-bold mb-3">{{$pv->width}}</p>
                        </div>

                        <div class="row">
                            <p class="col-sm-3 text-muted mb-3">Hauteur</p>
                            <p class="col-sm-9 text-right font-weight-bold mb-3">{{$pv->height}}</p>
                        </div>
                        @endif
                        <h5 class="font-weight-bold my-2 py-2">Liens numériques/internet</h5>

                        <div class="row">
                            <p class="col-sm-3 text-muted mb-3">Capacité attribuée</p>
                            @if($pv->bon->capacite_id != null)
                            <p class="col-sm-9 text-right font-weight-bold mb-3">{{$pv->bon->capacite->capacite}}</p>
                            @endif
                        </div>

                        <div class="row">
                            <p class="col-sm-3 text-muted mb-3">ville</p>
                            <p class="col-sm-9 text-right font-weight-bold mb-3">{{$pv->ville}} </p>
                        </div>

                        <div class="row">
                            <p class="col-sm-3 text-muted mb-3">Site</p>
                            <p class="col-sm-9 text-right font-weight-bold mb-3">{{$pv->site}} </p>
                        </div>

                        <div class="row">
                            <p class="col-sm-3 text-muted mb-3">Distance Cotonou-station</p>
                            @if($pv->bon->distances_id != null)
                            <p class="col-sm-9 text-right font-weight-bold mb-3">{{$pv->bon->distance->distance}}</p>
                            @endif
                        </div>


                    @if ($pv-> new_num_commande_protocoles != null )
                        <div class="row">
                            <p class="col-sm-3 text-muted mb-3">Nouveau numero de PV</p>
                            <p class="col-sm-9 text-right font-weight-bold mb-3">{{$pv->new_num_commande_protocoles}}</p>
                        </div>

                        <div class="row">
                            <p class="col-sm-3 text-muted mb-3">Date du nouveau numero de PV</p>
                            {{-- <p class="col-sm-9 text-right font-weight-bold mb-3">{{$date_updated}}</p> --}}
                        </div>
                    @endif

                    <div class="row">
                            <p class="col-sm-3 text-muted mb-3">Montant</p>
                            <p class="col-sm-9 text-right font-weight-bold mb-3">{{$pv->bon->total_price_ht}}</p>
                        </div>

                </div>
            </div>
        </div>
    </div>
</div>

@endsection

