@extends('dashboard.layouts.app', ['title' => 'Dashboard'])

@section('content')
<div class="content container-fluid">

    <!-- Page Header -->
	<section class="comp-section">
		<nav aria-label="breadcrumb">
			<ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="{{ url('dashboard/home') }}">Home</a></li>
				<li class="breadcrumb-item"><a href="{{ url('dashboard/pv') }}">Procès verbaux</a></li>
				<li class="breadcrumb-item active" aria-current="page">Ajouter un procès verbal de capacité</li>
			</ol>
		</nav>
	</section>
	<!-- /Page Header -->

	<!-- Row -->
	<div class="row">
		<div class="col-sm-12">

			<!-- Custom Boostrap Validation -->
			<div class="card">
				<div class="card-header">
					<h4 class="card-title">Ajouter un procès verbal de capacité</h4>
				</div>
				<div class="card-body">
                    <!--debut formulaire-->
					<form method="POST" action="{{ url('dashboard/pv/create') }}">
                        @csrf

                        <div>
                            <input type="hidden" name="type" value="capacite">
                        </div>

                        <div class="form-row mb-3">
                            <div class="col-md-4 mb-3">
                                <label for="bon_id">Bon de commande<span style="color:red">*</span></label>
                                <select style="font-size : 18px;" name="bon_id" class="form-control" id="bon_id" required>
                                    <option disabled selected>Sélectionnez le bon de commande</option>
                                    @foreach ($bons as $bon)
                                        <option value="{{$bon->id}}">{{$bon->num_bon}}</option>
                                    @endforeach
                                </select>
                                @error('bon_id')
                                    <span class="text-danger" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                        </div>
                            <div class="col-md-4 mb-3" style=" display:none;" id="site">
                                <label for="site">Site<span >*</span></label>
                                <select style="font-size : 18px;" name="site" class="form-control">
                                    <option disabled selected>Sélectionnez le site</option>
                                    @foreach($sites as $site )
                                    <option >{{$site->type}}</option>
                                    @endforeach
                                </select>
                                @error('site')
                                    <span class="text-danger" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>

                            <div class="col-md-6 mb-3">
                                <label for="ville">ville<span style="color:red">*</span></label>
                                <input type="text" class="form-control" id="ville" name="ville" value="{{old('ville')}}"  required>
                                @error('ville')
                                    <span class="text-danger" role="alert">
                                        <strong>Ce champ est obligatoire.</strong>
                                    </span>
                                @enderror
                            </div>

                            <div class="col-md-6 mb-3">
                                <label for="reception_room">Salle d'acceuil<span style="color:red">*</span></label>
                                <input type="text" class="form-control" id="reception_room" name="reception_room" required value="{{ old('reception_room') }}">
                                @error('reception_room')
                                    <span class="text-danger" role="alert">
                                        <strong>Ce champ est obligatoire.</strong>
                                    </span>
                                @enderror
                            </div>

                            <div class="col-md-6 mb-3">
                                <label for="local_conditions">Condition du local<span style="color:red">*</span></label>
                                <select style="font-size : 18px;" class="form-control" name="local_conditions" required>
                                    <option disabled selected>Sélectionnez la condition</option>
                                    <option>LCPN</option>
                                    <option>LCFP</option>
                                    <option>LNC</option>
                                </select>
                                @error('local_conditions')
                                    <span class="text-danger" role="alert">
                                        <strong>Ce champ est obligatoire.</strong>
                                    </span>
                                @enderror
                            </div>

                            <div class="col-md-6 mb-3">
                                <label for="commissioningDate">Date de mise en service<span style="color:red">*</span></label>
                                <input type="date" class="form-control" id="commissioningDate" name="commissioningDate" value="{{ old('commissioningDate') }}" required>
                                @error('commissioningDate')
                                    <span class="text-danger" role="alert">
                                        <strong>Ce champ est obligatoire.</strong>
                                    </span>
                                @enderror
                            </div>

                            <div class="col-md-6 mb-3">
                                <label for="status">Statut<span style="color:red"></span></label>
                                <select style="font-size : 18px;" type="text" class="form-control" id="status" name="status" value="{{ old('status') }}" >

                                <option disabled selected>Sélectionnez un statut</option>
                                    <option>ACTIVE</option>
                                    <option>DESACTIVE</option>
                                    <option>SUSPENDU</option>
                                    <option>UPGRADE</option>
                                    <option>DOWNGRADE</option>

                                </select>

                            </div>
                        </div>

                        <h5 class="font-weight-bold">Equipements</h5>

                        <div class="form-row mb-3">
                            <div class="col-md-4 mb-3">
                                <label for="support">Support</label>
                                <select style="font-size : 18px;" class="form-control" name="support">
                                    <option disabled selected>Sélectionnez une option</option>
                                    <option value="1">Oui</option>
                                    <option value="0">Non</option>
                                </select>
                            </div>

                            <div class="col-md-4 mb-3">
                                <label for="guide_length">Longueur guides (m)</label>
                                <input type="number" class="form-control" name="guide_length" id="guide_length" value="{{old('guide_length')}}" required>
                                @error('guide_length')
                                    <span class="text-danger" role="alert">
                                        <strong>Ce champ est obligatoire.</strong>
                                    </span>
                                @enderror
                            </div>

                            <div class="col-md-4 mb-3">
                                <label for="nbr_antenna">Nombre d'antennes</label>
                                <input type="number" class="form-control" name="nbr_antenna" value="{{old('nbr_antenna')}}" id="nbr_antenna" required>
                                @error('nbr_antenna')
                                    <span class="text-danger" role="alert">
                                        <strong>Ce champ est obligatoire.</strong>
                                    </span>
                                @enderror
                            </div>

                            <div class="col-md-4 mb-3">
                                <label for="type_antenna">Type d'antennes</label>
                                <input type="text" class="form-control" name="type_antenna"  value="{{old('type_antenna')}}" id="type_antenna" required>
                                @error('type_antenna')
                                    <span class="text-danger" role="alert">
                                        <strong>Ce champ est obligatoire.</strong>
                                    </span>
                                @enderror
                            </div>

                            <div class="col-md-4 mb-3">
                                <label for="weight">Poids d'antennes + Support (kg)</label>
                                <input type="number" class="form-control" name="weight" value="{{old('weight')}}"  id="weight" required>
                                @error('weight')
                                    <span class="text-danger" role="alert">
                                        <strong>Ce champ est obligatoire.</strong>
                                    </span>
                                @enderror
                            </div>

                            <div class="col-md-4 mb-3">
                                <label for="position_tma">Positon TMA depuis le sol (m)</label>
                                <input type="number" class="form-control" name="position_tma" value="{{old('position_tma')}}"  id="position_tma" required>
                                @error('position_tma')
                                    <span class="text-danger" role="alert">
                                        <strong>Ce champ est obligatoire.</strong>
                                    </span>
                                @enderror
                            </div>


                            <div class="col-md-12 mb-3">
                                <label for="work_done">Travaux effectués<span style="color:red">*</span></label>
                                <input type="text" class="form-control" id="work_done" name="work_done" required value="{{ old('work_done') }}">
                                @error('work_done')
                                    <span class="text-danger" role="alert">
                                        <strong>Ce champ est obligatoire.</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>
                        <div>
                            <a href="{{ url('dashboard/pv') }}" style="margin-right: 10px" class="btn btn-primary">Annuler</a>
                            <button type="submit" class="btn btn-success">
                                Ajouter
                            </button>
                        </div>
					</form>
                    <!--fin formulaire-->
				</div>
			</div>
		</div>
	</div>
	<!-- /Row -->

</div>
@endsection

@section('scripts')
    <script src="{!!asset('/js/jquery-3.2.1.min.js')!!}"></script>
    <script>

        jQuery(document).ready(function($){


            $('#fs').change(function(){
                switch( $(this).val() )
                {
                    case '1':
                    $('#Sfs1').show();
                    $('#Sfs2').hide();
                    $('#Sfs3').hide();
                    break;


                    case '2':
                    $('#Sfs2').show();
                    $('#Sfs1').hide();
                    $('#Sfs3').hide();
                    break;

                    case '3':
                    $('#Sfs2').hide();
                    $('#Sfs1').hide();
                    $('#Sfs3').show();
                    break;

                    case '4':
                    $('#Sfs2').hide();
                    $('#Sfs1').hide();
                    $('#Sfs3').hide();
                    break;


                    default:
                        break;
                }


            });


            $('#Sfs11_id').change(function(){
                switch( $(this).val() )
                {
                    case '1':
                    $('#Ssfs1').show();
                    $('#Ssfs2').hide();
                    $('#Ssfs3').hide();
                    break;

                    case '2':
                    $('#Ssfs1').hide();
                    $('#Ssfs2').hide();
                    $('#Ssfs3').hide();
                    break;

                    case '3':
                    $('#Ssfs1').hide();
                    $('#Ssfs2').hide();
                    $('#Ssfs3').hide();
                    break;

                    case '4':
                    $('#Ssfs2').show();
                    $('#Ssfs1').hide();
                    $('#Ssfs3').hide();
                    break;

                    case '5':
                    $('#Ssfs2').hide();
                    $('#Ssfs1').hide();
                    $('#Ssfs3').hide();
                    break;

                    case '6':
                    $('#Ssfs2').hide();
                    $('#Ssfs1').hide();
                    $('#Ssfs3').hide();
                    break;

                    default:
                    break;
                }


            });

            $('#Sfs13_id').change(function(){
                switch( $(this).val() )
                {
                    case '8':
                    $('#Ssfs1').hide();
                    $('#Ssfs2').hide();
                    $('#Ssfs3').hide();
                    break;

                    case '9':
                    $('#Ssfs1').hide();
                    $('#Ssfs2').hide();
                    $('#Ssfs3').hide();
                    break;

                    case '10':
                    $('#Ssfs1').hide();
                    $('#Ssfs2').hide();
                    $('#Ssfs3').show();
                    break;

                    case '11':
                    $('#Ssfs2').hide();
                    $('#Ssfs1').hide();
                    $('#Ssfs3').hide();
                    break;

                    case '12':
                    $('#Ssfs2').hide();
                    $('#Ssfs1').hide();
                    $('#Ssfs3').hide();
                    break;

                    default:
                    break;
                }


            });
        })

    </script>
@endsection('scripts')
