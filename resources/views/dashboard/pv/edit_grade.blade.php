@extends('dashboard.layouts.app', ['title' => 'Dashboard'])

@section('content')
<div class="content container-fluid">

    <!-- Page Header -->
	<section class="comp-section">
		<nav aria-label="breadcrumb">
			<ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="{{ url('dashboard/home') }}">Home</a></li>
				<li class="breadcrumb-item"><a href="{{ url('dashboard/pv') }}">Procès verbaux</a></li>
				<li class="breadcrumb-item active" aria-current="page">Ajouter un procès verbal de capacité</li>
			</ol>
		</nav>
	</section>
	<!-- /Page Header -->

	<!-- Row -->
	<div class="row">
		<div class="col-sm-12">

			<!-- Custom Boostrap Validation -->
			<div class="card">
				<div class="card-header">
					<h4 class="card-title">Downgrade ou Upgrade de capacité</h4>
				</div>
				<div class="card-body">
                    <!--debut formulaire-->
					<form method="POST" action="{{ route('pv.update',$id) }}">
                        @csrf

                        <div>
                            <input type="hidden" name="type" value="capacity">
                        </div>

                        <div class="form-row mb-3">
                            <div class="col-md-4 mb-3">
                                <label for="bon_id">Bon de commande<span style="color:red">*</span></label>
                                <select style="font-size : 18px;" name="bon_id" class="form-control" id="bon_id" disabled >
                                    <option  selected>{{$pv->bon->num_ref}}</option>

                                </select>
                            </div>

                            <div class="col-md-4 mb-3">
                                <label for="type_id">Type offre<span style="color:red">*</span></label>
                                <select style="font-size : 18px;" name="type_id" class="form-control" id="type_id" disabled>
                                    @if ($pv->bon->ssfs_id == null )
                                            <option disabled selected>{{$pv->bon->sfs->libelle}}</option>
                                        @else
                                            <option disabled selected>{{$pv->bon->ssfs->libelle}}</option>
                                    @endif
                                </select>
                            </div>
                            <input type="text" hidden value="{{$pv->bon->sfs->id}}" name="Sfs_id">
                            @if ($pv->bon->ssfs_id == null )
                            <input type="text" hidden value="{{$pv->bon->ssfs_id}}" name="Sfss_id">
                            @else
                            <input type="text" hidden value="{{$pv->bon->ssfs->id}}" name="Ssfs_id">
                            @endif

                            <div class="col-md-4 mb-3">
                                <label for="site">Site<span style="color:red">*</span></label>
                                <select style="font-size : 18px;" name="site" class="form-control" id="site" disabled>
                                    @if($pv->bon->zones_id != null)
                                        <option  selected value="{{$pv->bon->zone->id}}">{{$pv->bon->zone->zone}}</option>
                                      @else
                                        <option  selected value="{{$pv->bon->zones_id}}"> </option>
                                      @endif
                                </select>
                            </div>

                            <div class="col-md-6 mb-3">
                                <label for="ville">ville<span style="color:red">*</span></label>
                                <input type="text" class="form-control" id="ville" value= '{{$pv->ville}}' disabled >
                            </div>

                            <div class="col-md-6 mb-3">
                                <label for="reception_room">Salle d'acceuil<span style="color:red">*</span></label>
                                <input type="text" class="form-control" id="reception_room" value= '{{$pv->reception_room}}' disabled>

                            </div>

                            <div class="col-md-6 mb-3">
                                <label for="local_conditions">Condition du local<span style="color:red">*</span></label>
                                <select style="font-size : 18px;" class="form-control"  disabled>
                                    <option  selected>{{$pv->local_conditions}}</option>
                                </select>

                            </div>

                            <div class="col-md-6 mb-3">
                                <label for="commissioningDate">Date de mise en service<span style="color:red">*</span></label>
                                <input type="date" class="form-control" id="commissioningDate" value="{{ $pv->commissioningDate}}" disabled>
                            </div>

                            <div class="col-md-6 mb-3">
                                <label for="status">Statut<span style="color:red"></span></label>
                                <select style="font-size : 18px;" type="text" class="form-control" id="status" disabled >
                                     <option  selected>{{$pv->status}}</option>
                                </select>

                            </div>
                        </div>

                        <h5 class="font-weight-bold">Equipements</h5>

                        <div class="form-row mb-3">
                            <div class="col-md-4 mb-3">
                                <label for="support">Support</label>
                                <select style="font-size : 18px;" class="form-control"" disabled>
                                    <option  selected>{{$pv->support}}</option>
                                </select>
                            </div>

                            <div class="col-md-4 mb-3">
                                <label for="guide_length">Longueur guides (m)</label>
                                <input type="number" class="form-control"  id="guide_length" value="{{$pv->guide_length}}" disabled>

                            </div>

                            <div class="col-md-4 mb-3">
                                <label for="nbr_antenna">Nombre d'antennes</label>
                                <input type="number" class="form-control" value="{{$pv->nbr_antenna}}" id="nbr_antenna" disabled>

                            </div>

                            <div class="col-md-4 mb-3">
                                <label for="type_antenna">Type d'antennes</label>
                                <input type="text" class="form-control"  id="type_antenna" value="{{$pv->type_antenna}}" disabled>

                            </div>

                            <div class="col-md-4 mb-3">
                                <label for="weight">Poids d'antennes + Support (kg)</label>
                                <input type="number" class="form-control" id="weight" value="{{$pv->weight}}" disabled>

                            </div>

                            <div class="col-md-4 mb-3">
                                <label for="position_tma">Positon TMA depuis le sol (m)</label>
                                <input type="number" class="form-control" id="position_tma" value="{{$pv->position_tma}}" disabled>

                            </div>


                            <div class="col-md-12 mb-3">
                                <label for="work_done">Travaux effectués<span style="color:red">*</span></label>
                                <input type="text" class="form-control" id="work_done"  disabled value="{{ $pv->work_done }}">

                            </div>
                        </div>

                        <h5 class="font-weight-bold">Liens numériques/internet</h5>
                        <div class="form-row">
                            @if($pv->bon->capacite_id != null)
                                <div class="col-md-6 mb-3">
                                <label for="capacite"> Capacité <span style="color:red">*</span></label>
                                        <select style="font-size : 18px;" class="form-control" name="capacite" >

                                            <option selected value="{{$pv->bon->capacite->id}}" >{{$pv->bon->capacite->capacite}}</option>
                                            @foreach($capacites as $capacite)

                                                @if ($capacite->id == $pv->bon->capacite->id)

                                                @else
                                                <option value="{{$capacite->id}}">{{$capacite->capacite}}</option>
                                                @endif

                                            @endforeach
                                        </select>
                                        @error('capacite')
                                            <span class="text-danger" role="alert">
                                                <strong>Ce champ est obligatoire.</strong>
                                            </span>
                                        @enderror
                                </div>
                            @else
                                <div class="col-md-6 mb-3">
                                    <label for="capacite"> Capacité <span style="color:red">*</span></label>
                                            <select disabled style="font-size : 18px;" class="form-control" name="capacite" >

                                                <option disabled selected > </option>

                                            </select>

                                </div>
                            @endif
                            <div class="col-md-6 mb-3">
                                    <label for="distance">Distance cotonou-station (Km)<span style="color:red">*</span></label>
                                    <select style="font-size : 18px;" class="form-control" name="distance" disabled >
                                        <option disabled selected>Distance cotonou-station (Km)</option>
                                        @if($pv->bon->distances_id != null)
                                        <option  selected value="{{$pv->bon->distance->id}}">{{$pv->bon->distance->distance}}</option>
                                      @else
                                        <option  selected value="{{$pv->bon->distances_id}}"> </option>
                                      @endif
                                    </select>
                                    @error('distance')
                                        <span class="text-danger" role="alert">
                                            <strong>Ce champ est obligatoire.</strong>
                                        </span>
                                    @enderror
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="col-md-6 mb-3">
                                    <label for="volume">Volume<span style="color:red">*</span></label>
                                    <select style="font-size : 18px;" class="form-control" disabled >
                                      @if($pv->volume != null)
                                        <option  selected >{{$pv->bon->volume->volume}}</option>
                                      @endif
                                    </select>
                            </div>
                            <div class="col-md-6 mb-3">
                                <label for="surface">Surface</label>
                                <input type="number" class="form-control" id="surface"  value="{{ $pv->surface }}" disabled>
                            </div>
                        </div>

                        <div class="form-row">
                            <div class="col-md-6 mb-3">
                                <label for="frais_installation">Frais d'installation</label>
                                <input type="number" class="form-control" id="frais_installation" disabled value="{{ $pv->frais_installation }}">
                            </div>
                        </div>


                        <div class="form-row mb-12 justify-content-end">
                            <div class="col-md-5 float-right">
                            <div class="form-row">
                                <div class="custom-control custom-checkbox mb-3 ml-2">
                                    <input type="checkbox" class="custom-control-input" id="price_id" name="price_id" onclick="afficherMont()">
                                    <label class="custom-control-label" for="price_id">Voir le montant</label>
                                </div>
                            </div>
                                <div style="display:none;" id="look_price"  style="display:none">
                                    <label for="total_price"><span style="color:red; font-weight:900;">Prix total HT</span></label>
                                    <input type="text" class="form-control" id="total_price_ht" name="total_price_ht" readonly>
                                </div>

                            </div>

                            </div>
                                <div id="total_price" style="display:none" class="col-md-5 float-right" id="total_price">
                                    <label for="total_price"><span style="color:red; font-weight:900;">Saisir le montant</span></label>
                                    <input type="text" class="form-control" id="total_price" name="total_price" >
                                </div>
                            </div>
                        </div>



                        <div>
                            <a href="{{ url('dashboard/pv') }}" style="margin-right: 10px" class="btn btn-primary">Annuler</a>
                            <button type="submit" class="btn btn-success">
                                Ajouter
                            </button>
                        </div>
					</form>
                    <!--fin formulaire-->
				</div>
			</div>
		</div>
	</div>
	<!-- /Row -->

</div>
@endsection


@section('scripts')

<script>


function afficherMont(){

        if (price_id) {

            $.ajaxSetup({
            headers: {
                        'X-CSRF-TOKEN' : $('meta[name="csrf-token"]').attr('content')
                    }
            });

            $.ajax({
                type: "POST",
                url: '{{route('ajaxrequestgrade')}}',
                dataType: "json",
                data: {
                    "Ssfs_id" : $('[name = Ssfs_id]').val() ,
                    "Sfs_id" : $('[name = Sfs_id]').val() ,
                    "distance" : $('[name = distance]').val() ,
                    "capacite" : $('[name = capacite]').val() ,
                    "site" : $('[name = site]').val() ,
                        },
                success:function(data){
                    console.log(data.message);


                    total_price_ht.value = data.message ;
                    if (data.message == 'Has not price') {
                        $("#total_price").show();
                        document.getElementById("total_price").placeholder = 'Saisir le montant' ;
                    }else{
                        $("#total_price").hide();
                    }

                    },
                error:function(data){
                    console.log('error') ;
                }
            });

            $("#look_price").show();


        }else if(price_id){ 
            $("#look_price").hide();
        }

}
</script>

@endsection
