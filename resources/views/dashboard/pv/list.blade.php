@extends('dashboard.layouts.app', ['title' => 'Dashboard'])

@section('content')
<div class="content container-fluid">
	<section class="comp-section">
		<nav aria-label="breadcrumb">
			<ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="{{ url('dashboard/home') }}">Home</a></li>
				<li class="breadcrumb-item active" aria-current="page">Procès verbaux</li>
			</ol>
		</nav>
    </section>

	<div class="row">
		<div class="col-sm-12">
			<div class="card">
				<div class="card-header">
                    <h4 class="card-title">Liste des Procès verbaux</h4>
                    <!--a href="{{ url('dashboard/pv/create') }}" class="btn btn-success mt-4 float-right" style="margin-top: -2rem !important;">Nouveau procès verbal</a-->
					<!--debut liste-->
					<div >
                    <a href="{{ url('dashboard/pv/pdfPv') }}" title="Imprimer la liste des pv" class="btn btn-info mt-4 float-right" style="margin-top: -2rem !important;">
                        <i class="fa fa-print"></i>
                        Imprimer
                    </a>
					<!--fin liste-->
				</div>
				<div class="card-body">
                    <!--reseach--->
                    {{-- <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
                    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script> --}}

                    <div class="container">
                        <div class="row my-4">
                            <input class="form-control" id="myInput" type="text" placeholder="Recherche..">
                        </div>
                    </div>
                    <!--end-reseach--->

					<div class="table-responsive">
                        <!--debut table pv-->
                        <table class="datatable table table-stripped">
                            <thead>
                                <tr>
                                    <th>Numero</th>
                                    <th>Entreprise</th>
                                    <th>Numero de référence</th>
                                    <th>Date</th>
                                    <th>Type PV</th>
                                    <th class="text-center">Actions</th>
                                </tr>
                            </thead>
                            <tbody id="myTable">
                                @foreach ($pvs as $pv)
                                    <tr>
                                        <td> {{ $pv->num_commande_protocoles }}</td>
                                        <td> {{ $pv->bon->client->enterprise_name }}</td>
                                        <td> {{ $pv->bon->num_ref }} </td>
                                        <td> {{ $pv->date }}</td>
                                        <td> {{ $pv->type }} </td>
                                        <td class="text-center">
                                            <a href="{{ url('dashboard/pv/' . $pv->id .'/detail') }}" title="Afficher les details" class="btn btn-sm btn-info px-2" style="border-radius: .5rem;">
                                                <i class="fa fa-eye"></i>
                                            </a>

                                            <a href="{{ url('dashboard/pv/' . $pv->id . '/edit') }}" title="Editer le pv" class="btn btn-sm btn-danger" style="border-radius: .5rem;">
                                                <i class="fa fa-edit"></i>
                                            </a>

                                            <a id="{{ $pv->id }}" onclick="setPvId(event)" href="{{ url('dashboard/pv/' . $pv->id . '/delete') }}" data-toggle="modal" data-target="#deleteModal" title="Supprimer le pv" class="btn btn-sm btn-danger" style="border-radius: .5rem;">
                                                <i class="fa fa-trash"></i>
                                            </a>

                                            @if( $pv->bon->capacite_id != NULL )
                                            <a href="{{route('pv.grade',$pv->id)}}" class="btn btn-sm btn-danger" title="Downgrade et Upgrade" style="border-radius: .5rem;">
                                                <i class="fa fa-arrows-v"></i>
                                            </a>
                                            @endif
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                        <!-- fin table pv-->
					</div>
				</div>
			</div>
		</div>
	</div>

    <div class="modal fade" id="deleteModal" tabindex="-1" role="dialog" aria-labelledby="deleteModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="deleteModalLabel">Suppression</h5>
                <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="modal-body">Voulez-vous vraiment supprimer ce procès verbal de référence ?</div>
            <div class="modal-footer">
                <button class="btn btn-secondary" type="button" data-dismiss="modal">Non</button>

                <a data-href="#" class="btn btn-primary" data-dismiss="modal" onclick="deletePv()">
                    Oui
                </a>
            </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('scripts')
<!--reseach--->
<script type="text/javascript">
    var id;
    function setPvId(e) {
        if(e.composedPath){
            var path = (e.composedPath && e.composedPath());

            if(path[1].id != ''){
                this.id = path[1].id;
            }else{
                this.id = path[0].id;
            }
        }
    }

    function deletePv() {
        window.location.replace('pv/' + id + '/delete');
    }

   $(document).ready(function(){
       $("#myInput").on("keyup", function() {
           var value = $(this).val().toLowerCase();
           $("#myTable tr").filter(function() {
           $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
           });
       });
   });
</script>
<!--end-reseach--->
@endsection


