@extends('dashboard.layouts.app', ['title' => 'Dashboard'])

@section('content')
<div class="content container-fluid">

					<!-- Page Header -->
	<section class="comp-section">
		<nav aria-label="breadcrumb">
			<ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="{{ url('dashboard/home') }}">Home</a></li>
				<li class="breadcrumb-item"><a href="{{ url('dashboard/pv') }}">Procès verbaux</a></li>
				<li class="breadcrumb-item active" aria-current="page">Ajouter un procès verbal de colocalisation</li>
			</ol>
		</nav>
	</section>
	<!-- /Page Header -->

	<!-- Row -->
	<div class="row">
		<div class="col-sm-12">

			<!-- Custom Boostrap Validation -->
			<div class="card">
				<div class="card-header">
					<h4 class="card-title">Ajouter un procès verbal de colocalisation</h4>
				</div>
				<div class="card-body">
					<form method="POST" action="{{ url('dashboard/pv/create') }}">
                        @csrf

                        <div>
                            <input type="hidden" name="type" value="colocalisation">
                        </div>

                        <div class="form-row mb-3">
                            <div class="col-md-4 mb-3">
                                <label for="bon_id">Bon de commande<span style="color:red">*</span></label>
                                <select style="font-size : 18px;" name="bon_id" class="form-control" id="bon_id" required>
                                    <option disabled selected>Sélectionnez le bon de commande</option>
                                    @foreach ($bons as $bon)
                                        <option value="{{$bon->id}}">{{$bon->num_ref}}</option>
                                    @endforeach
                                </select>
                                @error('bon_id')
                                    <span class="text-danger" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>

                            <div class="col-md-6 mb-3">
                                <label for="ville">Ville<span style="color:red">*</span></label>
                                <input type="text" class="form-control" id="ville" name="ville" required>
                                @error('ville')
                                    <span class="text-danger" role="alert">
                                        <strong>Ce champ est obligatoire.</strong>
                                    </span>
                                @enderror
                            </div>

                            <div class="col-md-6 mb-3">
                                <label for="reception_room">Salle d'acceuil<span style="color:red">*</span></label>
                                <input type="text" class="form-control" id="reception_room" name="reception_room" required>
                                @error('reception_room')
                                    <span class="text-danger" role="alert">
                                        <strong>Ce champ est obligatoire.</strong>
                                    </span>
                                @enderror
                            </div>

                            <div class="col-md-6 mb-3">
                                <label for="local_conditions">Condition du local<span style="color:red">*</span></label>
                                <select style="font-size : 18px;" class="form-control" name="local_conditions" required>
                                    <option disabled selected>Sélectionnez la condition</option>
                                    <option>LCPN</option>
                                    <option>LCFP</option>
                                    <option>LNC</option>
                                </select>
                                @error('local_conditions')
                                    <span class="text-danger" role="alert">
                                        <strong>Ce champ est obligatoire.</strong>
                                    </span>
                                @enderror
                            </div>

                            <div class="col-md-6 mb-3">
                                <label for="commissioningDate">Date mise en service<span style="color:red">*</span></label>
                                <input type="date" class="form-control" id="commissioningDate" name="commissioningDate" required>
                                @error('commissioningDate')
                                    <span class="text-danger" role="alert">
                                        <strong>Ce champ est obligatoire.</strong>
                                    </span>
                                @enderror
                            </div>
                            <div class="col-md-6 mb-3">
                                <label for="status">Statut<span style="color:red"></span></label>
                                <select style="font-size : 18px;" type="text" class="form-control" id="status" name="status" value="{{ old('status') }}" >

                                <option disabled selected>Sélectionnez un statut</option>
                                    <option>ACTIVE</option>
                                    <option>DESACTIVE</option>
                                    <option>SUSPENDU</option>
                                    <option>UPGRADE</option>
                                    <option>DOWNGRADE</option>

                                </select>

                            </div>

                        </div>

                        <h5 class="font-weight-bold my-4">Energie</h5>

                        <div class="form-row">
                            <div class="col-md-4 mb-3">
                                <label for="source_ac">Source 220 AC</label>
                                <input type="text" class="form-control" id="source_ac" name="source_ac">
                            </div>

                            <div class="col-md-4 mb-3">
                                <label for="source_cc">Source 48 CC</label>
                                <input type="text" class="form-control" id="source_cc" name="source_cc">
                            </div>

                            <div class="col-md-4 mb-3">
                                <label for="provider">Fournie par</label>
                                <input type="text" class="form-control" id="provider" name="provider">
                            </div>
                        </div>

                        <div class="form-row">
                            <div class="col-md-4 mb-3">
                                <label for="intensity_consomme">Intensité consommée par l'équipement (A)</label>
                                <input type="number" class="form-control" id="intensity_consomme" name="intensity_consomme">
                            </div>

                            <div class="col-md-4 mb-3">
                                <label for="tension">Tension d'alimentation de l'équipements (V)</label>
                                <input type="number" class="form-control" id="tension" name="tension">
                            </div>

                            <div class="col-md-4 mb-3">
                                <label for="energy_consomme">Energie consommée (Kwh) </label>
                                <input type="number" class="form-control" id="energy_consomme" name="energy_consomme">
                            </div>

                            <div class="col-md-4 mb-3">
                                <label for="power_consomme">Puissance consommée (w)</label>
                                <input type="number" class="form-control" id="power_consomme" name="power_consomme">
                            </div>

                            <div class="col-md-4 mb-3">
                                <label for="energy_consomme_per_month">Energie consommée par mois (Kwh) </label>
                                <input type="number" readonly class="form-control" id="energy_consomme_per_month" name="energy_consomme_per_month">
                            </div>
                        </div>

                        <h5 class="font-weight-bold my-4">Equipements</h5>

                        <div class="form-row">
                            <div class="col-md-4 mb-3">
                                <label for="type_equipment">Type équipements</label>
                                <input type="text" class="form-control" name="type_equipment" id="type_equipment">
                            </div>

                            <div class="col-md-4 mb-3">
                                <label for="support">Support</label>
                                <select style="font-size : 18px;" class="form-control" name="support">
                                    <option disabled selected>Sélectionnez une option</option>
                                    <option value="1">Oui</option>
                                    <option value="0">Non</option>
                                </select>
                            </div>

                            <div class="col-md-4 mb-3">
                                <label for="guide_length">Longueur guides (m)</label>
                                <input type="text" class="form-control" name="guide_length" id="guide_length" required>
                                @error('guide_length')
                                    <span class="text-danger" role="alert">
                                        <strong>Ce champ est obligatoire.</strong>
                                    </span>
                                @enderror
                            </div>

                            <div class="col-md-4 mb-3">
                                <label for="nbr_antenna">Nombre d'antennes</label>
                                <input type="text" class="form-control" name="nbr_antenna" id="nbr_antenna" required>
                                @error('nbr_antenna')
                                    <span class="text-danger" role="alert">
                                        <strong>Ce champ est obligatoire.</strong>
                                    </span>
                                @enderror
                            </div>

                            <div class="col-md-4 mb-3">
                                <label for="type_antenna">Type d'antennes</label>
                                <input type="text" class="form-control" name="type_antenna" id="type_antenna" required>
                                @error('type_antenna')
                                    <span class="text-danger" role="alert">
                                        <strong>Ce champ est obligatoire.</strong>
                                    </span>
                                @enderror
                            </div>

                            <div class="col-md-4 mb-3">
                                <label for="weight">Poids d'antennes + Support (kg)</label>
                                <input type="text" class="form-control" name="weight" id="weight" required>
                                @error('weight')
                                    <span class="text-danger" role="alert">
                                        <strong>Ce champ est obligatoire.</strong>
                                    </span>
                                @enderror
                            </div>

                            <div class="col-md-4 mb-3">
                                <label for="position_tma">Positon TMA depuis le sol (m)</label>
                                <input type="number" class="form-control" name="position_tma" id="position_tma" required>
                                @error('position_tma')
                                    <span class="text-danger" role="alert">
                                        <strong>Ce champ est obligatoire.</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <h5 class="font-weight-bold my-4">Dimensions physiques</h5>

                        <div class="form-row">
                            <div class="col-md-3 mb-3">
                                <label for="length">Longueur (m)</label>
                                <input type="number" id="long" class="form-control" name="length">
                            </div>

                            <div class="col-md-3 mb-3">
                                <label for="width">Largeur (m)</label>
                                <input type="number" id="lag" class="form-control" name="width">
                            </div>

                            <div class="col-md-3 mb-3">
                                <label for="height">Hauteur (m)</label>
                                <input type="number" id="haut" class="form-control" name="height">
                            </div>
                            <div class="col-md-3 mb-3">
                                <label for="sol_capacity">Volume total occupé au sol (m)<sup>3</sup></label>
                                <input type="number" id="vol" readonly class="form-control" name="sol_capacity">
                            </div>
                        </div>

                        <h5 class="font-weight-bold my-4">Liens numériques</h5>

                        {{-- <div class="form-row">
                            <div class="col-md-6 mb-3">
                                <label for="capacity">Capacité attribuée (Mb/s)</label>
                                <input type="text" class="form-control" name="capacity">
                            </div>

                            <div class="col-md-6 mb-3">
                                <label for="distance">Distance cotonou-station (km)</label>
                                <input type="number" class="form-control" id="distance" name="distance">
                            </div>
                        </div>--}}
                        <div class="form-row">
                            <div class="col-md-6 mb-3">
                                    <label for="volume">Volume<span style="color:red">*</span></label>
                                    <select style="font-size : 18px;" class="form-control" name="volume" >
                                        <option disabled selected>Sélectionnez la condition</option>
                                        @foreach($volumes as $volume)
                                            <option value="{{$volume->id}}">{{$volume->volume}}</option>
                                        @endforeach
                                    </select>
                                    @error('volume')
                                        <span class="text-danger" role="alert">
                                            <strong>Ce champ est obligatoire.</strong>
                                        </span>
                                    @enderror
                            </div>
                            <div class="col-md-6 mb-3">
                                <label for="surface">Surface</label>
                                <input type="number" class="form-control" id="surface" name="surface" value="{{ old('surface') }}">
                            </div>
                        </div>

                        <div>
                            <a href="{{ url('dashboard/pv') }}" style="margin-right: 10px" class="btn btn-primary">Annuler</a>
                            <button type="submit" class="btn btn-success">
                                Ajouter
                            </button>
                        </div>
					</form>
				</div>
				</div>
			</div>
		</div>
    </div>
</div>
@endsection

@section('scripts')
<script>
    $(document).ready(function(e){

        $("#long, #lag, #haut, #energy_consomme").on('change',function(){

          var nbre1 = parseInt($("#long").val());
          var nbre2 = parseInt($("#lag").val());
          var nbre3 = parseInt($("#haut").val());
          var nbre4 = parseInt($("#energy_consomme").val());


            $("#vol").val((nbre1 * nbre2 * nbre3));
            $("#energy_consomme_per_month").val(nbre4*24*30);
        });

    });
  </script>
@endsection
