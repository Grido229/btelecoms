@extends('dashboard.layouts.app', ['title' => 'Dashboard'])

@section('content')
<div class="content container-fluid">

    <!-- Page Header -->
	<section class="comp-section">
		<nav aria-label="breadcrumb">
			<ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="{{ url('dashboard/home') }}">Home</a></li>
				<li class="breadcrumb-item"><a href="{{ url('dashboard/pv') }}">Procès verbaux</a></li>
				<li class="breadcrumb-item active" aria-current="page">Modifier un procès verbal</li>
			</ol>
		</nav>
	</section>
	<!-- /Page Header -->

	<!-- Row -->
	<div class="row">
		<div class="col-sm-12">

			<!-- Custom Boostrap Validation -->
			<div class="card">
				<div class="card-header">
					<h4 class="card-title">Modifier un procès verbal</h4>
				</div>
				<div class="card-body">
                    <!--debut formulaire-->
					<form method="POST" action="{{ url('dashboard/pv/' . $pv->id . '/edit') }}">
                        @csrf

                        <div class="form-row mb-3">
                            <div class="col-md-6 mb-3">
                                <label for="bon_id">Bon de commande<span style="color:red">*</span></label>
                                <select style="font-size : 18px;" name="bon_id" class="form-control" id="bon_id" required>
                                    <option disabled selected>Sélectionnez le bon de commande</option>
                                    @foreach ($bons as $bon)
                                        @if ($bon->id == $pv->bon_id)
                                            <option selected value="{{$bon->id}}">{{$bon->num_ref}}</option>
                                        @else
                                            <option value="{{$bon->id}}">{{$bon->num_ref}}</option>
                                        @endif
                                    @endforeach
                                </select>
                                </select>
                                @error('bon_id')
                                    <span class="text-danger" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>

                            <div class="col-md-6 mb-3">
                                <label for="type_id">Type offre<span style="color:red">*</span></label>
                                <select style="font-size : 18px;" name="type_id" class="form-control" id="type_id" required>
                                    <option disabled >Sélectionnez le type de l'offre</option>
                                    @foreach ($offres_types as $offres_type)
                                        @if ($offres_type->id == $pv->type_id)
                                            <option selected value="{{$offres_type->id}}">{{$offres_type->name}}</option>
                                        @else
                                            <option value="{{$offres_type->id}}">{{$offres_type->name}}</option>
                                        @endif
                                    @endforeach
                                </select>
                                @error('role_id')
                                    <span class="text-danger" role="alert">
                                        <strong>Vous devez sélectionner le type de l'offre.</strong>
                                    </span>
                                @enderror
                            </div>

                            

                            <div class="col-md-6 mb-3">
                                <label for="ville">ville<span style="color:red">*</span></label>
                                <input type="text" class="form-control" id="ville" name="ville" required value="{{ $pv->ville }}">
                                @error('ville')
                                    <span class="text-danger" role="alert">
                                        <strong>Ce champ est obligatoire.</strong>
                                    </span>
                                @enderror
                            </div>

                            <div class="col-md-6 mb-3">
                                <label for="reception_room">Salle d'acceuil<span style="color:red">*</span></label>
                                <input type="text" class="form-control" id="reception_room" name="reception_room" required value="{{ $pv->reception_room }}">
                                @error('reception_room')
                                    <span class="test-danger" role="alert">
                                        <strong>Ce champ est obligatoire.</strong>
                                    </span>
                                @enderror
                            </div>

                            <div class="col-md-6 mb-3">
                                <label for="local_conditions">Condition du local<span style="color:red">*</span></label>
                                <select style="font-size : 18px;" class="form-control" name="local_conditions" required>
                                    <option disabled selected>Sélectionnez la condition</option>
                                    @if ($pv->local_conditions == 'LCPN')
                                        <option selected value="LCPN">LCPN</option>
                                    @else
                                        <option value="LCPN">LCPN</option>
                                    @endif

                                    @if ($pv->local_conditions == 'LCFP')
                                        <option selected value="LCFP">LCFP</option>
                                    @else
                                        <option value="LCFP">LCFP</option>
                                    @endif

                                    @if ($pv->local_conditions == 'LNC')
                                        <option selected value="LNC">LNC</option>
                                    @else
                                        <option value="LNC">LNC</option>
                                    @endif
                                </select>
                            </div>

                            <div class="col-md-6 mb-3">
                                <label for="commissioningDate">Date de mise en service<span style="color:red">*</span></label>
                                <input type="date" class="form-control" id="commissioningDate" name="commissioningDate" required value="{{ $pv->commissioningDate }}">
                                @error('commissioningDate')
                                    <span class="text-danger" role="alert">
                                        <strong>Ce champ est obligatoire.</strong>
                                    </span>
                                @enderror
                            </div>
                            <div class="col-md-6 mb-3">
                                <label for="status">Statut<span style="color:red"></span></label>
                                <select style="font-size : 18px;" type="text" class="form-control" id="status" name="status" value="{{  $pv->status  }}" >

                                <option selected>{{$pv->status}}</option>
                                    <option>ACTIVE</option>
                                    <option>DESACTIVE</option>
                                    <option>SUSPENDU</option>
                                    <option>UPGRADE</option>
                                    <option>DOWNGRADE</option>

                                </select>

                            </div>

                            <div class="col-md-6 mb-3">
                                    <label for="status_date">Date statut<span style="color:red"></span></label>
                                    <input type="date" class="form-control" id="status_date" name="status_date" value="{{ $pv->status_date }}">

                                </div>

                                <div class="col-md-12 mb-3">
                                    <label for="status_object">Objet statut<span style="color:red"></span></label>
                                    <input type="text" class="form-control" id="status_object" name="status_object" value="{{ $pv->status_object }}">

                                </div>
                        </div>

                        @if ($pv->type == 'capacite')
                            <div>
                                <input type="hidden" name="type" value="capacity">
                            </div>

                            <h5 class="font-weight-bold">Equipements</h5>

                            <div class="form-row mb-3">
                                <div class="col-md-6 mb-3">
                                    <label for="support">Support</label>
                                    <select style="font-size : 18px;" class="form-control" name="support">
                                        <option disabled selected>Sélectionnez une option</option>
                                        @if ($pv->support == 1)
                                            <option selected value="1">Oui</option>
                                        @else
                                            <option value="1">Oui</option>
                                        @endif

                                        @if ($pv->support == 0)
                                            <option selected value="0">Non</option>
                                        @else
                                            <option value="0">Non</option>
                                        @endif
                                    </select>
                                </div>

                                <div class="col-md-6 mb-3">
                                    <label for="work_done">Travaux effectués<span style="color:red">*</span></label>
                                    <input type="text" class="form-control" id="work_done" name="work_done" required value="{{ $pv->work_done }}">
                                    @error('work_done')
                                        <span class="test-danger" role="alert">
                                            <strong>Ce champ est obligatoire.</strong>
                                        </span>
                                    @enderror
                                </div>
                            </div>
                        @endif

                        @if ($pv->type == 'colocalisation')
                            <div>
                                <input type="hidden" name="type" value="colocalisation">
                            </div>

                            <h5 class="font-weight-bold my-3">Energie</h5>

                            <div class="form-row">
                                <div class="col-md-4 mb-3">
                                    <label for="source_ac">Source 220 AC</label>
                                    <input type="text" class="form-control" id="source_ac" name="source_ac" value="{{ $pv->source_ac }}">
                                </div>

                                <div class="col-md-4 mb-3">
                                    <label for="source_cc">Source 48 CC</label>
                                    <input type="text" class="form-control" id="source_cc" name="source_cc" value="{{ $pv->source_cc }}">
                                </div>

                                <div class="col-md-4 mb-3">
                                    <label for="provider">Fournie par</label>
                                    <input type="text" class="form-control" id="provider" name="provider" value="{{ $pv->provider }}">
                                </div>
                            </div>

                            <div class="form-row">
                                <div class="col-md-4 mb-3">
                                    <label for="intensity_consomme">Intensité consommée par l'équipement (A)</label>
                                    <input type="number" class="form-control" id="intensity_consomme" name="intensity_consomme" value="{{ $pv->intensity_consomme }}">
                                </div>

                                <div class="col-md-4 mb-3">
                                    <label for="tension">Tension d'alimentation de l'équipement (V)</label>
                                    <input type="number" class="form-control" id="tension" name="tension" value="{{ $pv->tension }}">
                                </div>

                                <div class="col-md-4 mb-3">
                                    <label for="energy_consomme">Energie consommée (Kwh)</label>
                                    <input type="number" class="form-control" id="energy_consomme" name="energy_consomme" value="{{ $pv->energy_consomme }}">
                                </div>

                                <div class="col-md-4 mb-3">
                                    <label for="power_consomme">Puissance consommée (w)</label>
                                    <input type="number" class="form-control" id="power_consomme" name="power_consomme" value="{{ $pv->power_consomme }}">
                                </div>

                                <div class="col-md-4 mb-3">
                                    <label for="energy_consomme_per_month">Energie consommée par mois (Kwh) </label>
                                    <input type="number" readonly class="form-control" id="energy_consomme_per_month" name="energy_consomme_per_month" value="{{ $pv->energy_consomme_per_month }}">
                                </div>
                            </div>

                            <h5 class="font-weight-bold my-3">Equipements</h5>

                            <div class="form-row">
                                <div class="col-md-4 mb-3">
                                    <label for="type_equipment">Type équipements</label>
                                    <input type="text" class="form-control" name="type_equipment" id="type_equipment" value="{{ $pv->type_equipment }}">
                                </div>

                                <div class="col-md-4 mb-3">
                                    <label for="support">Support</label>
                                    <select style="font-size : 18px;" class="form-control" name="support">
                                        <option disabled selected>Sélectionnez une option</option>
                                        @if ($pv->support == 1)
                                            <option selected value="1">Oui</option>
                                        @else
                                            <option value="1">Oui</option>
                                        @endif

                                        @if ($pv->support == 0)
                                            <option selected value="0">Non</option>
                                        @else
                                            <option value="0">Non</option>
                                        @endif
                                    </select>
                                </div>
                            </div>
                        @endif

                        <div class="form-row mb-3">
                            <div class="col-md-4 mb-3">
                                <label for="guide_length">Longueur guides (m)</label>
                                <input type="number" class="form-control" name="guide_length" id="guide_length" required value="{{ $pv->guide_length }}">
                                @error('guide_length')
                                    <span class="text-danger" role="alert">
                                        <strong>Ce champ est obligatoire.</strong>
                                    </span>
                                @enderror
                            </div>

                            <div class="col-md-4 mb-3">
                                <label for="nbr_antenna">Nombre d'antennes</label>
                                <input type="number" class="form-control" name="nbr_antenna" id="nbr_antenna" required value="{{ $pv->nbr_antenna }}">
                                @error('nbr_antenna')
                                    <span class="text-danger" role="alert">
                                        <strong>Ce champ est obligatoire.</strong>
                                    </span>
                                @enderror
                            </div>

                            <div class="col-md-4 mb-3">
                                <label for="type_antenna">Type d'antennes</label>
                                <input type="text" class="form-control" name="type_antenna" id="type_antenna" required value="{{ $pv->type_antenna }}">
                                @error('type_antenna')
                                    <span class="text-danger" role="alert">
                                        <strong>Ce champ est obligatoire.</strong>
                                    </span>
                                @enderror
                            </div>

                            <div class="col-md-4 mb-3">
                                <label for="weight">Poids d'antennes + Support (kg)</label>
                                <input type="number" class="form-control" name="weight" id="weight" required value="{{ $pv->weight }}">
                                @error('weight')
                                    <span class="text-danger" role="alert">
                                        <strong>Ce champ est obligatoire.</strong>
                                    </span>
                                @enderror
                            </div>

                            <div class="col-md-4 mb-3">
                                <label for="position_tma">Positon TMA depuis le sol (m)</label>
                                <input type="number" class="form-control" name="position_tma" id="position_tma" required value="{{ $pv->position_tma }}">
                                @error('position_tma')
                                    <span class="text-danger" role="alert">
                                        <strong>Ce champ est obligatoire.</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>


                        @if ($pv->type == 'colocalisation')
                            <h5 class="font-weight-bold my-3">Dimensions physiques</h5>

                            <div class="form-row">
                                <div class="col-md-3 mb-3">
                                    <label for="length">Longueur</label>
                                    <input type="number" class="form-control" id="long" name="length" value="{{ $pv->length }}">
                                </div>

                                <div class="col-md-3 mb-3">
                                    <label for="width">Largeur</label>
                                    <input type="number" class="form-control" id="lag" name="width" value="{{ $pv->width }}">
                                </div>

                                <div class="col-md-3 mb-3">
                                    <label for="height">Hauteur</label>
                                    <input type="number" class="form-control" id="haut" name="height" value="{{ $pv->height }}">
                                </div>

                                <div class="col-md-3 mb-3">
                                    <label for="sol_capacity">Volume total occupé au sol (m)<sup>3</sup></label>
                                    <input type="number" id="vol" readonly class="form-control" name="sol_capacity" value="{{ $pv->sol_capacity }}">
                                </div>
                            </div>

                            <h5 class="font-weight-bold my-3">Liens numériques</h5>

                            <div class="form-row">
                                <div class="col-md-6 mb-3">
                                    <label for="capacity">Capacité attribuée</label>
                                    <input type="number" class="form-control" name="capacity" value="{{ $pv->capacity }}">
                                </div>

                                <div class="col-md-6 mb-3">
                                    <label for="distance">Distance cotonou-station (Km)</label>
                                    <input type="number" class="form-control" id="distance" name="distance" value="{{ $pv->distance }}">
                                </div>
                            </div>

                        @endif

                        <div>
                            <a href="{{ url('dashboard/pv') }}" style="margin-right: 10px" class="btn btn-primary">Annuler</a>
                            <button type="submit" class="btn btn-success">
                                Modifier
                            </button>
                        </div>
					</form>
                    <!--fin formulaire-->
				</div>
			</div>
		</div>
	</div>
	<!-- /Row -->
</div>
@endsection

@section('scripts')
<script>
    $(document).ready(function(e){

        $("#long, #lag, #haut, #energy_consomme").on('change',function(){

          var nbre1 = parseInt($("#long").val());
          var nbre2 = parseInt($("#lag").val());
          var nbre3 = parseInt($("#haut").val());
          var nbre4 = parseInt($("#energy_consomme").val());


            $("#vol").val((nbre1 * nbre2 * nbre3));
            $("#energy_consomme_per_month").val(nbre4*24*30);
        });

    });
  </script>
@endsection
