@extends('dashboard.layouts.app', ['title' => 'Dashboard'])

@section('content')
<div class="content container-fluid">

    <!-- Page Header -->
	<section class="comp-section">
		<nav aria-label="breadcrumb">
			<ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="{{ url('dashboard/home') }}">Home</a></li>
				<li class="breadcrumb-item"><a href="{{ url('dashboard/pv') }}">Procès verbaux</a></li>
				<li class="breadcrumb-item active" aria-current="page">Ajouter un procès verbal</li>
			</ol>
		</nav>
	</section>
	<!-- /Page Header -->

	<!-- Row -->
	<div class="row">
		<div class="col-sm-12">

			<!-- Custom Boostrap Validation -->
			<div class="card">
				<div class="card-header">
					<h4 class="card-title">Ajouter un procès verbal</h4>
				</div>
				<div class="card-body">
                    <!--debut formulaire-->
					<form method="POST" action="{{ url('dashboard/pv/create') }}">
                        @csrf

                        <div>
                            <input type="hidden" name="type" value="capacity">
                        </div>

                        <div class="form-row mb-3">
                            <div class="col-md-4 mb-3">
                                <label for="bon_id">Bon de commande<span style="color:red">*</span></label>
                                <select name="bon_id" class="form-control" id="bon_id">
                                    <option disabled selected>Sélectionnez le bon de commande</option>
                                    @foreach ($bons as $bon)
                                        <option value="{{$bon->id}}">{{$bon->num_bon}}</option>
                                    @endforeach
                                </select>
                                @error('bon_id')
                                    <span class="text-danger" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>

                            <div class="col-md-4 mb-3">
                                <label for="reception_room">Salle d'acceuil<span style="color:red">*</span></label>
                                <input type="text" class="form-control" id="reception_room" name="reception_room" required>
                                @error('reception_room')
                                    <span class="test-danger" role="alert">
                                        <strong>Ce champ est obligatoire.</strong>
                                    </span>
                                @enderror
                            </div>

                            <div class="col-md-4 mb-3">
                                <label for="local_conditions">Condition du local<span style="color:red">*</span></label>
                                <select class="form-control" name="local_conditions">
                                    <option disabled selected>Sélectionnez la condition</option>
                                    <option>LCPN</option>
                                    <option>LCFP</option>
                                    <option>LNC</option>
                                </select>
                                @error('local_conditions')
                                    <span class="test-danger" role="alert">
                                        <strong>Ce champ est obligatoire.</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <h5 class="font-weight-bold">Equipements</h5>

                        <div class="form-row mb-3">
                            <div class="col-md-6 mb-3">
                                <label for="support">Support<span style="color:red">*</span></label>
                                <select class="form-control" name="support">
                                    <option disabled selected>Sélectionnez une option</option>
                                    <option value="1">Oui</option>
                                    <option value="0">Non</option>
                                </select>
                                @error('support')
                                    <span class="test-danger" role="alert">
                                        <strong>Ce champ est obligatoire.</strong>
                                    </span>
                                @enderror
                            </div>

                            <div class="col-md-6 mb-3">
                                <label for="work_done">Travaux effectuer<span style="color:red">*</span></label>
                                <input type="text" class="form-control" id="work_done" name="Teffectuer" required>
                                @error('work_done')
                                    <span class="test-danger" role="alert">
                                        <strong>Ce champ est obligatoire.</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <h5 class="font-weight-bold">Liens numériques/internet</h5>

                        <div class="form-row">
                            <div class="col-md-4 mb-3">
                                <label for="number_links_attributed">Nombre de liens attribués<span style="color:red">*</span></label>
                                <input type="text" class="form-control" id="number_links_attributed" name="liens" required>
                                @error('number_links_attributed')
                                    <span class="test-danger" role="alert">
                                        <strong>Ce champ est obligatoire.</strong>
                                    </span>
                                @enderror
                            </div>

                            <div class="col-md-4 mb-3">
                                <label for="distance">Distance cotonou-station<span style="color:red">*</span></label>
                                <input type="text" class="form-control" id="distance" name="distance" required>
                                @error('distance')
                                    <span class="test-danger" role="alert">
                                        <strong>Ce champ est obligatoire.</strong>
                                    </span>
                                @enderror
                            </div>

                            <div class="col-md-4 mb-3">
                                <label for="commissioningDate">Date de mise en service<span style="color:red">*</span></label>
                                <input type="date" class="form-control" id="commissioningDate" name="commissioningDate">
                                @error('date')
                                    <span class="text-danger" role="alert">
                                        <strong>Ce champ est obligatoire.</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div>
                            <a href="{{ url('dashboard/pv') }}" style="margin-right: 10px" class="btn btn-primary">Annuler</a>
                            <button type="submit" class="btn btn-success">
                                Ajouter
                            </button>
                        </div>
					</form>
                    <!--fin formulaire-->
				</div>
			</div>
		</div>
	</div>
	<!-- /Row -->

				</div>
@endsection
