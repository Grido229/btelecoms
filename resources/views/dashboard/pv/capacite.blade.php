@extends('dashboard.layouts.app', ['title' => 'Dashboard'])

@section('content')
<div class="content container-fluid">

					<!-- Page Header -->
	<section class="comp-section">
		<nav aria-label="breadcrumb">
			<ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="{{ url('dashboard/home') }}">Home</a></li>
				<li class="breadcrumb-item"><a href="{{ url('dashboard/pv') }}">Procès verbaux</a></li>
				<li class="breadcrumb-item active" aria-current="page">Ajouter un procès verbal de capacité</li>
			</ol>
		</nav>
	</section>
	<!-- /Page Header -->

	<!-- Row -->
	<div class="row">
		<div class="col-sm-12">
			<!-- Custom Boostrap Validation -->
			<div class="card">
				<div class="card-header">
					<h4 class="card-title">Ajouter un procès verbal de capacité</h4>
				</div>
				<div class="card-body">

                    <!--debut formulaire-->
					<form method="POST" action="{{ url('dashboard/bon/create') }}">
                        @csrf

                        <div class="form-row">
                            <div class="col-md-6 mb-3">
                                <label for="lastname">Id commande<span style="color:red">*</span></label>
                                <input type="text" class="form-control" id="lastname" name="id" required>
                                @error('lastname')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>Ce champ est obligatoire.</strong>
                                    </span>
                                @enderror
                            </div>

                            <div class="col-md-6 mb-3">
                                <label for="firstname">Opérateur<span style="color:red">*</span></label>
                                <input type="text" class="form-control" id="firstname" name="Opérateur" required>
                                @error('firstname')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>Ce champ est obligatoire.</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-row">
                            <div class="col-md-6 mb-3">
                                <label for="enterprise_name">Salle d'acceuil équipements<span style="color:red">*</span></label>
                                <input type="text" class="form-control" id="enterprise_name" name="sall_equi" required>
                                @error('enterprise_name')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>Ce champ est obligatoire.</strong>
                                    </span>
                                @enderror
                            </div>

                            <div class="col-md-6 mb-3">
                                <label for="tel">Type d'équipements<span style="color:red">*</span></label>
                                <input type="text" class="form-control" id="tel" name="type" required>
                                @error('tel')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>Ce champ est obligatoire.</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-row">
                            <div class="col-md-6 mb-3">
                                <label for="email">Condition du local<span style="color:red">*</span></label>

                                <select class="form-control" name="type_local"  >
                                    <option></option>
                                    <option>LCPN</option>
                                    <option>LCFP</option>
                                    <option>LNC</option>
                                </select>
                                @error('email')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>Ce champ est obligatoire.</strong>
                                    </span>
                                @enderror
                            </div>

                            <div class="col-md-6 mb-3">
                                <label for="city">Dimenssions physiques<span style="color:red">*</span></label>
                                <!--input type="text" class="form-control" id="city" name="city" required-->
                                <!--debut form-->


                                <div class="form-row">
                                    <div class="col">
                                        <input type="text" class="form-control" name="long" placeholder="longueur">
                                    </div>
                                    <div class="col">
                                        <input type="text" class="form-control" name="larg" placeholder="largeur">
                                    </div>
                                    <div class="col">
                                        <input type="text" class="form-control" name="haut" placeholder="hauteur">
                                    </div>
                                </div>


                                  <!--fin form-->
                                @error('city')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>Ce champ est obligatoire.</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-row">
                            <div class="col-md-6 mb-3">
                                <label for="adress">Date de mise en service<span style="color:red">*</span></label>
                                <input type="text" class="form-control" id="adress" name="adress" required>
                                @error('adress')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>Ce champ est obligatoire.</strong>
                                    </span>
                                @enderror
                            </div>

                            <div class="col-md-6 mb-3">
                                <label for="seat">Energie<span style="color:red">*</span></label>
                                <select class="form-control" name="gender" required>
                                    <option value="">Source</option>
                                    <option value="Male"> 220V AC </option>
                                    <option value="Female"> 48V CC </option>

                                </select>
                                @error('seat')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>Ce champ est obligatoire.</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

						<!--second debut-->


                        <div class="form-row">
                            <div class="col-md-6 mb-3">
                                <label for="lastname">Fournie par<span style="color:red">*</span></label>
                                <input type="text" class="form-control" id="lastname" name="f_par" required>
                                @error('lastname')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>Ce champ est obligatoire.</strong>
                                    </span>
                                @enderror
                            </div>

                            <div class="col-md-6 mb-3">
                                <label for="firstname">tension d'alimentation de l'équipements<span style="color:red">*</span></label>
                                <input type="text" class="form-control" id="firstname" name="tension" required>
                                @error('firstname')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>Ce champ est obligatoire.</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-row">
                            <div class="col-md-6 mb-3">
                                <!--label for="enterprise_name">Nom de l'entreprise<span style="color:red">*</span></label-->
                                <div class="form-row">
                                    <div class="col">
                                        <label for="">Intensité consommée<span style="color:red">*</span></label>
                                        <input type="text" class="form-control" name="int" placeholder="">
                                    </div>
                                    <div class="col">
                                        <label for="">Puissance consom..<span style="color:red">*</span></label>
                                        <input type="text" class="form-control" name="puissance" placeholder="">
                                    </div>
                                    <div class="col">
                                        <label for="">Energie consommée<span style="color:red">*</span></label>
                                        <input type="text" class="form-control" name="energie" placeholder="">
                                    </div>
                                </div>

                                @error('enterprise_name')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>Ce champ est obligatoire.</strong>
                                    </span>
                                @enderror
                            </div>

                            <div class="col-md-6 mb-3">
                                <label for="tel">Support pylone<span style="color:red">*</span></label>
                                <select class="form-control" name="support_p"  >
                                    <option></option>
                                    <option>OUI</option>
                                    <option>NON</option>

                                </select>
                                @error('tel')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>Ce champ est obligatoire.</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-row">
                            <div class="col-md-6 mb-3">
                                <label for="email">Nombre d'antennes<span style="color:red">*</span></label>
                                <input type="number" class="form-control" id="email" name="antenne" required>
                                @error('email')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>Ce champ est obligatoire.</strong>
                                    </span>
                                @enderror
                            </div>

                            <div class="col-md-6 mb-3">
                                <label for="city"> Poids d'antennes + Support<span style="color:red">*</span></label>
                                <input type="text" class="form-control" id="city" name="poid" required>
                                @error('city')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>Ce champ est obligatoire.</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-row">
                            <div class="col-md-6 mb-3">
                                <label for="adress">Positon TMA depuis le sol<span style="color:red">*</span></label>
                                <input type="text" class="form-control" id="adress" name="position" required>
                                @error('adress')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>Ce champ est obligatoire.</strong>
                                    </span>
                                @enderror
                            </div>

                            <div class="col-md-6 mb-3">
                               <div class="form-row">
                                    <div class="col">
                                        <label for="">Longueur guides<span style="color:red">*</span></label>
                                        <input type="text" class="form-control" name="guides" placeholder="">
                                    </div>
                                    <div class="col">
                                        <label for="">Type d'antennes<span style="color:red">*</span></label>
                                        <input type="text" class="form-control" name="ant_type" placeholder="">
                                    </div>
                                    <div class="col">
                                        <label for="">Capacité attribué<span style="color:red">*</span></label>
                                        <input type="text" class="form-control" name="cap_attribu" placeholder="">
                                    </div>
                                </div>

                                @error('seat')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>Ce champ est obligatoire.</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

						<!--second fin-->

                        <div>
                            <a href="{{ url('dashboard/pv') }}" style="margin-right: 10px" class="btn btn-primary">Annuler</a>
                            <button type="submit" class="btn btn-success">
                                Ajouter
                            </button>
                        </div>
					</form>
                    <!--fin formulaire-->

				</div>
			</div>

		</div>
	</div>
	<!-- /Row -->

				</div>
@endsection
