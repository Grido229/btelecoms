@extends('dashboard.layouts.app', ['title' => 'Dashboard'])

@section('content')
<div class="content container-fluid">

					<!-- Page Header -->
	<section class="comp-section">
		<nav aria-label="breadcrumb">
			<ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="{{ url('dashboard/home') }}">Home</a></li>
				<li class="breadcrumb-item"><a href="{{ url('dashboard/pv') }}">Procès verbaux</a></li>
				<li class="breadcrumb-item active" aria-current="page">Ajouter un procès verbal de colocalisation</li>
			</ol>
		</nav>
	</section>
	<!-- /Page Header -->

	<!-- Row -->
	<div class="row">
		<div class="col-sm-12">

			<!-- Custom Boostrap Validation -->
			<div class="card">
				<div class="card-header">
					<h4 class="card-title">Ajouter un procès verbal de colocalisation</h4>
				</div>
				<div class="card-body">
					<form method="POST" action="{{ url('dashboard/pv/create') }}">
                        @csrf

                        <div>
                            <input type="hidden" name="type" value="colocalisation">
                        </div>

                        <div class="form-row mb-3">
                            <div class="col-md-4 mb-3">
                                <label for="bon_id">Bon de commande<span style="color:red">*</span></label>
                                <select name="bon_id" class="form-control" id="bon_id">
                                    <option disabled selected>Sélectionnez le bon de commande</option>
                                    @foreach ($bons as $bon)
                                        <option value="{{$bon->id}}">{{$bon->num_bon}}</option>
                                    @endforeach
                                </select>
                                @error('bon_id')
                                    <span class="text-danger" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>

                            <div class="col-md-4 mb-3">
                                <label for="reception_room">Salle d'acceuil<span style="color:red">*</span></label>
                                <input type="text" class="form-control" id="reception_room" name="reception_room" required>
                                @error('reception_room')
                                    <span class="test-danger" role="alert">
                                        <strong>Ce champ est obligatoire.</strong>
                                    </span>
                                @enderror
                            </div>

                            <div class="col-md-4 mb-3">
                                <label for="local_conditions">Condition du local<span style="color:red">*</span></label>
                                <select class="form-control" name="local_conditions">
                                    <option disabled selected>Sélectionnez la condition</option>
                                    <option>LCPN</option>
                                    <option>LCFP</option>
                                    <option>LNC</option>
                                </select>
                                @error('local_conditions')
                                    <span class="test-danger" role="alert">
                                        <strong>Ce champ est obligatoire.</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <h5 class="font-weight-bold my-3">Energie</h5>

                        <div class="form-row">
                            <div class="col-md-4 mb-3">
                                <label for="source_ac">Source 220 AC<span style="color:red">*</span></label>
                                <input type="text" class="form-control" id="source_ac" name="source_ac" required>
                            </div>

                            <div class="col-md-4 mb-3">
                                <label for="source_cc">Source 48 CC<span style="color:red">*</span></label>
                                <input type="text" class="form-control" id="source_cc" name="source_cc" required>

                            </div>

                            <div class="col-md-4 mb-3">
                                <label for="provider">Fournie par<span style="color:red">*</span></label>
                                <input type="text" class="form-control" id="provider" name="provider" required>

                            </div>
                        </div>

                        <div class="form-row">
                            <div class="col-md-4 mb-3">
                                <label for="intensity_consomme">Intensité consommée par l'équipements<span style="color:red">*</span></label>
                                <input type="text" class="form-control" id="intensity_consomme" name="intensity_consomme" required>
                            </div>

                            <div class="col-md-4 mb-3">
                                <label for="tension">Tension d'alimentation de l'équipements<span style="color:red">*</span></label>
                                <input type="text" class="form-control" id="tension" name="tension" required>
                            </div>

                            <div class="col-md-4 mb-3">
                                <label for="energy_consomme">Energie consommée<span style="color:red">*</span></label>
                                <input type="text" class="form-control" id="energy_consomme" name="energy_consomme" required>
                            </div>

                            <div class="col-md-4 mb-3">
                                <label for="puissance_consomme">Puissance consommée<span style="color:red">*</span></label>
                                <input type="text" class="form-control" id="puissance_consomme" name="puissance_consomme" required>
                            </div>

                            <div class="col-md-4 mb-3">
                                <label for="energy_consomme_mois">Energie consommée par mois<span style="color:red">*</span></label>
                                <input type="text" class="form-control" id="energy_consomme_mois" name="energy_consomme_mois" required>
                            </div>
                        </div>

                        <h5 class="font-weight-bold my-3">Equipements</h5>

                        <div class="form-row">
                            <div class="col-md-6 mb-3">

                            <div class="form-group">
                                    <label for="equi_type">Type équipements<span style="color:red">*</span></label>
                                    <input type="text" class="form-control" name="equi_type" id="equi_type">
                                </div>
                                
                                <div class="form-group">
                                    <label for="supp_pylone">Support pylône<span style="color:red">*</span></label>
                                    <input type="text" class="form-control" name="supp_pylone" id="supp_pylone">
                                </div>
                                <div class="form-group">
                                    <label for="guide_length">Longueur guides<span style="color:red">*</span></label>
                                    <input type="text" class="form-control" name="guide_length" id="guide_length">
                                </div>
                                <div class="form-group">
                                    <label for="nbr_antenna">Nombre d'antennes<span style="color:red">*</span></label>
                                    <input type="text" class="form-control" name="nbr_antenna" id="nbr_antenna">
                                </div>

                                <div class="form-group">
                                    <label for="type_antenna">Type d'antennes<span style="color:red">*</span></label>
                                    <input type="text" class="form-control" name="type_antenna" id="type_antenna">
                                </div>

                                <div class="form-group">
                                    <label for="poids_ant">Poids d'antennes + Support<span style="color:red">*</span></label>
                                    <input type="text" class="form-control" name="poids_ant" id="poids_ant">
                                </div>

                                <div class="form-group">
                                    <label for="TMA">Positon TMA depuis le sol<span style="color:red">*</span></label>
                                    <input type="text" class="form-control" name="TMA" id="TMA">
                                </div>
                            </div>
                            
                            <div class="col-md-6 mb-3">
                                <div class="form-group">
                                    <label for="length">Longueur</label>
                                    <input type="text" class="form-control" name="length">
                                </div>

                                <div class="form-group">
                                    <label for="width">Largeur</label>
                                    <input type="text" class="form-control" name="width">
                                </div>

                                <div class="form-group">
                                    <label for="height">Hauteur</label>
                                    <input type="text" class="form-control" name="height">
                                </div>
                                <div class="form-group">
                                    <label for="vol_sol">Volume total occupé au sol</label>
                                    <input type="text" class="form-control" name="vol_sol">
                                </div>

                                <div class="form-group">
                                    
                                    <h5 class="font-weight-bold my-3">Liens numériques</h5>   
                                </div>

                                <div class="form-group">
                                    <label for="capacity">Capacité attribuée</label>
                                    <input type="text" class="form-control" name="capacity">
                                </div>
                                <div class="form-group">
                                    <label for="distance">Distance Cotonou-Station</label>
                                    <input type="text" class="form-control" name="distance">
                                </div>
                            </div>
                        </div>

                        <div class="form-row">
                            <!--div class="col-md-4 mb-3">
                                <label for="weight"> Poids d'antennes + Support<span style="color:red">*</span></label>
                                <input type="text" class="form-control" id="weight" name="weight" required>
                            </div>

                            <div class="col-md-4 mb-3">
                                <label for="postion_tma">Positon TMA depuis le sol<span style="color:red">*</span></label>
                                <input type="text" class="form-control" id="postion_tma" name="postion_tma" required>
                            </div-->

                            <div class="col-md-4 mb-3">
                                <label for="commissioningDate">Date mise en service<span style="color:red">*</span></label>
                                <input type="date" class="form-control" id="commissioningDate" name="commissioningDate">
                            </div>
                        </div>

                        <div>
                            <a href="{{ url('dashboard/pv') }}" style="margin-right: 10px" class="btn btn-primary">Annuler</a>
                            <button type="submit" class="btn btn-success">
                                Ajouter
                            </button>
                        </div>
					</form>
				</div>
				</div>
			</div>


		</div>
	</div>
	<!-- /Row -->

</div>
@endsection
