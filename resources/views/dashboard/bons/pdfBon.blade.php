<!DOCTYPE html>
<html>
<head>
  <title>clients</title>

  <!--link href="{{ asset('css/bootstrap/bootstrap.min.css') }}" rel="stylesheet"-->
    <!--link href="{{ asset('css/fontawesome-all.css') }}" rel="stylesheet" -->
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <meta  charset = " UTF-8 " >
  <!--img src="{{ asset('assets/images/btcom.png') }}" width="" alt="Logo" -->


</head>
<body>
<center><p style="line-height:90px; margin-top : 50px;"><h2>BENIN TELECOMS INFRASTRUCTURES</h2></p></center>
</br>
<center><h3>Liste des bons de commandes</h3></center>

<div  class="container">
<table align="center" valign="middle" style="border-collapse: collapse">
                                <thead>
                                    <tr>
                                    <th style="border: 1px solid; padding:12px" width="15%">ID Bon</th>
                                    <th style="border: 1px solid; padding:12px" width="15%">Objet</th>
                                    <th style="border: 1px solid; padding:12px" width="15%">Entreprise</th>
                                    <th style="border: 1px solid; padding:12px" width="15%">Num-Ref</th>
                                    <th style="border: 1px solid; padding:12px" width="15%">Date</th>

                                    </tr>
                                </thead>
                                <tbody>
                                @foreach ($bons as $bon)
                                        <tr>
                                        <td style="border: 1px solid; padding:12px" width="15%"> {{ $bon->num_bon }}</td>
                                        <td style="border: 1px solid; padding:12px" width="15%"> {{ $bon->object }}</td>
                                        <td style="border: 1px solid; padding:12px" width="15%"> {{ $bon->client->enterprise_name }}</td>
                                        <td style="border: 1px solid; padding:12px" width="15%"> {{ $bon->num_ref }} </td>
                                        <td style="border: 1px solid; padding:12px" width="15%">{{ $bon->date }}</td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                            </div>
</body>
</html>
