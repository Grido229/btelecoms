@extends('dashboard.layouts.app', ['title' => 'Dashboard'])

@section('content')
<div class="content container-fluid">
	<section class="comp-section">
		<nav aria-label="breadcrumb">
			<ol class="breadcrumb">
				<li class="breadcrumb-item"><a href="{{ url('dashboard/home') }}">Home</a></li>
				<li class="breadcrumb-item active" aria-current="page">Demandes de services</li>
			</ol>
		</nav>
    </section>

	<div class="row">
		<div class="col-sm-12">
			<div class="card">
				<div class="card-header">
                    <h4 class="card-title">Liste des demandes</h4>
                    <a href="{{ url('dashboard/bons/pdfBon') }}" title="Imprimer la liste des commandes" class="btn btn-success mt-4 float-right" style="margin-top: -2rem !important;"> <i class="fa fa-print"></i> Imprimer</a>
				</div>
				<div class="card-body">
                    <!--reseach--->
                    {{-- <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
                    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script> --}}

                    <div class="container">
                        <div class="row my-4">
                            <input class="form-control" id="myInput" type="text" placeholder="Recherche..">
                        </div>
                    </div>
                    <!--end-reseach--->
<div class="table-responsive">
                        <table class="datatable table table-stripped">
                            <thead>
                                <tr>
                                    <th>ID Bon</th>
                                    <th>Objet</th>
                                    <th>Entreprise</th>
                                    <th>Num-Ref</th>
                                    <th>Date</th>
                                    <th class="text-center">Actions</th>
                                </tr>
                            </thead>
                            <tbody id="myTable">
                                @foreach ($bons as $bon)

                                    <tr>
                                        <td> {{ $bon->num_bon }}</td>
                                        <td>
                                            <?php
                                                echo substr($bon->object, 0 , 36);
                                                if (strlen($bon->object) > 36) {
                                                    echo "...";
                                                }
                                            ?>
                                        </td>
                                        <td> {{ $bon->client->enterprise_name }}</td>
                                        <td> {{ $bon->num_ref }} </td>
                                        <td>{{ $bon->date }}</td>
                                        <td class="text-center">
                                            <a href="{{ url('dashboard/bons/' . $bon->id .'/detailsBon') }}" title="Details" class="btn btn-sm btn-info px-2" style="border-radius: .5rem;">
                                                <i class="fa fa-eye"></i>
                                            </a>

                                            @if (!$is_dcm)
                                                @if($bon->is_validate_by_dcm != 1 )
                                                    <a href="{{ url('dashboard/bons/' . $bon->id . '/edit') }}" title="Editer" class="btn btn-sm btn-success px-2" style="border-radius: .5rem;">
                                                        <i class="fa fa-edit"></i>
                                                    </a>
                                                @endif

                                                <a id="{{ $bon->id }}" onclick="setBonId(event)" href="{{ url('dashboard/bons/' . $bon->id . '/delete') }}" data-toggle="modal" data-target="#deleteModal" title="Supprimer" class="btn btn-sm btn-danger" style="border-radius: .5rem;">
                                                    <i class="fa fa-trash"></i>
                                                </a>
                                            @endif
                                        </td>
                                    </tr>
                                @endforeach

                            </tbody>
                        </table>
					</div>
				</div>
			</div>
		</div>
    </div>


    <div class="modal fade" id="deleteModal" tabindex="-1" role="dialog" aria-labelledby="deleteModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="deleteModalLabel">Suppression</h5>
                <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="modal-body">Voulez-vous vraiment supprimer cette demande de service ?'</div>
            <div class="modal-footer">
                <button class="btn btn-secondary" type="button" data-dismiss="modal">Non</button>

                <a data-href="#" class="btn btn-primary" data-dismiss="modal"  onclick="deleteBon()">
                    Oui
                </a>


            </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('scripts')
<!--reseach--->
<script type="text/javascript">
    var id;
    function setBonId(e) {
        if(e.composedPath){
            var path = (e.composedPath && e.composedPath());

            if(path[1].id != ''){
                this.id = path[1].id;
            }else{
                this.id = path[0].id;
            }
        }
    }

    function deleteBon() {
        window.location.replace('bons/' + id + '/delete');
    }

   $(document).ready(function(){
       $("#myInput").on("keyup", function() {
           var value = $(this).val().toLowerCase();
           $("#myTable tr").filter(function() {
           $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
           });
       });
   });
</script>
<!--end-reseach--->
@endsection
