@extends('dashboard.layouts.app', ['title' => 'Dashboard'])

@section('content')
<div class="content container">

    <!-- Page Header -->
	<section class="comp-section">
		<nav aria-label="breadcrumb">
			<ol class="breadcrumb">
				<li class="breadcrumb-item"><a href="{{ url('dashboard/home') }}">Home</a></li>
            <li class="breadcrumb-item"><a href="{{ url('dashboard/bons') }}">Demande de service</a></li>
				<li class="breadcrumb-item active" aria-current="page">Détails</li>
			</ol>
		</nav>
	</section>
    <!-- /Page Header -->
    <div class="row">
        <div class="col-lg-12 mx-auto">
            <div class="card">
                <div class="card-header">
                    <h4 class="card-title">Informations de la demande de service</h4>
                    <a href="{{ url('dashboard/bons/' . $bon->id . '/edit') }}" class="btn btn-success mt-4 float-right" style="margin-top: -2rem !important;">Modifier</a>
                    <!--a href="{{ url('dashboard/bons/pdfDetail') }}" class="btn btn-primary mt-4 float-right" style="margin-top: -2rem !important;">Imprimer</a-->

                </div>
                <div class="card-body">
                    <div class="row">
                        <p class="col-sm-3 text-muted text-sm-left mb-0 mb-sm-3">Numero du bon de service :</p>
                        <p class="col-sm-9 text-sm-right font-weight-bold">{{ $bon->num_bon }}</p>
                    </div>

                    <div class="row">
                        <p class="col-sm-3 text-muted text-sm-left mb-0 mb-sm-3">Objet :</p>
                        <p class="col-sm-9 text-sm-right font-weight-bold">{{ $bon->object }}</p>
                    </div>

                    <div class="row">
                        <p class="col-sm-3 text-muted text-sm-left mb-0 mb-sm-3">Entreprise :</p>
                        <p class="col-sm-9 text-sm-right font-weight-bold"> {{ $bon->client->enterprise_name }}</p>
                    </div>

                    <div class="row">
                        <p class="col-sm-3 text-muted text-sm-left mb-0 mb-sm-3">Numéro de reférence :</p>
                        <p class="col-sm-9 text-sm-right font-weight-bold">{{ $bon->num_ref }}</p>
                    </div>

                    <div class="row">
                        <p class="col-sm-3 text-muted text-sm-left mb-0 mb-sm-3">Date :</p>
                        <p class="col-sm-9 text-sm-right font-weight-bold">{{ $bon->date }}</p>
                    </div>

                    <div class="row">
                        <p class="col-sm-3 text-muted text-sm-left mb-0 mb-sm-3">Service :</p>
                        <p class="col-sm-9 text-sm-right font-weight-bold">{{ $bon->sfs->fs->libelle }}</p>
                    </div>

                    <div class="row">
                        <p class="col-sm-3 text-muted text-sm-left mb-0 mb-sm-3">Sous service :</p>
                        <p class="col-sm-9 text-sm-right font-weight-bold">{{ $bon->sfs->libelle }}</p>
                    </div>

                    <div class="row">
                        <p class="col-sm-3 text-muted text-sm-left mb-0 mb-sm-3">Sous-sous service :</p>
                        @if($bon->ssfs_id != null)
                        <p class="col-sm-9 text-sm-right font-weight-bold">{{ $bon->ssfs->libelle }}</p>
                        @else
                        <p class="col-sm-9 text-sm-right font-weight-bold" style="color:red;"> Pas de sous-sous service </p>
                        @endif
                    </div>

                    <div class="row">
                        <p class="col-sm-3 text-muted text-sm-left mb-0 mb-sm-3">Capacité :</p>
                        @if($bon->capacite_id != null)
                        <p class="col-sm-9 text-sm-right font-weight-bold">{{ $bon->capacite->capacite }}</p>
                        @else
                        <p class="col-sm-9 text-sm-right font-weight-bold" style="color:red;"> Pas de capacité </p>
                        @endif
                    </div>

                    <div class="row">
                        <p class="col-sm-3 text-muted text-sm-left mb-0 mb-sm-3">Distance :</p>
                        @if($bon->distances_id != null)
                        <p class="col-sm-9 text-sm-right font-weight-bold">{{ $bon->distance->distance }}</p>
                        @else
                        <p class="col-sm-9 text-sm-right font-weight-bold" style="color:red;"> Pas de distance </p>
                        @endif
                    </div>

                    <div class="row">
                        <p class="col-sm-3 text-muted text-sm-left mb-0 mb-sm-3">Site :</p>
                        @if($bon->zones_id != null)
                        <p class="col-sm-9 text-sm-right font-weight-bold">{{ $bon->zone->type }}</p>
                        @else
                        <p class="col-sm-9 text-sm-right font-weight-bold" style="color:red;"> Pas de site </p>
                        @endif
                    </div>



                    <div class="row">
                        <p class="col-sm-3 text-muted text-sm-left mb-0">Prix total :</p>
                        <p class="col-sm-9 text-sm-right font-weight-bold mb-0">{{ $bon->total_price_ht }}</p>
                    </div>

                    @if ($bon->contrat)
                        <h5 class="font-weight-bold text-center my-3 py-3">Contrat associé à la demande</h5>

                        <div class="row">
                            <p class="col-sm-3 text-muted text-sm-left mb-0 mb-sm-3">Numero:</p>
                            <p class="col-sm-9 text-sm-right font-weight-bold"> {{ $bon->contrat->num_contrat }}</p>
                        </div>

                        <div class="row">
                            <p class="col-sm-3 text-muted text-sm-left mb-0  mb-sm-3">Objet :</p>
                            <p class="col-sm-9 text-sm-right font-weight-bold mb-0">{{ $bon->contrat->object_contrat }} </p>
                        </div>

                        <div class="row">
                            <p class="col-sm-3 text-muted text-sm-left mb-0 mb-sm-3">Durée :</p>
                            <p class="col-sm-9 text-sm-right font-weight-bold">{{ $bon->contrat->duree_contrat > 1 ? $bon->contrat->duree_contrat . ' ans': $bon->contrat->duree_contrat . ' an' }}</p>
                        </div>

                        <div class="row">
                            <p class="col-sm-3 text-muted text-sm-left mb-0 mb-sm-3">Date de début :</p>
                            <p class="col-sm-9 text-sm-right font-weight-bold">{{ $bon->contrat->begin_date }}</p>
                        </div>

                        <div class="row">
                            <p class="col-sm-3 text-muted text-sm-left mb-0">Date de fin :</p>
                            <p class="col-sm-9 text-sm-right font-weight-bold mb-0">{{ $bon->contrat->end_date }} </p>
                        </div>
                    @endif

                    @if ($bon->details->count() > 0)
                        <h5 class="font-weight-bold text-center my-3 py-3">Détails de la demande</h5>

                        <div class="table-responsive">
                            <table class="datatable table table-stripped">
                                <thead>
                                    <tr>
                                        <th>ID</th>
                                        <th>Description</th>
                                        <th>Capacité</th>
                                        <th>Mensualité</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach ($bon->details as $key=>$detail)
                                        <tr>
                                            <td> {{ $key + 1 }}</td>
                                            <td>{{ $detail->description }}</td>
                                            <td> {{ $detail->capacity }}</td>
                                            <td> {{ $detail->mensuality }} </td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    @endif

                </div>
            </div>
        </div>
    </div>
</div>

@endsection
