@extends('dashboard.layouts.app', ['title' => 'Dashboard'])

@section('content')
<div class="content container-fluid">

					<!-- Page Header -->
	<section class="comp-section">
		<nav aria-label="breadcrumb">
			<ol class="breadcrumb">
				<li class="breadcrumb-item"><a href="{{ url('dashboard/home') }}">Home</a></li>
				<li class="breadcrumb-item"><a href="{{ url('dashboard/bons') }}">Bons de commande</a></li>
				<li class="breadcrumb-item active" aria-current="page">Modifier un bon de commande</li>
			</ol>
		</nav>
	</section>
	<!-- /Page Header -->

	<!-- Row -->
	<div class="row">
		<div class="col-sm-12">

			<!-- Custom Boostrap Validation -->
			<div class="card">
				<div class="card-header">
					<h4 class="card-title">Modifier un bon de commande</h4>
				</div>
				<div class="card-body">
                    <form method="POST" action="{{ url('dashboard/bons/' . $bon->id . '/edit') }}">
                        @csrf

                        @error('montantActivation-invalid')
                            <span class="text-danger" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                        <div class="form-row">
                            <div class="col-md-4 mb-3">
                                <label for="client_id">Client<span style="color:red">*</span></label>
                                <select style="font-size : 18px;" name="client_id" class="form-control" id="client_id" required>
                                    <option disabled selected>Sélectionnez le client</option>
                                    @foreach ($clients as $client)
                                        @if ($bon->client_id == $client->id)
                                            <option selected value="{{$client->id}}">{{$client->enterprise_name}}</option>
                                        @else
                                            <option value="{{$client->id}}">{{$client->enterprise_name}}</option>
                                        @endif
                                    @endforeach
                                </select>
                                @error('client_id')
                                    <span class="text-danger" role="alert">
                                        <strong>Vous devez sélectionner un client.</strong>
                                    </span>
                                @enderror
                            </div>

                            <div class="col-md-4 mb-3">
                                <label for="num_ref">Numero de référence<span style="color:red">*</span></label>
                                <input type="text" class="form-control" id="num_ref" name="num_ref" value="{{ $bon->num_ref }}" required>
                                @error('num_ref')
                                    <span class="text-danger" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>

                            <div class="col-md-4 mb-3">
                                <label for="date">Date<span style="color:red">*</span></label>
                                <input type="date" class="form-control" id="date" name="date" value="{{ $bon->date }}" required>
                                @error('date')
                                    <span class="text-danger" role="alert">
                                        <strong>Ce champ est obligatoire.</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-row">
                            <div class="col-md-12 mb-3">
                                <label for="object">Object<span style="color:red">*</span></label>
                                <input type="text" class="form-control" id="object" name="object" value="{{ $bon->object }}" required>
                                @error('object')
                                    <span class="text-danger" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-row">
                            <div class="col-md-4 mb-3">
                                <label>Famille de Service<span style="color:red">*</span></label>
                                <input type="text" class="form-control" value="{{ $bon->sfs->fs->libelle }}" disabled>
                            </div>

                            <div class="col-md-4 mb-3">
                                <label>Sous Famille de Service<span style="color:red">*</span></label>
                                <input type="text" class="form-control" value="{{ $bon->sfs->libelle }}" disabled>
                            </div>
                            @if($bon->ssfs_id != NULL)
                            <div class="col-md-4 mb-3">
                                <label>Sous-sous Famille de Service<span style="color:red">*</span></label>
                                <input type="text" class="form-control" value="{{ $bon->ssfs->libelle }}" disabled>
                            </div>
                            @endif
                        </div>
                        <div class="form-row">
                            @if($bon->capacite_id != NULL)
                            <div class="col-md-4 mb-3">
                                <label>Capacité<span style="color:red">*</span></label>
                                <input type="text" class="form-control" value="{{ $bon->capacite->capacite }}" disabled>
                            </div>
                            @endif

                            @if($bon->distances_id != NULL)
                            <div class="col-md-4 mb-3">
                                <label>Distance<span style="color:red">*</span></label>
                                <input type="text" class="form-control" value="{{ $bon->distance->distance }}" disabled>
                            </div>
                            @endif

                            @if($bon->zones_id != NULL)
                            <div class="col-md-4 mb-3">
                                <label>Site<span style="color:red">*</span></label>
                                <input type="text" class="form-control" value="{{ $bon->zone->type }}" disabled>
                            </div>
                            @endif
                        </div>

                        {{-- ======================================Services======================= --}}

                        <div class="form-row mb-3 col-md-12">

                                <div class="col-md-4 mb-3">
                                    <label for="bon_id">Famille de Service<span style="color:red">*</span></label>
                                    <select style="font-size : 18px;" name="fs" class="form-control" id="fs" >
                                        {{--<option >Sélectionnez la famille de service</option>--}}
                                            <option disabled selected value="{{ $bon->sfs->fs->id }}">{{$bon->sfs->fs->libelle}}</option>
                                        @foreach ($fs as $fs)
                                            <option value="{{$fs->id}}">{{$fs->libelle}}</option>
                                        @endforeach
                                    </select>
                                    @error('fs')
                                        <span class="text-danger" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>

                                <div id="Sfs1" name="Sfs" style="display: none" class="col-md-4 mb-3">
                                    <label for="bon_id">Sous Famille de Service<span style="color:red">*</span></label>
                                    <select style="font-size : 18px;"  name="Sfs_id" class="form-control" id="Sfs11_id"  >

                                        <option disabled selected>Sélectionnez la sous famille de service</option>
                                        @foreach ($sous_famille_service_transport as $sous_famille_service_transport)
                                            <option value="{{$sous_famille_service_transport->id}}">{{$sous_famille_service_transport->libelle}}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div id="Sfs2" name="Sfs" style="display: none" class="col-md-4 mb-3">
                                    <label for="bon_id">Sous Famille de Service<span style="color:red">*</span></label>
                                    <select style="font-size : 18px;" name="Sfs_id" class="form-control" id="Sfs12_id"  >
                                        <option disabled selected>Sélectionnez la sous famille de service</option>
                                        @foreach ($sous_famille_access_service as $sous_famille_access_service)
                                            <option value="{{$sous_famille_access_service->id}}">{{$sous_famille_access_service->libelle}}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div id="Sfs3" name="Sfs" style="display: none" class="col-md-4 mb-3">
                                    <label for="bon_id">Sous Famille de Service<span style="color:red">*</span></label>
                                    <select style="font-size : 18px;" name="Sfs_id" class="form-control" id="Sfs13_id"  >
                                        <option disabled selected>Sélectionnez la sous famille de service</option>
                                        @foreach ($sous_famille_hebergement_service as $sous_famille_hebergement_service)
                                            <option value="{{$sous_famille_hebergement_service->id}}">{{$sous_famille_hebergement_service->libelle}}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div id="Sfs4" name="Sfs" style="display: none" class="col-md-4 mb-3">
                                    <label for="bon_id">Sous Famille de Service<span style="color:red">*</span></label>
                                    <select style="font-size : 18px;" name="Sfs_id" class="form-control" id="Sfs14_id"  >
                                        <option disabled selected>Sélectionnez la sous famille de service</option>
                                        @foreach ($sous_famille_voice_service as $sous_famille_voice_service)
                                            <option value="{{$sous_famille_voice_service->id}}">{{$sous_famille_voice_service->libelle}}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div id="Ssfs1" name="Ssfs" style="display: none" class="col-md-4 mb-3">
                                    <label for="bon_id">Sous-sous Famille de Service<span style="color:red">*</span></label>
                                    <select style="font-size : 18px;" name="Ssfs_id" class="form-control" id="Ssfs1_id">
                                        <option disabled selected>Sélectionnez la sous famille de service</option>
                                        @foreach ($ssfs_transport as $ssfs_transport)
                                            <option value="{{$ssfs_transport->id}}">{{$ssfs_transport->libelle}}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div id="Ssfs2" name="Ssfs" style="display: none" class="col-md-4 mb-3">
                                    <label for="bon_id">Sous-sous Famille de Service<span style="color:red">*</span></label>
                                    <select style="font-size : 18px;" name="Ssfs_id" class="form-control" id="Ssfs4_id">
                                        <option disabled selected>Sélectionnez la sous famille de service</option>
                                        @foreach ($ssfs_transit as $ssfs_transit)
                                            <option value="{{$ssfs_transit->id}}">{{$ssfs_transit->libelle}}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div id="Ssfs3" name="Ssfs" style="display: none" class="col-md-4 mb-3">
                                    <label for="bon_id">Sous-sous Famille de Service<span style="color:red">*</span></label>
                                    <select style="font-size : 18px;" name="Ssfs_id" class="form-control" id="Ssfs10_id">
                                        <option disabled selected>Sélectionnez la sous famille de service</option>
                                        @foreach ($ssfs_pylones as $ssfs_pylones)
                                            <option value="{{$ssfs_pylones->id}}">{{$ssfs_pylones->libelle}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>

                        {{-- ====================================Autres================================== --}}
                                <div class="form-row mb-3 col-md-12">
                                <div class="col-md-6 mb-3" style="display:none;" id="capacite">
                                        <label for="capacite"> Capacité <span >*</span></label>
                                        <select style="font-size : 18px;" class="form-control" name="capacite" id="capacite_id"  >
                                            <option disabled selected>Sélectionnez la capacité</option>
                                            @foreach($capacites as $capacite)
                                                <option value="{{$capacite->id}}">{{$capacite->capacite}}</option>
                                            @endforeach
                                        </select>
                                </div>

                                <div class="col-md-6 mb-3" style=" display:none;" id="distance">
                                        <label for="distance">Distance cotonou-station (Km)<span>*</span></label>
                                        <select style="font-size : 18px;" class="form-control" name="distance" id="distance_id"  >
                                            <option disabled selected>Distance cotonou-station (Km)</option>
                                            @foreach($distances as $distance)
                                                <option value="{{$distance->id}}">{{$distance->distance}}</option>
                                            @endforeach
                                        </select>
                                </div>

                            <div class="col-md-4 mb-3" style=" display:none;" id="site">
                                <label for="site">Site<span >*</span></label>
                                <select style="font-size : 18px;" name="site" class="form-control" id="site_id" >
                                    <option disabled selected>Sélectionnez le site</option>
                                    @foreach($sites as $site )
                                    <option value="{{$site->id}}">{{$site->type}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>

                        {{--========================================================================== --}}

                        @if ($bon->contrat)
                            <h3 class="text-center my-3">Contrat</h3>
                            <div class="form-row">
                                <div class="col-md-6 mb-3">
                                    <label for="begin_date">Date de début<span style="color:red">*</span></label>
                                    <input type="date" class="form-control" id="begin_date" name="begin_date" value="{{ $bon->contrat->begin_date }}" required>
                                    @error('begin_date')
                                        <span class="text-danger" role="alert">
                                            <strong>Ce champ est obligatoire.</strong>
                                        </span>
                                    @enderror
                                </div>
                                <div class="col-md-6 mb-3">
                                    <label for="end_date">Date de fin<span style="color:red">*</span></label>
                                    <input type="date" class="form-control" id="end_date" name="end_date" value="{{ $bon->contrat->end_date }}" required>
                                    @error('end_date')
                                        <span class="text-danger" role="alert">
                                            <strong>Ce champ est obligatoire.</strong>
                                        </span>
                                    @enderror
                                </div>

                                <div class="col-md-12 mb-3">
                                    <label for="object_contrat">Objet du contrat<span style="color:red">*</span></label>
                                    <textarea class="form-control" name="object_contrat" id="object_contrat" cols="30" rows="5" required>{{ $bon->contrat->object_contrat }}</textarea>
                                    @error('object_contrat')
                                        <span class="text-danger" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                            </div>

                        @else
                            <div class="form-row">
                                <div class="custom-control custom-checkbox mb-3 ml-2">
                                    <input type="checkbox" class="custom-control-input" id="add_contrat" name="add_contrat" onchange="toggleAddContrat()">
                                    <label class="custom-control-label" for="add_contrat">Ajouter un contrat à cette demande</label>
                                </div>
                            </div>

                            <h3 class="text-center my-3" id="contrat_title" style="display:none;">Contrat</h3>
                            <div id="contrat"></div>
                        @endif

                        <h3 class="text-center">Détails de la commande</h3>

                        @foreach ($bon->commande_details as $detail)
                            <div class="form-row mb-3">
                                <input type="hidden" class="form-control" name="detail_id[]" id="detail_id" value="{{ $detail->id }}">

                                <div class="col-md-4 mb-3">
                                    <label for="description">Description<span style="color:red">*</span></label>
                                    <input type="text" class="form-control" name="description[]" id="description" value="{{ $detail->description }}">
                                </div>

                                <div class="col-md-4 mb-3">
                                    <label for="capacity">Capcité<span style="color:red">*</span></label>
                                    <input type="text" class="form-control" id="capacity" name="capacity[]" value="{{ $detail->capacity }}">
                                </div>

                                <div class="col-md-4 mb-3">
                                    <label for="mensuality">Mensualité<span style="color:red">*</span></label>
                                    <input type="number" class="form-control mensuality" id="mensuality{{$detail->id}}" name="mensuality[]" value="{{ $detail->mensuality }}">
                                </div>
                            </div>
                        @endforeach

                        <div class="form-row mb-3" id="details_wrapper"></div>

                        <button type="button" class="btn mb-3" id="add_detail">
                            <i class="fa fa-plus-circle fa-2x" aria-hidden="true"></i>
                        </button>


                        {{--===================================Montant================================== --}}



                        <div class="form-row mb-12 justify-content-end">
                            <div class="col-md-5 float-right">
                            <div class="form-row">
                                    @error('montant-invalid')
                                        <span class="text-danger" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                <div class="custom-control custom-checkbox mb-3 ml-2">
                                    <input type="checkbox" class="custom-control-input" id="price_id" name="price_id" onclick="afficherMont()" >
                                    <label class="custom-control-label" for="price_id">Voir le montant</label>
                                </div>
                            </div>
                                <div style="display:none;" id="look_price"  style="display:none">
                                    <label for="total_price"><span style="color:red; font-weight:900;">Prix total HT</span></label>
                                    <input type="text" class="form-control" id="total_price_ht" name="total_price_ht" readonly>
                                </div>

                            </div>

                            </div>
                                <div id="total_price" style="display:none" class="col-md-5 float-right" id="total_price">
                                    <label for="total_price"><span style="color:red; font-weight:900;">Saisir le montant</span></label>
                                    <input type="text" class="form-control" id="total_price" name="total_price">
                                </div>
                            </div>





                            <div id="blocActivation" style="display: none;">

                                <div style="text-align: center; font-size : 35px;">  Frais d'activation </div>
                                <div class="col-md-6 mb-3"  id="capaciteActivation" >
                                        <label for="capaciteActivation"> Capacité d'activation</label>
                                        <select style="font-size : 18px; "  class="form-control" name="capaciteActivation" id="capaciteActivation_id"  >
                                            <option disabled selected>Sélectionnez la capacité</option>
                                            @foreach($capacites as $capacite)
                                                <option value="{{$capacite->id}}">{{$capacite->capacite}}</option>
                                            @endforeach
                                        </select>
                                </div>


                                <div class="form-row mb-12 justify-content-end">
                                    <div class="col-md-5 float-right">
                                        <div class="custom-control custom-checkbox mb-3 ml-2">
                                            <input type="checkbox" class="custom-control-input" id="priceActivation_id" name="priceActivation_id" onclick="afficherMontActivation()" >
                                            <label class="custom-control-label" for="priceActivation_id">Voir le montant d'activation</label>
                                        </div>

                                        <div style="display:none;" id="look_priceActivation"  >
                                            <label for="total_priceActivation_ht"><span style="color:red; font-weight:900;">Prix Activation</span></label>
                                            <input type="text" class="form-control" id="total_priceActivation_ht" name="total_priceActivation_ht" readonly>
                                        </div>
                                    </div>
                                </div>

                            </div>
                        {{-- ===================================================================== --}}
                        <div class="form-row mb-3 justify-content-end">
                            <div class="col-md-3 float-right">
                                <label for="old_total_price"><span style="color:red; font-weight:900;">Ancien Prix total HT</span></label>
                                <input type="text" class="form-control" id="old_total_price" name="old_total_price" value="{{ $bon->total_price_ht }}" disabled>
                            </div>
                        </div>

                        <div>
                            <a href="{{ url('dashboard/bons') }}" style="margin-right: 10px" class="btn btn-primary">Annuler</a>
                            <button type="submit" class="btn btn-success">
                                Modifier
                            </button>
                        </div>
                    </form>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection

@section('scripts')
<script type="text/javascript">
    var mensuality = document.getElementsByClassName("mensuality");
    var total_price = document.getElementById("total_price");
    var total_price_ht = document.getElementById("total_price_ht");

    var i = 1;

    $("#add_detail").click(function(e){
        var name = "detail" + i;
        e.preventDefault();
        $("#details_wrapper").append(`
                            <div class="col-md-12 mb-3">
                                <div class="form-row mb-3">
                                    <div class="col-md-4 mb-3">
                                        <label for="description">Description<span style="color:red">*</span></label>
                                        <input type="text" class="form-control" name="description[]" id="description">
                                    </div>

                                    <div class="col-md-4 mb-3">
                                        <label for="capacity">Capcité<span style="color:red">*</span></label>
                                        <input type="text" class="form-control" id="capacity" name="capacity[]">
                                    </div>

                                    <div class="col-md-4 mb-3">
                                        <label for="mensuality">Mensualité<span style="color:red">*</span></label>
                                        <input type="number" class="form-control mensuality" id="mensuality${i}" name="mensuality[]" value="0">
                                    </div>
                                </div>
                            </div>`);
        i++;
        chargeEvent();
    });

    function chargeEvent(){
        var inputs = document.getElementsByClassName('mensuality');

        for (var i = 0; i < inputs.length; i++) {
            inputs[i].addEventListener("change", changeValue);
        }
    }

    function changeValue() {
        var i;
        var total = 0;
        for(i = 0; i < mensuality.length; i++) {
            console.log(document.getElementById(mensuality[i].id).value);

            total += parseFloat(document.getElementById(mensuality[i].id).value);
        }
        total_price.value = new Intl.NumberFormat("fr-FR", {style: "currency", currency: "XOF"}).format(total);
        total_price_ht.value = total;
    }

    function toggleAddContrat() {
        if(add_contrat) {
            $("#contrat_title").show();
            $("#contrat").append(`
                <div class="form-row">
                    <div class="col-md-6 mb-3">
                        <label for="duree_contrat">Durée contrat (en mois)<span style="color:red">*</span></label>
                        <input type="number" class="form-control" id="duree_contrat" name="duree_contrat" value="{{ old('duree_contrat') }}" required>
                        @error('duree_contrat')
                            <span class="text-danger" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>

                    <div class="col-md-6 mb-3">
                        <label for="begin_date">Date de début<span style="color:red">*</span></label>
                        <input type="date" class="form-control" id="begin_date" name="begin_date" value="{{ old('begin_date') }}" required>
                        @error('begin_date')
                            <span class="text-danger" role="alert">
                                <strong>Ce champ est obligatoire.</strong>
                            </span>
                        @enderror
                    </div>

                    <div class="col-md-12 mb-3">
                        <label for="object_contrat">Objet du contrat<span style="color:red">*</span></label>
                        <textarea class="form-control" name="object_contrat" id="object_contrat" cols="30" rows="5" value="{{ old('object_contrat') }}" required></textarea>
                        @error('object_contrat')
                            <span class="text-danger" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>
                </div>`);

        } else {
            $("#contrat_title").hide();
            $("#contrat")[0].children[0].remove();
        }
        add_contrat = !add_contrat;
    }

    $(document).ready( function () {
        chargeEvent();
    } );


</script>






<script>

jQuery(document).ready(function($){



$('#fs').change(function(){


    switch( $(this).val() )
    {

        case '1':
        $('#Sfs1').show();
        $('#Sfs2').hide();
        $('#Sfs3').hide();
        $('#Ssfs2').hide();
        $('#Ssfs1').hide();
        $('#Ssfs3').hide();
        $('#capacite').hide();
        $('#distance').hide();
        $('#site').hide();



        break;

        case '2':
        $('#Sfs2').show();
        $('#Sfs1').hide();
        $('#Sfs3').hide();
        $('#Ssfs2').hide();
        $('#Ssfs1').hide();
        $('#Ssfs3').hide();
        $('#capacite').hide();
        $('#distance').hide();
        $('#site').hide();


        break;

        case '3':
        $('#Sfs2').hide();
        $('#Sfs1').hide();
        $('#Sfs3').show();
        $('#Ssfs2').hide();
        $('#Ssfs1').hide();
        $('#Ssfs3').hide();
        $('#capacite').hide();
        $('#distance').hide();
        $('#site').hide();

        break;

        case '4':
        $('#Sfs2').hide();
        $('#Sfs1').hide();
        $('#Sfs3').hide();
        $('#Ssfs2').hide();
        $('#Ssfs1').hide();
        $('#Ssfs3').hide();
        $('#capacite').hide();
        $('#distance').hide();
        $('#site').hide();


        break;


        default:
            break;
    }


});



$('#Sfs11_id').change(function(){
    switch( $(this).val() )
    {
        case '1':
        $('#Ssfs1').show();
        $('#Ssfs2').hide();
        $('#Ssfs3').hide();
        $('#capacite').hide();
        $('#distance').hide();
        $('#site').hide();


        break;

        case '2':
        $('#Ssfs1').hide();
        $('#Ssfs2').hide();
        $('#Ssfs3').hide();
        $('#capacite').show();
        $('#distance').show();
        $('#site').hide();


        break;

        case '3':
        $('#Ssfs1').hide();
        $('#Ssfs2').hide();
        $('#Ssfs3').hide();
        $('#capacite').show();
        $('#distance').show();
        $('#site').hide();


        break;

        case '4':
        $('#Ssfs2').show();
        $('#Ssfs1').hide();
        $('#Ssfs3').hide();
        $('#capacite').hide();
        $('#distance').hide();
        $('#site').hide();


        break;

        case '5':
        $('#Ssfs2').hide();
        $('#Ssfs1').hide();
        $('#Ssfs3').hide();
        $('#capacite').hide();
        $('#distance').hide();
        $('#site').hide();


        break;

        case '6':
        $('#Ssfs2').hide();
        $('#Ssfs1').hide();
        $('#Ssfs3').hide();
        $('#capacite').show();
        $('#distance').hide();
        $('#site').hide();


        break;

        default:
        break;
    }


});


$('#Sfs12_id').change(function(){
    switch( $(this).val() )
    {
        case '7':
        $('#Ssfs1').hide();
        $('#Ssfs2').hide();
        $('#Ssfs3').hide();
        $('#capacite').show();
        $('#distance').hide();
        $('#site').hide();
    }

});


$('#Sfs13_id').change(function(){
    switch( $(this).val() )
    {
        case '8':
        $('#Ssfs1').hide();
        $('#Ssfs2').hide();
        $('#Ssfs3').hide();
        $('#capacite').hide();
        $('#distance').hide();
        $('#site').show();


        break;

        case '9':
        $('#Ssfs1').hide();
        $('#Ssfs2').hide();
        $('#Ssfs3').hide();
        $('#capacite').hide();
        $('#distance').hide();
        $('#site').hide();


        break;

        case '10':
        $('#Ssfs1').hide();
        $('#Ssfs2').hide();
        $('#Ssfs3').show();
        $('#capacite').hide();
        $('#distance').hide();
        $('#site').hide();



        break;

        case '11':
        $('#Ssfs2').hide();
        $('#Ssfs1').hide();
        $('#Ssfs3').hide();
        $('#capacite').hide();
        $('#distance').hide();
        $('#site').hide();


        break;

        case '12':
        $('#Ssfs2').hide();
        $('#Ssfs1').hide();
        $('#Ssfs3').hide();
        $('#capacite').hide();
        $('#distance').hide();
        $('#site').hide();


        break;

        default:
        break;
    }


});

$('#Ssfs1_id').change(function(){
    switch( $(this).val() )
    {
        case '1':

        $('#capacite').show();
        $('#distance').show();
        $('#site').hide();


        break;

        case '2':

        $('#capacite').show();
        $('#distance').hide();
        $('#site').hide();


        break;



        case '3':

        $('#capacite').show();
        $('#distance').show();
        $('#site').show();


        break;

        case '4':

        $('#capacite').show();
        $('#distance').show();
        $('#site').show();


        break;



        case '5':

        $('#capacite').hide();
        $('#distance').hide();
        $('#site').hide();


        break;



        case '6':

        $('#capacite').hide();
        $('#distance').hide();
        $('#site').hide();


        break;

        default:
        break;
    }


});


$('#Ssfs4_id').change(function(){
    switch( $(this).val() )
    {
        case '7':

        $('#capacite').show();
        $('#distance').hide();
        $('#site').hide();


        break;

        case '8':

        $('#capacite').show();
        $('#distance').hide();
        $('#site').hide();



        break;



        default:
        break;
    }


});


$('#Ssfs10_id').change(function(){
    switch( $(this).val() )
    {
        case '9':

        $('#capacite').hide();
        $('#distance').hide();
        $('#site').show();


        break;

        case '10':

        $('#capacite').hide();
        $('#distance').hide();
        $('#site').show();


        break;



        default:
        break;
    }


});


})

//*************************************************** */


jQuery(document).ready(function($){

$.ajaxSetup({
headers: {
            'X-CSRF-TOKEN' : $('meta[name="csrf-token"]').attr('content')
        }
});




$('[name = Sfs]').change(function(){

        if( jQuery($('[name = Ssfs]')).is(":visible") ){

            $('[name = Ssfs]').change(function(){

                $.ajax({

                        type: "POST",
                        url: '{{route('ajaxrequest.form')}}', 
                        dataType: "json",

                        data: {
                            "Ssfs_id" : $('[name = Ssfs_id]').val() ,
                            "Sfs_id" : $('[name = Sfs_id]').val() ,
                                },
                        success:function(data){
                            //console.log(data.message);
                            if(data.message == true){
                                $("#blocActivation").show();
                            }else{
                                $("#blocActivation").hide();
                            }

                            },
                        error:function(data){
                            //console.log('error') ;
                            }
                });

            });

        }else{
            $.ajax({

                    type: "POST",
                    url: '{{route('ajaxrequest.form')}}',
                    dataType: "json",

                    data: {
                        "Ssfs_id" : null ,
                        "Sfs_id" : $('[name = Sfs_id]').val() ,
                            },
                    success:function(data){
                        //console.log(data.message);
                        if(data.message == true){
                                $("#blocActivation").show();
                            }else{
                                $("#blocActivation").hide();
                            }

                        },
                    error:function(data){
                        //console.log('error') ;
                        }
            });

        }

});



})

//************************************************** */
function afficherMontActivation(){

var capaciteVisile = jQuery($('#capaciteActivation_id')).is(":visible");
var capacite = $('[name = capaciteActivation]').val();

if(!capaciteVisile){
capacite = null;
}


$.ajaxSetup({
headers: {
    'X-CSRF-TOKEN' : $('meta[name="csrf-token"]').attr('content')
}
});

if (priceActivation_id) {
$.ajax({



type: "POST",
url: '{{route('ajaxrequest.mont')}}',
dataType: "json",

data: {
"Ssfs_id" : $('[name = Ssfs_id]').val() ,
"Sfs_id" : $('[name = Sfs_id]').val() ,
"capaciteActivation_id" :  capacite  ,
    },
success:function(data){
console.log(data.message);

total_priceActivation_ht.value = data.message ;

},
error:function(data){
//console.log('error') ;
}
});

$("#look_priceActivation").show();


}else if(priceActivation_id){
$("#look_priceActivation").hide();
}

}

//************************************************** */


function afficherMont(){

//$ssfs = $('[name = Ssfs_id]').val();
var distanceVisile = jQuery($('#distance')).is(":visible");
var capaciteVisile = jQuery($('#capacite')).is(":visible");
var siteVisile = jQuery($('#site')).is(":visible");

var distance = $('[name = distance]').val();
var capacite = $('[name = capacite]').val();
var site = $('[name = site]').val();

if(!distanceVisile){
distance = null;
}
if(!capaciteVisile){
capacite = null;
}
if(!
siteVisile){
site = null;
}
//console.log($('#Sfs12_id').val() );
var sfs = $('[name = Sfs_id]').val();

if( jQuery($('#Sfs12_id')).is(":visible") ){
sfs = $('#Sfs12_id').val();
}else if(jQuery($('#Sfs13_id')).is(":visible")){
sfs = $('#Sfs13_id').val();
}else if(jQuery($('#Sfs14_id')).is(":visible")){
sfs = $('#Sfs14_id').val();
}

var ssfs = $('[name = Ssfs_id]').val();

if(jQuery($('#Ssfs4_id')).is(":visible")){
ssfs = $('#Ssfs4_id').val();
}else if(jQuery($('#Ssfs10_id')).is(":visible")){
ssfs = $('#Ssfs10_id').val();
}

if (price_id) {

$.ajaxSetup({
headers: {
    'X-CSRF-TOKEN' : $('meta[name="csrf-token"]').attr('content')
}
});

$.ajax({



type: "POST",
url: '{{route('ajaxrequestMontant')}}',
dataType: "json",

data: {
"Ssfs_id" : ssfs ,
"Sfs_id" : sfs ,
"distance" :  distance  ,
"capacite" :  capacite  ,
"site" :  site  ,
    },
success:function(data){
console.log(data.message);


   total_price_ht.value = data.message ;
   if (data.message == 'Has not price') {
       $("#total_price").show();
       document.getElementById("total_price").placeholder = 'Saisir le montant' ;
   }else{
    $("#total_price").hide();
   }
},
error:function(data){
console.log('error') ; 
}
});

$("#look_price").show();


}else if(price_id){
$("#look_price").hide();
}

}


</script>
@endsection









