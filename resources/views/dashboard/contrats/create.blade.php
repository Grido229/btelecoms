@extends('dashboard.layouts.app', ['title' => 'Dashboard'])

@section('content')
<div class="content container-fluid">

    <!-- Page Header -->
	<section class="comp-section">
		<nav aria-label="breadcrumb">
			<ol class="breadcrumb">
				<li class="breadcrumb-item"><a href="{{ url('dashboard/home') }}">Home</a></li>
            <li class="breadcrumb-item"><a href="{{ url('dashboard/bons') }}">Demande de service</a></li>
				<li class="breadcrumb-item active" aria-current="page">Nouveau contrat</li>
			</ol>
		</nav>
	</section>
	<!-- /Page Header -->

	<!-- Row -->
	<div class="row">
		<div class="col-sm-12">

			<!-- Custom Boostrap Validation -->
			<div class="card">
				<div class="card-header">
					<h4 class="card-title">Ajouter un contrat</h4>
                   
				</div>
				<div class="card-body">
					<form method="POST" action="{{ url('dashboard/bons/create') }}">
                        @csrf

                        <div class="form-row">
                            <div class="col-md-4 mb-3">
                                <label for="client_id">Client<span style="color:red">*</span></label>
                                <select name="client_id" class="form-control" id="client_id">
                                    <option disabled selected>Sélectionnez le client</option>
                                   
                                </select>
                                @error('client_id')
                                    <span class="text-danger" role="alert">
                                        <strong>Vous devez sélectionner un client.</strong>
                                    </span>
                                @enderror
                            </div>

                            <div class="col-md-4 mb-3">
                                <label for="num_ref">Numéro contrat<span style="color:red">*</span></label>
                                <input type="text" class="form-control" id="num_ref" name="num_ref" value="{{ old('num_ref') }}">
                                @error('num_ref')
                                    <span class="text-danger" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>

                            <div class="col-md-4 mb-3">
                                <label for="num_ref">Durée contrat<span style="color:red">*</span></label>
                                <input type="text" class="form-control" id="num_ref" name="num_ref" value="{{ old('num_ref') }}">
                                @error('num_ref')
                                    <span class="text-danger" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>

                            <div class="col-md-4 mb-3">
                                <label for="date">Date début<span style="color:red">*</span></label>
                                <input type="date" class="form-control" id="date" name="date" value="{{ old('date') }}">
                                @error('date')
                                    <span class="text-danger" role="alert">
                                        <strong>Ce champ est obligatoire.</strong>
                                    </span>
                                @enderror
                            </div>
                            <div class="col-md-4 mb-3">
                                <label for="date">Date fin<span style="color:red">*</span></label>
                                <input type="date" class="form-control" id="date" name="date" value="{{ old('date') }}">
                                @error('date')
                                    <span class="text-danger" role="alert">
                                        <strong>Ce champ est obligatoire.</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-row">
                            <div class="col-md-12 mb-3">
                                <label for="object">Object du contrat<span style="color:red">*</span></label>
                                <input type="text" class="form-control" id="object" name="object" value="{{ old('object') }}">
                                @error('object')
                                    <span class="text-danger" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        
                        </div>
                        <input name="total_price_ht" type="hidden" id="total_price_ht" value="0" />

                        <div>
                            <a href="{{ url('dashboard/bons') }}" style="margin-right: 10px" class="btn btn-primary">Annuler</a>
                            <button type="submit" class="btn btn-success">
                                Ajouter
                            </button>
                        </div>
					</form>
				</div>
            </div>
		</div>
	</div>
</div>

@endsection
