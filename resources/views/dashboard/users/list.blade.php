@extends('dashboard.layouts.app', ['title' => 'Dashboard'])

@section('content')
<div class="content">
    <div class="container-fluid">
        <section class="comp-section">
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="{{ url('dashboard/home') }}">Home</a></li>
                    <li class="breadcrumb-item"><a href="{{ url('dashboard/users') }}">Utilisateurs</a></li>
                    <li class="breadcrumb-item active" aria-current="page">Utilisateurs</li>
                </ol>
            </nav>
        </section>

        <div class="row">
            <div class="col-sm-12">
                <div class="card">
                    <div class="card-header">
                        <h4 class="card-title">Liste des Utilisateurs</h4>
                        <a href="{{ url('dashboard/users/create') }}" class="btn btn-success mt-4 float-right" style="margin-top: -2rem !important;">Ajouter un utilisateur</a>
                    </div>
                    <div class="card-body">

                        <div class="container">
                            <div class="row my-4">
                                <input class="form-control" id="myInput" type="text" placeholder="Recherche..">
                            </div>
                        </div>
                        <!--end-reseach--->

                        <div class="table-responsive">
                            <table class="datatable table table-stripped">
                                <thead>
                                    <tr>
                                        <th class="text-center">ID</th>
                                        <th  class="text-center">Nom & Prénom(s)</th>
                                        <th  class="text-center">Droit</th>
                                        <th  class="text-center">Code Utilisateur</th>
                                        <th  class="text-center">Adresse</th>
                                        <th  class="text-center">Email</th>
                                        <th  class="text-center">tel</th>
                                        <th  class="text-center">Service</th>
                                        <th class="text-center">Actions</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach ($users as $user)
                                        <tr>
                                            <td class="text-center"> {{ $user->id }}</td>
                                            <td> {{ $user->fullname }} </td>
                                            <td> {{ $user->role }}</td>
                                            <td>{{$user->code_users}}</td>
                                            @if( $user->adresse )
                                            <td  class="text-center"> {{ $user->adresse }}</td>
                                            @else
                                            <td style="text-align : center;"> ---</td>
                                            @endif

                                            @if( $user->email )
                                            <td  class="text-center"> {{ $user->email }}</td>
                                            @else
                                            <td style="text-align : center;"> ---</td>
                                            @endif

                                            @if( $user->tel )
                                            <td  class="text-center"> {{ $user->tel }}</td>
                                            @else
                                            <td style="text-align : center;"> ---</td>
                                            @endif

                                            @if( $user->service )
                                            <td  class="text-center"> {{ $user->service }}</td>
                                            @else
                                            <td style="text-align : center;"> ---</td>
                                            @endif

                                            <td class="text-center">
                                                <a href="{{ url('dashboard/users/' . $user->id . '/edit') }}" title="Editer" class="btn btn-sm btn-success" style="border-radius: .5rem;">
                                                    <i class="fa fa-edit"></i>
                                                </a>

                                                <a id="{{ $user->id }}" onclick="setUserId(event)" href="{{ url('dashboard/users/' . $user->id . '/delete') }}" data-toggle="modal" data-target="#deleteModal" title="Supprimer" class="btn btn-sm btn-danger" style="border-radius: .5rem;">
                                                    <i class="fa fa-trash"></i>
                                                </a>
                                            </td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>


                        <a href="{{ url('dashboard/users/pdfUsers') }}" class="btn btn-sm btn-danger my-3" style="border-radius: .5rem;">
                            <i class="fa fa-print"></i>
                        Imprimer liste </a>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="deleteModal" tabindex="-1" role="dialog" aria-labelledby="deleteModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="deleteModalLabel">Suppression</h5>
                    <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                </div>
                <div class="modal-body">Voulez-vous vraiment supprimer cet utilsateur?</div>
                <div class="modal-footer">
                    <button class="btn btn-secondary" type="button" data-dismiss="modal">Non</button>

                    <a href="#" class="btn btn-primary" onclick="deleteUser()">
                        Oui
                    </a>
                </div>
            </div>

        </div>

    </div>
</div>
@endsection

@section('scripts')
<!--reseach--->
<script>
    var id;
    function setUserId(e) {
        if(e.composedPath){
            var path = (e.composedPath && e.composedPath());

            if(path[1].id != ''){
                this.id = path[1].id;
            }else{
                this.id = path[0].id;
            }
        }
    }

    function deleteUser() {
        window.location.replace('users/' + id + '/delete');
    }

   $(document).ready(function(){
       $("#myInput").on("keyup", function() {
           var value = $(this).val().toLowerCase();
           $("#myTable tr").filter(function() {
           $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
           });
       });
   });
</script>
<!--end-reseach--->
@endsection
