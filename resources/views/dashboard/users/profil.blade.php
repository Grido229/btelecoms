@extends('dashboard.layouts.app', ['title' => 'Dashboard'])

@section('content')
<div class="content container-fluid">

					<!-- Page Header -->
	<section class="comp-section">
		<nav aria-label="breadcrumb">
			<ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="{{ url('dashboard/home') }}">Home</a></li>
				<li class="breadcrumb-item"><a href="{{ url('dashboard/users') }}">Profil</a></li>
				<li class="breadcrumb-item active" aria-current="page">Modifier mon profil</li>
			</ol>
		</nav>
	</section>
	<!-- /Page Header -->

	<!-- Row -->
	<div class="row">
		<div class="col-sm-12">

			<!-- Custom Boostrap Validation -->
			<div class="card">
				<div class="card-header">
					<h4 class="card-title">Modifier mon profil</h4>
				</div>
				<div class="card-body">
                    <form method="POST" action="{{ url('dashboard/users/profil') }}">
                        @csrf

                        <div class="form-row">
                            <div class="col-md-6 mb-3">
                                <label for="fullname">Nom et Prénom(s)<span style="color:red">*</span></label>
                                <input type="text" class="form-control" id="fullname" name="fullname" value="{{ $user->fullname }}">
                            </div>
                        </div>

                        @empty($record)
                            <div class="form-row">
                                <div class="custom-control custom-checkbox mb-3 ml-2">
                                    <input type="checkbox" class="custom-control-input" id="change_password" name="change_password" onchange="changePassword()">
                                    <label class="custom-control-label" for="change_password">Modifier mon mot de passe</label>
                                </div>
                            </div>

                            <div id="pass"></div>
                        @endempty

                        @error('old_password')
                            <div>
                                <div class="form-row">
                                    <div class="col-md-6 mb-3">
                                        <label for="old_password">Mot de passe actuel<span style="color:red">*</span></label>
                                        <input type="password" class="form-control" id="old_password" name="old_password" required>
                                        @error('old_password')
                                            <span class="text-danger" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                        @enderror
                                    </div>
                                </div>

                                <div class="form-row">
                                    <div class="col-md-6 mb-3">
                                        <label for="password">Nouveau mot de passe<span style="color:red">*</span></label>
                                        <input type="password" class="form-control" id="password" name="password" required>
                                        @error('password')
                                            <span class="text-danger" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                        @enderror
                                    </div>
                                </div>

                                <div class="form-row">
                                    <div class="col-md-6 mb-3">
                                        <label for="password-confirm">Confirmer le nouveau mot de passe<span style="color:red">*</span></label>
                                        <input type="password" class="form-control" id="password-confirm" name="password_confirmation" required>
                                    </div>
                                </div>
                            </div>
                        @enderror

                        <div>
                            <button type="submit" class="btn btn-success">
                                Modifier
                            </button>
                        </div>
                    </form>
				</div>
            </div>
		</div>
	</div>
</div>
@endsection

@section('scripts')

<script type="text/javascript">

    var change_password = document.getElementById("change_password");

    function changePassword() {
        if(change_password) {
            $("#pass").append(`
                        <div>
                            <div class="form-row">
                                <div class="col-md-6 mb-3">
                                    <label for="old_password">Mot de passe actuel<span style="color:red">*</span></label>
                                    <input type="password" class="form-control" id="old_password" name="old_password" required>
                                    @error('old_password')
                                        <span class="text-danger" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                            </div>

                            <div class="form-row">
                                <div class="col-md-6 mb-3">
                                    <label for="password">Nouveau mot de passe<span style="color:red">*</span></label>
                                    <input type="password" class="form-control" id="password" name="password" required>
                                    @error('password')
                                        <span class="text-danger" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                            </div>

                            <div class="form-row">
                                <div class="col-md-6 mb-3">
                                    <label for="password-confirm">Confirmer le nouveau mot de passe<span style="color:red">*</span></label>
                                    <input type="password" class="form-control" id="password-confirm" name="password_confirmation" required>
                                </div>
                            </div>
                        </div>`);



        } else {
            $("#pass")[0].children[0].remove();
        }
        change_password = !change_password;
    }
</script>
@endsection


