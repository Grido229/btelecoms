<!DOCTYPE html>
<html>
<head>
  <title>clients</title>
  
  <!--link href="{{ asset('css/bootstrap/bootstrap.min.css') }}" rel="stylesheet"-->
    <!--link href="{{ asset('css/fontawesome-all.css') }}" rel="stylesheet" -->
</head>
<body>
<!--img src="{{ asset('assets/images/btcom.png') }}" height="" width="" -->
<p style="float:left;"><img src="<?php echo $base64?>" width="80" height="80"/></p>
<center><p style="line-height:90px;"><h2>BENIN TELECOMS INFRASTRUCTURES</h2></p></center>
</br>
<center><h1>Liste des utilisateurs</h1></center>
<div >
<table style="border-collapse: collapse" align="center" valign="middle">
                                <thead>
                                    <tr>
                                        <th style="border: 1px solid; padding:12px" width="15%">ID</th>
                                        <th style="border: 1px solid; padding:12px" width="15%">Nom & Prénom(s)</th>
                                        <th style="border: 1px solid; padding:12px" width="15%">Droit</th>
                                        
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach ($users as $user)
                                        <tr>
                                            <td style="border: 1px solid; padding:12px" width="15%"> {{ $user->id }}</td>
                                            <td style="border: 1px solid; padding:12px" width="15%"> {{ $user->fullname }} </td>
                                            <td style="border: 1px solid; padding:12px" width="15%"> {{ $user->role }}</td>
                                           
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                            </div>
</body>
</html>