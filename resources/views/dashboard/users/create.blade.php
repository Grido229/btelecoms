@extends('dashboard.layouts.app', ['title' => 'Dashboard'])

@section('content')
<div class="content container-fluid">

    <!-- Page Header -->
	<section class="comp-section">
		<nav aria-label="breadcrumb">
			<ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="{{ url('dashboard/home') }}">Home</a></li>
                <li class="breadcrumb-item"><a href="{{ url('dashboard/users') }}">Utilisateurs</a></li>
				<li class="breadcrumb-item active" aria-current="page">Ajouter un utilisateur</li>
			</ol>
		</nav>
	</section>
	<!-- /Page Header -->

	<!-- Row -->
	<div class="row">
		<div class="col-sm-12">

			<!-- Custom Boostrap Validation -->
			<div class="card">
				<div class="card-header">
					<h4 class="card-title">Ajouter un utilisateur</h4>
				</div>
				<div class="card-body">
					<form method="POST" action="{{ url('dashboard/users/create') }}">
                        @csrf

                        <div class="form-row">
                            <div class="col-md-6 mb-3">
                                <label for="fullname">Nom <span style="color:red">*</span></label>
                                <input type="text" class="form-control" id="fullname" name="lastName" required value="{{ old('lastName') }}">
                                @error('fullname')
                                    <span class="text-danger" role="alert">
                                        <strong>Ce champ est obligatoire.</strong> 
                                    </span>
                                @enderror
                            </div>

                            
                            <div class="col-md-6 mb-3">
                                <label for="fullname">Prénom(s) <span style="color:red">*</span></label>
                                <input type="text" class="form-control" id="fullname" name="firstName" required value="{{ old('firstName') }}">
                                @error('fullname')
                                    <span class="text-danger" role="alert">
                                        <strong>Ce champ est obligatoire.</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="col-lg-12 mb-3">
                                <label for="role_id">Droit<span style="color:red">*</span></label>
                                <select name="role_id" class="form-control" id="role_id" required>
                                    <option disabled selected>Sélectionnez un droit</option>
                                    @foreach ($roles as $role)
                                        <option value="{{$role->id}}">{{$role->name}}</option>
                                    @endforeach
                                </select>
                                @error('role_id')
                                    <span class="text-danger" role="alert">
                                        <strong>Vous devez sélectionner un rôle.</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>


                        <div class="form-row">
                            <div class="col-md-6 mb-3">
                                <label for="adresse">Adresse<span style="color:red">*</span></label>
                                <input type="text" class="form-control" id="adresse" name="adresse" value="{{ old('adresse') }}">
                                @error('adresse')
                                    <span class="text-danger" role="alert">
                                        <strong>Ce champ est obligatoire.</strong>
                                    </span>
                                @enderror
                            </div>

                            <div class="col-md-6 mb-3">
                                <label for="email">Email<span style="color:red">*</span></label>
                                <input type="email" class="form-control" id="email" name="email" value="{{ old('email') }}">
                                @error('email')
                                    <span class="text-danger" role="alert">
                                        <strong>Ce champ est obligatoire.</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-row">
                            <div class="col-md-6 mb-3">
                                <label for="tel">Telephone<span style="color:red">*</span></label>
                                <input type="tel" class="form-control" id="tel" name="tel" value="{{ old('tel') }}">
                                @error('tel')
                                    <span class="text-danger" role="alert">
                                        <strong>Ce champ est obligatoire.</strong>
                                    </span>
                                @enderror
                            </div>

                            <div class="col-md-6 mb-3">
                                <label for="tel">service<span style="color:red">*</span></label>
                                <input type="service" class="form-control" id="service" name="service" value="{{ old('service') }}">
                                @error('service')
                                    <span class="text-danger" role="alert">
                                        <strong>Ce champ est obligatoire.</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div>
                            <a href="{{ url('dashboard/users') }}" style="margin-right: 10px" class="btn btn-primary">Annuler</a>
                            <button type="submit" class="btn btn-success">
                                Ajouter
                            </button>
                        </div>
					</form>
				</div>
            </div>
		</div>
	</div>
</div>

@endsection
