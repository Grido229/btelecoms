@extends('dashboard.layouts.app', ['title' => 'Dashboard'])

@section('content')
<div class="content container-fluid">
	<section class="comp-section">
		<nav aria-label="breadcrumb">
            <ol class="breadcrumb">
				<li class="breadcrumb-item"><a href="{{ url('dashboard/home') }}">Home</a></li>
				<li class="breadcrumb-item active" aria-current="page">Etats</li>
			</ol>
		</nav>
	</section>

	<div class="row">
		<div class="col-sm-12">
			<div class="card">
				<div class="card-header">
                    <h4 class="card-title">Liste des Etats des services aux clients </h4>
                    {{--<a href="#" title="Imprimer la liste des pv" class="btn btn-info mt-4 float-right" style="margin-top: -2rem !important;">
                        <i class="fa fa-print"></i>
                        Imprimer
                    </a> --}}
                </div>
				<div class="card-body">

                    <div class="container">
                        <div class="row my-4">
                            <input class="form-control" id="myInput" type="text" placeholder="Recherche..">
                        </div>
                    </div>

					<div class="table-responsive">
                    <table class="datatable table table-stripped">


                            <thead>
                                <tr>
                                    <th>Ref PV</th>
                                    <th>Nom Service</th>
                                    <th>Type Service</th>

                                    <th>Date Activation</th>
                                    <th>Frais</th>
                                    <th>Reference PV</th>
                                    <th>Redevance</th>
                                    <th>Frequence Redevance</th>
                                    <th>Date premiere facturation</th>
                                    <th>Statut</th>
                                    <th>Date Statut</th>
                                    <th>Raison</th>
                                    <th>Reference Contrat</th>
                                    <th>Reference Demande</th>

                                    <th>Nom Operateur</th>

                                    {{--<th style="text-align: center;">Action</th>--}}
                                </tr>
                            </thead>
                            <tbody id="myTable">

                            @if($etats)
                                @foreach($etats as $etat)
                                    <tr>
                                        @if($etat->ref_pv != NULL)
                                        <td> {{$etat->ref_pv}} </td>
                                        @else
                                            <td style="text-align:center;"> ... </td>
                                        @endif

                                        @if($etat->nom_service  != NULL)
                                        <td> {{$etat->nom_service }} </td>
                                        @else
                                            <td style="text-align:center;"> ... </td>
                                        @endif

                                        @if($etat->type_service  != NULL)
                                        <td> {{$etat->type_service }} </td>
                                        @else
                                            <td style="text-align:center;"> ... </td>
                                        @endif

                                        @if($etat->date_activation != NULL)
                                        <td>{{$etat->date_activation}}</td>
                                        @else
                                        <td style="text-align:center;"> ... </td>
                                        @endif

                                        @if($etat->frais != NULL)
                                        <td>{{$etat->frais}}</td>
                                        @else
                                        <td style="text-align:center;"> ... </td>
                                        @endif

                                        @if($etat->reference_pv != NULL)
                                        <td>{{$etat->reference_pv}}</td>
                                        @else
                                        <td style="text-align:center;"> ... </td>
                                        @endif

                                        @if($etat->redevance != NULL)
                                        <td>{{$etat->redevance}}</td>
                                        @else
                                        <td style="text-align:center;"> ... </td>
                                        @endif

                                        @if($etat->frequence_redevance != NULL)
                                        <td>{{$etat->frequence_redevance}}</td>
                                        @else
                                        <td style="text-align:center;"> ... </td>
                                        @endif

                                        @if($etat->date_premiere_facturation != NULL)
                                        <td>{{$etat->date_premiere_facturation}}</td>
                                        @else
                                        <td style="text-align:center;"> ... </td>
                                        @endif

                                        @if($etat->statut != NULL)
                                        <td>{{$etat->statut}}</td>
                                        @else
                                        <td style="text-align:center;"> ... </td>
                                        @endif

                                        @if($etat->sate_statut != NULL)
                                        <td>{{$etat->sate_statut}}</td>
                                        @else
                                        <td style="text-align:center;"> ... </td>
                                        @endif

                                        @if($etat->raison != NULL)
                                        <td>{{$etat->raison}}</td>
                                        @else
                                        <td style="text-align:center;"> ... </td>
                                        @endif

                                        @if($etat->reference_contrat != NULL)
                                        <td>{{$etat->reference_contrat}}</td>
                                        @else
                                        <td style="text-align:center;"> ... </td>
                                        @endif

                                        @if($etat->reference_demande != NULL)
                                        <td>{{$etat->reference_demande}}</td>
                                        @else
                                        <td style="text-align:center;"> ... </td>
                                        @endif

                                        @if($etat->nom_operateur != NULL)
                                        <td> {{$etat->nom_operateur}} </td>
                                        @else
                                        <td style="text-align:center;"> ... </td>
                                        @endif
                                        {{--<td>
                                        <a href="{{route('list.etat.detail' , $etat->id )}}" title="Afficher les details" class="btn btn-sm btn-info px-2" style="border-radius: .5rem;">
                                                        <i class="fa fa-eye"></i>
                                        </a>
                                        </td> --}}
                                    </tr>
                                @endforeach
                            @endif

                            </tbody>
                        </table>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection


@section('scripts')
    <script>
        $(document).ready(function(){
       $("#myInput").on("keyup", function() {
           var value = $(this).val().toLowerCase();
           $("#myTable tr").filter(function() {
           $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
           });
       });
   });
    </script>
@endsection
