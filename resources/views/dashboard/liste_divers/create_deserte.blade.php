@extends('dashboard.layouts.app', ['title' => 'Dashboard'])

@section('content')
<div class="content container-fluid">

					<!-- Page Header -->
	<section class="comp-section">
		<nav aria-label="breadcrumb">
			<ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="{{ url('dashboard/home') }}">Home</a></li>
                <li class="breadcrumb-item"><a href="{{ url('dashboard/list/deserte') }}">Dessertes</a></li>
				<li class="breadcrumb-item active" aria-current="page">Enrégistrer une facture</li>
			</ol>
		</nav>
	</section>
	<!-- /Page Header -->

	<!-- Row -->
	<div class="row">
		<div class="col-sm-12">

			<!-- Custom Boostrap Validation -->
			<div class="card">
				<div class="card-header">
					<h4 class="card-title">Enrégistrer la facture</h4>
				</div>
                <div class="card-body">
					<form method="POST" action="{{ route('store.deserte') }}">
                        @csrf

                    <marquee style="font-size:30px; color:red;"> Renseigner pour les champs Fourniture et Prestation juste les prix unitaires</marquee>

                    <div class="form-row">

                            <div class="col-md-12 mb-3">
                                <label for="intitule">Intitule<span style="color:red"></span></label>
                                <input type="text" class="form-control @error('intitule') is-invalid @enderror" id="intitule" name="intitule" value="{{ old('intitule') }}">
                                @error('intitule')
                                    <span class="text-danger" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>

                    </div>


                    <div class="form-row">

                            <div class="col-md-6 mb-3">
                                <label for="operateur"> Operateur <span style="color:red"></span></label>
                                <select style="font-size : 18px;" class="form-control @error('operateur') is-invalid @enderror" name="operateur" value="{{ old('operateur') }}">
                                    <option disabled selected>Sélectionnez une option</option>
                                    @foreach($clients as $client)
                                        <option value="{{ $client->id }}">{{ $client->enterprise_name }}</option>
                                    @endforeach
                                </select>
                                @error('operateur')
                                    <span class="text-danger" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>


                            <div class="col-md-6 mb-3">
                                <label for="type_devis" name="type_devis">Type de devis<span style="color:red"></span></label>
                                <select style="font-size : 18px;" class="form-control @error('type_devis') is-invalid @enderror" name="type_devis"  id="type_devis" value="{{ old('type_devis') }}">
                                    <option disabled selected>Sélectionnez une option</option>
                                    <option value="DESSERTE">DESSERTE</option>
                                    <option value="DEGAT">DEGAT</option>
                                    <option value="DEPLACEMENT">DEPLACEMENT</option>
                                </select>
                                @error('type_devis')
                                    <span class="text-danger" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>

                    </div>


                    <div class="form-row">

                        <div class="col-md-6 mb-3">
                            <label for="ref_fact"> Reference Facture <span style="color:red"></span></label>
                            <input type="text" class="form-control @error('ref_fact') is-invalid @enderror" id="ref_fact" name="ref_fact" value="{{ old('ref_fact') }}">
                            @error('ref_fact')
                                <span class="text-danger" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>

                    </div>
                    {{-- ================================== TRAVAUX DE GENIE CIVIL ============================= --}}
                    <div class="card-header">
                        <div class="card-title" style="text-align: center; "> <strong><h1>TRAVAUX DE GENIE CIVIL</h1></strong> </div>
                    </div> <br>


                    <div> <strong>  <h4> POSE DE CONDUITE ALLEGEE </h4> </strong> </div>
                    <div> <strong> 1Q45 </strong> </div>
                    <div class="form-row">

                            <div class="col-md-6 mb-3">
                                <label for="pU_fourniture_1">Fourniture<span style="color:red"></span></label>
                                <input type="number" class="form-control @error('pU_fourniture_1') is-invalid @enderror" id="pU_fourniture_1" name="pU_fourniture_1" value="{{ old('pU_fourniture_1') }}" >
                                @error('pU_fourniture_1')
                                    <span class="text-danger" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror

                            </div>


                            <div class="col-md-6 mb-3">
                                <label for="pU_prestation_1">Prestation<span style="color:red"></span></label>
                                <input type="number" class="form-control @error('pU_prestation_1') is-invalid @enderror" id="pU_prestation_1" name="pU_prestation_1" value="{{ old('pU_prestation_1') }}">
                                @error('pU_prestation_1')
                                    <span class="text-danger" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror

                            </div>

                    </div>
                    <div class="form-row">

                            <div class="col-md-6 mb-3">
                                <label for="quantite_1">Quantite<span style="color:red"></span></label>
                                <input type="number" class="form-control @error('quantite_1') is-invalid @enderror" id="quantite_1" name="quantite_1" value="{{ old('quantite_1') }}">
                                @error('quantite_1')
                                    <span class="text-danger" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror

                            </div>

                    </div>

                    <div> <strong> 2Q45 </strong> </div>
                    <div class="form-row">

                            <div class="col-md-6 mb-3">
                                <label for="pU_fourniture_2">Fourniture<span style="color:red"></span></label>
                                <input type="number" class="form-control @error('pU_fourniture_2') is-invalid @enderror" id="pU_fourniture_2" name="pU_fourniture_2" value="{{ old('pU_fourniture_2') }}">
                                @error('pU_fourniture_2')
                                    <span class="text-danger" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror

                            </div>


                            <div class="col-md-6 mb-3">
                                <label for="pU_prestation_2">Prestation<span style="color:red"></span></label>
                                <input type="number" class="form-control @error('pU_prestation_2') is-invalid @enderror" id="pU_prestation_2" name="pU_prestation_2" value="{{ old('pU_prestation_2') }}">
                                @error('pU_prestation_2')
                                    <span class="text-danger" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror

                            </div>

                    </div>

                    <div class="form-row">

                            <div class="col-md-6 mb-3">
                                <label for="quantite_2">Quantite<span style="color:red"></span></label>
                                <input type="number" class="form-control @error('quantite_2') is-invalid @enderror" id="quantite_2" name="quantite_2"  value="{{ old('quantite_2') }}">
                                @error('quantite_2')
                                    <span class="text-danger" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror

                            </div>

                    </div>

                    <div> <strong><h4>REPARATION DE CONDUITE ALLEGEE</h4></strong> </div>
                    <div class="form-row">

                        <div class="col-md-6 mb-3">
                            <label for="pU_fourniture_3">Fourniture<span style="color:red"></span></label>
                            <input type="number" class="form-control @error('pU_fourniture_3') is-invalid @enderror" id="pU_fourniture_3" name="pU_fourniture_3" value="{{ old('pU_fourniture_2') }}" >
                            @error('pU_fourniture_3')
                                    <span class="text-danger" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror

                        </div>


                        <div class="col-md-6 mb-3">
                            <label for="pU_prestation_3">Prestation<span style="color:red"></span></label>
                            <input type="number" class="form-control @error('pU_prestation_3') is-invalid @enderror" id="pU_prestation_3" name="pU_prestation_3" value="{{ old('pU_prestation_3') }}">
                            @error('pU_prestation_3')
                                    <span class="text-danger" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror

                        </div>

                    </div>

                    <div class="form-row">

                            <div class="col-md-6 mb-3">
                                <label for="quantite_3">Quantite<span style="color:red"></span></label>
                                <input type="number" class="form-control @error('quantite_3') is-invalid @enderror" id="quantite_3" name="quantite_3" value="{{ old('quantite_3') }}">
                                @error('quantite_3')
                                    <span class="text-danger" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror

                            </div>

                    </div>

                    <div> <strong><h4>DEMOLITION ET REPRISE D'ENTREE EN CHAMBRE</h4></strong> </div>
                    <div><strong>Type L5T250</strong></div>
                    <div class="form-row">

                        <div class="col-md-6 mb-3">
                            <label for="pU_fourniture_4">Fourniture<span style="color:red"></span></label>
                            <input type="number" class="form-control @error('pU_fourniture_4') is-invalid @enderror" id="pU_fourniture_4" name="pU_fourniture_4" value="{{ old('pU_fourniture_4') }}">
                            @error('pU_fourniture_4')
                                    <span class="text-danger" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror

                        </div>


                        <div class="col-md-6 mb-3">
                            <label for="pU_prestation_4">Prestation<span style="color:red"></span></label>
                            <input type="number" class="form-control @error('pU_prestation_4') is-invalid @enderror" id="pU_prestation_4" name="pU_prestation_4" value="{{ old('pU_prestation_4') }}">
                            @error('pU_prestation_4')
                                    <span class="text-danger" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror

                        </div>

                    </div>
                    <div class="form-row">

                            <div class="col-md-6 mb-3">
                                <label for="quantite_4">Quantite<span style="color:red"></span></label>
                                <input type="number" class="form-control @error('quantite_4') is-invalid @enderror" id="quantite_4" name="quantite_4" value="{{ old('quantite_4') }}">
                                @error('quantite_4')
                                    <span class="text-danger" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror

                            </div>

                    </div>
                    <div> <strong><h4>CONSTRUCTION DE CHAMBRE </h4></strong> </div>
                    <div><strong>Type LT3 verrouillable</strong></div>
                    <div class="form-row">

                        <div class="col-md-6 mb-3">
                            <label for="pU_fourniture_5">Fourniture<span style="color:red"></span></label>
                            <input type="number" class="form-control @error('pU_fourniture_5') is-invalid @enderror" id="pU_fourniture_5" name="pU_fourniture_5" value="{{ old('pU_fourniture_5') }}">
                            @error('pU_fourniture_5')
                                    <span class="text-danger" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror

                        </div>


                        <div class="col-md-6 mb-3">
                            <label for="pU_prestation_5">Prestation<span style="color:red"></span></label>
                            <input type="number" class="form-control @error('pU_prestation_5') is-invalid @enderror" id="pU_prestation_5" name="pU_prestation_5" value="{{ old('pU_prestation_5') }}">
                            @error('pU_prestation_5')
                                    <span class="text-danger" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror

                        </div>

                    </div>
                    <div class="form-row">

                            <div class="col-md-6 mb-3">
                                <label for="quantite_5">Quantite<span style="color:red"></span></label>
                                <input type="number" class="form-control @error('quantite_5') is-invalid @enderror" id="quantite_5" name="quantite_5" value="{{ old('quantite_5') }}">
                                @error('quantite_5')
                                    <span class="text-danger" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror

                            </div>

                    </div>


                    <div> <strong><h4> REFECTION DE SURFACE </h4></strong> </div>
                    <div><strong>Pavé</strong></div>
                    <div class="form-row">

                        <div class="col-md-6 mb-3">
                            <label for="pU_fourniture_6">Fourniture<span style="color:red"></span></label>
                            <input type="number" class="form-control @error('pU_fourniture_6') is-invalid @enderror" id="pU_fourniture_6" name="pU_fourniture_6" value="{{ old('pU_fourniture_6') }}">
                            @error('pU_fourniture_6')
                                    <span class="text-danger" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror

                        </div>


                        <div class="col-md-6 mb-3">
                            <label for="pU_prestation_6">Prestation<span style="color:red"></span></label>
                            <input type="number" class="form-control @error('pU_prestation_6') is-invalid @enderror" id="pU_prestation_6" name="pU_prestation_6" value="{{ old('pU_prestation_6') }}">
                            @error('pU_prestation_6')
                                    <span class="text-danger" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                            @enderror

                        </div>

                    </div>
                    <div class="form-row">

                            <div class="col-md-6 mb-3">
                                <label for="quantite_6">Quantite<span style="color:red"></span></label>
                                <input type="number" class="form-control @error('quantite_6') is-invalid @enderror" id="quantite_6" name="quantite_6" value="{{ old('quantite_6') }}">
                                @error('quantite_6')
                                    <span class="text-danger" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror

                            </div>

                    </div>
                    {{-- ==========================================TRAVAUX DE CABLAGE  ======================== --}}

                    <div class="card-header">
                        <div class="card-title" style="text-align: center; "> <strong> <h1>TRAVAUX DE CABLAGE</h1></strong> </div>
                    </div> <br>


                    <div> <strong> <h4> AIGUILLAGE DE CONDUITE ET TIRAGE DE CABLE EN CONDUITE </h4></strong> </div>
                    <div> <strong> Cable 06 à, structure microgaine diélect </strong> </div>
                    <div class="form-row">

                            <div class="col-md-6 mb-3">
                                <label for="pU_fourniture_7">Fourniture<span style="color:red"></span></label>
                                <input type="number" class="form-control @error('pU_fourniture_7') is-invalid @enderror" id="pU_fourniture_7" name="pU_fourniture_7" value="{{ old('pU_fourniture_7') }}" >
                                @error('pU_fourniture_7')
                                    <span class="text-danger" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror

                            </div>


                            <div class="col-md-6 mb-3">
                                <label for="pU_prestation_7">Prestation<span style="color:red"></span></label>
                                <input type="number" class="form-control @error('pU_prestation_7') is-invalid @enderror" id="pU_prestation_7" name="pU_prestation_7" value="{{ old('pU_prestation_7') }}">
                                @error('pU_prestation_7')
                                    <span class="text-danger" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror

                            </div>

                    </div>
                    <div class="form-row">

                            <div class="col-md-6 mb-3">
                                <label for="quantite_7">Quantite<span style="color:red"></span></label>
                                <input type="number" class="form-control @error('quantite_7') is-invalid @enderror" id="quantite_7" name="quantite_7" value="{{ old('quantite_7') }}">
                                @error('quantite_7')
                                    <span class="text-danger" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror

                            </div>

                    </div>

                    <div> <strong> Gainage et clouage </strong> </div>
                    <div class="form-row">

                            <div class="col-md-6 mb-3">
                                <label for="pU_fourniture_8">Fourniture<span style="color:red"></span></label>
                                <input type="number" class="form-control @error('pU_fourniture_8') is-invalid @enderror" id="pU_fourniture_8" name="pU_fourniture_8" value="{{ old('pU_fourniture_8') }}">
                                @error('pU_fourniture_8')
                                    <span class="text-danger" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror

                            </div>


                            <div class="col-md-6 mb-3">
                                <label for="pU_prestation_8">Prestation<span style="color:red"></span></label>
                                <input type="number" class="form-control @error('pU_prestation_8') is-invalid @enderror" id="pU_prestation_8" name="pU_prestation_8" value="{{ old('pU_prestation_8') }}">
                                @error('pU_prestation_8')
                                    <span class="text-danger" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror

                            </div>

                    </div>

                    <div class="form-row">

                            <div class="col-md-6 mb-3">
                                <label for="quantite_8">Quantite<span style="color:red"></span></label>
                                <input type="number" class="form-control @error('quantite_8') is-invalid @enderror" id="quantite_8" name="quantite_8"  value="{{ old('quantite_8') }}">
                                @error('quantite_8')
                                    <span class="text-danger" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror

                            </div>

                    </div>


                    <div> <strong>  <h4>RACORDEMENT DE CABLE FO </h4></strong> </div>
                    <div> <strong> Tiroir optique de 6 FO équipé de pigtails SC-PC </strong> </div>
                    <div class="form-row">

                            <div class="col-md-6 mb-3">
                                <label for="pU_fourniture_9">Fourniture<span style="color:red"></span></label>
                                <input type="number" class="form-control @error('pU_fourniture_9') is-invalid @enderror" id="pU_fourniture_9" name="pU_fourniture_9" value="{{ old('pU_fourniture_9') }}" >
                                @error('pU_fourniture_9')
                                    <span class="text-danger" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror

                            </div>


                            <div class="col-md-6 mb-3">
                                <label for="pU_prestation_9">Prestation<span style="color:red"></span></label>
                                <input type="number" class="form-control @error('pU_prestation_9') is-invalid @enderror" id="pU_prestation_9" name="pU_prestation_9" value="{{ old('pU_prestation_9') }}">
                                @error('pU_prestation_9')
                                    <span class="text-danger" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror

                            </div>

                    </div>
                    <div class="form-row">

                            <div class="col-md-6 mb-3">
                                <label for="quantite_9">Quantite<span style="color:red"></span></label>
                                <input type="number" class="form-control @error('quantite_9') is-invalid @enderror" id="quantite_9" name="quantite_9" value="{{ old('quantite_9') }}">
                                @error('quantite_9')
                                    <span class="text-danger" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror

                            </div>

                    </div>

                    <div> <strong> Joint 96 FO </strong> </div>
                    <div class="form-row">

                            <div class="col-md-6 mb-3">
                                <label for="pU_fourniture_10">Fourniture<span style="color:red"></span></label>
                                <input type="number" class="form-control @error('pU_fourniture_10') is-invalid @enderror" id="pU_fourniture_10" name="pU_fourniture_10" value="{{ old('pU_fourniture_10') }}">
                                @error('pU_fourniture_10')
                                    <span class="text-danger" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror

                            </div>


                            <div class="col-md-6 mb-3">
                                <label for="pU_prestation_10">Prestation<span style="color:red"></span></label>
                                <input type="number" class="form-control @error('pU_prestation_10') is-invalid @enderror" id="pU_prestation_10" name="pU_prestation_10" value="{{ old('pU_prestation_10') }}">
                                @error('pU_prestation_10')
                                    <span class="text-danger" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror

                            </div>

                    </div>

                    <div class="form-row">

                            <div class="col-md-6 mb-3">
                                <label for="quantite_10">Quantite<span style="color:red"></span></label>
                                <input type="number" class="form-control @error('quantite_10') is-invalid @enderror" id="quantite_10" name="quantite_10"  value="{{ old('quantite_10') }}">
                                @error('quantite_10')
                                    <span class="text-danger" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror

                            </div>

                    </div>

                    {{-- =======================================SIGNATAIRE========================== --}}

                    <div class="form-row">
                            <div class="col-md-12 mb-3">
                                <label for="signataire_id">Nom du signataire<span style="color:red"></span></label>
                                <select style="font-size : 18px;" class="form-control" name="signataire_id" required>
                                     <option disabled selected>Sélectionnez le nom du signataire</option>
                                     @foreach ($signataires as $signataire)
                                            <option value="{{$signataire->id}}">{{$signataire->fullname}}</option>
                                     @endforeach
                                </select>
                            </div>
                    </div>

                    {{-- ====================================== VALIDATION ========================= --}}

                        <div>
                             <a href="{{ url('dashboard/list/deserte') }}" style="margin-right: 10px" class="btn btn-primary">Annuler</a>
                            <button type="submit" class="btn btn-success">
                                Enrégistrer
                            </button>
                        </div>
					</form>
				</div>
			</div>
          </div>

		</div>
	</div>
	<!-- /Row -->

				</div>
@endsection


