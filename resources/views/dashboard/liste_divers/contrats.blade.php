@extends('dashboard.layouts.app', ['title' => 'Dashboard'])

@section('content')
<div class="content container-fluid">
	<section class="comp-section">
		<nav aria-label="breadcrumb">
            <ol class="breadcrumb">
				<li class="breadcrumb-item"><a href="{{ url('dashboard/home') }}">Home</a></li>
				<li class="breadcrumb-item active" aria-current="page">Desserte</li>
			</ol>
		</nav>
	</section>

	<div class="row">
		<div class="col-sm-12">
			<div class="card">
				<div class="card-header">
                    <h4 class="card-title">Liste des contrats</h4>
                    <a href="{{ route('impression.contrats') }}" title="Imprimer la liste des pv" class="btn btn-info mt-4 float-right" style="margin-top: -2rem !important;">
                        <i class="fa fa-print"></i>
                        Imprimer
                    </a>
                </div>
				<div class="card-body">

                    <div class="container">
                        <div class="row my-4">
                            <input class="form-control" id="myInput" type="text" placeholder="Recherche..">
                        </div>
                    </div>

					<div class="table-responsive">
                    <table class="datatable table table-stripped">

                            <thead>
                                <tr>
                                    <th>Ref Contrat</th>
                                    <th>Date Contrat</th>
                                    <th>Nom operateur</th>
                                    <th>Objet Contrat</th>
                                    <th>Duree</th>
                                    <th>Nature contrat</th>
                                </tr>
                            </thead>
                            <tbody id="myTable">
                                @if($contrats != null)
                                    @foreach($contrats as $contrat)

                                    <tr>
                                        <td> {{$contrat->num_contrat}}</td>
                                        <td>{{$contrat->created_at}}</td>
                                        <td>{{$contrat->bon->client->enterprise_name}}</td>
                                        <td>{{$contrat->object_contrat}}</td>
                                        <td>{{$contrat->duree_contrat}}</td>
                                        <td>{{$contrat->nature_contrat}}</td>
                                    </tr>
                                    @endforeach
                                @endif

                            </tbody>
                        </table>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection

@section('scripts')
    <script>
        $(document).ready(function(){
       $("#myInput").on("keyup", function() {
           var value = $(this).val().toLowerCase();
           $("#myTable tr").filter(function() {
           $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
           });
       });
   });
    </script>
@endsection
