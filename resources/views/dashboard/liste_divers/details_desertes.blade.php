@extends('dashboard.layouts.app', ['title' => 'Dashboard'])

@section('content')
<div class="content container">

    <!-- Page Header -->
	<section class="comp-section">
		<nav aria-label="breadcrumb">
			<ol class="breadcrumb">
				<li class="breadcrumb-item"><a href="{{ url('dashboard/home') }}">Home</a></li>
				<li class="breadcrumb-item"><a href="{{ url('dashboard/list/deserte') }}">Deserte</a></li>
				<li class="breadcrumb-item active" aria-current="page">Détails deserte</li>
			</ol>
		</nav>
	</section>
    <!-- /Page Header -->
    <div class="row">
        <div class="col-lg-12">
            <div class="card">
                <div class="card-header">
                    <h4 class="card-title">Informations la deserte </h4>
                    {{-- <a href="#" class="btn btn-primary mt-4 float-right" style="margin-top: -2rem !important;">Generer</a> --}}

                </div>
                <div class="card-body">
                    <div class="row">
                        <p class="col-sm-3 text-muted mb-0 mb-sm-3">Ref Devis</p>
                        <p class="col-sm-9 text-right font-weight-bold">{{ $deserte->ref_devis }}</p>
                    </div>

                    <div class="row">
                        <p class="col-sm-3 text-muted mb-0 mb-sm-3">Intitule</p>
                        <p class="col-sm-9 text-right font-weight-bold"> {{$deserte->intitule}}</p>
                    </div>

                    <div class="row">
                        <p class="col-sm-3 text-muted mb-0 mb-sm-3">Type devis</p>
                        <p class="col-sm-9 text-right font-weight-bold"> {{$deserte->type_devis}}</p>
                    </div>

                    <div class="row">
                        <p class="col-sm-3 text-muted mb-0 mb-sm-3">Ref facture</p>
                        <p class="col-sm-9 text-right font-weight-bold"> {{$deserte->ref_facture}}</p>
                    </div>


                    <div class="row">
                        <p class="col-sm-3 text-muted mb-0 mb-sm-3">Date Devis</p>
                        <p class="col-sm-9 text-right font-weight-bold"> {{$deserte->created_at}}</p>
                    </div>

                    <div class="row">
                        <p class="col-sm-3 text-muted mb-0 mb-sm-3">Montant Facturé</p>
                        <p class="col-sm-9 text-right font-weight-bold"> {{$deserte->montant_facture}} F CFA</p>
                    </div>



                </div>
            </div>
        </div>
    </div>
</div>

@endsection


