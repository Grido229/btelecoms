

<!DOCTYPE html>
<html>
<head>
	<style type="text/css">
		.bloc{

			width:27cm;
		}

		h1{
			text-align: center;
		}

		td , th{
        border : solid 1px black;
        font-size: 15px;
        }

        table{
        border-collapse: collapse;
        width: 27cm;
        }

        th{
            background-color : rgba(0,0,0,0.1);
        }

	</style>
	<title>contrats_table</title>
</head>
<body>
	<div class="bloc">

		<div>
            <p style="float:left;"><img src="<?php echo $base64?>" width="100" height="100"/></p>

			<br><br><br><br><h1>Liste des contrats des operateurs</h3>
		</div>
		<table>
			<tr>
				<th>Reference PV</th>
				<th> Objet de demande </th>
				<th> Type d'offre </th>
				<th> Type Service </th>
				<th> Date d'activation </th>
                <th>Frais</th>
                <th>Redevance</th>
                <th>Frequence Redevance</th>
                <th>Statut</th>
                <th>Reference contrat</th>
                <th>Reference Demande</th>
                <th>Nom operateur</th>
			</tr>
            @foreach($factures as $facture)

            <tr>
                <td> {{$facture->pv->num_commande_protocoles}} </td>
                <td> {{$facture->pv->bon->object}} </td>

                @if($facture->pv->bon->sfs_id == NULL)
                <td> {{$facture->pv->bon->ssfs->libelle}} </td>
                @else
                <td> {{$facture->pv->bon->sfs->libelle}} </td>
                @endif

                <td> {{$facture->pv->bon->sfs->fs->libelle}} </td>
                <td>  {{ $facture->pv->created_at }}  </td>


                @if($facture->frais_acces_id != null)
                <td> {{ $facture->frais_acces->frais .' F CFA'}} </td>
                @else
                <td> 0 F CFA</td>
                @endif

                <td> {{ $facture->montant_TTC .' F CFA'}} </td>
                <td>  {{ $facture->type_facture}} </td>
                <td>  {{ $facture->pv->status }} </td>
                @if($facture->pv->bon->contrat)
                <td> {{ $facture->pv->bon->contrat->num_contrat }} </td>
                @else
                <td> Pas de contrat </td>
                @endif
                <td> {{ $facture->pv->bon->num_bon }}  </td>
                <td> {{ $facture->pv->bon->client->enterprise_name}} </td>
                </tr>

            @endforeach

		</table>

	</div>
</body>
</html>
