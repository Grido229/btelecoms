@extends('dashboard.layouts.app', ['title' => 'Dashboard'])

@section('content')
<div class="content container-fluid">
	<section class="comp-section">
		<nav aria-label="breadcrumb">
            <ol class="breadcrumb">
				<li class="breadcrumb-item"><a href="{{ url('dashboard/home') }}">Home</a></li>
				<li class="breadcrumb-item active" aria-current="page">services operateurs</li>
			</ol>
		</nav>
	</section>

	<div class="row">
		<div class="col-sm-12">
			<div class="card">
				<div class="card-header">
                    <h4 class="card-title">Liste des services aux Operateurs</h4>
                    <a href="{{ route('impression.services') }}" title="Imprimer la liste des pv" class="btn btn-info mt-4 float-right" style="margin-top: -2rem !important;">
                        <i class="fa fa-print"></i>
                        Imprimer
                    </a>
                </div>
				<div class="card-body">

                    <div class="container">
                        <div class="row my-4">
                            <input class="form-control" id="myInput" type="text" placeholder="Recherche..">
                        </div>
                    </div>

					<div class="table-responsive">
                    <table class="datatable table table-stripped">


                            <thead>
                                <tr>
                                    <th>Ref PV</th>
                                    <th>Ref Demande</th>
                                    <th>Reference contrat</th>
                                    <th>Objet de demande</th>
                                    <th>Type d'offre</th>
                                    <th>Date activation</th>
                                    <th>Frais</th>
                                    <th>Redevance</th>
                                    <th>Frequence Redevance</th>
                                    {{--<th>Date premiere Redevance</th>
                                    <th>Date statut</th>--}}


                                    <th>Nom Operateur</th>
                                    {{--<th>Action</th>--}}
                                </tr>
                            </thead>
                            <tbody id="myTable">
                                    @if($factures != null )
                                        @foreach($factures as $facture)
                                            @if($facture->type_facture != null)
                                                <tr>
                                                    <td> {{$facture->pv->num_commande_protocoles }}</td>
                                                    <td>{{ $facture->pv->bon->num_bon}}</td>
                                                    @if( $facture->pv->bon->contrat)
                                                    <td>{{ $facture->pv->bon->contrat->num_contrat}}</td>
                                                    @else
                                                    <td> Pas de contrats</td>
                                                    @endif
                                                    <td>{{$facture->pv->bon->object}}</td>

                                                    @if($facture->pv->sfs_id != NULL)
                                                    <td>{{$facture->pv->bon->ssfs->libelle}}</td>
                                                    @else
                                                    <td>{{$facture->pv->bon->sfs->libelle}}</td>
                                                    @endif

                                                    <td>{{ $facture->pv->created_at}}</td>

                                                    @if($facture->frais_acces_id != null)
                                                    <td>{{ $facture->frais_acces->frais .'F FCA'}}</td>
                                                    @else
                                                    <td>0 F CFA</td>
                                                    @endif

                                                    <td>{{ $facture->montant_TTC.' F CFA'}}</td>
                                                    <td>{{ $facture->type_facture}}</td>


                                                    <td>{{ $facture->pv->bon->client->enterprise_name}}</td>
                                                    {{-- <td>{{ $facture->}}</td>
                                                    <td>{{ $facture->}}</td>--}}


                                                </tr>
                                            @endif
                                        @endforeach
                                    @endif
                            </tbody>
                        </table>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection


@section('scripts')
    <script>
        $(document).ready(function(){
       $("#myInput").on("keyup", function() {
           var value = $(this).val().toLowerCase();
           $("#myTable tr").filter(function() {
           $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
           });
       });
   });
    </script>
@endsection
