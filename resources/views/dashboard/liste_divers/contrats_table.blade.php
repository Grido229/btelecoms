

<!DOCTYPE html>
<html>
<head>
	<style type="text/css">
		.bloc{

			width:18cm;
			margin:auto;
		}

		h1{
			text-align: center;
		}

		td , th{
        border : solid 1px black;
        }

        table{
        border-collapse: collapse;
        width: 18cm;
        }

        th{
            background-color : rgba(0,0,0,0.1);
        }

	</style>
	<title>contrats_table</title>
</head>
<body>
	<div class="bloc">

		<div>
            <p style="float:left;"><img src="<?php echo $base64?>" width="80" height="80"/></p>

			<br><br><br><h1>Liste des contrats des operateurs</h3>
		</div>
		<table>
			<tr>
				<th>Ref contrat</th>
				<th>Date contrat</th>
				<th>Nom opération</th>
				<th>Objet contrat</th>
				<th>Duree</th>
				<th>Nature contrat</th>
			</tr>
            @foreach ($contrats as $contrat)

            <tr>
                <td>{{$contrat->num_contrat}}</td>
                <td>{{$contrat->begin_date}}</td>
                <td>{{$contrat->bon->client->enterprise_name}}</td>
                <td>{{$contrat->object_contrat}}</td>
                <td>{{$contrat->duree_contrat}}</td>
                <td>{{$contrat->nature_contrat}}</td>
                </tr>

            @endforeach

		</table>

	</div>
</body>
</html>
