@extends('dashboard.layouts.app', ['title' => 'Dashboard'])

@section('content')
<div class="content container">

    <!-- Page Header -->
	<section class="comp-section">
		<nav aria-label="breadcrumb">
			<ol class="breadcrumb">
				<li class="breadcrumb-item"><a href="{{ url('dashboard/home') }}">Home</a></li>
				<li class="breadcrumb-item"><a href="{{ url('dashboard/list/service') }}">Service</a></li>
				<li class="breadcrumb-item active" aria-current="page">Détails services</li>
			</ol>
		</nav>
	</section>
    <!-- /Page Header -->
    <div class="row">
        <div class="col-lg-12">
            <div class="card">
                <div class="card-header">
                    <h4 class="card-title">Informations les services </h4>

                </div>

                <div class="card-body">
                @if($facture != null)
                    <div class="row">
                        <p class="col-sm-3 text-muted mb-0 mb-sm-3">Reference PV</p>
                        <p class="col-sm-9 text-right font-weight-bold"> {{$facture->pv->num_commande_protocoles}} </p>
                    </div>

                    <div class="row">
                        <p class="col-sm-3 text-muted mb-0 mb-sm-3">Objet de demande</p>
                        <p class="col-sm-9 text-right font-weight-bold"> {{$facture->pv->bon->object}} </p>
                    </div>

                    <div class="row">
                        <p class="col-sm-3 text-muted mb-0 mb-sm-3">Type d'offre</p>

                         @if($facture->pv->sfs_id != NULL)
                        <p class="col-sm-9 text-right font-weight-bold">{{$facture->pv->bon->ssfs->libelle}}</p>
                        @endif

                        @if($facture->pv->ssfs_id == NULL)
                        <p class="col-sm-9 text-right font-weight-bold">{{$facture->pv->bon->sfs->libelle}}</p>
                        @endif


                    </div>

                    <div class="row">
                        <p class="col-sm-3 text-muted mb-0 mb-sm-3">Type Service</p>
                        <p class="col-sm-9 text-right font-weight-bold">{{$facture->pv->bon->sfs->fs->libelle}}</p>
                    </div>

                    <div class="row">
                        <p class="col-sm-3 text-muted mb-0 mb-sm-3">Date d'activation</p>
                        <p class="col-sm-9 text-right font-weight-bold"> {{ $facture->pv->created_at }} </p>
                    </div>

                    <div class="row">
                        <p class="col-sm-3 text-muted mb-0 mb-sm-3">Date Facture</p>
                        <p class="col-sm-9 text-right font-weight-bold"> {{ $facture->created_at}} </p>
                    </div>

                    <div class="row">
                        <p class="col-sm-3 text-muted mb-0 mb-sm-3">Frais</p>
                        @if($facture->frais_acces_id != null)
                    <p class="col-sm-9 text-right font-weight-bold"> {{ $facture->frais_acces->frais .'F FCA'}} </p>
                        @endif

                        @if($facture->frais_acces_id == null)
                        <p class="col-sm-9 text-right font-weight-bold"> 0 F CFA </p>
                        @endif

                    </div>

                    <div class="row">
                        <p class="col-sm-3 text-muted mb-0 mb-sm-3">Redevance</p>
                        <p class="col-sm-9 text-right font-weight-bold"> {{ $facture->montant_TTC .' F CFA'}} </p>
                    </div>

                    <div class="row">
                        <p class="col-sm-3 text-muted mb-0 mb-sm-3">Frequence Redevance</p>
                        <p class="col-sm-9 text-right font-weight-bold"> {{ $facture->type_facture}}</p>
                    </div>

                    {{-- <div class="row">
                        <p class="col-sm-3 text-muted mb-0 mb-sm-3">Date premiere redevance</p>
                        <p class="col-sm-9 text-right font-weight-bold"> </p>
                    </div> --}}

                    <div class="row">
                        <p class="col-sm-3 text-muted mb-0 mb-sm-3">Statut</p>
                        <p class="col-sm-9 text-right font-weight-bold"> {{ $facture->pv->status }}</p>
                    </div>


                    <div class="row">
                        <p class="col-sm-3 text-muted mb-0 mb-sm-3">Reference contrat </p>
                        @if( $facture->pv->bon->contrat)
                        <p class="col-sm-9 text-right font-weight-bold">  {{ $facture->pv->bon->contrat->num_contrat }} </p>
                        @else
                        <p class="col-sm-9 text-right font-weight-bold"> Pas de contrats  </p>
                        @endif
                    </div>

                    <div class="row">
                        <p class="col-sm-3 text-muted mb-0 mb-sm-3">Reference Demande</p>

                        <p class="col-sm-9 text-right font-weight-bold"> {{ $facture->pv->bon->num_bon }}  </p>


                    </div>

                    <div class="row">
                        <p class="col-sm-3 text-muted mb-0 mb-sm-3">Nom operateur</p>
                        @if($facture->pv->bon->client != null)
                        <p class="col-sm-9 text-right font-weight-bold"> {{ $facture->pv->bon->client->enterprise_name}}</p>
                        @endif
                    </div>

                @endif

                </div>
            </div>
        </div>
    </div>
</div>

@endsection


