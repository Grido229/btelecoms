<!DOCTYPE html>
<html>
<head>
	<style>

		#societe{
				padding: 0px 0px 50px 340px;
				font-size: 14px;
				font-family: Kinnari ;
				font-style: italic;
		}

		#num_fact{
			margin-bottom: 40px;
		}



		#bloc{
			width: 18cm;
			margin:auto;
		}
		#facture{
			border : 1px solid black;
			padding: 10px 100px;
			position: relative;
            top: -40.2px;
			float: right;


		}

		#trait{
			width: 18cm;
			border-bottom: 1px solid black;
		}

		#client{
			border: 1px solid black;
			border-left : white;
			padding-right: 210px;
			float: left;
		}

		header{
			margin: 0px 0px 40px 0px;

		}

		span{
			margin-left: 250px;
		}

		#t1{
			margin-bottom: 20px;
		}

		.bordure{
			border:white;
		}

      .col2{
          position: relative ;
          left: 130px ;
          width: 10cm ;

          border-bottom: 1px solid gray ;

      }
      .col3
      {
          position: relative ;

          left: 130px ;
          width: 10cm ;
      }
      .Gras{
          text-decoration: underline ;

      }

      td , th{
		border : solid 1px black;
	  }

	#t2{
		border-collapse: collapse;
		width: 18cm;
		/*margin: auto;*/
	}

    .arrete{
        width:18cm;
        background-color : rgba(0,0,0,0.1);
        margin-top : 15px;
        padding: 10px 0px;
    }

    .color_mont{
        background-color : rgba(0,0,0,0.1);

    }

	</style>
	<title>Desserte Facture</title>
</head>
<body>

	<div id="bloc">
		<header>
            <div> <h5 id="societe"> SOCIETE EN LIQUIDATION </h5> </div>
            @if( $deserte->ref_facture != 'null')
			<div id="num_fact">{{$deserte->ref_facture}}</div>
            @else
            <div id="num_fact">Non definie</div>
            @endif
			<div>
				<div id="facture">FACTURE</div>
				<div id="trait"> </div>
				<div id="client">Client</div>
			</div>
		</header>

		<section>

			<div class="bloc_1">


				<table id="t1">
		            <tr>
                        <td class="bordure" >Nom :</td>
                        @if($deserte->clients_id != NULL)
                            <td class="col2 bordure" ><strong> {{$deserte->client->enterprise_name}} </strong></td>
                        @else
                        <td class="col2 bordure" ><strong> Inconnu </strong></td>
                        @endif
		            </tr>
		            <tr>
                        <td class="bordure" >Adresse :</td>
                        @if($deserte->clients_id != NULL)
                        <td class="col2 bordure">{{$deserte->client->adress}}</td>
                        @else
                        <td class="col2 bordure" ><strong> Inconnu </strong></td>
                        @endif
		            </tr>

		            <tr>
		                <td class="Gras bordure">Objet : </td>
		                <td class="col3 bordure"><strong>{{$deserte->intitule}}</strong></td>
		            </tr>
		            <tr>
		                <td class="Gras bordure">Reference Devis : </td>
		                <td class="col3 bordure"><strong>{{$deserte->ref_devis}}</strong></td>
		            </tr>
		            <tr>
		                <td class="Gras bordure" >Période : </td>
		                <td class="col3 bordure"><strong>janv-18</strong></td>
		            </tr>
		            <tr>
		                <td class="bordure"><strong>Date limite de paiement</strong></td>
		                <td class="col3 bordure"><strong>Dès reception</strong></td>
		            </tr>
		        </table>

			</div>

			<div class="bloc_2">

                    <table id="t2">
                            <tr>
                                <th>Désignation</th>
                                <th>Quantité</th>
                                <th>Prix Unitaire</th>
                                <th>Montant</th>
                            </tr>

                            <tr>
                                <td style="text-align: center;" colspan="4"><strong>TRAVAUX DE GENIE CIVIL</strong></td>
                            </tr>

                            <tr>
                                <td colspan="4" style="text-align: left; font-weight: bold;">Pose de conduite allégée</td>
                            </tr>

                            <tr>
                                <td style="text-align: left; font-weight: bold;">1Q45</td>
                                <td style="text-align: center;" rowspan="3">{{$deserte->quantite_1}}</td>
                                <td> </td>
                                <td class="color_mont"> </td>
                            </tr>

                            <tr>
                                <td style="text-align: left;">Fourniture</td>
                                <td>{{$deserte->pU_fourniture_1 }}</td>
                                <td class="color_mont">{{$deserte->fourniture_1 }}</td>
                            </tr>

                            <tr>
                                <td style="text-align: left;">Prestation</td>
                                <td>{{$deserte->pU_prestation_1 }}</td>
                                <td class="color_mont">{{$deserte->prestation_1 }}</td>
                            </tr>

                            <tr>
                                <td colspan="4" style="text-align: right;" ><strong>{{$deserte->montant_1}}</strong></td>
                            </tr>

                            <tr>
                                <td style="text-align: left; font-weight: bold;">2Q45</td>
                                <td  style="text-align: center;" rowspan="3">{{$deserte->quantite_2}}</td>
                                <td> </td>
                                <td class="color_mont"> </td>
                            </tr>

                            <tr>
                                <td style="text-align: left;">Fourniture</td>
                                <td>{{$deserte->pU_fourniture_2 }}</td>
                                <td class="color_mont">{{$deserte->fourniture_2 }}</td>
                            </tr>

                            <tr>
                                <td style="text-align: left;">Prestation</td>
                                <td>{{$deserte->pU_prestation_2 }}</td>
                                <td class="color_mont">{{$deserte->prestation_2 }}</td>
                            </tr>

                            <tr>
                                <td colspan="4" style="text-align: right;"><strong>{{$deserte->montant_2}}</strong></td>
                            </tr>

                            <tr>
                                <td style="text-align: left; font-weight: bold;">Réparation de conduite allégée</td>
                                <td style="text-align: center;" rowspan="3">{{$deserte->quantite_3}}</td>
                                <td> </td>
                                <td class="color_mont"> </td>
                            </tr>

                            <tr>
                                <td style="text-align: left;">Fourniture</td>
                                <td>{{$deserte->pU_fourniture_3 }}</td>
                                <td class="color_mont">{{$deserte->fourniture_3 }}</td>
                            </tr>

                            <tr>
                                <td style="text-align: left;">Prestation</td>
                                <td>{{$deserte->pU_prestation_3 }}</td>
                                <td class="color_mont">{{$deserte->prestation_3 }}</td>
                            </tr>

                            <tr>
                                <td colspan="4" style="text-align: right;"><strong>{{$deserte->montant_3}}</strong></td>
                            </tr>

                            <tr>
                                <td style="text-align: left; font-weight: bold;">Démolition et reprise d'entrée en chambre</td>
                                <td></td>
                                <td> </td>
                                <td class="color_mont"> </td>
                            </tr>

                            <tr>
                                <td style="text-align: left;"><strong>Type L5T250</strong></td>
                                <td style="text-align: center;" rowspan="3">{{$deserte->quantite_4}}</td>
                                <td> </td>
                                <td class="color_mont"> </td>
                            </tr>

                            <tr>
                                <td style="text-align: left;">Fourniture</td>
                                <td>{{$deserte->pU_fourniture_4 }}</td>
                                <td class="color_mont">{{$deserte->fourniture_4 }}</td>
                            </tr>

                            <tr>
                                <td style="text-align: left;">Prestation</td>
                                <td>{{$deserte->pU_prestation_4 }}</td>
                                <td class="color_mont">{{$deserte->prestation_4 }}</td>
                            </tr>

                            <tr>
                                <td colspan="4" style="text-align: right;"><strong>{{$deserte->montant_4}}</strong></td>
                            </tr>

                            <tr>
                                <td style="text-align: left; font-weight: bold;">Construction de chambre</td>
                                <td></td>
                                <td> </td>
                                <td class="color_mont"> </td>
                            </tr>

                            <tr>
                                <td style="text-align: left;">Type LT3 verrouillable</td>
                                <td style="text-align: center;" rowspan="3">{{$deserte->quantite_5}}</td>
                                <td> </td>
                                <td class="color_mont"> </td>
                            </tr>

                            <tr>
                                <td style="text-align: left;">Fourniture</td>
                                <td>{{$deserte->pU_fourniture_5 }}</td>
                                <td class="color_mont">{{$deserte->fourniture_5 }}</td>
                            </tr>

                            <tr>
                                <td style="text-align: left;">Prestation</td>
                                <td>{{$deserte->pU_prestation_5 }}</td>
                                <td class="color_mont">{{$deserte->prestation_5 }}</td>
                            </tr>

                            <tr>
                                <td colspan="4" style="text-align: right;"><strong>{{$deserte->montant_5}}</strong></td>
                            </tr>
                </table>
        </div>

        <div  style="text-align: right; font-weight: bold; font-size : 12px;" > Page 1 de 2 </div>
        <div> <h5 id="societe"> SOCIETE EN LIQUIDATION </h5> </div>
        <div class="bloc_3">
        <table id="t2">
		            <tr>
		                <th>Désignation</th>
		                <th>Quantité</th>
		                <th>Prix Unitaire</th>
		                <th>Montant</th>
                    </tr>

                    <tr>
                                <td style="text-align: left; font-weight: bold;">Refection de surface</td>
                                <td></td>
                                <td> </td>
                                <td class="color_mont"> </td>
                            </tr>

                            <tr>
                                <td style="text-align: left;"><strong>Pavé</strong></td>
                                <td style="text-align: center;" rowspan="3">{{$deserte->quantite_6}}</td>
                                <td> </td>
                                <td class="color_mont"> </td>
                            </tr>

                            <tr>
                                <td style="text-align: left;">Fourniture</td>
                                <td>{{$deserte->pU_fourniture_6 }}</td>
                                <td class="color_mont">{{$deserte->fourniture_6 }}</td>
                            </tr>

                            <tr>
                                <td style="text-align: left;">Prestation</td>
                                <td>{{$deserte->pU_prestation_6 }}</td>
                                <td class="color_mont">{{$deserte->prestation_6 }}</td>
                            </tr>
                            <tr>
                                <td colspan="4" style="text-align: right;"><strong>{{$deserte->montant_6}}</strong></td>
                            </tr>
                            <tr>
                                <td colspan="3" style="text-align: center;"><strong>Sous-Total 1</strong></td>
                                <td class="color_mont"> {{$deserte->sous_total_1}}</td>
                            </tr>

                            <tr>
                                <td style="text-align: center;" colspan="4"><strong>TRAVAUX DE CABLAGE</strong></td>
                            </tr>

                            <tr>
                                <td colspan="4" style="text-align: left; font-weight: bold;">Aiguillage de conduite et tirage de cable en conduite </td>
                            </tr>

                            <tr>
                                <td style="text-align: left; font-weight: bold;">Cable 06 FO, structure microgaine diélect</td>
                                <td style="text-align: center;" rowspan="3">{{$deserte->quantite_7}}</td>
                                <td> </td>
                                <td class="color_mont"> </td>
                            </tr>

                            <tr>
                                <td style="text-align: left;">Fourniture</td>
                                <td>{{$deserte->pU_fourniture_7 }}</td>
                                <td class="color_mont">{{$deserte->fourniture_7 }}</td>
                            </tr>

                            <tr>
                                <td style="text-align: left;">Prestation</td>
                                <td>{{$deserte->pU_prestation_7 }}</td>
                                <td class="color_mont">{{$deserte->prestation_7 }}</td>
                            </tr>

                            <tr>
                                <td colspan="4" style="text-align: right;"><strong>{{$deserte->montant_7}}</strong></td>
                            </tr>

                            <tr>
                                <td style="text-align: left; font-weight: bold;">Gainage et clouage</td>
                                <td  style="text-align: center;" rowspan="3">{{$deserte->quantite_8}}</td>
                                <td> </td>
                                <td class="color_mont"> </td>
                            </tr>

                            <tr>
                                <td style="text-align: left;">Fourniture</td>
                                <td>{{$deserte->pU_fourniture_8 }}</td>
                                <td class="color_mont">{{$deserte->fourniture_8 }}</td>
                            </tr>

                            <tr>
                                <td style="text-align: left;">Prestation</td>
                                <td>{{$deserte->pU_prestation_8 }}</td>
                                <td class="color_mont">{{$deserte->prestation_8 }}</td>
                            </tr>

                            <tr>
                                <td colspan="4" style="text-align: right;"><strong>{{$deserte->montant_8}}</strong></td>
                            </tr>

                            <tr>
                                <td colspan="4" style="text-align: left; font-weight: bold;">Racoredement de câble FO </td>
                            </tr>

                            <tr>
                                <td style="text-align: left; font-weight: bold;">Tiroir optique de 6 FO équipé de pigtails SC-PC</td>
                                <td style="text-align: center;" rowspan="3">{{$deserte->quantite_9}}</td>
                                <td> </td>
                                <td class="color_mont"> </td>
                            </tr>

                            <tr>
                                <td style="text-align: left;">Fourniture</td>
                                <td>{{$deserte->pU_fourniture_9 }}</td>
                                <td class="color_mont">{{$deserte->fourniture_9 }}</td>
                            </tr>

                            <tr>
                                <td style="text-align: left;">Prestation</td>
                                <td>{{$deserte->pU_prestation_9 }}</td>
                                <td class="color_mont">{{$deserte->prestation_9 }}</td>
                            </tr>

                            <tr>
                                <td colspan="4" style="text-align: right;"><strong>{{$deserte->montant_9}}</strong></td>
                            </tr>

                            <tr>
                                <td style="text-align: left; font-weight: bold;">Joint 96 FO</td>
                                <td  style="text-align: center;" rowspan="3">{{$deserte->quantite_10}}</td>
                                <td> </td>
                                <td class="color_mont"> </td>
                            </tr>

                            <tr>
                                <td style="text-align: left;">Fourniture</td>
                                <td>{{$deserte->pU_fourniture_10 }}</td>
                                <td class="color_mont">{{$deserte->fourniture_10 }}</td>
                            </tr>

                            <tr>
                                <td style="text-align: left;">Prestation</td>
                                <td>{{$deserte->pU_prestation_10 }}</td>
                                <td class="color_mont">{{$deserte->prestation_10 }}</td>
                            </tr>

                            <tr>
                                <td colspan="4" style="text-align: right;"><strong>{{$deserte->montant_10}}</strong></td>
                            </tr>

                            <tr>
                                <td colspan="3" style="text-align: center;"><strong>Sous-Total 2</strong></td>
                                <td class="color_mont"> {{$deserte->sous_total_2}}</td>
                            </tr>
                            <tr>
                                <td colspan="3" style="text-align: right;"><strong>TOTAL HT</strong></td>
                                <td class="color_mont">  {{$deserte->total_HT}}</td>
                            </tr>
                            <tr>
                                <td colspan="3" style="text-align: right;"><strong>TVA 18% du TOTAL</strong></td>
                                <td class="color_mont"> {{$deserte->total_TVA}}</td>
                            </tr>
                            <tr>
                                <td colspan="3" style="text-align: right;"><strong> MONTANT TOTAL DU DEVIS</strong></td>
                                <td class="color_mont"> {{$deserte->montant_facture}}</td>
                            </tr>

        </table>

        <div class="arrete" ><strong>  Arrêté la présente facture à la somme de : {{$montant_lettre}} ({{ $deserte->montant_facture }}) </strong></div>


        @if($deserte->signataires_id == NULL)
        <h4 style="text-align: center; font-weight: normal; margin-top:20px;" > Pas de signataire </h4>

        <h4 style="text-decoration: underline; text-align: center; margin-top:0px;">Pas de nom</h4>
        @else
        <h4 style="text-align: center; font-weight: normal; margin-top:50px;" >{{$deserte->signataire->status}}</h4>

        <h4 style="text-decoration: underline; text-align: center; margin-top:50px;">{{$deserte->signataire->fullname }}</h4>

        @endif

        <div style="text-align: right; margin-top:0px; font-weight: bold; font-size : 12px;" > Page 2 de 2 </div>


		</section>
	</div>
</body>
</html>
