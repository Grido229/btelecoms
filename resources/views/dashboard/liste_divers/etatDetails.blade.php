@extends('dashboard.layouts.app', ['title' => 'Dashboard'])

@section('content')
<div class="content container">

    <!-- Page Header -->
	<section class="comp-section">
		<nav aria-label="breadcrumb">
			<ol class="breadcrumb">
				<li class="breadcrumb-item"><a href="{{ url('dashboard/home') }}">Home</a></li>
				<li class="breadcrumb-item"><a href="{{ url('dashboard/list/etat') }}">Etats</a></li>
				<li class="breadcrumb-item active" aria-current="page">Détails Etats</li>
			</ol>
		</nav>
	</section>
    <!-- /Page Header -->
    <div class="row">
        <div class="col-lg-12">
            <div class="card">
                <div class="card-header">
                    <h4 class="card-title">Informations les services </h4>

                </div>

                <div class="card-body">
                @if($etat != null)
                    <div class="row">
                        <p class="col-sm-3 text-muted mb-0 mb-sm-3">Reference PV</p>
                        <p class="col-sm-9 text-right font-weight-bold"> {{$etat->ref_pv}} </p>
                    </div>

                    <div class="row">
                        <p class="col-sm-3 text-muted mb-0 mb-sm-3">Nom service</p>
                        <p class="col-sm-9 text-right font-weight-bold"> {{$etat->nom_service}} </p>
                    </div>

                    <div class="row">
                        <p class="col-sm-3 text-muted mb-0 mb-sm-3"> Type service  </p>
                        <p class="col-sm-9 text-right font-weight-bold"> {{$etat->type_service }} </p>
                    </div>

                    <div class="row">
                        <p class="col-sm-3 text-muted mb-0 mb-sm-3">Date Activation</p>
                        <p class="col-sm-9 text-right font-weight-bold"> {{$etat->date_activation}} </p>
                    </div>

                    <div class="row">
                        <p class="col-sm-3 text-muted mb-0 mb-sm-3">frais</p>
                        <p class="col-sm-9 text-right font-weight-bold"> {{$etat->frais}} </p>
                    </div>

                    <div class="row">
                        <p class="col-sm-3 text-muted mb-0 mb-sm-3">Reference PV</p>
                        <p class="col-sm-9 text-right font-weight-bold"> {{$etat->ref_pv}} </p>
                    </div>



                    <div class="row">
                        <p class="col-sm-3 text-muted mb-0 mb-sm-3">Redevance</p>
                        <p class="col-sm-9 text-right font-weight-bold"> {{$etat->redevance}} </p>
                    </div>

                    <div class="row">
                        <p class="col-sm-3 text-muted mb-0 mb-sm-3">Frequence Redevance</p>
                        <p class="col-sm-9 text-right font-weight-bold"> {{$etat->frequence_redevance}} </p>
                    </div>

                    <div class="row">
                        <p class="col-sm-3 text-muted mb-0 mb-sm-3">Date premiere facturation</p>
                        <p class="col-sm-9 text-right font-weight-bold"> {{$etat->date_premiere_facturation}} </p>
                    </div>

                    <div class="row">
                        <p class="col-sm-3 text-muted mb-0 mb-sm-3">Statut</p>
                        <p class="col-sm-9 text-right font-weight-bold"> {{$etat->statut}} </p>
                    </div>

                    <div class="row">
                        <p class="col-sm-3 text-muted mb-0 mb-sm-3">Date Statut</p>
                        <p class="col-sm-9 text-right font-weight-bold"> {{$etat->date_statut}} </p>
                    </div>

                    <div class="row">
                        <p class="col-sm-3 text-muted mb-0 mb-sm-3">Raison</p>
                        <p class="col-sm-9 text-right font-weight-bold"> {{$etat->raison}} </p>
                    </div>

                    <div class="row">
                        <p class="col-sm-3 text-muted mb-0 mb-sm-3">Reference Contrat</p>
                        <p class="col-sm-9 text-right font-weight-bold"> {{$etat->ref_contrat}} </p>
                    </div>

                    <div class="row">
                        <p class="col-sm-3 text-muted mb-0 mb-sm-3">Reference Demande</p>
                        <p class="col-sm-9 text-right font-weight-bold"> {{$etat->ref_demande}} </p>
                    </div>

                    <div class="row">
                        <p class="col-sm-3 text-muted mb-0 mb-sm-3">Nom operateur</p>
                        <p class="col-sm-9 text-right font-weight-bold"> {{$etat->nom_operateur}} </p>
                    </div>
                @endif

                </div>
            </div>
        </div>
    </div>
</div>

@endsection


