@extends('dashboard.layouts.app', ['title' => 'Dashboard'])

@section('content')
<div class="content container-fluid">
	<section class="comp-section">
		<nav aria-label="breadcrumb">
            <ol class="breadcrumb">
				<li class="breadcrumb-item"><a href="{{ url('dashboard/home') }}">Home</a></li>
				<li class="breadcrumb-item active" aria-current="page">Desserte</li>
			</ol>
		</nav>
	</section>

	<div class="row">
		<div class="col-sm-12">
			<div class="card">
				<div class="card-header">
                    <h4 class="card-title">Liste Desserte</h4>
                    <a href="{{ route('impression.desertes') }} " title="Imprimer la liste des pv" class="btn btn-info mt-4 float-right" style="margin-top: -2rem !important; margin-left : 5px;">
                        <i class="fa fa-print"></i>
                        Imprimer
                    </a>
                    <a href="{{route('create.deserte')}}" class="btn btn-success mt-4 float-right" style="margin-top: -2rem !important;">Nouvelle Deserte</a>
				</div>
				<div class="card-body">

                    <div class="container">
                        <div class="row my-4">
                            <input class="form-control" id="myInput" type="text" placeholder="Recherche..">
                        </div>
                    </div>

					<div class="table-responsive">
                    <table class="datatable table table-stripped">



                            <thead>
                                <tr>
                                    <th>Ref Defis</th>
                                    <th>Intitule</th>
                                    <th>Type devis</th>
                                    <th class="text-center" style="padding : 0px 48px">Actions</th>
                                </tr>
                            </thead>
                            <tbody id="myTable">
                                @if($desertes != null)
                                    @foreach($desertes as $deserte)

                                    <tr>
                                        <td> {{$deserte->ref_devis}}</td>
                                        <td>{{$deserte->intitule}}</td>
                                        <td>{{$deserte->type_devis}}</td>
                                        <td class="text-center">
                                            <a href="{{ route('details.deserte',$deserte->id) }}" title="Afficher les details" class="btn btn-sm btn-info px-2" style="border-radius: .5rem;">
                                                <i class="fa fa-eye"></i>
                                            </a>

                                            <a href="{{ route('edit.deserte' , $deserte->id) }}" title="Editer la desere" class="btn btn-sm btn-danger" style="border-radius: .5rem;">
                                                <i class="fa fa-edit"></i>
                                            </a>

                                            <a href="{{ route('suppression.deserte' , $deserte->id) }}"  id="{{$deserte->id}}" onclick="setDesserteId(event)" data-toggle="modal" data-target="#deleteModal" title="Supprimer la desserte" class="btn btn-sm btn-danger" style="border-radius: .5rem;">
                                                <i class="fa fa-trash"></i>
                                            </a>

                                            <a href="{{ route('impression.deserte' , $deserte->id) }}" title="Imprimer la desserte" class="btn btn-sm btn-info px-2" style="border-radius: .5rem;">
                                                <i class="fa fa-file-pdf-o"></i>
                                            </a>

                                        </td>
                                    </tr>
                                    @endforeach
                                @endif
                            </tbody>
                        </table>
					</div>
				</div>
			</div>
		</div>
    </div>

    <div class="modal fade" id="deleteModal" tabindex="-1" role="dialog" aria-labelledby="deleteModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="deleteModalLabel">Suppression</h5>
                <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="modal-body">Voulez-vous vraiment supprimer ce procès verbal?</div>
            <div class="modal-footer">
                <button class="btn btn-secondary" type="button" data-dismiss="modal">Non</button>

                <a data-href="#" class="btn btn-primary" data-dismiss="modal" onclick="deleteDesserte()">
                    Oui
                </a>
            </div>
            </div>
        </div>
    </div>
</div>

</div>
@endsection

@section('scripts')
<script type="text/javascript">
    var id;
    function setDesserteId(e) {
        if(e.composedPath){
            var path = (e.composedPath && e.composedPath());

            if(path[1].id != ''){
                this.id = path[1].id;
            }else{
                this.id = path[0].id;
            }
        }
    }

    function deleteDesserte() {
        window.location.replace('suppression/deserte/' + id);
    }

   $(document).ready(function(){
       $("#myInput").on("keyup", function() {
           var value = $(this).val().toLowerCase();
           $("#myTable tr").filter(function() {
           $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
           });
       });
   });
</script>
@endsection
