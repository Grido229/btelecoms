

<!DOCTYPE html>
<html>
<head>
	<style type="text/css">
		.bloc{

			width:18cm;
			margin:auto;
		}

		h1{
			text-align: center;
		}

		td , th{
        border : solid 1px black;
        }

        table{
        border-collapse: collapse;
        width: 18cm;
        }

        th{
            background-color : rgba(0,0,0,0.1);
        }

	</style>
	<title>contrats_table</title>
</head>
<body>
	<div class="bloc">

		<div>
            <p style="float:left;"><img src="<?php echo $base64?>" width="80" height="80"/></p>

			<br><br><br><h1>Liste des contrats des operateurs</h3>
		</div>
		<table>
			<tr>
                <th>Ref Devis</th>
                <th>Ref facture</th>
				<th>Intitule</th>
				<th>Type devis</th>
                <th>Montant Facture</th>
				<th>Date Devis</th>
			</tr>
            @foreach ($desertes as $deserte)

            <tr>
                <td>{{$deserte->ref_devis}}</td>
                <td>{{$deserte->ref_facture}}</td>
                <td>{{$deserte->intitule}}</td>
                <td>{{$deserte->type_devis}}</td>
                <td>{{$deserte->montant_facture}}</td>
                <td>{{$deserte->created_at}}</td>
                </tr>

            @endforeach

		</table>

	</div>
</body>
</html>
