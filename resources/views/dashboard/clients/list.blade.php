@extends('dashboard.layouts.app', ['title' => 'Dashboard'])

@section('content')
<div class="content container-fluid">
	<section class="comp-section">
		<nav aria-label="breadcrumb">
			<ol class="breadcrumb">
				<li class="breadcrumb-item"><a href="{{ url('dashboard/home') }}">Home</a></li>
				<li class="breadcrumb-item active" aria-current="page">Clients</li>
			</ol>
		</nav>
	</section>

	<div class="row">
		<div class="col-sm-12">
			<div class="card">
				<div class="card-header">
                    <h4 class="card-title">Liste des Clients</h4>
                    <a href="{{ url('dashboard/clients/pdfClients') }}" title="Imprimer la liste des clients" class="btn btn-info mt-4 float-right" style="margin-top: -2rem !important;">
                        <i class="fa fa-print"></i>
                        Imprimer
                    </a>
				</div>
				<div class="card-body">
                    <div class="text-center">
                        @error('delete-invalid')
                            <span class="text-danger" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>

                    <div class="container">
                        <div class="row my-4">
                            <input class="form-control" id="myInput" type="text" placeholder="Recherche..">
                        </div>
                    </div>
                    <!--end-reseach--->

					<div class="table-responsive">
						<table class="datatable table table-stripped">
							<thead>
								<tr>
                                    <th>Numero</th>
                                    <th>Entreprise</th>
                                    <th>Type</th>
                                    <th>Email</th>
                                    <th>Téléphone</th>
                                    <th>Ville</th>

                                    @if (!$is_dcm)<th>Actions</th>@endif
								</tr>
							</thead>
							<tbody id="myTable">

                                @foreach ($clients as $client)

                                    <tr>
                                        <td> {{ $client->num_client }}</td>
                                        <td> {{ $client->enterprise_name }}</td>
                                        <td> {{ $client->client_type }}</td>
                                        <td> {{ $client->email }}</td>
                                        <td> {{ $client->tel }}</td>
                                        <td> {{ $client->city }}</td>

                                        @if (!$is_dcm)
                                        <td class="text-center">
                                            <a href="{{ url('dashboard/clients/' . $client->id . '/edit') }}" title="Editer " class="btn btn-sm btn-success px-2" style="border-radius: .5rem;">
                                                <i class="fa fa-edit"></i>
                                            </a>



                                            {{--<a id="{{ $client->id }}"  onclick="setClientId(event)" href="{{ url('dashboard/clients/' . $client->id . '/delete') }}" data-toggle="modal" data-target="#deleteModal" title="Supprimer" class="btn btn-sm btn-danger" style="border-radius: .5rem;">--}}

                                            <a id="{{ $client->id }}" onclick="setClientId(event)"  href="{{ route('client.delete',$client->id) }}" data-toggle="modal" data-target="#deleteModal" title="Supprimer" class="btn btn-sm btn-danger" style="border-radius: .5rem;">

                                                <i class="fa fa-trash"></i>
                                            </a>
                                        </td>
                                        @endif
                                    </tr>
                                    
                                    
                                @endforeach
							</tbody>
						</table>

                        <div class="modal fade" id="deleteModal" tabindex="-1" role="dialog" aria-labelledby="deleteModalLabel" aria-hidden="true">
                                        <div class="modal-dialog" role="document">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <h5 class="modal-title" id="deleteModalLabel">Supprimer</h5>
                                                    <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                                                        <span aria-hidden="true">×</span>
                                                    </button>
                                                </div>
                                                <div class="modal-body">Voulez-vous vraiment supprimer ce client ?</div>
                                                <div class="modal-footer">
                                                    <button class="btn btn-secondary" type="button" data-dismiss="modal">Non</button>


                                                <a data-href="#" class="btn btn-primary" data-dismiss="modal"  onclick="deleteClient()">
                                                Oui
                                                </a>

                                            </div>
                                            </div>
                                        </div>
                                    </div>
					</div>
				</div>
			</div>
		</div>
    </div>





    <div class="modal fade" id="deleteModal" tabindex="-1" role="dialog" aria-labelledby="deleteModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="deleteModalLabel">Supprimer</h5>
                    <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                </div>
                <div class="modal-body">Voulez-vous vraiment supprimer ce client?</div>
                <div class="modal-footer">
                    <button class="btn btn-secondary" type="button" data-dismiss="modal">Non</button>


                {{--<a class="btn btn-primary"  onclick="deleteClient()">
                    Oui--}}

                <a data-href="#" class="btn btn-primary" data-dismiss="modal"  onclick="deleteClient()">
                Oui
                </a>

            </div>
            </div>
        </div>
    </div>

</div>


@endsection

@section('scripts')
<!--reseach--->
<script type="text/javascript">
    var id ;

    // function setClientId(e) {
    //     console.log(e.path[1].id);
    //     if(e.path[1].id != '') {
    //         this.id = e.path[1].id;
    //         console.log(id);
    //     } else {
    //         this.id = e.path[0].id;

    //     }
    // }

   /* function setClientId(e) {
        if (e.composedPath) {
            var path = (e.composedPath && e.composedPath());
              console.log(path[1].id) ;
              if (path[1].id != '') {
                this.id = path[1].id;
              } else{
                this.id = path[0].id;
              }*/

    function setClientId(e) {
        /*if(e.path[1].id != '') {
            id = e.path[1].id;
        } else {
            id = e.path[0].id;
        }*/
        if(e.composedPath){
            var path = (e.composedPath && e.composedPath());

            if(path[1].id != ''){
                this.id = path[1].id;
            }else{
                this.id = path[0].id;
            }
        }

    }

    function deleteClient() {

        window.location.replace('clients/' + id + '/delete');

    }

   $(document).ready(function(){
       $("#myInput").on("keyup", function() {
           var value = $(this).val().toLowerCase();
           $("#myTable tr").filter(function() {
           $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
           });
       });

   });
</script>
<script> 

</script>
<!--end-reseach--->
@endsection
