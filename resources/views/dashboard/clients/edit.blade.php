@extends('dashboard.layouts.app', ['title' => 'Dashboard'])

@section('content')
<div class="content container-fluid">

    <!-- Page Header -->
	<section class="comp-section">
		<nav aria-label="breadcrumb">
			<ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="{{ url('dashboard/home') }}">Home</a></li>
                <li class="breadcrumb-item"><a href="{{ url('dashboard/clients') }}">Clients</a></li>
				<li class="breadcrumb-item active" aria-current="page">Modifier un client</li>

			</ol>
		</nav>
	</section>
	<!-- /Page Header -->

	<!-- Row -->
	<div class="row">
		<div class="col-sm-12">

			<!-- Custom Boostrap Validation -->
			<div class="card">
				<div class="card-header">
					<h4 class="card-title">Modifier un client</h4>
				</div>
				<div class="card-body">
					<form method="POST" action="{{ url('dashboard/clients/' . $client->id . '/edit') }}">
                        @csrf

                        <div class="form-row">
                            <div class="col-md-6 mb-3">
                                <label for="enterprise_name">Nom de l'entreprise<span style="color:red">*</span></label>
                                <input type="text" class="form-control" id="enterprise_name" name="enterprise_name" value="{{ $client->enterprise_name }}">
                                @error('enterprise_name')
                                    <span class="text-danger" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>

                            <div class="col-md-6 mb-3">
                                <label for="client_type">Type client<span style="color:red">*</span></label>
                               <select name="client_type" class="form-control" id="client_type">
                                    <option disabled selected>Sélectionnez un droit</option>
                                    @foreach ($client_types as $client_type)
                                        @if ($client->client_type == $client_type->id)
                                            <option selected value="{{$client_type->id}}">{{$client_type->name}}</option>
                                        @else
                                            <option value="{{$client_type->id}}">{{$client_type->name}}</option>
                                        @endif
                                    @endforeach
                                </select>
                                @error('role_id')
                                    <span class="text-danger" role="alert">
                                        <strong>Vous devez sélectionner le type du client.</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-row">
                            <div class="col-md-6 mb-3">
                                <label for="email">Email<span style="color:red">*</span></label>
                                <input type="email" class="form-control" id="email" name="email" value="{{ $client->email }}">
                                @error('email')
                                    <span class="text-danger" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>

                            <div class="col-md-6 mb-3">
                                <label for="city">Ville<span style="color:red">*</span></label>
                                <input type="text" class="form-control" id="city" name="city" value="{{ $client->city }}">
                                @error('city')
                                    <span class="text-danger" role="alert">
                                        <strong>Ce champ est obligatoire.</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-row">
                            <div class="col-md-6 mb-3">
                                <label for="adress">Adresse<span style="color:red">*</span></label>
                                <input type="text" class="form-control" id="adress" name="adress" value="{{ $client->adress }}">
                                @error('adress')
                                    <span class="text-danger" role="alert">
                                        <strong>Ce champ est obligatoire.</strong>
                                    </span>
                                @enderror
                            </div>

                            <div class="col-md-6 mb-3">
                                <label for="tel">Téléphone<span style="color:red">*</span></label>
                                <input type="text" class="form-control" id="tel" name="tel" value="{{ $client->tel }}">
                                @error('tel')
                                    <span class="text-danger" role="alert">
                                        <strong>Ce champ est obligatoire.</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div>
                            <a href="{{ url('dashboard/clients') }}" style="margin-right: 10px" class="btn btn-primary">Annuler</a>
                            <button type="submit" class="btn btn-success">
                                Modifier
                            </button>
                        </div>
					</form>
				</div>
            </div>
		</div>
	</div>
	<!-- /Row -->

</div>
@endsection
