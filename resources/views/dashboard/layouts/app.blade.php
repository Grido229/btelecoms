<!DOCTYPE html>
<html lang="en">
	<head>
	    <meta charset="utf-8">
	    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0">
	    <title>{{ config('app.name', 'BENIN TELECOM COMMANDE') }} | {{ $title }}</title>

        {{-- CSRF Token --}}
        <meta name="csrf-token" content="{{ csrf_token() }}">

        {{-- Icon --}}
        <link href="{{ asset('assets/images/btcom.png') }}" rel="shortcut icon" type="image/x-icon">

		<!-- Bootstrap CSS -->
	    <link rel="stylesheet" href="{{ asset('assets/css/bootstrap.min.css') }}">

		<!-- Fontawesome CSS -->
	    <link rel="stylesheet" href="{{ asset('assets/css/font-awesome.min.css') }}">

		<!-- Feathericon CSS -->
	    <link rel="stylesheet" href="{{ asset('assets/css/feathericon.min.css') }}">

		<!-- Datatables CSS -->
		<link rel="stylesheet" href="{{ asset('assets/plugins/datatables/datatables.min.css') }}">

		<!-- Main CSS -->
        <link rel="stylesheet" href="{{ asset('assets/css/style.css') }}">
		<link rel="stylesheet" href="{!!asset('css/toastr.min.css')!!}">  
	    @yield('stylesheets')
	</head>

	<body onload="affiherChaqueSeconde()">
	    <div class="main-wrapper">
            @include('dashboard.layouts.header')
            @include('dashboard.layouts.menu')


			<!-- Page Wrapper -->
	        <div class="page-wrapper">
	            @yield('content')
			</div>
	    </div>

	    <script src="{{ asset('assets/js/jquery-3.2.1.min.js') }}"></script>

		<!-- Bootstrap Core JS -->
	    <script src="{{ asset('assets/js/popper.min.js') }}"></script>
	    <script src="{{ asset('assets/js/bootstrap.min.js') }}"></script>

		<!-- Slimscroll JS -->
	    <script src="{{ asset('assets/plugins/slimscroll/jquery.slimscroll.min.js') }}"></script>

		<!-- Datatables JS -->
		<script src="{{ asset('assets/plugins/datatables/jquery.dataTables.min.js') }}"></script>
		<script src="{{ asset('assets/plugins/datatables/datatables.min.js') }}"></script>

		<!-- Custom JS -->
        <script  src="{{ asset('assets/js/script.js') }}"></script>
		<script src="{!!asset('js/toastr.min.js')!!}"></script>
	    @yield('scripts')
	</body>
</html>
