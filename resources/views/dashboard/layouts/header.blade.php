@inject('notification', 'App\Utilities\Notification')

<div class="header">
    <div class="header-left">
        <a href="#" class="logo">
            <img src="{{ asset('assets/images/btcom.png') }}" alt="Logo">
        </a>

    </div>

    <ul class="nav user-menu">

        <!-- Notifications -->
        <li class="nav-item dropdown noti-dropdown">
            <a href="#" class="dropdown-toggle nav-link" data-toggle="dropdown">
                <i class="fe fe-bell"></i> <span class="badge badge-pill">{{ $notification->all_commandes_without_validation() }}</span>
            </a>

            <div class="dropdown-menu notifications">
                <div class="topnav-dropdown-header">
                    <span class="notification-title">Notifications</span>
                    {{-- <a href="javascript:void(0)" class="clear-noti"> Effacer </a> --}}
                </div>
                <div class="noti-content">
                    <ul class="notification-list">
                        <li class="notification-message">
                            <a href="#">
                                <div class="media">
                                    <div class="media-body">
                                        <p class="noti-details"><span class="noti-title"><strong>
                                            {{ $notification->commandes_without_dpm_validation() }}
                                            Commandes en attente de validation DPM</strong></span></p>
                                        {{-- <p class="noti-time"><span class="notification-time">4 mins ago</span></p> --}}
                                    </div>
                                </div>
                            </a>
                        </li>
                        <li class="notification-message">
                            <a href="#">
                                <div class="media">
                                    <div class="media-body">
                                        <p class="noti-details"><span class="noti-title"><strong>
                                            {{ $notification->commandes_without_dcm_validation() }}
                                            Commandes en attente de validation DCM</strong></span></p>
                                        {{-- <p class="noti-time"><span class="notification-time">6 mins ago</span></p> --}}
                                    </div>
                                </div>
                            </a>
                        </li>
                    </ul>
                </div>
                <div class="topnav-dropdown-footer">
                    <a href="#">View all Notifications</a>
                </div>
            </div>
        </li>
        <!-- /Notifications -->

        <li class="nav-item dropdown has-arrow">
            <a href="#" class="dropdown-toggle nav-link" data-toggle="dropdown">
                <span class="user-img"><img class="rounded-circle" src="{{ asset('assets/images/avatar-01.jpg') }}" width="31" alt="Avatar"></span>
                {{ Auth::user()->fullname }}
            </a>

            <div class="dropdown-menu">
                <a class="dropdown-item" href="{{ url('dashboard/users/profil') }}" >
                    Editer mon Profil
                </a>

                <a class="dropdown-item" href="{{ route('logout') }}" data-toggle="modal" data-target="#logoutModal">
                    Déconnexion
                </a>
                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                    @csrf
                </form>
            </div>
        </li>


    </ul>
</div>


<div class="modal fade" id="logoutModal" tabindex="-1" role="dialog" aria-labelledby="logoutModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="logoutModalLabel">Déconnexion</h5>
                <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="modal-body">Voulez-vous vraiment vous déconnecter?</div>
            <div class="modal-footer">
                <button class="btn btn-secondary" type="button" data-dismiss="modal">Non</button>

                <a href="#" class="btn btn-primary"
                    onclick="event.preventDefault();
                        document.getElementById('logout-form').submit();">
                    Oui
                </a>
            </div>
        </div>
    </div>
</div>
