
<div class="sidebar" id="sidebar">
    <div class="sidebar-inner slimscroll">
		<div id="sidebar-menu" class="sidebar-menu font-weight-600">

			<ul>
				<li {!! ($_url==('home') ? 'class=active' : '') !!} class="my-3">
					<a href="{{ url('dashboard/home') }}"><i class="fe fe-home"></i> <span>Acceuil</span></a>
				</li>


				@if(!$is_dpm)<li class="submenu mb-3">
                    <a href="#{{ url('dashboard/clients') }}" role="button" data-toggle="dropdown"><i class="fe fe-user"></i> <span>Clients</span> <span class="menu-arrow"></span></a>
                    <ul style="display: none;">
                        <li {!! ($_url==('customers') ? 'class=active' : '') !!}><a href="{{ url('dashboard/clients') }}">Liste de clients</a></li>
                        <li {!! ($_url==('new-customer') ? 'class=active' : '') !!}><a href="{{ url('dashboard/clients/create') }}">Nouveau client</a></li>
                    </ul>
                </li>@endif


				<li class="submenu mb-3">
                @if(!$is_dpm)<a href="#{{ url('dashboard/bons') }}" role="button" data-toggle="dropdown"><i class="fa fa-list-alt"></i> <span>Demande de service</span> <span class="menu-arrow"></span></a>@endif
                    <ul style="display: none;">
                        @if(!$is_dpm)<li {!! ($_url==('service-demande') ? 'class=active' : '') !!}><a href="{{ url('dashboard/bons') }}">Liste des demandes</a></li>@endif
                        @if(!$is_dpm)<li {!! ($_url==('new-service-demande') ? 'class=active' : '') !!}><a href="{{ url('dashboard/bons/create') }}">Ajouter une demande</a></li>@endif
                        @if(!$is_dpm)<li {!! ($_url==('dcm-validation') ? 'class=active' : '') !!}><a href="{{ url('dashboard/validations/dcm') }}">Validation DC</a></li>@endif
                    </ul>
                </li>

				@if(!$is_dcm)<li class="submenu mb-3">
					<a href="#{{ url('dashboard/pv') }}" role="button" data-toggle="dropdown"><i class="fe fe-bar-chart"></i> <span>Procès verbaux</span> <span class="menu-arrow"></span></a>
                    <ul style="display: none;">
                    @if(!$is_dcm)<li {!! ($_url==('dpm-validation') ? 'class=active' : '') !!}><a href="{{ url('dashboard/validations/dpm') }}">Homologation DPM</a></li>@endif
                        <li {!! ($_url==('commande-protocoles') ? 'class=active' : '') !!}><a href="{{ url('dashboard/pv') }}">Liste des PV</a></li>
                        <li {!! ($_url==('trafique-commande-protocole') ? 'class=active' : '') !!}><a href="{{ url('dashboard/pv/trafique') }}">Ajouter PV de trafic</a></li>
                        <li {!! ($_url==('capacity-commande-protocole') ? 'class=active' : '') !!}><a href="{{ url('dashboard/pv/create') }}">Ajouter PV de Capacité</a></li>
                        <li {!! ($_url==('colocalisation-commande-protocole') ? 'class=active' : '') !!}><a href="{{ url('dashboard/pv/colocalisation') }}">Ajouter PV de Colocalisation</a></li>
                    </ul>
                </li>@endif

				@if(!$is_dcm && !$is_dpm)<li class="submenu mb-3">
					<a href="#{{ url('dashboard/users') }}" role="button" data-toggle="dropdown"><i class="fe fe-users"></i> <span>Utilisateurs</span> <span class="menu-arrow"></span></a>
                    <ul style="display: none;">
                        <li {!! ($_url==('users') ? 'class=active' : '') !!}><a href="{{ url('dashboard/users') }}">Liste des utilisateurs</a></li>
                        <li {!! ($_url==('new-user') ? 'class=active' : '') !!}><a href="{{ url('dashboard/users/create') }}">Nouvel utilisateur</a></li>
                    </ul>
                </li>@endif

                @if(!$is_dcm && !$is_dpm)<li class="submenu mb-3">
					<a href="{{ url('dashboard/factures') }}" role="button" data-toggle="dropdown"><i class="fa fa-navicon"></i> <span>Facturation</span> <span class="menu-arrow"></span></a>
                    <ul style="display: none;">
                        <li {!! ($_url==('bordereau') ? 'class=active' : '') !!}><a href="{{ url('dashboard/factures/bordereau') }}">MAJ Bordereau</a></li>

                        <li {!! ($_url==('create_signataire') ? 'class=active' : '') !!}><a href="{{ url('dashboard/factures/signataire') }}">MAJ Signataire</a></li>
                        <li {!! ($_url==('list_signataire') ? 'class=active' : '') !!}><a href="{{ url('dashboard/factures/signataire/list') }}">Liste des Signataires</a></li>
                        <li {!! ($_url==('list-invoices') ? 'class=active' : '') !!}><a href="{{ url('dashboard/factures') }}">Liste des factures</a></li>
                        <li {!! ($_url==('list-global') ? 'class=active' : '') !!}><a href="{{ url('dashboard/factures/factures_synthese/list') }}">Liste des factures de synthèse</a></li>
                        <li {!! ($_url==('list-invoices-fraisActivation') ? 'class=active' : '') !!}><a href="{{ url('dashboard/factures/frais_activation') }}">List de factures d'ativation</a></li>

                        <li {!! ($_url==('list_bordereau') ? 'class=active' : '') !!}><a href="{{ url('dashboard/factures/list_bordereau') }}">Liste des  Bordereaux</a></li>
                    </ul>
                </li>@endif

                @if(!$is_dcm && !$is_dpm)<li class="submenu mb-3">
					<a href="{{ url('dashboard/list') }}" role="button" data-toggle="dropdown"><i class="fa fa-navicon"></i> <span>Edition divers</span> <span class="menu-arrow"></span></a>
                    <ul style="display: none;">
                        <li {!! ($_url==('list_contrats') ? 'class=active' : '') !!}><a href="{{ url('dashboard/list/contrats') }}">liste des Contrats</a></li>
                        <li {!! ($_url==('list_deserte') ? 'class=active' : '') !!}><a href="{{ url('dashboard/list/deserte') }}">Liste de Deplacement de Degat Desserte</a></li>
                        <li {!! ($_url==('list_service') ? 'class=active' : '') !!}><a href="{{ url('dashboard/list/service') }}">Liste de Service des Operateurs</a></li>
                        <li {!! ($_url==('list_client') ? 'class=active' : '') !!}><a href="{{ url('dashboard/list/etat') }}">Etat des services aux clients</a></li>

                    </ul>
                </li>@endif

                @if(!$is_dcm && !$is_dpm)<li {!! ($_url==('recouvrement') ? 'class=active' : '') !!} class="mb-3">
					<a href="{{ url('dashboard/recouvrements') }}"><i class="fa fa-recycle"></i> <span>Recouvrement</span></a>
				</li>@endif

                <div style="text-align: center; ">
                    <div style="color : yellow; font-size : 20px ;" id="date_heure"> </div>
                </div>

			</ul>
		</div>
    </div>
</div>

<script>


            var tableMonth = new Array('Jan' , 'Fer' , 'Mar' , 'Avr' , 'Mai' , 'Jui' , 'Jue' , 'Aou' , 'Sep' , 'Oct' , 'Nov' , 'Dec');
            var tableDay = new Array('Dim' , 'Lun' , 'Mar' , 'Mer' , 'Jeu' , 'Ven' , 'Sam');

            function afficherDateHeure(){
                var dateGlobale = new Date();

                var annee = dateGlobale.getFullYear();

                var mois = dateGlobale.getMonth();

                var jour = dateGlobale.getDay();

                var num_jour = dateGlobale.getDate();

                mois = tableMonth[mois];

                jour = tableDay[jour];

                var heure = dateGlobale.getHours();

                    if (heure < 10) {
                        heure = "0" + heure.toString();
                    }
                var minute = dateGlobale.getMinutes();

                    if (minute < 10) {
                        minute = "0" + minute.toString();
                    }

                var seconde = dateGlobale.getSeconds();

                    if (seconde < 10) {
                        seconde = "0" + seconde.toString();
                    }

                var dateHeure = document.getElementById("date_heure");


                dateHeure.innerHTML =   heure + ":" + minute + ":" + seconde + "<br>" +jour + " " + num_jour + " " + mois + " " + annee;



            }

            function affiherChaqueSeconde(){
                afficherDateHeure();

                var delai = 1000;

                setInterval('afficherDateHeure()' , delai);
            }

</script>

