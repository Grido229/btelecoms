@extends('dashboard.layouts.app', ['title' => 'Dashboard'])

@section('content')
<div class="content container-fluid">

					<!-- Page Header -->
	<section class="comp-section">
		<nav aria-label="breadcrumb">
			<ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="{{ url('dashboard/home') }}">Home</a></li>
                <li class="breadcrumb-item"><a href="{{ url('dashboard/factures') }}">Factures</a></li>
				<li class="breadcrumb-item active" aria-current="page">Créer un bordereau</li>
			</ol>
		</nav>
	</section>
	<!-- /Page Header -->

	<!-- Row -->
	<div class="row">
		<div class="col-sm-12">

			<!-- Custom Boostrap Validation -->
			<div class="card">
				<div class="card-header">
					<h4 class="card-title">Créer un bordereau</h4>
				</div>
                <div class="card-body">
					<form method="POST" action="{{route('bordereau.store')}}"> 
                        @csrf

                    

                    {{-- =========================== Champs à renseigers ============================ --}}

                    <div class="form-row">
                            <div class="col-md-6 mb-3">
                                <label for="client_name">Numero du poste<span style="color:red">*</span></label>
                                <input type="text" class="form-control" id="client_name" name="num_poste" value="{{old('num_poste')}}">
                            </div>
                            @error('num_poste')
                                    <span class="text-danger" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                            @enderror
                            <div class="col-md-6 mb-3">
                                <label for="client_name">Type du Bordereau<span style="color:red">*</span></label>
                                <input type="text" class="form-control" id="client_name" name="type_bord" value="{{old('type_bord')}}">
                            </div>

                            @error('type_bord')
                                    <span class="text-danger" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                            @enderror

                    </div>

                    <div class="form-row">
                            <div class="col-md-6 mb-3">
                                <label for="invoice_amount">Responsable de la mise à jour<span style="color:red">*</span></label>
                                <input type="text" class="form-control" id="invoice_amount" name="resp_maj" value="{{old('resp_maj')}}" >
                            </div>
                            @error('resp_maj')
                                    <span class="text-danger" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                            @enderror
                            <div class="col-md-6 mb-3">
                                <label for="tva">Numero Device<span style="color:red">*</span></label>
                                <input type="text" class="form-control" id="tva" name="num_device" value="{{old('num_device')}}">
                            </div>
                            @error('num_device')
                                    <span class="text-danger" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                            @enderror
                    </div>
                    
                    <div>
                        {{-- <a href="{{ url('dashboard/clients') }}" style="margin-right: 10px" class="btn btn-primary">Annuler</a> --}}
                        <button type="submit" class="offset-md-5 btn btn-success">
                            Créer
                        </button>
                    </div>
					</form>
				</div>
			</div>
          </div>

		</div>
	</div>
	<!-- /Row -->

				</div>
@endsection

@section('scripts')
   
@endsection
