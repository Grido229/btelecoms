<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<title>FACTURE</title>

<head>

    <style>

		#societe{
				padding: 0px 0px 100px 340px;
				font-size: 14px;
				font-family: Kinnari ;
				font-style: italic;
		}

		#num_fact{
			margin-bottom: 40px;
		}



		#bloc{
			width: 18cm;
			/*margin:auto;*/
            margin-left: -32px;
		}
		#facture{
			border : 1px solid black;
			padding: 10px 100px;
			margin-top: -40px;
			float: right;


		}

		#trait{
			width: 18cm;
			border-bottom: 1px solid black;
		}

		#client{
			border: 1px solid black;
			border-left : white;
			padding-right: 210px;
			float: left;
		}

		header{
			margin: 50px 0px 40px 0px;

		}

		span{
			margin-left: 250px;
		}

		#t1{
			margin-bottom: 20px;
		}

		.bordure{
			border:white;
		}

      .col2{
          position: relative ;
          left: 130px ;
          width: 9cm ;

          border-bottom: 1px solid gray ;

      }
      .col3
      {
          position: relative ;

          left: 130px ;
          width: 9cm ;
      }
      .Gras{
          text-decoration: underline ;

      }

      td , th{
		border : solid 1px black;
	  }

	  #t2{
	  	width : 18cm;
	  	border-collapse: collapse;
          margin-top: 55px;
	  }

      .arrete{
        width:18cm;
        background-color : rgba(0,0,0,0.1);
        margin-top : 30px;
        padding: 10px 0px;
    }

	   .color_mont{
        background-color : rgba(0,0,0,0.1);

    }
    </style>
</head>

<body style="margin: 50px;">
    <div id="bloc">
		<header>
			<div> <h5 id="societe"> SOCIETE EN LIQUIDATION </h5> </div>
			<div id="num_fact"> {{ $fraisActivation->num_facture }}</div>

			<div>
				<div id="facture">FACTURE</div>
				<div id="trait"> </div>
				<div id="client">Client</div>
			</div>
		</header>

		<section>

			<div class="bloc_1">


				<table id="t1">
		            <tr>
		                <td class="bordure" >Nom :</td>
		                <td class="col2 bordure" ><strong> {{ $fraisActivation->client->enterprise_name }} </strong></td>
		            </tr>
		            <tr>
		                <td class="bordure" >Adresse :</td>
		                <td class="col2 bordure">{{$fraisActivation->client->adress}}</td>
		            </tr>
		            <tr>
		                <td class="bordure" >Tel :</td>
		                <td class="col2 bordure">{{ $fraisActivation->client->tel }}</td>
		            </tr>
		             <tr>
		                <td class="bordure" >Fax :</td>
		                <td class="col2 bordure"> </td>
		            </tr>

		            <tr>
		                <td class="Gras bordure">Objet : </td>
		                <td class="col3 bordure"><strong>{{$fraisActivation->objet}}</strong></td>
		            </tr>
		            <tr>
		                <td class="Gras bordure">Bon de commande :  </td>
		                <td class="col3 bordure"><strong>{{$fraisActivation->bon->num_bon}}</strong></td>
		            </tr>
		            <tr>
		                <td class="Gras bordure" >Période : </td>
		                <td class="col3 bordure"><strong>{{ $fraisActivation->periode }}</strong></td>
		            </tr>
		            <tr>
		                <td class="bordure"><strong>Date limite de paiement</strong></td>
		                <td class="col3 bordure"><strong>Dès reception</strong></td>
		            </tr>
		        </table>

			</div>







        <table id="t2">
            <tr>
                <th>Désignation</th>
                <th>Période</th>
                <th>Capacité</th>
                <th>Prix mensuel</th>
                <th>Total</th>
            </tr>

            <tr>
                <td style="text-align: left;">Frais de mise en service</td>
                <td style="text-align: center;">{{ $fraisActivation->periode }}</td>
                <td style="text-align: center;">{{ $fraisActivation->capacite->capacite }}</td>
                <td>{{ $fraisActivation->prix }}</td>
                <td>{{ $fraisActivation->prix }}</td>
            </tr>

            <tr>
                <td style="border: none"> </td>
                <td style="border: none"> </td>
                <td style="border: none"> </td>
                <td><strong>Sous-total</strong></td>
                <td class="color_mont"><strong>{{ $fraisActivation->prix }}</strong></td>
            </tr>

            <tr>
                <td style="border: none"> </td>
                <td style="border: none"> </td>
                <td style="border: none"> </td>
                <td><strong>TVA 18%</strong></td>
                <td class="color_mont"><strong>{{ $fraisActivation->TVA }}</strong></td>
            </tr>

            <tr>
                <td style="border: none"> </td>
                <td style="border: none"> </td>
                <td style="border: none"> </td>
                <td><strong>Total TTC</strong></td>
                <td class="color_mont"><strong>{{ $fraisActivation->TTC }}</strong></td>
            </tr>

        </table>
            <h4 class="color_mont arrete"><strong>Arrêté la présente facture à la somme de : {{ $montant_lettre }} ({{ $fraisActivation->TTC }}) F CFA TTC</strong></h4>

            <h4 style="text-align: center; font-weight: normal; margin-top:50px;" >{{$fraisActivation->signataire->status}}</h4>

            <h4 style="text-decoration: underline; text-align: center; margin-top:50px;">{{$fraisActivation->signataire->fullname }}</h4>

    </div>
</body>

</html>
