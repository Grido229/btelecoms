@extends('dashboard.layouts.app', ['title' => 'Dashboard'])

@section('content')
<div class="content container-fluid">

					<!-- Page Header -->
	<section class="comp-section">
		<nav aria-label="breadcrumb">
			<ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="{{ url('dashboard/home') }}">Home</a></li>
                <li class="breadcrumb-item"><a href="{{ url('dashboard/factures') }}">Factures</a></li>
				<li class="breadcrumb-item active" aria-current="page">Créer un bordereau</li>
			</ol>
		</nav>
	</section>
	<!-- /Page Header -->

	<!-- Row -->
	<div class="row">
		<div class="col-sm-12">

			<!-- Custom Boostrap Validation -->
			<div class="card">
				<div class="card-header">
					<h4 class="card-title">Créer un signataire</h4>
				</div>
                <div class="card-body">
					<form method="POST" action="{{route('signataire.store')}}"> 
                        @csrf

                    

                    {{-- =========================== Champs à renseigers ============================ --}}

                    <div class="form-row">
                            <div class="col-md-6 mb-3">
                                <label for="client_name">Nom et Prénom du signataire<span style="color:red">*</span></label>
                                <input type="text" class="form-control" id="client_name" name="name" value="{{old('num_poste')}}">
                            </div>
                            @error('name')
                                    <span class="text-danger" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                            @enderror
                            <div class="col-md-6 mb-3">
                                <label for="function">Fonction du signataire<span style="color:red">*</span></label>
                                <input type="text" class="form-control" id="function" name="function" value="{{old('function')}}">
                            </div>

                            @error('function')
                                    <span class="text-danger" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                            @enderror

                    </div>
                    <div>
                        {{-- <a href="{{ url('dashboard/clients') }}" style="margin-right: 10px" class="btn btn-primary">Annuler</a> --}}
                        <button type="submit" class="offset-md-5 btn btn-success">
                            Créer
                        </button>
                    </div>
					</form>
				</div>
			</div>
          </div>

		</div>
	</div>
	<!-- /Row -->

				</div>
@endsection

@section('scripts')
   
@endsection
