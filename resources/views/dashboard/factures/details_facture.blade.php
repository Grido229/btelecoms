@extends('dashboard.layouts.app', ['title' => 'Dashboard'])

@section('content')
<div class="content container">

    <!-- Page Header -->
	<section class="comp-section">
		<nav aria-label="breadcrumb">
			<ol class="breadcrumb">
				<li class="breadcrumb-item"><a href="{{ url('dashboard/home') }}">Home</a></li>
				<li class="breadcrumb-item"><a href="{{ url('dashboard/factures') }}">Factures</a></li>
				<li class="breadcrumb-item active" aria-current="page">Détails</li>
			</ol>
		</nav>
	</section>
    <!-- /Page Header -->
    <div class="row">
        <div class="col-lg-12">
            <div class="card">
                <div class="card-header">
                    <h4 class="card-title">Informations de la facture</h4>
                    @if($facture->type_facture == null)
                    <a href="{{route('facture.generate',$facture->id)}}" class="btn btn-primary mt-4 float-right" style="margin-top: -2rem !important;">Generer</a>
                    @else
                    <a href="{{route('facture.generate.generate',$facture->id)}}" class="btn btn-primary mt-4 float-right" style="margin-top: -2rem !important;">Generer</a>
                    @endif
                </div>
                <div class="card-body">
                    <div class="row">
                        <p class="col-sm-3 text-muted mb-0 mb-sm-3">No Facture</p>
                        <p class="col-sm-9 text-right font-weight-bold">{{ $facture->No_facture }}</p>
                    </div>

                    @if($facture->pv->bon->fs_id == NULL)
                    <div class="row">
                        <p class="col-sm-3 text-muted mb-0 mb-sm-3">Type d'offre</p>
                        <p class="col-sm-9 text-right font-weight-bold">{{ $facture->pv->bon->ssfs->libelle }}</p>
                    </div>
                    @endif

                    @if($facture->pv->bon->ssfs_id == NULL)
                    <div class="row">
                        <p class="col-sm-3 text-muted mb-0 mb-sm-3">Type d'offre</p>
                        <p class="col-sm-9 text-right font-weight-bold">{{ $facture->pv->bon->sfs->libelle }}</p>
                    </div>
                    @endif

                    <div class="row">
                        <p class="col-sm-3 text-muted mb-0 mb-sm-3">Numero PV</p>
                        <p class="col-sm-9 text-right font-weight-bold"> {{$facture->pv->num_commande_protocoles}}</p>
                    </div>

                    <div class="row">
                        <p class="col-sm-3 text-muted mb-0 mb-sm-3">Nom client</p>
                        <p class="col-sm-9 text-right font-weight-bold">{{ $facture->pv->bon->client->enterprise_name }}</p>
                    </div>

                    <div class="row">
                        <p class="col-sm-3 text-muted mb-0 mb-sm-3">Adresse</p>
                        <p class="col-sm-9 text-right font-weight-bold">{{$facture->pv->bon->client->adress}}</p>
                    </div>

                    <div class="row">
                        <p class="col-sm-3 text-muted mb-0 mb-sm-3">Téléphone</p>
                        <p class="col-sm-9 text-right font-weight-bold">{{ $facture->pv->bon->client->num_client }}</p>
                    </div>

                    <div class="row">
                        <p class="col-sm-3 text-muted mb-0 mb-sm-3">Objet</p>
                        <p class="col-sm-9 text-right font-weight-bold">{{$facture->pv->bon->object}}</p>
                    </div>

                    <div class="row">
                        <p class="col-sm-3 text-muted mb-0 mb-sm-3">Bon de Commande</p>
                        <p class="col-sm-9 text-right font-weight-bold">{{$facture->pv->bon->num_ref}}</p>
                    </div>


                    <div class="row">
                        <p class="col-sm-3 text-muted mb-0 mb-sm-3">Date Limite paiement</p>
                        <p class="col-sm-9 text-right font-weight-bold">  </p>
                    </div>

                    <div class="row">
                        <p class="col-sm-3 text-muted mb-0 mb-sm-3">Capacite</p>
                        @if($facture->pv->bon->capacite != NULL)
                        <p class="col-sm-9 text-right font-weight-bold">{{$facture->pv->bon->capacite->capacite}}</p>
                        @endif
                    </div>

                    <div class="row">
                        <p class="col-sm-3 text-muted mb-0 mb-sm-3">Distance</p>
                        @if($facture->pv->bon->distance != NULL)
                        <p class="col-sm-9 text-right font-weight-bold">{{$facture->pv->bon->distance->distance}}</p>
                        @endif
                    </div>

                    <div class="row">
                        <p class="col-sm-3 text-muted mb-0 mb-sm-3">Site</p>
                        @if($facture->pv->bon->zones_id != NULL)
                        <p class="col-sm-9 text-right font-weight-bold">{{$facture->pv->bon->zone->type}}</p>
                        @endif
                    </div>

                    <div class="row">
                        <p class="col-sm-3 text-muted mb-0 mb-sm-3">Volume</p>
                        @if($facture->pv->volume != NULL)
                        <p class="col-sm-9 text-right font-weight-bold">{{$facture->pv->volume->volume}}</p>
                        @endif
                    </div>

                    <div class="row">
                        <p class="col-sm-3 text-muted mb-0 mb-sm-3">Surface</p>
                        @if($facture->pv->surface != NULL)
                        <p class="col-sm-9 text-right font-weight-bold">{{$facture->pv->surface}}</p>
                        @endif
                    </div>

                    <div class="row">
                        <p class="col-sm-3 text-muted mb-0 mb-sm-3">Frais d'installation</p>
                        @if($facture->pv->frais_installation != NULL)
                        <p class="col-sm-9 text-right font-weight-bold">{{$facture->pv->frais_installation}}</p>
                        @endif
                    </div>

                    <div class="row">
                        <p class="col-sm-3 text-muted mb-0 mb-sm-3">Montant</p>
                        <p class="col-sm-9 text-right font-weight-bold"> {{$facture->montant_fact.' F CFA'}} </p>
                    </div>

                    <div class="row">
                        <p class="col-sm-3 text-muted mb-0 mb-sm-3">TVA</p>
                        <p class="col-sm-9 text-right font-weight-bold"> 18% </p>
                    </div>

                    <div class="row">
                        <p class="col-sm-3 text-muted mb-0 mb-sm-3">Nombre de jours facturé</p>
                        @if($facture->nbr_jours_fact != null)
                        <p class="col-sm-9 text-right font-weight-bold" > {{$facture->nbr_jours_fact}}</p>
                        @else
                        <p class="col-sm-9 text-right font-weight-bold" style="color : red"> A renseigner</p>
                        @endif
                    </div>

                    <div class="row">
                        <p class="col-sm-3 text-muted mb-0 mb-sm-3" >Date limite</p>
                        @if($facture->date_limite != null)
                        <p class="col-sm-9 text-right font-weight-bold" > {{$facture->date_limite}}</p>
                        @else
                        <p class="col-sm-9 text-right font-weight-bold" style="color : red"> A renseigner</p>
                        @endif
                    </div>

                    <div class="row">
                        <p class="col-sm-3 text-muted mb-0 mb-sm-3">Numero bordereau</p>
                        @if($facture->bordereau_id != null)
                        <p class="col-sm-9 text-right font-weight-bold" > {{$facture->bordereau->num_bordereau}}</p>
                        @else
                        <p class="col-sm-9 text-right font-weight-bold" style="color : red"> A renseigner</p>
                        @endif
                    </div>

                   {{-- <div class="row">
                        <p class="col-sm-3 text-muted mb-0 mb-sm-3">Date Envoie</p>
                        @if($facture->date_limite != null)
                        <p class="col-sm-9 text-right font-weight-bold" > {{$facture->date_limite}}</p>
                        @else
                        <p class="col-sm-9 text-right font-weight-bold" style="color : red"> A renseigner</p>
                        @endif
                    </div> --}}

                    <div class="row">
                        <p class="col-sm-3 text-muted mb-0 mb-sm-3">Type fature</p>
                        @if($facture->type_facture != null)
                        <p class="col-sm-9 text-right font-weight-bold" > {{$facture->type_facture}}</p>
                        @else
                        <p class="col-sm-9 text-right font-weight-bold" style="color : red"> A renseigner</p>
                        @endif
                    </div>


                </div>
            </div>
        </div>
    </div>
</div>

@endsection


