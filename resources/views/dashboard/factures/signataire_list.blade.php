@extends('dashboard.layouts.app', ['title' => 'Dashboard'])

@section('content')
<div class="content container-fluid">
	<section class="comp-section">
		<nav aria-label="breadcrumb">
            <ol class="breadcrumb">
				<li class="breadcrumb-item"><a href="{{ url('dashboard/home') }}">Home</a></li>
				<li class="breadcrumb-item active" aria-current="page">Signataires</li>
			</ol>
		</nav>
	</section>

	<div class="row">
		<div class="col-sm-12">
			<div class="card">
				<div class="card-header">
                    <h4 class="card-title">Liste des Signataires</h4>
                    <a href="{{ url('dashboard/factures/signataire') }}" class="btn btn-success mt-4 float-right" style="margin-top: -2rem !important;">Nouveau Signataire</a>
				</div>
				<div class="card-body">

                    <div class="container">
                        <div class="row my-4">
                            <input class="form-control" id="myInput" type="text" placeholder="Recherche..">
                        </div>
                    </div>

					<div class="table-responsive">
                    <table class="datatable table table-stripped">



                            <thead>
                                <tr>
                                    <th>Nom et Prénoms</th>
                                    <th>Fonction</th>
                                </tr>
                            </thead>
                            <tbody id="myTable">
                                @foreach($signataires as $signataire)

                                    <tr>
                                        <td> {{ $signataire->fullname}}</td>
                                        <td> {{$signataire->status}}</td>


                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection
@section('scripts')
    <script>
        $(document).ready(function(){
       $("#myInput").on("keyup", function() {
           var value = $(this).val().toLowerCase();
           $("#myTable tr").filter(function() {
           $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
           });
       });
   });
    </script>
@endsection
