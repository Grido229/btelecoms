@extends('dashboard.layouts.app', ['title' => 'Dashboard'])

@section('content')
<div class="content container-fluid">
	<section class="comp-section">
		<nav aria-label="breadcrumb">
            <ol class="breadcrumb">
				<li class="breadcrumb-item"><a href="{{ url('dashboard/home') }}">Home</a></li>
				<li class="breadcrumb-item active" aria-current="page">Factures</li>
			</ol>
		</nav>
	</section>

	<div class="row">
		<div class="col-sm-12">
			<div class="card">
				<div class="card-header">
                    <h4 class="card-title">Liste des Factures de Synthèse</h4>
                    <a href="{{route('facture_globale.create')}}" class="btn btn-success mt-4 float-right" style="margin-top: -2rem !important;">Nouvelle facture de synthèse</a>
                </div>
				<div class="card-body">

                    <div class="container">
                        <div class="row my-4">
                            <input class="form-control" id="myInput" type="text" placeholder="Recherche..">
                        </div>
                    </div>

					<div class="table-responsive">
                    <table class="datatable table table-stripped">
                            <thead>
                                <tr>
                                    <th>Numero</th>
                                    <th>No Facture Globale</th>
                                    <th>No Facture Capacité </th>
                                    <th>No Facture Colocalisation </th>
                                    <th>Nom Client</th>
                                    <th class="text-center">Actions</th>
                                </tr>
                            </thead>
                            <tbody id="myTable">
                                @php
                                $num = 1;
                                @endphp

                                @foreach($factures_globale as $facture_globale )

                                    <tr>
                                        <td> {{ $num++ }}</td>
                                        <td>{{$facture_globale->No_facture}} </td>
                                        @isset ($facture_globale->facture->No_facture)
                                            <td>{{$facture_globale->facture->No_facture}}</td>
                                        @endisset
                                        @isset ($facture_globale->pv->facture->No_facture)
                                            <td>{{$facture_globale->pv->facture->No_facture}}</td>
                                        @endisset
                                        @if($facture_globale->pv_id != null)
                                            <td>{{$facture_globale->pv->bon->client->enterprise_name }}</td>
                                            @else
                                            <td>Néant</td>
                                        @endif
                                        <td class="text-center">
                                            <a href="{{route('factureGlobale.detail',$facture_globale->id)}}" title="Afficher les details" class="btn btn-sm btn-info px-2" style="border-radius: .5rem;">
                                                <i class="fa fa-eye"></i>
                                            </a>
                                            <a href="{{route('facture_globale.generate' , $facture_globale->id)}}" title="Générer la facture" class="btn btn-sm btn-info px-2" style="border-radius: .5rem;">
                                                <i class="fa fa-file-pdf-o"></i>
                                            </a>
                                            {{--<a href="#" title="Afficher les details" class="btn btn-sm btn-info px-2" style="border-radius: .5rem;">
                                                <i class="fa fa-eye"></i>--}}
                                            </a>
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection

@section('scripts')
    <script>
        $(document).ready(function(){
       $("#myInput").on("keyup", function() {
           var value = $(this).val().toLowerCase();
           $("#myTable tr").filter(function() {
           $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
           });
       });
   });

   @if (Session::has('error suscription'))
    toastr.error('Veuillez choisir un client qui a souscrit a deux offres','',[timeOut=>6000]);
@endif

@if (Session::has('any suscription'))
    toastr.error('Veuillez choisir un client valide puis réessayer','Ce client n\'a souscrit a aucu offre',[timeOut=>6000]);
@endif

@if (Session::has('Error Facture Capacite'))
    toastr.error('Veuillez générer d\'abord les factures périodiques de ce client','',[timeOut=>6000]);
@endif


@if (Session::has('Error Facture Colocalisation'))
    toastr.error('Veuillez générer d\'abord les factures périodiques de ce client','',[timeOut=>6000]);
@endif
    </script>
@endsection
