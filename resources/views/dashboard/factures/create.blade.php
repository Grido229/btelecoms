@extends('dashboard.layouts.app', ['title' => 'Dashboard'])

@section('content')
<div class="content container-fluid">

					<!-- Page Header -->
	<section class="comp-section">
		<nav aria-label="breadcrumb">
			<ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="{{ url('dashboard/home') }}">Home</a></li>
                <li class="breadcrumb-item"><a href="{{ url('dashboard/factures') }}">Factures</a></li>
				<li class="breadcrumb-item active" aria-current="page">Créer une facture</li>
			</ol>
		</nav>
	</section>
	<!-- /Page Header -->

	<!-- Row -->
	<div class="row">
		<div class="col-sm-12">

			<!-- Custom Boostrap Validation -->
			<div class="card">
				<div class="card-header">
					<h4 class="card-title">Créer une facture</h4>
				</div>
                <div class="card-body">
					<form method="POST" action="{{ url('dashboard/factures/create') }}">
                        @csrf

                            <div class="col-md-6 mb-3">
                                <label for="facture_type">Type Facture<span style="color:red"></span></label>
                                <select class="form-control" name="facture_type" required>
                                    <option disabled selected>Sélectionnez le type de facture</option>
                                    <option value="">dfbvjdf</option>
                                </select>
                                @error('facture_type')
                                    <span class="text-danger" role="alert">
                                        <strong>Vous devez sélectionner le type du client.</strong>
                                    </span>
                                @enderror
                            </div>

                    {{-- =========================== Chant à renseigers ============================ --}}

                    <div class="form-row">

                            <div class="col-md-6 mb-3">
                                <label for="numero_facture">Numero Facture<span style="color:red"></span></label>
                                <input type="text" class="form-control" id="numero_facture" name="numero_facture" >
                            </div>

                        </div>

                        <div class="form-row">

                            <div class="col-md-6 mb-3">
                                <label for="client_name">Nom Client<span style="color:red"></span></label>
                                <input type="text" class="form-control" id="client_name" name="client_name" >
                            </div>

                            <div class="col-md-6 mb-3">
                                <label for="client_name">Nom Service<span style="color:red"></span></label>
                                <select name="service_PV" id="service_PV" class="form-control">
                                    <option value="" selected> selectionner le service</option>
                                    <option value="">periode1</option>
                                </select>
                            </div>

                        </div>

                        <div class="form-row">
                            <div class="col-md-6 mb-3">
                                <label for="start_date">Date Début<span style="color:red"></span></label>
                                <input type="date" class="form-control" id="start_date" name="start_date" >
                            </div>

                            <div class="col-md-6 mb-3">
                                <label for="end_date">Date Fin<span style="color:red"></span></label>
                                <input type="date" class="form-control" id="end_date" name="end_date" >
                            </div>
                        </div>

                        <div class="form-row">
                            <div class="col-md-6 mb-3">
                                <label for="periode">Periode<span style="color:red"></span></label>
                                <select name="periode" id="periode" class="form-control">
                                    <option value="" selected> selectionner la periode</option>
                                    <option value="">periode1</option>
                                </select>
                            </div>

                            <div class="col-md-6 mb-3">
                                <label for="invoice_amount">Montant Facture<span style="color:red"></span></label>
                                <input type="text" class="form-control" id="invoice_amount" name="invoice_amount" >
                            </div>
                        </div>



                        <div class="form-row">
                            <div class="col-md-6 mb-3">
                                <label for="tva">TVA<span style="color:red"></span></label>
                                <input type="text" class="form-control" id="tva" name="tva" >
                            </div>

                            <div class="col-md-6 mb-3">
                                <label for="total_invoice_amount">Montant Total Facture<span style="color:red"></span></label>
                                <input type="text" class="form-control" id="total_invoice_amount" name="total_invoice_amount" >
                            </div>
                        </div>


                        <div class="form-row">
                            <div class="col-md-6 mb-3">
                                <label for="service_PV">PV Service<span style="color:red"></span></label>
                                <select name="service_PV" id="service_PV" class="form-control">
                                    <option value="" selected> selectionner le PV service</option>
                                    <option value="">periode1</option>
                                </select>
                            </div>

                            <div class="col-md-6 mb-3">
                                <label for="capacity_race">Taux Capacité<span style="color:red"></span></label>
                                <input type="text" class="form-control" id="capacity_race" name="capacity_race" >
                            </div>

                        </div>


                        <div class="form-row">
                            <div class="col-md-6 mb-3">
                                <label for="service_type">Type Service<span style="color:red"></span></label>
                                <select name="service_type" id="service_type" class="form-control">
                                    <option value="" selected> selectionner le PV service</option>
                                    <option value="">periode1</option>
                                </select>
                            </div>

                            <div class="col-md-6 mb-3">
                                <label for="invoice_bond">Numero bond facture<span style="color:red"></span></label>
                                <input type="text" class="form-control" id="invoice_bond" name="invoice_bond" >
                            </div>
                        </div>


                        <div class="form-row">
                            <div class="col-md-6 mb-3">
                                <label for="Num_Pv">Numero PV<span style="color:red"></span></label>
                                <input type="text" class="form-control" id="Num_Pv" name="Num_Pv" >
                            </div>

                            <div class="col-md-6 mb-3">
                                <label for="Num_account">Numero Compte<span style="color:red"></span></label>
                                <input type="text" class="form-control" id="Num_account" name="Num_account" >
                            </div>
                        </div>





                        <div>
                            {{-- <a href="{{ url('dashboard/clients') }}" style="margin-right: 10px" class="btn btn-primary">Annuler</a> --}}
                            <button type="submit" class="btn btn-success">
                                Créer
                            </button>
                        </div>
					</form>
				</div>
			</div>
          </div>

		</div>
	</div>
	<!-- /Row -->

				</div>
@endsection

@section('scripts')
    <script>
        jQuery(document).ready(function($){

            $('.select2').select2();

            $('#type').change(function(){
                switch( $(this).val() )
                {
                    case '1' :
                    $('#start_date').show(300);
                    $('#end_date').show(300);
                    $('#periode').show(300);
                    $('#invoice_amount').show(300);
                    $('#tva').show(300);
                    $('#total_invoice_amount').show(300);
                    $('#service_PV').show(300);
                    $('#service_name').show(300);
                    $('#client_name').show(300);
                    $('#service_type').show(300);
                    $('#invoice_bond').show(300);
                    $('#Num_Pv').show(300);
                    $('#Num_account').show(300);
                    $('#facture_type').show(300);
                    $('#capacity_race').show(300);
                    break;

                    case '2' :
                    $('#souscategorie').hide(300);
                    break;

                    case '3' :
                    $('#souscategorie').hide(300);
                    break;
                    default:
                }
            });

        })
    </script>
@endsection
