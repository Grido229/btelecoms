@extends('dashboard.layouts.app', ['title' => 'Dashboard'])

@section('content')
<div class="content container-fluid">

					<!-- Page Header -->
	<section class="comp-section">
		<nav aria-label="breadcrumb">
			<ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="{{ url('dashboard/home') }}">Home</a></li>
                <li class="breadcrumb-item"><a href="{{ url('dashboard/factures') }}">Factures</a></li>
				<li class="breadcrumb-item active" aria-current="page">Créer une facture</li>
			</ol>
		</nav>
	</section>
	<!-- /Page Header -->

	<!-- Row -->
	<div class="row">
		<div class="col-sm-12">

			<!-- Custom Boostrap Validation -->
			<div class="card">
				<div class="card-header">
					<h4 class="card-title">Créer une facture</h4>
				</div>
                <div class="card-body">
					<form method="POST" action="{{route('facture_globale.store')}}">
                        @csrf

                    <div class="form-row">
                            <div class="col-md-6 mb-3">
                                <label for="client_name">Nom du client<span style="color:red"></span></label>
                                <select style="font-size : 18px;" class="form-control" name="client" required>
                                     <option disabled selected>Sélectionnez le nom du client</option>
                                            @foreach ($clients as $client)
                                                    <option value="{{$client->id}}">{{$client->enterprise_name}}</option>  
                                            @endforeach
                                </select>
                            </div>

							<div class="col-md-6 mb-3">
                                <label for="client_name">Nom du signataire<span style="color:red"></span></label>
                                <select style="font-size : 18px;" class="form-control" name="signataire" required>
                                     <option disabled selected>Sélectionnez le nom du signataire</option>
                                            @foreach ($signataire as $signataire)
                                                        <option value="{{$signataire->id}}">{{$signataire->fullname}}</option>  
                                            @endforeach
                                </select>
                            </div>
                    </div>

                        
                        <div>
                            {{-- <a href="{{ url('dashboard/clients') }}" style="margin-right: 10px" class="btn btn-primary">Annuler</a> --}}
                            <button type="submit" class="btn btn-success">
                                Générer
                            </button>
                        </div>
					</form>
				</div>
			</div>
          </div>


	</div>
	<!-- /Row -->

				</div>
@endsection

@section('scripts')

<script>


</script>
@endsection
