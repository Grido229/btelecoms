@extends('dashboard.layouts.app', ['title' => 'Dashboard'])

@section('content')
<div class="content container-fluid">
	<section class="comp-section">
		<nav aria-label="breadcrumb">
            <ol class="breadcrumb">
				<li class="breadcrumb-item"><a href="{{ url('dashboard/home') }}">Home</a></li>
				<li class="breadcrumb-item active" aria-current="page">Factures</li>
			</ol>
		</nav>
	</section>

	<div class="row">
		<div class="col-sm-12">
			<div class="card">
				<div class="card-header">
                    <h4 class="card-title">Liste des Factures</h4>
                </div>
				<div class="card-body">

                    <div class="container">
                        <div class="row my-4">
                            <input class="form-control" id="myInput" type="text" placeholder="Recherche..">
                        </div>
                    </div>

					<div class="table-responsive">
                    <table class="datatable table table-stripped">



                            <thead>
                                <tr>
                                    <th>Numero</th>
                                    <th>Numero Facture</th>
                                    <th>Type de Facture</th>
                                    <th>NumeroPV</th>
                                    <th>Nom Client</th>
                                    <th class="text-center">Actions</th>
                                </tr>
                            </thead>
                            <tbody id="myTable">
                                @php
                                $num = 0;
                                @endphp

                                @foreach($factures as $facture )

                                    <tr>
                                        <td> {{ $num++ }}</td>
                                        <td> {{$facture->No_facture}}</td>
                                        <td>{{$facture->pv->type}}</td>
                                        <td>{{$facture->pv->num_commande_protocoles}} </td>
                                        <td> {{$facture->pv->bon->client->enterprise_name}} </td>

                                        <td class="text-center">
                                            <a href="{{ route('factures.details',$facture->id) }}" title="Afficher les details" class="btn btn-sm btn-info px-2" style="border-radius: .5rem;">
                                                <i class="fa fa-eye"></i>
                                            </a>

                                            @if($facture->type_facture == null)
                                            <a href="{{route('facture.generate',$facture->id)}}" title="Générer la facture" class="btn btn-sm btn-info px-2" style="border-radius: .5rem;">
                                                <i class="fa fa-file-pdf-o"></i>
                                            </a>
                                            @else
                                            <a href="{{ route('facture.generate.generate',$facture->id)}}" title="Générer la facture" class="btn btn-sm btn-info px-2" style="border-radius: .5rem; background-color:green; border-color : green;">
                                                <i class="fa fa-file-pdf-o"></i>
                                            </a>
                                            @endif

                                            {{--<a href="#" title="Afficher les details" class="btn btn-sm btn-info px-2" style="border-radius: .5rem;">
                                                <i class="fa fa-eye"></i> --}}
                                            </a>
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection

@section('scripts')
    <script>
        $(document).ready(function(){
       $("#myInput").on("keyup", function() {
           var value = $(this).val().toLowerCase();
           $("#myTable tr").filter(function() {
           $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
           });
       });
   });
    </script>

    <script>

    toastr.success('Facture en vert','Facture déjà générée', [timeOut=>5000]) ;


    </script>
@endsection


