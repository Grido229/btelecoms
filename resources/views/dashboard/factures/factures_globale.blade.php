<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title> Facture de Synthèse</title>
    <link rel="stylesheet" href="{{asset('/css/factures.css')}}">
</head>
<body> 
	<div class="bloc">
     <div id="head">
            <h5 id="numFac">N°{{$facture->No_facture}}</h5>
            <h3 id="FACTURE"><span id="headTitle">FACTURE</span></h3>
     </div>

     <h4 id = "header">Client</h4>

        <table id="t1">
            <tr>
                <td class="col1">Nom : </td>
                <td class="col2">{{ $facture->pv->bon->client->enterprise_name}}</td>
            </tr>
            <tr>
                <td class="col1">Adresse : </td>
                <td class="col2">{{  $facture->pv->bon->client->adress }}</td>
            </tr>
            <tr >
                <td class="col1">Téléphone: </td>
                <td class="col2">{{ $facture->pv->bon->client->tel}}</td>
            </tr>
            <tr>
                <td class="col1">Fax : </td>
                <td class="col2"></td>
            </tr>
        </table>

        <table id="t1">
            <tr>
                <td class="Gras">Objet :</td>
                <td class="col3"><strong> {{ $facture->pv->bon->object}}</strong></td>
            </tr>
            <tr>
                <td class="Gras">Bon de commande: </td>
                <td><strong class="col3">{{  $facture->pv->bon->num_bon }}</strong></td>
            </tr>
            <tr>
                <td class="Gras" >Période : </td>
                <td class="col3"><strong>{{$facture->facture->periode_fact}}</strong></td>
            </tr>
            <tr>
                <td class="Gras" >Nombre de jours facturé : </td>
                <td class="col3"><strong>{{ $facture->facture->nbr_jours_fact}}</strong></td>
            </tr>
            <tr>
                <td><strong>DATE LIMITE PAIEMENT</strong></td>
                <td class="col3"><strong></strong></td>
            </tr>
        </table>

        <div id="table1">
            <table id="Tableau1">
                <tr>
                    <th>Désignation</th>
                    <th>Période</th>
                    <th>Capacité</th>
                    <th>Prix Unitaire</th>
                    <th>Total TTC</th>
                </tr>
                @if ($facture->factures_capacite_id != null )
                        <tr>
                            <td class="table-1">Capacité service</td>
                            <td class="table-1" >{{$facture->facture->periode_fact}}</td>
                            <td class="table-1" >{{$facture->pv->bon->capacite->capacite}}</td>
                            <td class="table-1">{{$facture->facture->montant_fact}}</td>
                            <td class="last table-1">{{$facture->facture->montant_TTC}}</td>
                        </tr>
                @endif

                @if ($facture->pv_id != null)
                        <tr>
                            <td class="table-1">Colocalisation service</td>
                            <td class="table-1" >{{$facture->pv->facture->periode_fact}}</td>
                            <td class="table-1" >{{$facture->pv->bon->capacite->capacite}}</td>
                            <td class="table-1">{{$facture->pv->facture->montant_fact}}</td>
                            <td class="last table-1">{{$facture->pv->facture->montant_TTC}}</td>
                        </tr>
                @endif
                <tr>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                </tr>
                <tr>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td ></td>
                </tr>
                <tr>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td><strong>TOTAL TTC</strong></td>
                    <td class="last table-1">{{$montant_global}}</td>
                </tr>
            </table>
        </div>

        <div id="signe">
            <p>Arrêté la présente facture à la somme de :{{$montant_lettre}} ({{$montant_global}})
            Francs CFA TTC
            </p>
        </div>

        <div class="table2">
            <table class="centrer" id='Tableau2'>
              <tr>
                <td colspan="2" class="table-1">SITUATION COMPTABLE</td>
              </tr>
              <tr>
                <td class="table-1">Solde antérieur</td>
                <td class="table-1"> </td>

              </tr>
              <tr>
                <td class="table-1">Montant Facturé</td>
                <td class="table-1"> </td>
              </tr>
              <tr>
                <td class="table-1">Total dû</td>
                <td class="table-1"> </td>
              </tr>
            </table>
          </div>

          <div class="centrer">
          {{$facture->signataire->signataire_function}}
            <div class="nom"><strong>{{$facture->signataire->signataire}}</strong> </div>
          </div>
          <div class="">INSAE 2917200245534 <span id="ifu"> IFU N°  4200901793914 </span>  </div>

        </div>
</body>
</html>
