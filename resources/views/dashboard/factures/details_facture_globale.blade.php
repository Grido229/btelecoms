@extends('dashboard.layouts.app', ['title' => 'Dashboard'])

@section('content')
<div class="content container">

    <!-- Page Header -->
	<section class="comp-section">
		<nav aria-label="breadcrumb">
			<ol class="breadcrumb">
				<li class="breadcrumb-item"><a href="{{ url('dashboard/home') }}">Home</a></li>
				<li class="breadcrumb-item"><a href="{{ url('dashboard/factures') }}">Factures</a></li>
				<li class="breadcrumb-item active" aria-current="page">Détails</li>
			</ol>
		</nav>
	</section>
    <!-- /Page Header -->
    <div class="row">
        <div class="col-lg-12">
            <div class="card">
                <div class="card-header">
                    <h4 class="card-title">Informations de la facture</h4>
                    <a href="{{route('facture_globale.generate' , $facture->id)}}" class="btn btn-primary mt-4 float-right" style="margin-top: -2rem !important;">Generer</a>

                </div>
                <div class="card-body">

                    <div class="row">
                        <p class="col-sm-3 text-muted mb-0 mb-sm-3">No Facture</p>
                        <p class="col-sm-9 text-right font-weight-bold">{{ $facture->No_facture }}</p>
                    </div>

                    <div class="row">
                        <p class="col-sm-3 text-muted mb-0 mb-sm-3">Nom client</p>
                        <p class="col-sm-9 text-right font-weight-bold">{{ $facture->pv->bon->client->enterprise_name }}</p>
                    </div>

                    <div class="row">
                        <p class="col-sm-3 text-muted mb-0 mb-sm-3">Adresse</p>
                        <p class="col-sm-9 text-right font-weight-bold">{{$facture->pv->bon->client->adress}}</p>
                    </div>

                    <div class="row">
                        <p class="col-sm-3 text-muted mb-0 mb-sm-3">Téléphone</p>
                        <p class="col-sm-9 text-right font-weight-bold">{{ $facture->pv->bon->client->num_client }}</p>
                    </div>
                    
                </div>
            </div>
        </div>
    </div>
</div>

@endsection


