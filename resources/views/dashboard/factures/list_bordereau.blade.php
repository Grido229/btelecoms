@extends('dashboard.layouts.app', ['title' => 'Dashboard'])

@section('content')
<div class="content container-fluid">
	<section class="comp-section">
		<nav aria-label="breadcrumb">
            <ol class="breadcrumb">
				<li class="breadcrumb-item"><a href="{{ url('dashboard/home') }}">Home</a></li>
				<li class="breadcrumb-item active" aria-current="page">bordereau</li>
			</ol>
		</nav>
	</section>

	<div class="row">
		<div class="col-sm-12">
			<div class="card">
				<div class="card-header">
                    <h4 class="card-title">Liste des Bordereaux</h4>
                    <a href="{{ url('dashboard/factures/bordereau') }}" class="btn btn-success mt-4 float-right" style="margin-top: -2rem !important;">Nouveau Bordereau</a>
				</div>
				<div class="card-body">

                    <div class="container">
                        <div class="row my-4">
                            <input class="form-control" id="myInput" type="text" placeholder="Recherche..">
                        </div>
                    </div>

					<div class="table-responsive">
                    <table class="datatable table table-stripped">

                    @php
                    $num = 0;
                    @endphp

                            <thead>
                                <tr>
                                    <th>Numero bordereau</th>
                                    <th>Nmero Poste</th>
                                    <th>Type bordereau</th>
                                    <th>Date MAJ </th>
                                    <th>Responsable MAJ </th>
                                    <th>Numero Device</th>
                                </tr>
                            </thead>
                            <tbody id="myTable">
                                @foreach($bordereaux as $bordereau)

                                    <tr>
                                        <td> {{ $bordereau->num_bordereau}}</td>
                                        <td> {{$bordereau->num_poste}}</td>
                                        <td> {{$bordereau->type_bordereau}} </td>
                                        <td> {{$bordereau->created_at}}</td>
                                        <td> {{$bordereau->resp_maj}}</td>
                                        <td> {{$bordereau->num_device}}</td>


                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection


@section('scripts')
    <script>
        $(document).ready(function(){
       $("#myInput").on("keyup", function() {
           var value = $(this).val().toLowerCase();
           $("#myTable tr").filter(function() {
           $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
           });
       });
   });
    </script>
@endsection
