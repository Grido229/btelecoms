<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<link rel="stylesheet" href="{{asset('/css/colocalisation_facture_style.css')}}">
	<title>format 3</title>
</head>
<body>
		<div class="bloc">
			<section>
					<div class="ref">
						<div >Réf :</div>
						<div class="facture" >FACTURE</div>
					</div>


					<div class="blocs_secondaire">
						<div class="centrer">{{ $facture->type_facture }}</div>
						<table>
							<tr>
								<td>{{$facture->No_facture}}</td>
								<td>{{ $facture->pv->bon->client->enterprise_name}}</td>
							</tr>
							<tr>
								<td>Date d'émission : 22/11/2016</td>
								<td></td>
							</tr>
							<tr>
								<td>Code Client : ACE</td>
								<td>{{  $facture->pv->bon->client->adress }}</td>
							</tr>
						</table>
						<div class="objet">Objet : <span>Destinataire : GIE BENIN ACE : GIE BENIN ACEisation</span></div>
					</div>

					<div class="blocs_secondaire">
						<div class="centrer">Date limite de paiement: {{$date_limite}}</div>
						<table>
							<tr>
								<th>Période de Facturation</th>
								<th>Montant</th>
							</tr>
								<tr>
									<td>{{$facture->periode}}</td>
									<td class="centrer">{{$facture->montant_fact}}</td>
								</tr>
								<tr>
									<td>Montant TVA</td>
									<td class="centrer">{{ $facture->TVA}}</td>
								</tr>
								<tr>
									<td>Montant TTC</td>
									<td class="centrer">{{$facture->montant_TTC}}</td>
								</tr>
						</table>
					</div>

					<div class="arrete" class="blocs_secondaire">
						Arrêté la présente facture à la somme de:{{$montant_lettre}} ({{$facture->montant_TTC}})
           				 Francs CFA TTC
					</div>

					<div class="blocs_secondaire">
						<table class="centrer">
							<tr>
								<td colspan="2">SITUATION COMPTABLE</td>
							</tr>
							<tr>
								<td>Solide antérieur</td>
								<td> </td>
							</tr>
							<tr>
								<td>Paiement effectué</td>
								<td></td>
							</tr>
							<tr>
								<td>Montant facturé</td>
								<td>{{$facture->montant_TTC}}</td>
							</tr>
							<tr>
								<td>Total dû</td>
								<td> </td>
							</tr>
						</table>
					</div>

					<div class="nb" >
						NB : <span>Vous trouverez annexé à la présente facture le detail des éléments colocalisés.</span>
					</div>

					<div class="centrer">
					{{$facture->signataire->status}}
						<div class="nom">{{$facture->signataire->fullname}}</div>
					</div>
					<div>INSAE : 2917200245534 <div>IFU N° : 4200901793914</div> </div>

			</section>
			<div class="trait"></div>
			<footer >
					01BP : 5959 Cotonou . Tél : 21 31 20 45 / 21 31 20 49 . Télex : 5206 . Fax : 21 31 38 43 . <br>
					Email : sp.dgtelecoms@intnet.bj . RB Cotonou 2006 B1234
			</footer>

		</div>
</body>
</html>
