@extends('dashboard.layouts.app', ['title' => 'Dashboard'])

@section('content')
<div class="content container-fluid">

					<!-- Page Header -->
	<section class="comp-section">
		<nav aria-label="breadcrumb">
			<ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="{{ url('dashboard/home') }}">Home</a></li>
                <li class="breadcrumb-item"><a href="{{ url('dashboard/factures/frais_activation') }}">Factures</a></li>
				<li class="breadcrumb-item active" aria-current="page">Generer une facture de frais d'activation</li>
			</ol>
		</nav>
	</section>
	<!-- /Page Header -->

	<!-- Row -->
	<div class="row">
		<div class="col-sm-12">

			<!-- Custom Boostrap Validation -->
			<div class="card">
				<div class="card-header">
					<h4 class="card-title">Generer une facture de frais d'activation</h4>
				</div>
                <div class="card-body">
					<form method="POST" action="{{ route('facture.frais_activation.store' , $id) }}">
                        @csrf




                        <div class="form-row">
                            <div class="col-md-12 mb-3">
                                <label for="object">Object<span style="color:red">*</span></label>
                                <input type="text" class="form-control" id="object" name="object" value="{{ old('object') }}">
                                @error('object')
                                    <span class="text-danger" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>


                        <div class="form-row">
                            <div class="col-md-6 mb-3">
                                <label for="num_facture">Numero Facture<span style="color:red"></span></label>
                                <input type="text" class="form-control" id="num_facture" name="num_facture" value="{{ old('num_facture') }}" >
                                @error('num_facture')
                                    <span class="text-danger" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>

                            <div class="col-md-6 mb-3">
                                <label for="designation">Designation<span style="color:red"></span></label>
                                <input type="text" class="form-control" id="designation" name="designation" value="{{ old('designation') }}">
                                @error('designation')
                                    <span class="text-danger" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>


                        <div class="form-row">
                            <div class="col-md-12 mb-3">
                                <label for="signataire_id">Nom du signataire<span style="color:red"></span></label>
                                <select style="font-size : 18px;" class="form-control" name="signataire_id" >
                                     <option disabled selected>Sélectionnez le nom du signataire</option>
                                     @foreach ($signataires as $signataire)
                                            <option value="{{$signataire->id}}">{{$signataire->fullname}}</option>
                                     @endforeach
                                </select>
                                @error('signataire_id')
                                    <span class="text-danger" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>





                            {{-- <a href="{{ url('dashboard/clients') }}" style="margin-right: 10px" class="btn btn-primary">Annuler</a> --}}
                            <button type="submit" class="btn btn-success">
                                Generer
                            </button>
                        </div>
					</form>
				</div>
			</div>
          </div>

		</div>
	</div>
	<!-- /Row -->

				</div>
@endsection

