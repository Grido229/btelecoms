@extends('dashboard.layouts.app', ['title' => 'Dashboard'])

@section('content')
<div class="content container-fluid">
	<section class="comp-section">
		<nav aria-label="breadcrumb">
            <ol class="breadcrumb">
				<li class="breadcrumb-item"><a href="{{ url('dashboard/home') }}">Home</a></li>
				<li class="breadcrumb-item active" aria-current="page">Liste des frais d'activations</li>
			</ol>
		</nav>
	</section>

	<div class="row">
		<div class="col-sm-12">
			<div class="card">
				<div class="card-header">
                    <h4 class="card-title">Liste des Frais d'activations</h4>

                    {{--<a href="{{route('facture.frais_activation.create')}}" class="btn btn-success mt-4 float-right" style="margin-top: -2rem !important;">Nouveau frais d'activation</a>--}}

                </div>
				<div class="card-body">

                    <div class="container">
                        <div class="row my-4">
                            <input class="form-control" id="myInput" type="text" placeholder="Recherche..">
                        </div>
                    </div>

					<div class="table-responsive">
                    <table class="datatable table table-stripped">



                            <thead>
                                <tr>
                                    <th>Designation</th>
                                    <th>Objet</th>
                                    <th>Capacite</th>
                                    <th>Prix mensuel</th>
                                    <th class="text-center">Actions</th>
                                </tr>
                            </thead>
                            <tbody id="myTable">

                            @if($fraisActivations)
                                @foreach($fraisActivations as $fraisActivation)
                                    <tr>
                                        <td> {{$fraisActivation->designation}} </td>
                                        <td> {{$fraisActivation->objet}}       </td>
                                        <td> {{$fraisActivation->capacite->capacite}}   </td>
                                        <td> {{$fraisActivation->prix .' F CFA'}}       </td>
                                        <td>
                                            <a href="{{route('facture.frais_activation.details',$fraisActivation->id)}}" title="Afficher les details" class="btn btn-sm btn-info px-2" style="border-radius: .5rem;">
                                                <i class="fa fa-eye"></i>
                                            </a>

                                            @if($fraisActivation->objet == null)
                                            <a href="{{route('facture.frais_activation.create',$fraisActivation->id)}}" title="Générer la facture" class="btn btn-sm btn-info px-2" style="border-radius: .5rem;">
                                                <i class="fa fa-file-pdf-o"></i>
                                            </a>
                                            @else
                                           <a href="{{route('facture.frais_activation.pdf',$fraisActivation->id)}}" title="Générer la facture" class="btn btn-sm btn-info px-2" style="border-radius: .5rem; background-color:green; border-color:green;">
                                                <i class="fa fa-file-pdf-o"></i>
                                            </a>
                                            @endif
                                        </td>

                                    </tr>
                                @endforeach
                            @endif
                            </tbody>
                        </table>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection

@section('scripts')
    <script>
        $(document).ready(function(){
       $("#myInput").on("keyup", function() {
           var value = $(this).val().toLowerCase();
           $("#myTable tr").filter(function() {
           $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
           });
       });
   });


   toastr.success('Facture en vert','Facture déjà générée', [timeOut=>5000]) ;
    </script>
@endsection
