<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="{{asset('css/esmFacture.css')}}">
    <title>Facture</title> 
<style>
    

</style>
</head>
<body>

    <div class="bloc">

    <center><h3>SOCIETE EN LIQUIDATION</h3></center>
            <center><h3>DECLARATION DE VERSEMENT</h3></center>       
            <center><h4> Date limite de payement : 15/05/2019 </h4></center>     
            
        <table id="t1">
            
                
                    <tr>
                        <td id="NF"><strong>No Facture     <span class="left">2000008800</span></strong> </td>
                        <td id="MF"><strong>Montant Facturé </strong> </td>
                    </tr>
                    <tr>
                        <td id="NC"><strong>No Client   <span class="left">88299768</span> </strong>  </td>
                        <td id="remarque">Joindre ce coupon <br> au mandat ou chèque <br> ou règlement</td>
                        <td id="DT"><strong>Dont TVA</strong></td>
                    </tr> 
                    <tr>
                        <td id="MOOV"><strong>MOOV </strong> <span id="Pays" >Bénin</span> <br> <strong class="left">COTONOU<span></span></strong></td>
                        <td id="Somme"><strong>Somme due FCFA DB</strong></td>
                    </tr>

                    <tr>
                            <td id="sommev"><strong>Somme versée <div id="total"></div></strong></td>
                    </tr>
                    <tr>
                        <td id="INT"><strong>INT2001</strong></td>
                        <td id="code"><strong>4550417</strong></td>
                    </tr>
                
           
                
            
        </table>
        <div id="foot"><strong id="foot1">Détacher ici</strong></div>
        <div id="Partie2">
             <h4 id="delai">Période du ...........     au ............. </h4>
             <table>
                 <tr>
                     <td><strong>Opérateur</strong></td>
                     <td class="col2">MOOV INT2001</td>
                     <td class="col3"><strong id="SC">Situation Comptable</strong></td>
                     
                 </tr>
                 <tr>
                     <td><strong>No Compte</strong></td>
                     <td class="col2">4550417</td>
                     <td class="col3"><strong >Ancien solde    DB</strong></td>
                     <td class="col4"><strong>1 935 546 726</strong></td>
                 </tr>
                 <tr>
                     <td><strong>No Facture</strong></td>
                     <td class="col2"><strong>2000008800</strong></td>
                     <td class="col3"><strong>Versement       DB</strong></td>
                     <td class="col4"><strong>0</strong></td>
                 </tr>
                 <tr>
                     <td><strong>No Client</strong></td>
                     <td class="col2">88299768</td>
                     <td class="col3"><strong>Montant Facturé</strong></td>
                     <td class="col4"><strong>153 960</strong></td>
                 </tr>
                 <tr>
                     <td><strong>No IFU</strong></td>
                     <td class="col2">42009017933914</td>
                     <td class="col3" ><strong>Somme due</strong></td>
                     <td class="col4"><strong>1 935 700 686</strong></td>
                 </tr>
                 <tr>
                     <td><strong>RCCM</strong></td>
                     <td class="col2">RB/COTONOU/09 B 4399</td>
                 </tr>
                 <tr>
                     <td><strong>TVA</strong></td>
                     <td class="col2">18%</td>
                 </tr>
             </table>

              <table>
                  <thead id="thead">
                      <th id="Trafic">TRAFFIC DE L'OPERATEUR MOOV</th>
                  </thead>
              </table>

              <table id="table">
                  <tr class="tr1">
                      <th id="empty"></th>
                  </tr>
                  <tr class="tr1">
                      <th class="tr2">Type de trafic</th>
                      <th class="tr2">Volume (mn)</th>
                      <th class="tr2">Montant (F CFA)</th>
                  </tr>    
                  <tr class="tr1">
                      <td class="tr3">SORTANT INTERNATIONAL</td>
                      <td id="Vol">653</td>
                      <td id="MontantF">130 474</td>
                  </tr>
                  <tr>
                      <td> <strong>Total HT</strong></td>
                      <td class="final"><strong>130 474</strong></td>
                  </tr>
                  <tr>
                      <td>
                          <strong>TVA</strong>
                      </td>
                      <td class="final">
                          <strong>23485</strong>
                      </td>
                  </tr>
                  <tr>
                      <td></td>
                      <td></td>
                      <td id="Ttal"></td>
                  </tr>
                  <tr>
                      <td><strong>Montant Facturé</strong> </td>
                      <td class="final"><strong>153 960</strong></td>
                  </tr>
              </table>

              <p id="confirm">
                  <strong>Arreté la présente facture à la somme de: FCFA   
                <span class="final">      153 960 Montant TTC</span></strong>
              </p>
        </div>
       



    </div>
        
           
</body> 
</html>