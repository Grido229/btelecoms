@extends('dashboard.layouts.app', ['title' => 'Dashboard'])

@section('content')
<div class="content container-fluid">

					<!-- Page Header -->
	<section class="comp-section">
		<nav aria-label="breadcrumb">
			<ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="{{ url('dashboard/home') }}">Home</a></li>
                <li class="breadcrumb-item"><a href="{{ url('dashboard/factures') }}">Factures</a></li>
				<li class="breadcrumb-item active" aria-current="page">Créer une facture</li>
			</ol>
		</nav>
	</section>
	<!-- /Page Header -->

	<!-- Row -->
	<div class="row">
		<div class="col-sm-12">

			<!-- Custom Boostrap Validation -->
			<div class="card">
				<div class="card-header">
					<h4 class="card-title">Créer une facture</h4>
				</div>
				<div class="card-body">

				</div>
			</div>


		</div>
	</div>
	<!-- /Row -->

				</div>
@endsection
