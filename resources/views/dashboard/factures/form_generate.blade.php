@extends('dashboard.layouts.app', ['title' => 'Dashboard'])

@section('content')
<div class="content container-fluid">

					<!-- Page Header -->
	<section class="comp-section">
		<nav aria-label="breadcrumb">
			<ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="{{ url('dashboard/home') }}">Home</a></li>
                <li class="breadcrumb-item"><a href="{{ url('dashboard/factures') }}">Factures</a></li>
				<li class="breadcrumb-item active" aria-current="page">Créer une facture</li>
			</ol>
		</nav>
	</section>
	<!-- /Page Header -->

	<!-- Row -->
	<div class="row">
		<div class="col-sm-12">

			<!-- Custom Boostrap Validation -->
			<div class="card">
				<div class="card-header">
					<h4 class="card-title">Générer une facture</h4>
				</div>
                <div class="card-body">
					<form method="POST" action="{{ route('facture.update',$id) }}">
                        @csrf

                            <div class="col-md-6 mb-3">
                                <label for="facture_type">Type Facture<span style="color:red"></span></label>
                                <select style="font-size : 18px;" class="form-control" name="facture_type" required>
                                    <option disabled selected>Sélectionnez le type de facture</option>
                                    <option value="Mensuelle">Mensuelle</option>
                                    <option value="Trimestrielle"> Trimestrielle</option>
                                    <option value="Semestrielle"> Semestrielle</option>
                                    <option value="Annuelle">Annuelle</option>
                                </select>
                                @error('facture_type')
                                    <span class="text-danger" role="alert">
                                        <strong>Vous devez sélectionner le type du facture.</strong>
                                    </span>
                                @enderror
                            </div>


                    {{-- =========================== Champs à renseigers ============================ --}}

                    <div class="form-row">

                            <div class="col-md-6 mb-3">
                                <label for="date_limite">Date limite de paiement<span style="color:red"></span></label>
                                <input type="date" class="form-control" id="date_limite" name="date_limite" >
                            </div>
                            <div class="col-md-6 mb-3">
                                <label for="client_name">Numero du bordereau<span style="color:red"></span></label>
                                <select style="font-size : 18px;" class="form-control" name="num_bord" required>
                                     <option disabled selected>Sélectionnez le numero du bordereau</option>
                                        @foreach ($bordereaux as $bordereau)
                                            <option value="{{$bordereau->id}}">{{$bordereau->num_bordereau}}</option>
                                        @endforeach
                                </select>
                            </div>

                        </div>

                        <div class="form-row">
                            <div class="col-md-6 mb-3">
                                <label for="start_date">Date Début<span style="color:red"></span></label>
                                <input type="date" class="form-control" id="start_date" name="start_date" required>
                            </div>

                            <div class="col-md-6 mb-3">
                                <label for="end_date">Date Fin<span style="color:red"></span></label>
                                <input type="date" class="form-control" id="end_date" name="end_date" required>
                            </div>
                        </div>

                        <div class="form-row">

                            <div class="col-md-6 mb-3">
                                <label for="client_name">Nom du signataire<span style="color:red"></span></label>
                                <select style="font-size : 18px;" class="form-control" name="signataires_id" required>
                                     <option disabled selected>Sélectionnez le nom du signataire</option>
                                     @foreach ($signataires as $signataire)
                                            <option value="{{$signataire->id}}">{{$signataire->fullname}}</option>
                                     @endforeach
                                </select>
                                @error('signataires_id')
                                    <span class="text-danger" role="alert">
                                        <strong>{{$message}}</strong>
                                    </span>
                                @enderror
                            </div>

                        </div>

                        <div>
                            {{-- <a href="{{ url('dashboard/clients') }}" style="margin-right: 10px" class="btn btn-primary">Annuler</a> --}}
                            <button type="submit" class="btn btn-success">
                                Générer
                            </button>
                        </div>
					</form>
				</div>
			</div>
          </div>


	</div>
	<!-- /Row -->

				</div>
@endsection

@section('scripts')
   @endsection
