@extends('layouts.app')

@section('content')
    <form method="POST" action="{{ route('login.store') }}"> 
        @csrf

        @error('fullname')
            <span class="invalid-feedback" role="alert">
                <strong>Nom d'utilisateur ou mot de passe incorrect.</strong>
            </span>
        @enderror

        <div class="form-style-agile">
            <label>
                <i class="fas fa-user"></i>
                Code d'utilisateur
            </label>
            <select onchange="recupval(this)" name="fullname" class="" id="titre">
                <option value="" selected>Choisir le nom d'utilisateur</option>
                @foreach ($users as $user)
                    <option value="{{$user->fullname}}">{{$user->code_users}}</option>
                @endforeach
            </select>
        </div>

        <div class="form-style-agile">
            <label>
                <i class="fas fa-unlock-alt"></i>
                Mot de passe
            </label>
            <input placeholder="Mot de passe" name="password" type="password" required>
        </div>

        <div id="pass2" style="display:none" class="form-style-agile">
            <label>
                <i class="fas fa-unlock-alt"></i>
                Confirmez le mot de passe
            </label>
            <input placeholder="Mot de passe" name="password_confirmation" type="password" >
        </div>
       
       
        <input type="submit" value="Se Connecter">
    </form>
@endsection 


@section('scripts')
<script src="{!!asset('/js/jquery-1.11.1.min.js')!!}"></script> 

<script> 

    function recupval(obj){
      $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN' : $('meta[name="csrf-token"]').attr('content')
        }

      });
 
      $.ajax({
        url  : '{{route('ajaxrequest')}}',   
        type :'POST' , 
        data : {"fullname" : $('#titre').val()},
        success : function(data){
            console.log(data.message);
            if (data.message == 'Has not a password') {
                $('#pass2').show();
            }
            else{
                $('#pass2').hide(); 
            }
        },
        error : function(data){
                console.log('error');  
        }

      });
   
   
   
    } 



</script>


@endsection('scripts')
