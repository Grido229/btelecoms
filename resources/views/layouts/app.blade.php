<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

     {{-- CSRF Token --}}
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'BENIN TELECOM COMMANDE') }} | Login</title>

     {{-- Styles --}}
    <link href="{{ asset('css/login-style.css') }}" rel="stylesheet">
	<link href="{{ asset('css/styleperso.css') }}" rel="stylesheet" >
    <link href="{{ asset('css/fontawesome-all.css') }}" rel="stylesheet" >
    {{-- <meta name="csrf-token" content="{{ csrf_token() }}" /> --}}


	<link href="{{ asset('assets/images/btcom.png') }}" rel="shortcut icon" type="image/x-icon">

    {{-- web-fonts --}}
	<link href="//fonts.googleapis.com/css?family=Encode+Sans+Condensed:100,200,300,400,500,600,700,800,900&amp;subset=latin-ext,vietnamese"
        rel="stylesheet">
	<script>
		addEventListener("load", function () {
			setTimeout(hideURLbar, 0);
		}, false);

		function hideURLbar() {
			window.scrollTo(0, 1);
		}
	</script>
</head>
<body>
    <div id="app">
         {{-- title --}}
        <h1>
            BENIN TELECOMS
        </h1>
         {{-- //title --}}

         {{-- content --}}
        <div class="container">
            <div id="clouds">
                <div class="cloud x1"></div>
                 {{-- Time for multiple clouds to dance around --}}
                <div class="cloud x2"></div>
                <div class="cloud x3"></div>
                <div class="cloud x4"></div>
                <div class="cloud x5"></div>
            </div>
             {{-- content form --}}

            <div class="sub-main-w3">
                @yield('content')
            </div>

             {{-- //content form --}}
        </div>
         {{-- //content --}}

         {{-- copyright --}}
        <div class="footer">
            <h2>&copy; 2019 Bénin Telecoms espace commande. tout droit reservé | Design by
                <a href="#"></a>
            </h2>
        </div>
    </div>
    @yield('scripts')
</body>
</html>
