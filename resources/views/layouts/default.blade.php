<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        {{-- CSRF Token --}}
        <meta name="csrf-token" content="{{ csrf_token() }}">

        <title>{{ config('app.name', 'BENIN TELECOM COMMANDE') }} | {{ $title }}</title>

        {{-- Icon --}}
        <link href="{{ asset('assets/images/btcom.png') }}" rel="shortcut icon" type="image/x-icon">

        {{-- Styles --}}
        <link href="{{ asset('css/dashboard-styles.css') }}" rel="stylesheet">
        <link href="{{ asset('css/bootstrap/bootstrap.min.css') }}" rel="stylesheet">
        <link href="{{ asset('css/font-awesome.min.css') }}" rel="stylesheet" >
        <link href="{{ asset('css/datepicker3.css') }}" rel="stylesheet" >
        

		<!-- Datatables CSS -->
		<link rel="stylesheet" href="{{ asset('plugins/datatables/datatables.min.css') }}">

        {{-- web-fonts --}}
        <link href="https://fonts.googleapis.com/css?family=Montserrat:300,300i,400,400i,500,500i,600,600i,700,700i" rel="stylesheet">

        <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
    
    </head>

    <body>
        <nav class="navbar navbar-custom navbar-fixed-top" role="navigation">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#sidebar-collapse"><span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>

            <div class="container-fluid">
                <div class="navbar-header">

                    <a class="navbar-brand" href="Menu.html"><span>BENIN</span> TELECOMS</a>
                    <ul class="nav navbar-top-links navbar-right">
                        <li class="dropdown">
                            <a class="dropdown-toggle count-info" data-toggle="dropdown" href="#">
                                <em class="fa fa-envelope"></em><span class="label label-danger">15</span>
                            </a>
                            <ul class="dropdown-menu dropdown-messages">
                                <li>
                                    <div class="dropdown-messages-box">
                                    <a href="profile.html" class="pull-left">
                                        <img alt="image" class="img-circle" src="http://placehold.it/40/30a5ff/fff">
                                    </a>
                                </li>
                            </ul>
                        </li>

                        <li class="dropdown">
                            <a class="dropdown-toggle count-info" data-toggle="dropdown" href="#">
                                <em class="fa fa-bell"></em><span class="label label-info">5</span>
                            </a>
                        </li>
                    </ul>
                </div>
            </div>

            <div id="sidebar-collapse" class="col-sm-3 col-lg-2 sidebar">
                <div class="profile-sidebar">
                    <div class="profile-userpic">
                        <img src="{{ asset('assets/images/btcom.png') }}" height="100" width="50" >
                    </div>
                    <div class="profile-usertitle">
                        <div class="profile-usertitle-name">Nom d'utilisateur</div>
                        <div class="profile-usertitle-status"><span class="indicator label-success"></span>En ligne</div>
                    </div>
                    <div class="clear"></div>
                </div>

                <div class="divider"></div>

                <ul class="nav menu">
                    <li>
                        <a href="{{ url('dashboard/home') }}"><em class="fa fa-home">&nbsp;</em><strong>
Acceuil</strong>
</a>
                    </li>
                    <li>
                        <a href="{{ url('dashboard/bons') }}"><em class="fa fa-list-alt">&nbsp;</em><strong>
Demande de service</strong>
</a>
                    </li>
                    <li>
                        <a href="{{ url('dashboard/pv') }}"><em class="fa fa-bar-chart">&nbsp;</em><strong>
Procès verbaux</strong>
</strong>
</a>
                    </li>
                    <li>
                        <a href="{{ url('dashboard/users') }}"><em class="fa fa-users">&nbsp;</em><strong>

Utilisateurs</strong>
</a>
                        <a href="{{ url('dashboard/clients') }}"><em class="fa fa-users">&nbsp;</em>Clients</a>
                    </li>
                    <li>
                    </li>
                    <li>
                        <a href="{{ url('dashboard/factures') }}"><em class="fa fa-navicon">&nbsp;</em> <strong>
Facturation</strong>
</a>
                    </li>
                    <li>
                        <a href="{{ url('dashboard/recouvrements') }}"><em class="fa fa-recycle">&nbsp;</em><strong>
 Recouvrement</strong>
</a>
                    </li>
                    <li>
                        <a href="{{ url('dashboard/admin') }}"><em class="fa fa-user-circle" aria-hidden="true">&nbsp;</em> Administration</a>
                    </li>
                    <li>
                        <a href="{{ route('logout') }}"
                            onclick="event.preventDefault();
                                            document.getElementById('logout-form').submit();">
                            <em class="fa fa-power-off">&nbsp;</em>
                            Déconnexion
                        </a>

                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                            @csrf
                        </form>
                    </li>
                </ul>
            </div>
        </nav>

        <div class="col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2">
            @yield('content')
        </div>

		<!-- jQuery Core JS -->
        <script src="{{ asset('js/jquery-3.2.1.min.js') }}"></script>
        {{-- <script src="{{ asset('js/jquery-1.11.1.min.js') }}"></script> --}}

		<!-- Bootstrap Core JS -->
        <script src="{{ asset('js/bootstrap.min.js') }}"></script>

        <script src="{{ asset('js/chart.min.js') }}"></script>
        <script src="{{ asset('js/chart-data.js') }}"></script>
        <script src="{{ asset('js/easypiechart.js') }}"></script>
        <script src="{{ asset('js/easypiechart-data.js') }}"></script>
        <script src="{{ asset('js/bootstrap-datepicker.js') }}"></script>

		<!-- Datatables JS -->
		<script src="{{ asset('plugins/datatables/jquery.dataTables.min.js') }}"></script>
        <script src="{{ asset('plugins/datatables/datatables.min.js') }}"></script>

        <!-- Custom JS -->
        <script src="{{ asset('js/custom.js') }}"></script>
    </body>
</html>
