<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|

*/
// Back route

use App\Http\Controllers\SendMailController;
use App\Mail\SendMail;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Validator;




Route::group(['prefix' => 'dashboard'], function () {
    //Bons de commandes

    Route::group(['prefix' => 'bons'], function () {
        Route::get('/', 'BonController@index')->name('dashboard.bons');
        Route::get('{id}/detailsBon',    'BonController@detailsBon');
        Route::get('pdfBon', 'BonController@pdfBon');
        Route::get('pdfDetail', 'BonController@pdfDetail');
        Route::get('create', 'BonController@create');
        Route::post('create', 'BonController@store')->name('create.store');
        Route::get('{id}/edit', 'BonController@edit');
        Route::post('{id}/edit', 'BonController@update');
        Route::get('{id}/delete', 'BonController@delete');

        Route::get('/mail','SendMailController@send')->name('mail.send');

        Route::post('/ajaxRequestMontant' , 'BonController@ajaxRequest')->name('ajaxrequestMontant');
        Route::post('/ajaxRequestMontantService','BonController@ajaxRequestMontantService');

        Route::post('/ajaxRequestAfficherForm' , 'BonController@ajaxRequestAfficherForm')->name('ajaxrequest.form');
        Route::post('/ajaxRequestAfficherMont' , 'BonController@ajaxRequestAfficherMont')->name('ajaxrequest.mont');

    });

    //Procès verbaux
    Route::group(['prefix' => 'pv'], function () {
        Route::get('/', 'PvController@index')->name('dashboard.pv');
        Route::get('{id}/detail', 'PvController@detail');
        Route::get('detailColoc', 'PvController@detailColoc');
        Route::get('pdfPv', 'PvController@pdfPv');
        Route::get('{id}/pdfDetail', 'PvController@pdfDetail');
        Route::get('{id}/pdfColoc', 'PvController@pdfDetail');
        Route::get('create', 'PvController@create')->name('pv.create');
        Route::get('colocalisation', 'PvController@colocalisation');
        Route::get('trafique', 'PvController@trafique');
        Route::post('create', 'PvController@store');
        Route::get('{id}/edit', 'PvController@edit');
        Route::post('{id}/edit', 'PvController@update');
        Route::get('{id}/delete', 'PvController@delete');
        Route::get('{id}/edit_grade','PvController@gradeForm')->name('pv.grade');
        Route::post('edit_grade/{id}','PvController@grade')->name('pv.update');

        Route::post('/ajaxRequestMontant' , 'PvController@ajaxRequest')->name('ajaxrequestgrade');
    });

    //Validations
    Route::group(['prefix' => 'validations'], function () {
        Route::get('dcm', 'ValidationsController@dcm')->name('dashboard.validations.dcm')->middleware('status'); 
        Route::post('dcm/filter', 'ValidationsController@filterDcm');
        Route::post('dcm/validate/{id}', 'ValidationsController@validateDcmCommande');
        Route::get('dpm', 'ValidationsController@dpm')->name('dashboard.validations.dpm');
        Route::post('dpm/filter', 'ValidationsController@filterDpm');
        Route::post('dpm/validate/{id}', 'ValidationsController@validateDpmCommande');


        Route::get('dpm/details/{id}' , 'ValidationsController@validateDpmDetails')->name('dpm.datails');

        Route::get('dcm/details/{id}' , 'ValidationsController@validateDcmDetails')->name('dcm.datails');

        Route::get('/mail/dpm','SendMailController@sendDpm')->name('mail.dpm.send');

    });

    //Users
    Route::group(['prefix' => 'users'] , function () {
        Route::get('/', 'UserController@index')->name('dashboard.users');
        Route::get('create', 'UserController@create');
        Route::get('pdfUsers', 'UserController@pdfUsers');
        Route::post('create', 'UserController@store');
        Route::get('profil', 'UserController@profil');
        Route::post('profil', 'UserController@update_profil');
        Route::post('{id}/edit', 'UserController@update');
        Route::get('{id}/edit', 'UserController@edit');
        Route::get('{id}/delete', 'UserController@delete');
    });


    //Clients
    Route::group(['prefix' => 'clients'], function () {
        Route::get('/', 'ClientController@index')->name('dashboard.clients');
        Route::get('pdfClients', 'ClientController@pdfClients');
        Route::get('create', 'ClientController@create');
        Route::post('create', 'ClientController@store');
        Route::get('{id}/edit', 'ClientController@edit');
        Route::post('{id}/edit', 'ClientController@update');
        Route::get('{id}/delete', 'ClientController@delete')->name('client.delete');
    });

    //Factures
    Route::group(['prefix' => 'factures'], function () {
        Route::get('/', 'InvoiceController@index');
        Route::get('/create/{id}', 'InvoiceController@create')->name('factures.create');
        Route::post('create', 'InvoiceController@store');
        Route::get('/details/{id}' , 'InvoiceController@details')->name('factures.details');
        Route::post('{id}/add' , 'InvoiceController@store');
        Route::get('{id}/edit', 'InvoiceController@edit');
        Route::post('{id}/edit', 'InvoiceController@update');
        Route::get('{id}/delete', 'InvoiceController@delete');
        Route::get('/bordereau','InvoiceController@bordereauForm')->name('bordereau.create');
        Route::post('/bordereau_create','InvoiceController@bordereau')->name('bordereau.store');
        Route::get('/list_bordereau','InvoiceController@bordereauList')->name('bordereau.list');
        Route::get('/generate/{id}','InvoiceController@generateForm')->name('facture.generate');
        Route::post('/facture/generate/{id}','InvoiceController@factureUpdate')->name('facture.update');

        Route::get('/generateFacture/{id}','InvoiceController@generateFacture')->name('facture.generate.generate');

        Route::get('/signataire','InvoiceController@signataireForm')->name('signataire.create');
        Route::post('/signataire/store','InvoiceController@signataire')->name('signataire.store');
        Route::get('/signataire/list','InvoiceController@signataireList')->name('signataire.list');


        Route::get('/frais_activation','InvoiceController@frais_activation')->name('facture.frais_activation.list');
        Route::get('/frais_activation_create/{id}','InvoiceController@frais_activation_create')->name('facture.frais_activation.create');
        Route::post('/frais_activation_store/{id}','InvoiceController@frais_activation_store')->name('facture.frais_activation.store');
        Route::get('/frais_activation_details/{id}','InvoiceController@frais_activation_details')->name('facture.frais_activation.details');
        Route::get('/frais_activation_pdf/{id}','InvoiceController@frais_activation_pdf')->name('facture.frais_activation.pdf');

        Route::post('/ajaxRequestMontant' , 'InvoiceController@ajaxRequest');
        Route::get('/factures_synthese/list', 'InvoiceController@factureGlobale')->name('factureGlobale.list');
        Route::get('/facture_synthese/create' , 'InvoiceController@createFactureGlobale')->name('facture_globale.create');
        Route::post('/store/facture_synthese','InvoiceController@factureGlobaleStore')->name('facture_globale.store'); 
        Route::get('/{id}/facture_synthese/generate' , 'InvoiceController@generateFactureGlobale')->name('facture_globale.generate');
        Route::get('/{id}/detail','InvoiceController@detailFactureGlobale')->name('factureGlobale.detail');

    });


    //listes divers
    Route::group(['prefix' => 'list'], function () {
        Route::get('/contrats', 'ListController@contrats');
        Route::get('/impression/contrats' , 'ListController@Imp_contrats')->name('impression.contrats');

        Route::get('/deserte', 'ListController@deserte')->name('list.deserte');
        Route::get('/deserte/details/{id}' , 'ListController@detailsDeserte')->name('details.deserte');
        Route::get('/deserte/create' , 'ListController@createDeserte')->name('create.deserte');
        Route::post('/deserte/store' , 'ListController@storeDeserte')->name('store.deserte');
        Route::get('/impression/deserte/{id}' , 'ListController@Imp_deserte')->name('impression.deserte');
        Route::get('/impression/desertes' , 'ListController@Imp_desertes')->name('impression.desertes');
        Route::get('/suppression/deserte/{id}' , 'ListController@Supp_deserte')->name('suppression.deserte');
        Route::get('/edit/deserte/{id}' , 'ListController@Edit_deserte')->name('edit.deserte');
        Route::post('/update/deserte/{id}' , 'ListController@update_deserte')->name('update.deserte');

        Route::get('/service', 'ListController@service')->name('list.service');
        Route::get('/service/details/{id}' , 'ListController@detailsService')->name('details.service');
        Route::get('/impression/services' , 'ListController@Imp_services')->name('impression.services');
        Route::get('/etat', 'ListController@etat')->name('list.etat');
        Route::get('/etat/details/{id}', 'ListController@etatDetails')->name('list.etat.detail');

    });

    //Recouvrements        Route::get('{id}/delete', 'RecouvrementController@delete');

    Route::group(['prefix' => 'recouvrements'], function () {
    Route::get('/', 'RecouvrementController@index');
    Route::get('create', 'RecouvrementController@create');
    Route::post('create', 'RecouvrementController@store');
    Route::get('{id}/delete', 'RecouvrementController@delete');
    Route::get('{id}/edit', 'RecouvrementController@edit');
    Route::post('{id}/edit', 'RecouvrementController@update');

    });


});


//Auth::routes();
Route::get('/login', 'Custom_LoginController@create')->name('login');
Route::post('/login_store','Custom_LoginController@loginForm')->name('login.store');
Route::post('/logout', 'Custom_LoginController@logout')->name('logout');
Route::post('/ajaxRequest','Custom_LoginController@ajaxRequest')->name('ajaxrequest');
Route::get('/dashboard/home', 'HomeController@index')->name('dashboard.home');




