<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CommandeProtocole extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $guarded = ['id'];

    /**
     * Get the BonCommande that owns the CommandeProtocole.
     */
    public function bon()
    {
        return $this->belongsTo('App\BonCommande');
    }


    public function facture(){
        return $this->belongsTo('App\Facture' , 'id', 'pv_id');
    }
    public function sfs(){
        return $this->hasOne('App\Sfs'  , 'id' , 'sfs_id');
    }

    public function ssfs(){
        return $this->hasOne('App\Ssfs'  , 'id' , 'ssfs_id');
    }

    public function zone(){
        return $this->hasOne('App\Zone' , 'id' , 'zone_id' );
    }

    public function volume(){
        return $this->hasOne('App\Volume'  , 'id' , 'volume_id');
    }

    public function Distance(){
        return $this->hasOne('App\Distances'  , 'id', 'distance_id');
    }

    public function capacite(){
        return $this->hasOne('App\Capacites'  , 'id' , 'capacite_id');
    }
    public function facture_globale(){
        return $this->belongsTo('App\FactureGlobale' , 'id');
    }
}
