<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Fs extends Model
{
    protected $table = 'fs';
    protected $guarded = ['id' , 'code'];

    public function sfs(){
    return $this->belongsTo('App\Sfs' , 'id');
    }

}
