<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Desertes extends Model
{
    protected $table = 'deserte';
    protected $guarded = ['id'];

    public function client(){
        return $this->hasOne('App\Client' , 'id' , 'clients_id');
    }

    public function signataire(){
        return $this->hasOne('App\Signataire','id','signataires_id');
    }

}
