<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CommandeDetail extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'description',
        'capacity',
        'mensuality',
        'bon_id'
    ];

    /**
     * Get the BonCommande that owns the CommandeDetail.
     */
    public function bon()
    {
        return $this->belongsTo('App\BonCommande');
    }
}
