<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class FactureGlobale extends Model
{
    //
    protected $table = 'facture_globale' ;
    protected $guarded = ['id'];

    public function facture(){
        return $this->hasOne('App\Facture' , 'id' , 'factures_capacite_id');
    }

    public function pv(){
        return $this->hasOne('App\CommandeProtocole' , 'id' , 'pv_id');
    }

    public function signataire(){
        return $this->hasOne('App\Signataire','id','signataires_id');
    }
}
