<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class OffresType extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
    ];

    /**
     * Get the CommandeProtocole for the BonCommande.
     */
    public function offre()
    {
        return $this->belongsTo('App\CommandeDetail');
    }
}
