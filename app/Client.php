<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Client extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'email',
        'tel',
        'city',
        'adress',
        'enterprise_name',
        'client_type',
    ];

    /**
     * Get the BonCommande for the client.
     */
    public function bons() 
    {
        return $this->hasMany('App\BonCommande' , 'client_id' , 'id');
    }

    /**
     * The type that belong to the client.
     */
    public function type()
    {
        return $this->hasOne('App\ClientsType');
    }
    
    public function deserte(){
        return $this->belongsTo('App\Desertes' , 'id');
    }

    public function faris_activation(){
        return $this->belongsTo('App\FraisActivation' , 'id');
    }
}
