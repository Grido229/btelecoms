<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class RecouvrementController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(Request $request)
    {
        $request->user()->authorizeRoles(['ADMIN']);
        $is_dcm = $request->user()->hasRole('AGENT DCM');
        $is_dpm = $request->user()->hasRole('AGENT DPM');
        $_url = 'recouvrement';

        return view('dashboard.recouvrements.list', compact('is_dcm', 'is_dpm', '_url'));
    }
}
