<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\User;
use DB;
use App\BonCommande;



class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index(Request $request)
    {
        $request->user()->authorizeRoles(['ADMIN', 'AGENT DPM', 'AGENT DCM']);
        $is_dcm = $request->user()->hasRole('AGENT DCM');
        $is_dpm = $request->user()->hasRole('AGENT DPM');
        $is_dcm_counter = DB::table('bon_commandes')->where('is_validate_by_dcm', '=', 0)->count();
        $is_dpm_counter = DB::table('bon_commandes')->where('is_validate_by_dpm', '=', 0)->count();
        $bon = BonCommande::all();
        $_url = 'home';
        

        

        return view('dashboard.home', compact('bon','is_dcm', 'is_dpm', '_url','is_dcm_counter','is_dpm_counter'));
    }
}
