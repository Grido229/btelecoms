<?php

namespace App\Http\Controllers;

use App\Role;
use App\User;
use Auth;
use Illuminate\Http\Request;
use Barryvdh\DomPDF\Facade as PDF;
use  Dompdf\Options;
use Dompdf\Helpers;
use Dompdf\Exception\ImageException;

class UserController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(Request $request)
    {
        $request->user()->authorizeRoles(['ADMIN']);
        $is_dcm = $request->user()->hasRole('AGENT DCM');
        $is_dpm = $request->user()->hasRole('AGENT DPM');
        $_url = 'users';
        $users = User::all();
        //dd($users);
        foreach ($users as $user) {
            $role = $user->roles()->get();
            $user->role = $role[0]->name;
        }

        return view('dashboard.users.list', compact('users', 'is_dcm', 'is_dpm', '_url'));
    }




    public function pdfUsers(Request $request)
    {

        $path = 'assets/images/btcom.png';
        asset('assets/images/btcom.png');
        $type = pathinfo($path, PATHINFO_EXTENSION);
        $data = file_get_contents($path);
        $base64 = 'data:image/' . $type . ';base64,' . base64_encode($data);


        $request->user()->authorizeRoles(['ADMIN']);
        $users = User::all();

        foreach ($users as $user) {
            $role = $user->roles()->get();
            $user->role = $role[0]->name;
        }
        $options = new Options();
        $options->set('isRemoteEnabled', true);
        $pdf = new PDF($options);

        $pdf = \PDF::loadView('dashboard.users.pdfUsers', compact('users', 'base64'));
        return $pdf->stream('Liste_des_utilisateurs.pdf', compact('users', 'base64'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $request->user()->authorizeRoles(['ADMIN']);
        $is_dcm = $request->user()->hasRole('AGENT DCM');
        $is_dpm = $request->user()->hasRole('AGENT DPM');
        $_url = 'new-user';
        $roles = Role::all();

        return view('dashboard.users.create', compact('roles', 'is_dcm', 'is_dpm', '_url'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->user()->authorizeRoles(['ADMIN']);
        $request->validate([
            'lastName' => 'required|string|max:225',
            'firstName' => 'required|string|max:225',
            'role_id'  => 'required|string',
            'email'    =>   'email',
        ]);
        $code_lastName  = substr($request->lastName , 0 , 3) ;
        $code_firstName = substr($request->firstName , 0 , 3) ;
        $code_int = random_int(0 , 9);
        $code_users = $code_firstName.''.$code_lastName.''.$code_int ;
         //dd($code_firstName.''.$code_lastName.''.$code);
        $user = new User();
        $user->fullname = $request->firstName.' '.$request->lastName;
        $user->adresse = $request->adresse;
        $user->email = $request->email;
        $user->tel = $request->tel;
        $user->code_users = $code_users ; 
        $user->service = $request->service;

        $user->save();

        $role = Role::find($request->role_id);
        $user->roles()->attach($role);

        return redirect()->route('dashboard.users');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, $id)
    {
        $request->user()->authorizeRoles(['ADMIN']);
        $is_dcm = $request->user()->hasRole('AGENT DCM');
        $is_dpm = $request->user()->hasRole('AGENT DPM');
        $_url = 'users';
        $user = User::findOrFail($id);
        $role = $user->roles()->get();
        $user->role_id = $role[0]->id;
        $roles = Role::all();

        return view('dashboard.users.edit', compact('user', 'roles', 'is_dcm', 'is_dpm', '_url'));
    }

    public function profil(Request $request)
    {
        $request->user()->authorizeRoles(['ADMIN', 'AGENT DPM', 'AGENT DCM']);
        $is_dcm = $request->user()->hasRole('AGENT DCM');
        $is_dpm = $request->user()->hasRole('AGENT DPM');
        $_url = 'users';
        $user = Auth::user();

        return view('dashboard.users.profil', compact('user', 'is_dcm', 'is_dpm', '_url'));
    }

    public function update_profil(Request $request)
    {
        $request->user()->authorizeRoles(['ADMIN', 'AGENT DPM', 'AGENT DCM']);

        $user = Auth::user();

        if ($request->fullname != null) {
            $user->fullname = $request->fullname;
        }


        if (isset($request->change_password)) {
            $request->validate([
                'old_password' => 'required|string|max:225',
                'password' => 'required|string|max:225|confirmed|min:8',
            ]); /*@if( $facture->pv->bon->contrat)
            <td>{{ $facture->pv->bon->contrat->num_contrat}}</td>
            @else
            <td> Pas de contrats</td>
            @endif*/

            // dd(bcrypt($request->old_password) == Auth::user()->password);

            if (bcrypt($request->old_password) == Auth::user()->password) {
                $user->password = bcrypt($request->password);
            } else {
                return redirect()->back()->withErrors(['old_password' => 'Mot de passe actuel incorrect.']);
            }
        }

        $user->save();

        return redirect()->back();
    }

    public function ajaxRequest(Request $request){
        $user = User::where('fullname',$request->fullname)->first();
        $message = '' ;
         if ($user->password == null) {
            $message = 'Has not password' ;
         }

         else{
            $message = 'Has  password' ;
         }

         return response()->json(['message' => 'yes']);


    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->user()->authorizeRoles(['ADMIN']);
        $request->validate([
            'fullname' => 'required|string|max:225',
            'role_id' => 'required|string',
        ]);

        $user = User::findOrFail($id);

        if ($request->fullname) {
            $user->fullname = $request->fullname;
        }

        if ($request->password) {
            $user->password = bcrypt($request->password);
        }

        if ($request->adresse) {
            $user->adresse = $request->adresse;
        }else{
            $user->adresse = '---';
        }

        if ($request->email) {
            $user->email = $request->email;
        }else{
            $user->email = '---';
        }

        if ($request->tel) {
            $user->tel = $request->tel;
        }else{
            $user->tel = '---';
        }

        if ($request->service) {
            $user->service = $request->service;
        }else{
            $user->service = '---';
        }

        $user->save();

        if ($request->role_id) {
            $role = $user->roles()->get();
            $user->roles()->detach($role);

            $role = Role::find($request->role_id);
            $user->roles()->attach($role);
        }

        return redirect()->route('dashboard.users');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function delete(Request $request, $id)
    {
        //dd($id);
        $request->user()->authorizeRoles(['ADMIN']);
        $user = User::findOrFail($id);
        $user->delete();
        return redirect()->route('dashboard.users');
    }
}
