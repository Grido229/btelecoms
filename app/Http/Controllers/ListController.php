<?php

namespace App\Http\Controllers;

use App\Client;
use Illuminate\Http\Request;
use App\Desertes;
use App\Contrat;
use App\CommandeProtocole;
use App\Etat;
use App\Facture;
use App\Frais_acces;
use App\Signataire;
use PDF;
use DateTime;
use  Dompdf\Options;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Validator ;

class ListController extends Controller
{


    public function deserte(Request $request) {
        $request->user()->authorizeRoles(['ADMIN', 'AGENT DPM']);
        $is_dcm = $request->user()->hasRole('AGENT DCM');
        $is_dpm = $request->user()->hasRole('AGENT DPM');
        $_url = 'list_deserte';

        $desertes = Desertes::all();

        return view('dashboard.liste_divers.desertes'  , compact('desertes' , 'is_dcm' , 'is_dpm' , '_url'));
    }

    public function detailsDeserte($id , Request $request){
        $request->user()->authorizeRoles(['ADMIN', 'AGENT DPM']);
        $is_dcm = $request->user()->hasRole('AGENT DCM');
        $is_dpm = $request->user()->hasRole('AGENT DPM');
        $_url = 'list_deserte';
            $deserte = Desertes::where('id' , $id)->first();

        return view('dashboard.liste_divers.details_desertes'  , compact('deserte', 'is_dcm' , 'is_dpm' , '_url'));
    }

    public function createDeserte(Request $request){
        $request->user()->authorizeRoles(['ADMIN', 'AGENT DPM']);
        $is_dcm = $request->user()->hasRole('AGENT DCM');
        $is_dpm = $request->user()->hasRole('AGENT DPM');
        $_url = 'list_deserte';

        $clients = Client::all();
        $signataires = Signataire::all();
        return view('dashboard.liste_divers.create_deserte'  , compact( 'signataires' , 'clients' , 'is_dcm' , 'is_dpm' , '_url'));
    }

    public function storeDeserte(Request $request){
        $request->user()->authorizeRoles(['ADMIN', 'AGENT DPM']);
        $is_dcm = $request->user()->hasRole('AGENT DCM');
        $is_dpm = $request->user()->hasRole('AGENT DPM');
        $_url = 'list_deserte';


        $validator = Validator::make($request->all(),[
            'intitule'         => 'required',
            'type_devis'       => 'required',
            'ref_fact'         => 'required|unique',
            'operateur'        => 'required',
            'signataire_id'   => 'required',

            /*'pU_fourniture_1'  => 'required',
            'pU_fourniture_2'  => 'required',
            'pU_fourniture_3'  => 'required',
            'pU_fourniture_4'  => 'required',
            'pU_fourniture_5'  => 'required',
            'pU_fourniture_6'  => 'required',
            'pU_fourniture_7'  => 'required',
            'pU_fourniture_8'  => 'required',
            'pU_fourniture_9'  => 'required',
            'pU_fourniture_10' => 'required',

            'pU_prestation_1'  => 'required',
            'pU_prestation_2'  => 'required',
            'pU_prestation_3'  => 'required',
            'pU_prestation_4'  => 'required',
            'pU_prestation_5'  => 'required',
            'pU_prestation_6'  => 'required',
            'pU_prestation_7'  => 'required',
            'pU_prestation_8'  => 'required',
            'pU_prestation_9'  => 'required',
            'pU_prestation_10' => 'required',

            'quantite_1'  => 'required',
            'quantite_2'  => 'required',
            'quantite_3'  => 'required',
            'quantite_4'  => 'required',
            'quantite_5'  => 'required',
            'quantite_6'  => 'required',
            'quantite_7'  => 'required',
            'quantite_8'  => 'required',
            'quantite_9'  => 'required',
            'quantite_10' => 'required',*/
        ]);

            if ( $validator->fails() ) {
                return Redirect::back()->withErrors($validator)->withInput();
            } else{



        $deserte = Desertes::latest('id')->first();
            if($deserte == null){
                $ref_devis = '001/DPM/'.date('m').'/'.date('Y');
            }else{
                $ancien_Code = $deserte->ref_devis;
                $table_ancien_code = explode('/DPM/' , $ancien_Code);
                $code = $table_ancien_code[0] + 1;
                $code = sprintf('%03d' , $code);
                $ref_devis = $code.'/DPM/'.date('m').'/'.date('Y');

            }

            $request->pU_fourniture_1  = ($request->pU_fourniture_1 != '') ? $request->pU_fourniture_1 : '0' ;
            $request->pU_fourniture_2  = ($request->pU_fourniture_2 != '') ? $request->pU_fourniture_2 : '0' ;
            $request->pU_fourniture_3  = ($request->pU_fourniture_3 != '') ? $request->pU_fourniture_3 : '0' ;
            $request->pU_fourniture_4  = ($request->pU_fourniture_4 != '') ? $request->pU_fourniture_4 : '0' ;
            $request->pU_fourniture_5  = ($request->pU_fourniture_5 != '') ? $request->pU_fourniture_5 : '0' ;
            $request->pU_fourniture_6  = ($request->pU_fourniture_6 != '') ? $request->pU_fourniture_6 : '0' ;
            $request->pU_fourniture_7  = ($request->pU_fourniture_7 != '') ? $request->pU_fourniture_7 : '0' ;
            $request->pU_fourniture_8  = ($request->pU_fourniture_8 != '') ? $request->pU_fourniture_8 : '0' ;
            $request->pU_fourniture_9  = ($request->pU_fourniture_9 != '') ? $request->pU_fourniture_9 : '0' ;
            $request->pU_fourniture_10  = ($request->pU_fourniture_10 != '') ? $request->pU_fourniture_10 : '0' ;

            $request->pU_prestation_1  = ($request->pU_prestation_1 != '') ? $request->pU_prestation_1 : '0' ;
            $request->pU_prestation_2  = ($request->pU_prestation_2 != '') ? $request->pU_prestation_2 : '0' ;
            $request->pU_prestation_3  = ($request->pU_prestation_3 != '') ? $request->pU_prestation_3 : '0' ;
            $request->pU_prestation_4  = ($request->pU_prestation_4 != '') ? $request->pU_prestation_4 : '0' ;
            $request->pU_prestation_5  = ($request->pU_prestation_5 != '') ? $request->pU_prestation_5 : '0' ;
            $request->pU_prestation_6  = ($request->pU_prestation_6 != '') ? $request->pU_prestation_6 : '0' ;
            $request->pU_prestation_7  = ($request->pU_prestation_7 != '') ? $request->pU_prestation_7 : '0' ;
            $request->pU_prestation_8  = ($request->pU_prestation_8 != '') ? $request->pU_prestation_8 : '0' ;
            $request->pU_prestation_9  = ($request->pU_prestation_9 != '') ? $request->pU_prestation_9 : '0' ;
            $request->pU_prestation_10  = ($request->pU_prestation_10 != '') ? $request->pU_prestation_10 : '0' ;

            if ($request->pU_fourniture_1 == '0' && $request->pU_prestation_1 == '0') {
                $request->quantite_1  = ($request->quantite_1 != '' ) ? $request->quantite_1 : '0' ;
            }else{
                $request->quantite_1  = ($request->quantite_1 != '' ) ? $request->quantite_1 : '1' ;
            }

            if ($request->pU_fourniture_2 == '0' && $request->pU_prestation_2 == '0') {
                $request->quantite_2  = ($request->quantite_2 != '' ) ? $request->quantite_2 : '0' ;
            }else{
                $request->quantite_2  = ($request->quantite_2 != '' ) ? $request->quantite_2 : '1' ;
            }

            if ($request->pU_fourniture_3 == '0' && $request->pU_prestation_3 == '0') {
                $request->quantite_3  = ($request->quantite_3 != '' ) ? $request->quantite_3 : '0' ;
            }else{
                $request->quantite_3  = ($request->quantite_3 != '' ) ? $request->quantite_3 : '1' ;
            }

            if ($request->pU_fourniture_4 == '0' && $request->pU_prestation_4 == '0') {
                $request->quantite_4  = ($request->quantite_4 != '' ) ? $request->quantite_4 : '0' ;
            }else{
                $request->quantite_4  = ($request->quantite_4 != '' ) ? $request->quantite_4 : '1' ;
            }

            if ($request->pU_fourniture_5 == '0' && $request->pU_prestation_5 == '0') {
                $request->quantite_5  = ($request->quantite_5 != '' ) ? $request->quantite_5 : '0' ;
            }else{
                $request->quantite_5  = ($request->quantite_5 != '' ) ? $request->quantite_5 : '1' ;
            }

            if ($request->pU_fourniture_6 == '0' && $request->pU_prestation_6 == '0') {
                $request->quantite_6  = ($request->quantite_6 != '' ) ? $request->quantite_6 : '0' ;
            }else{
                $request->quantite_6  = ($request->quantite_6 != '' ) ? $request->quantite_6 : '1' ;
            }

            if ($request->pU_fourniture_7 == '0' && $request->pU_prestation_7 == '0') {
                $request->quantite_7  = ($request->quantite_7 != '' ) ? $request->quantite_7 : '0' ;
            }else{
                $request->quantite_7  = ($request->quantite_7 != '' ) ? $request->quantite_7 : '1' ;
            }

            if ($request->pU_fourniture_8 == '0' && $request->pU_prestation_8 == '0') {
                $request->quantite_8  = ($request->quantite_8 != '' ) ? $request->quantite_8 : '0' ;
            }else{
                $request->quantite_8  = ($request->quantite_8 != '' ) ? $request->quantite_8 : '1' ;
            }

            if ($request->pU_fourniture_9 == '0' && $request->pU_prestation_9 == '0') {
                $request->quantite_9  = ($request->quantite_9 != '' ) ? $request->quantite_9 : '0' ;
            }else{
                $request->quantite_9  = ($request->quantite_9 != '' ) ? $request->quantite_9 : '1' ;
            }

            if ($request->pU_fourniture_10 == '0' && $request->pU_prestation_10 == '0') {
                $request->quantite_10  = ($request->quantite_10 != '' ) ? $request->quantite_10 : '0' ;
            }else{
                $request->quantite_10  = ($request->quantite_10 != '' ) ? $request->quantite_10 : '1' ;
            }



        $montant_1 = ( $request->pU_fourniture_1 * $request->quantite_1 ) + ( $request->pU_prestation_1 * $request->quantite_1 );
        $montant_2 = ( $request->pU_fourniture_2 * $request->quantite_2 ) + ( $request->pU_prestation_2 * $request->quantite_2 );
        $montant_3 = ( $request->pU_fourniture_3 * $request->quantite_3 ) + ( $request->pU_prestation_3 * $request->quantite_3 );
        $montant_4 = ( $request->pU_fourniture_4 * $request->quantite_4 ) + ( $request->pU_prestation_4 * $request->quantite_4 );
        $montant_5 = ( $request->pU_fourniture_5 * $request->quantite_5 ) + ( $request->pU_prestation_5 * $request->quantite_5 );
        $montant_6 = ( $request->pU_fourniture_6 * $request->quantite_6 ) + ( $request->pU_prestation_6 * $request->quantite_6 );
        $montant_7 = ( $request->pU_fourniture_7 * $request->quantite_7 ) + ( $request->pU_prestation_7 * $request->quantite_7 );
        $montant_8 = ( $request->pU_fourniture_8 * $request->quantite_8 ) + ( $request->pU_prestation_8 * $request->quantite_8 );
        $montant_9 = ( $request->pU_fourniture_9 * $request->quantite_9 ) + ( $request->pU_prestation_9 * $request->quantite_9 );
        $montant_10 = ( $request->pU_fourniture_10 * $request->quantite_10 ) + ( $request->pU_prestation_10 * $request->quantite_10 );

        $sous_total_1 = $montant_1 +  $montant_2 +  $montant_3 + $montant_4 +  $montant_5  + $montant_6;
        $sous_total_2 = $montant_7 +  $montant_8 +  $montant_9 + $montant_10;

        $total_HT = $sous_total_1 + $sous_total_2;
        $total_TVA = 0.18 * $total_HT;

        $montant_fact =  $total_TVA + $total_HT;



            $deserte = new Desertes;

                $deserte->ref_devis =  $ref_devis;
                $deserte->intitule  = $request->intitule;
                $deserte->type_devis= $request->type_devis;
                $deserte->ref_Facture  = $request->ref_fact;
                $deserte->clients_id    = $request->operateur;
                $deserte->signataires_id = $request->signataire_id;

                $deserte->pU_fourniture_1  = $request->pU_fourniture_1 ;
                $deserte->pU_fourniture_2  = $request->pU_fourniture_2 ;
                $deserte->pU_fourniture_3  = $request->pU_fourniture_3 ;
                $deserte->pU_fourniture_4  = $request->pU_fourniture_4 ;
                $deserte->pU_fourniture_5  = $request->pU_fourniture_5 ;
                $deserte->pU_fourniture_6  = $request->pU_fourniture_6 ;
                $deserte->pU_fourniture_7  = $request->pU_fourniture_7 ;
                $deserte->pU_fourniture_8  = $request->pU_fourniture_8 ;
                $deserte->pU_fourniture_9  = $request->pU_fourniture_9 ;
                $deserte->pU_fourniture_10  =  $request->pU_fourniture_10 ;

                $deserte->pU_prestation_1  = $request->pU_prestation_1  ;
                $deserte->pU_prestation_2  = $request->pU_prestation_2  ;
                $deserte->pU_prestation_3  = $request->pU_prestation_3  ;
                $deserte->pU_prestation_4  = $request->pU_prestation_4  ;
                $deserte->pU_prestation_5  = $request->pU_prestation_5  ;
                $deserte->pU_prestation_6  = $request->pU_prestation_6  ;
                $deserte->pU_prestation_7  = $request->pU_prestation_7  ;
                $deserte->pU_prestation_8  = $request->pU_prestation_8  ;
                $deserte->pU_prestation_9  = $request->pU_prestation_9  ;
                $deserte->pU_prestation_10  =  $request->pU_prestation_10  ;

                $deserte->quantite_1  = $request->quantite_1  ;
                $deserte->quantite_2  = $request->quantite_2  ;
                $deserte->quantite_3  = $request->quantite_3  ;
                $deserte->quantite_4  = $request->quantite_4  ;
                $deserte->quantite_5  = $request->quantite_5  ;
                $deserte->quantite_6  = $request->quantite_6  ;
                $deserte->quantite_7  = $request->quantite_7  ;
                $deserte->quantite_8  = $request->quantite_8  ;
                $deserte->quantite_9  = $request->quantite_9  ;
                $deserte->quantite_10  =  $request->quantite_10  ;


                $deserte->fourniture_1  = $request->pU_fourniture_1 * $request->quantite_1;
                $deserte->fourniture_2  = $request->pU_fourniture_2 * $request->quantite_2;
                $deserte->fourniture_3  = $request->pU_fourniture_3 * $request->quantite_3;
                $deserte->fourniture_4  = $request->pU_fourniture_4 * $request->quantite_4;
                $deserte->fourniture_5  = $request->pU_fourniture_5 * $request->quantite_5;
                $deserte->fourniture_6  = $request->pU_fourniture_6 * $request->quantite_6;
                $deserte->fourniture_7  = $request->pU_fourniture_7 * $request->quantite_7;
                $deserte->fourniture_8  = $request->pU_fourniture_8 * $request->quantite_8;
                $deserte->fourniture_9  = $request->pU_fourniture_9 * $request->quantite_9;
                $deserte->fourniture_10  = $request->pU_fourniture_10 * $request->quantite_10;

                $deserte->prestation_1  = $request->pU_prestation_1 * $request->quantite_1;
                $deserte->prestation_2  = $request->pU_prestation_2 * $request->quantite_2;
                $deserte->prestation_3  = $request->pU_prestation_3 * $request->quantite_3;
                $deserte->prestation_4  = $request->pU_prestation_4 * $request->quantite_4;
                $deserte->prestation_5  = $request->pU_prestation_5 * $request->quantite_5;
                $deserte->prestation_6  = $request->pU_prestation_6 * $request->quantite_6;
                $deserte->prestation_7  = $request->pU_prestation_7 * $request->quantite_7;
                $deserte->prestation_8  = $request->pU_prestation_8 * $request->quantite_8;
                $deserte->prestation_9  = $request->pU_prestation_9 * $request->quantite_9;
                $deserte->prestation_10  = $request->pU_prestation_10 * $request->quantite_10;

                $deserte->montant_1     = $montant_1;
                $deserte->montant_2     = $montant_2;
                $deserte->montant_3     = $montant_3;
                $deserte->montant_4     = $montant_4;
                $deserte->montant_5     = $montant_5;
                $deserte->montant_6     = $montant_6;
                $deserte->montant_7     = $montant_7;
                $deserte->montant_8     = $montant_8;
                $deserte->montant_9     = $montant_9;
                $deserte->montant_10    = $montant_10;

                $deserte->sous_total_1 = $sous_total_1;
                $deserte->sous_total_2 = $sous_total_2;
                $deserte->total_HT  = $total_HT;
                $deserte->total_TVA = $total_TVA;

                $deserte->montant_facture= $montant_fact;

                $deserte->save();



        return redirect()->route('list.deserte'  , compact( 'deserte' , 'is_dcm' , 'is_dpm' , '_url'));
            }
    }



    public function Imp_deserte(Request $request , $id){
        $request->user()->authorizeRoles(['ADMIN', 'AGENT DPM']);
        $is_dcm = $request->user()->hasRole('AGENT DCM');
        $is_dpm = $request->user()->hasRole('AGENT DPM');
        $_url = 'list_deserte';


        $path = 'assets/images/btcom.png';
        asset('assets/images/btcom.png');
        $type = pathinfo($path, PATHINFO_EXTENSION);
        $data = file_get_contents($path);
        $base64 = 'data:image/' . $type . ';base64,' . base64_encode($data);
        $options = new Options();
        $options->set('isRemoteEnabled', true);
        $pdf = new PDF($options);

        $deserte = Desertes::where('id' , $id)->first();

        //dd($deserte);

        $formatter = \NumberFormatter::create('fr_FR', \NumberFormatter::SPELLOUT);
        $formatter->setAttribute(\NumberFormatter::FRACTION_DIGITS, 0);
        $formatter->setAttribute(\NumberFormatter::ROUNDING_MODE, \NumberFormatter::ROUND_HALFUP);
        $montant_lettre = $formatter->format($deserte->montant_facture);
            $pdf = \PDF::loadView('dashboard.liste_divers.deserte_facture' , compact('montant_lettre' , 'deserte' , 'base64' , 'is_dcm' , 'is_dpm' , '_url'));
            //$pdf->setPaper('a4','landscape');
            return $pdf->stream('deserte_facture.pdf' , compact('montant_lettre' , 'deserte' , 'base64' , 'is_dcm' , 'is_dpm' , '_url'));
    }

    public function Imp_desertes(Request $request){
        $request->user()->authorizeRoles(['ADMIN', 'AGENT DPM']);
        $is_dcm = $request->user()->hasRole('AGENT DCM');
        $is_dpm = $request->user()->hasRole('AGENT DPM');
        $_url = 'list_deserte';


        $path = 'assets/images/btcom.png';
        asset('assets/images/btcom.png');
        $type = pathinfo($path, PATHINFO_EXTENSION);
        $data = file_get_contents($path);
        $base64 = 'data:image/' . $type . ';base64,' . base64_encode($data);
        $options = new Options();
        $options->set('isRemoteEnabled', true);
        $pdf = new PDF($options);

        $desertes = Desertes::all();

            $pdf = \PDF::loadView('dashboard.liste_divers.desertes_table' , compact('desertes' , 'base64' , 'is_dcm' , 'is_dpm' , '_url'));
            //$pdf->setPaper('a4','landscape');
            return $pdf->stream('desertes_table.pdf' , compact('desertes' , 'base64' , 'is_dcm' , 'is_dpm' , '_url'));
    }

    public function Supp_deserte(Request $request , $id){
        $request->user()->authorizeRoles(['ADMIN', 'AGENT DPM']);
        $is_dcm = $request->user()->hasRole('AGENT DCM');
        $is_dpm = $request->user()->hasRole('AGENT DPM');
        $_url = 'list_deserte';


            $deserte = Desertes::where('id' , $id)->delete();

        return redirect()->back();

    }

    public function Edit_deserte(Request $request , $id){
        $request->user()->authorizeRoles(['ADMIN', 'AGENT DPM']);
        $is_dcm = $request->user()->hasRole('AGENT DCM');
        $is_dpm = $request->user()->hasRole('AGENT DPM');
        $_url = 'list_deserte';
            $deserte = Desertes::where('id' , $id)->first();
            $clients = Client::all();
            $signataires = Signataire::all();
        return view('dashboard.liste_divers.edit_deserte'  , compact('signataires' , 'clients', 'deserte' , 'is_dcm' , 'is_dpm' , '_url'));
    }

    public function update_deserte(Request $request , $id){
        $request->user()->authorizeRoles(['ADMIN', 'AGENT DPM']);
        $is_dcm = $request->user()->hasRole('AGENT DCM');
        $is_dpm = $request->user()->hasRole('AGENT DPM');
        $_url = 'list_deserte';


        $validator = Validator::make($request->all(),[
            'intitule'         => 'required',
            'type_devis'       => 'required',
            'ref_fact'      => 'required|unique',
            'operateur'      => 'required',
            'signataire_id'   => 'required',

        ]);

            if ( $validator->fails() ) {
                return Redirect::back()->withErrors($validator)->withInput();
            } else{


                $request->pU_fourniture_1  = ($request->pU_fourniture_1 != '') ? $request->pU_fourniture_1 : '0' ;
                $request->pU_fourniture_2  = ($request->pU_fourniture_2 != '') ? $request->pU_fourniture_2 : '0' ;
                $request->pU_fourniture_3  = ($request->pU_fourniture_3 != '') ? $request->pU_fourniture_3 : '0' ;
                $request->pU_fourniture_4  = ($request->pU_fourniture_4 != '') ? $request->pU_fourniture_4 : '0' ;
                $request->pU_fourniture_5  = ($request->pU_fourniture_5 != '') ? $request->pU_fourniture_5 : '0' ;
                $request->pU_fourniture_6  = ($request->pU_fourniture_6 != '') ? $request->pU_fourniture_6 : '0' ;
                $request->pU_fourniture_7  = ($request->pU_fourniture_7 != '') ? $request->pU_fourniture_7 : '0' ;
                $request->pU_fourniture_8  = ($request->pU_fourniture_8 != '') ? $request->pU_fourniture_8 : '0' ;
                $request->pU_fourniture_9  = ($request->pU_fourniture_9 != '') ? $request->pU_fourniture_9 : '0' ;
                $request->pU_fourniture_10  = ($request->pU_fourniture_10 != '') ? $request->pU_fourniture_10 : '0' ;

                $request->pU_prestation_1  = ($request->pU_prestation_1 != '') ? $request->pU_prestation_1 : '0' ;
                $request->pU_prestation_2  = ($request->pU_prestation_2 != '') ? $request->pU_prestation_2 : '0' ;
                $request->pU_prestation_3  = ($request->pU_prestation_3 != '') ? $request->pU_prestation_3 : '0' ;
                $request->pU_prestation_4  = ($request->pU_prestation_4 != '') ? $request->pU_prestation_4 : '0' ;
                $request->pU_prestation_5  = ($request->pU_prestation_5 != '') ? $request->pU_prestation_5 : '0' ;
                $request->pU_prestation_6  = ($request->pU_prestation_6 != '') ? $request->pU_prestation_6 : '0' ;
                $request->pU_prestation_7  = ($request->pU_prestation_7 != '') ? $request->pU_prestation_7 : '0' ;
                $request->pU_prestation_8  = ($request->pU_prestation_8 != '') ? $request->pU_prestation_8 : '0' ;
                $request->pU_prestation_9  = ($request->pU_prestation_9 != '') ? $request->pU_prestation_9 : '0' ;
                $request->pU_prestation_10  = ($request->pU_prestation_10 != '') ? $request->pU_prestation_10 : '0' ;

                if ($request->pU_fourniture_1 == '0' && $request->pU_prestation_1 == '0') {
                    $request->quantite_1  = ($request->quantite_1 != '' ) ? $request->quantite_1 : '0' ;
                }else{
                    $request->quantite_1  = ($request->quantite_1 != '' ) ? $request->quantite_1 : '1' ;
                }

                if ($request->pU_fourniture_2 == '0' && $request->pU_prestation_2 == '0') {
                    $request->quantite_2  = ($request->quantite_2 != '' ) ? $request->quantite_2 : '0' ;
                }else{
                    $request->quantite_2  = ($request->quantite_2 != '' ) ? $request->quantite_2 : '1' ;
                }

                if ($request->pU_fourniture_3 == '0' && $request->pU_prestation_3 == '0') {
                    $request->quantite_3  = ($request->quantite_3 != '' ) ? $request->quantite_3 : '0' ;
                }else{
                    $request->quantite_3  = ($request->quantite_3 != '' ) ? $request->quantite_3 : '1' ;
                }

                if ($request->pU_fourniture_4 == '0' && $request->pU_prestation_4 == '0') {
                    $request->quantite_4  = ($request->quantite_4 != '' ) ? $request->quantite_4 : '0' ;
                }else{
                    $request->quantite_4  = ($request->quantite_4 != '' ) ? $request->quantite_4 : '1' ;
                }

                if ($request->pU_fourniture_5 == '0' && $request->pU_prestation_5 == '0') {
                    $request->quantite_5  = ($request->quantite_5 != '' ) ? $request->quantite_5 : '0' ;
                }else{
                    $request->quantite_5  = ($request->quantite_5 != '' ) ? $request->quantite_5 : '1' ;
                }

                if ($request->pU_fourniture_6 == '0' && $request->pU_prestation_6 == '0') {
                    $request->quantite_6  = ($request->quantite_6 != '' ) ? $request->quantite_6 : '0' ;
                }else{
                    $request->quantite_6  = ($request->quantite_6 != '' ) ? $request->quantite_6 : '1' ;
                }

                if ($request->pU_fourniture_7 == '0' && $request->pU_prestation_7 == '0') {
                    $request->quantite_7  = ($request->quantite_7 != '' ) ? $request->quantite_7 : '0' ;
                }else{
                    $request->quantite_7  = ($request->quantite_7 != '' ) ? $request->quantite_7 : '1' ;
                }

                if ($request->pU_fourniture_8 == '0' && $request->pU_prestation_8 == '0') {
                    $request->quantite_8  = ($request->quantite_8 != '' ) ? $request->quantite_8 : '0' ;
                }else{
                    $request->quantite_8  = ($request->quantite_8 != '' ) ? $request->quantite_8 : '1' ;
                }

                if ($request->pU_fourniture_9 == '0' && $request->pU_prestation_9 == '0') {
                    $request->quantite_9  = ($request->quantite_9 != '' ) ? $request->quantite_9 : '0' ;
                }else{
                    $request->quantite_9  = ($request->quantite_9 != '' ) ? $request->quantite_9 : '1' ;
                }

                if ($request->pU_fourniture_10 == '0' && $request->pU_prestation_10 == '0') {
                    $request->quantite_10  = ($request->quantite_10 != '' ) ? $request->quantite_10 : '0' ;
                }else{
                    $request->quantite_10  = ($request->quantite_10 != '' ) ? $request->quantite_10 : '1' ;
                }



        $montant_1 = ( $request->pU_fourniture_1 * $request->quantite_1 ) + ( $request->pU_prestation_1 * $request->quantite_1 );
        $montant_2 = ( $request->pU_fourniture_2 * $request->quantite_2 ) + ( $request->pU_prestation_2 * $request->quantite_2 );
        $montant_3 = ( $request->pU_fourniture_3 * $request->quantite_3 ) + ( $request->pU_prestation_3 * $request->quantite_3 );
        $montant_4 = ( $request->pU_fourniture_4 * $request->quantite_4 ) + ( $request->pU_prestation_4 * $request->quantite_4 );
        $montant_5 = ( $request->pU_fourniture_5 * $request->quantite_5 ) + ( $request->pU_prestation_5 * $request->quantite_5 );
        $montant_6 = ( $request->pU_fourniture_6 * $request->quantite_6 ) + ( $request->pU_prestation_6 * $request->quantite_6 );
        $montant_7 = ( $request->pU_fourniture_7 * $request->quantite_7 ) + ( $request->pU_prestation_7 * $request->quantite_7 );
        $montant_8 = ( $request->pU_fourniture_8 * $request->quantite_8 ) + ( $request->pU_prestation_8 * $request->quantite_8 );
        $montant_9 = ( $request->pU_fourniture_9 * $request->quantite_9 ) + ( $request->pU_prestation_9 * $request->quantite_9 );
        $montant_10 = ( $request->pU_fourniture_10 * $request->quantite_10 ) + ( $request->pU_prestation_10 * $request->quantite_10 );

        $sous_total_1 = $montant_1 +  $montant_2 +  $montant_3 + $montant_4 +  $montant_5  + $montant_6;
        $sous_total_2 = $montant_7 +  $montant_8 +  $montant_9 + $montant_10;

        $total_HT = $sous_total_1 + $sous_total_2;
        $total_TVA = 0.18 * $total_HT;

        $montant_fact =  $total_TVA + $total_HT;



        $deserte = Desertes::where('id' , $id)->first();

                //$deserte->ref_devis =  $ref_devis;
                $deserte->intitule  = $request->intitule;
                $deserte->type_devis= $request->type_devis;
                $deserte->ref_Facture  = $request->ref_fact;
                $deserte->clients_id    = $request->operateur;
                $deserte->signataires_id = $request->signataire_id;

                $deserte->pU_fourniture_1  = $request->pU_fourniture_1 ;
                $deserte->pU_fourniture_2  = $request->pU_fourniture_2 ;
                $deserte->pU_fourniture_3  = $request->pU_fourniture_3 ;
                $deserte->pU_fourniture_4  = $request->pU_fourniture_4 ;
                $deserte->pU_fourniture_5  = $request->pU_fourniture_5 ;
                $deserte->pU_fourniture_6  = $request->pU_fourniture_6 ;
                $deserte->pU_fourniture_7  = $request->pU_fourniture_7 ;
                $deserte->pU_fourniture_8  = $request->pU_fourniture_8 ;
                $deserte->pU_fourniture_9  = $request->pU_fourniture_9 ;
                $deserte->pU_fourniture_10  =  $request->pU_fourniture_10 ;

                $deserte->pU_prestation_1  = $request->pU_prestation_1  ;
                $deserte->pU_prestation_2  = $request->pU_prestation_2  ;
                $deserte->pU_prestation_3  = $request->pU_prestation_3  ;
                $deserte->pU_prestation_4  = $request->pU_prestation_4  ;
                $deserte->pU_prestation_5  = $request->pU_prestation_5  ;
                $deserte->pU_prestation_6  = $request->pU_prestation_6  ;
                $deserte->pU_prestation_7  = $request->pU_prestation_7  ;
                $deserte->pU_prestation_8  = $request->pU_prestation_8  ;
                $deserte->pU_prestation_9  = $request->pU_prestation_9  ;
                $deserte->pU_prestation_10  =  $request->pU_prestation_10  ;

                $deserte->quantite_1  = $request->quantite_1  ;
                $deserte->quantite_2  = $request->quantite_2  ;
                $deserte->quantite_3  = $request->quantite_3  ;
                $deserte->quantite_4  = $request->quantite_4  ;
                $deserte->quantite_5  = $request->quantite_5  ;
                $deserte->quantite_6  = $request->quantite_6  ;
                $deserte->quantite_7  = $request->quantite_7  ;
                $deserte->quantite_8  = $request->quantite_8  ;
                $deserte->quantite_9  = $request->quantite_9  ;
                $deserte->quantite_10  =  $request->quantite_10  ;

                $deserte->fourniture_1  = $request->pU_fourniture_1 * $request->quantite_1;
                $deserte->fourniture_2  = $request->pU_fourniture_2 * $request->quantite_2;
                $deserte->fourniture_3  = $request->pU_fourniture_3 * $request->quantite_3;
                $deserte->fourniture_4  = $request->pU_fourniture_4 * $request->quantite_4;
                $deserte->fourniture_5  = $request->pU_fourniture_5 * $request->quantite_5;
                $deserte->fourniture_6  = $request->pU_fourniture_6 * $request->quantite_6;
                $deserte->fourniture_7  = $request->pU_fourniture_7 * $request->quantite_7;
                $deserte->fourniture_8  = $request->pU_fourniture_8 * $request->quantite_8;
                $deserte->fourniture_9  = $request->pU_fourniture_9 * $request->quantite_9;
                $deserte->fourniture_10  = $request->pU_fourniture_10 * $request->quantite_10;

                $deserte->prestation_1  = $request->pU_prestation_1 * $request->quantite_1;
                $deserte->prestation_2  = $request->pU_prestation_2 * $request->quantite_2;
                $deserte->prestation_3  = $request->pU_prestation_3 * $request->quantite_3;
                $deserte->prestation_4  = $request->pU_prestation_4 * $request->quantite_4;
                $deserte->prestation_5  = $request->pU_prestation_5 * $request->quantite_5;
                $deserte->prestation_6  = $request->pU_prestation_6 * $request->quantite_6;
                $deserte->prestation_7  = $request->pU_prestation_7 * $request->quantite_7;
                $deserte->prestation_8  = $request->pU_prestation_8 * $request->quantite_8;
                $deserte->prestation_9  = $request->pU_prestation_9 * $request->quantite_9;
                $deserte->prestation_10  = $request->pU_prestation_10 * $request->quantite_10;

                $deserte->montant_1     = $montant_1;
                $deserte->montant_2     = $montant_2;
                $deserte->montant_3     = $montant_3;
                $deserte->montant_4     = $montant_4;
                $deserte->montant_5     = $montant_5;
                $deserte->montant_6     = $montant_6;
                $deserte->montant_7     = $montant_7;
                $deserte->montant_8     = $montant_8;
                $deserte->montant_9     = $montant_9;
                $deserte->montant_10     = $montant_10;

                $deserte->sous_total_1 = $sous_total_1;
                $deserte->sous_total_2 = $sous_total_2;
                $deserte->total_HT  = $total_HT;
                $deserte->total_TVA = $total_TVA;

                $deserte->montant_facture= $montant_fact;

                $deserte->save();



        return redirect()->route('list.deserte'  , compact( 'deserte' , 'is_dcm' , 'is_dpm' , '_url'));
    }
}
    //============================= SERVICES ==============================================

    public function service(Request $request){
        $request->user()->authorizeRoles(['ADMIN', 'AGENT DPM']);
        $is_dcm = $request->user()->hasRole('AGENT DCM');
        $is_dpm = $request->user()->hasRole('AGENT DPM');
        $_url = 'list_service';

        //$pvs = CommandeProtocole::all();
        $factures = Facture::all();
       // dd($factures);
        return view('dashboard.liste_divers.services' , compact('factures','is_dcm' , 'is_dpm' , '_url'));
    }

    public function detailsService( Request $request , $id){
        $request->user()->authorizeRoles(['ADMIN', 'AGENT DPM']);
        $is_dcm = $request->user()->hasRole('AGENT DCM');
        $is_dpm = $request->user()->hasRole('AGENT DPM');
        $_url = 'list_service';

        $facture = Facture::where('id' , $id)->first();
       /* $frais_access = Frais_acces::where([
                                        ['id_sfs' , $facture->pv->sfs_id],
                                        ['id_ssfs' , $facture->pv->ssfs_id],
                                        ['id_capacite' ,  $facture->pv->capacite_id],
                                            ])->first();*/
            //dd($frais_access);

        return view('dashboard.liste_divers.details_services' , compact('facture' , 'is_dcm' , 'is_dpm' , '_url'));

    }


    public function Imp_services(Request $request){
        $request->user()->authorizeRoles(['ADMIN', 'AGENT DPM']);
        $is_dcm = $request->user()->hasRole('AGENT DCM');
        $is_dpm = $request->user()->hasRole('AGENT DPM');
        $_url = 'list_service';

        $path = 'assets/images/btcom.png';
        asset('assets/images/btcom.png');
        $type = pathinfo($path, PATHINFO_EXTENSION);
        $data = file_get_contents($path);
        $base64 = 'data:image/' . $type . ';base64,' . base64_encode($data);
        $options = new Options();
        $options->set('isRemoteEnabled', true);
        $pdf = new PDF($options);

        $factures = Facture::all();

            $pdf = \PDF::loadView('dashboard.liste_divers.services_table' , compact('factures' , 'base64' , 'is_dcm' , 'is_dpm' , '_url'));

            $pdf->setPaper('a4','landscape');
            return $pdf->stream('services_table.pdf' , compact('factures' , 'base64' , 'is_dcm' , 'is_dpm' , '_url'));
    }

    //================================= CONTRATS =================================================
    public function contrats(Request $request){
        $request->user()->authorizeRoles(['ADMIN', 'AGENT DPM']);
        $is_dcm = $request->user()->hasRole('AGENT DCM');
        $is_dpm = $request->user()->hasRole('AGENT DPM');
        $_url = 'list_contrats';

        $contrats = Contrat::all();

            if ($contrats != null) {

                foreach($contrats as $contrat){
                    if ( new DateTime() <= new DateTime($contrat->end_date) ) {

                        $contrat->nature_contrat = 'Actif';
                        $contrat->save();
                    }else{

                        $contrat->nature_contrat = 'Inactif';
                        $contrat->save();
                    }
                }

            }


        return view('dashboard.liste_divers.contrats' ,compact('contrats' , 'is_dcm' , 'is_dpm' , '_url') );
    }


    public function Imp_contrats(Request $request){
        $request->user()->authorizeRoles(['ADMIN', 'AGENT DPM']);
        $is_dcm = $request->user()->hasRole('AGENT DCM');
        $is_dpm = $request->user()->hasRole('AGENT DPM');
        $_url = 'list_contrats';

        $path = 'assets/images/btcom.png';
        asset('assets/images/btcom.png');
        $type = pathinfo($path, PATHINFO_EXTENSION);
        $data = file_get_contents($path);
        $base64 = 'data:image/' . $type . ';base64,' . base64_encode($data);
        $options = new Options();
        $options->set('isRemoteEnabled', true);
        $pdf = new PDF($options);
        $contrats = Contrat::all();

            $pdf = \PDF::loadView('dashboard.liste_divers.contrats_table' , compact('contrats' , 'base64' , 'is_dcm' , 'is_dpm' , '_url'));
            //$pdf = \PDF::loadView('dashboard.liste_divers.contrats_table' , compact(   'base64' , 'is_dcm' , 'is_dpm' , '_url'));
            return $pdf->stream('contrats_table.pdf' , compact('contrats' , 'base64' , 'is_dcm' , 'is_dpm' , '_url'));

    }

        //================================= ETAT =================================================

    public function etat(Request $request){
        $request->user()->authorizeRoles(['ADMIN', 'AGENT DPM']);
        $is_dcm = $request->user()->hasRole('AGENT DCM');
        $is_dpm = $request->user()->hasRole('AGENT DPM');
        $_url = 'list_client';
        $etats = Etat::all();
        //$etats = Etat::where('id' , 1)->first();
       // dd($etats);
        return view('dashboard.liste_divers.etats' , compact('etats', 'is_dcm' , 'is_dpm' , '_url'));
    }

    public function etatDetails(Request $request , $id){
        $request->user()->authorizeRoles(['ADMIN', 'AGENT DPM']);
        $is_dcm = $request->user()->hasRole('AGENT DCM');
        $is_dpm = $request->user()->hasRole('AGENT DPM');
        $_url = 'list_client';
        $etat = Etat::where('id' , $id)->first();

        return view('dashboard.liste_divers.etatDetails' , compact('etat', 'is_dcm' , 'is_dpm' , '_url'));

    }


}
