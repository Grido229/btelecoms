<?php

namespace App\Http\Controllers;

use App\BonCommande;
use App\Capacites;
use App\CommandeProtocole;
use App\Distances;
use App\Facture;
use App\Fs;
use App\OffresType;
use App\Sfs;
use App\Ssfs;
use App\Volume;
use App\Zone;
use App\Tarifs;
use DateTime;
use Illuminate\Http\Request;
use Barryvdh\DomPDF\Facade as PDF;
use  Dompdf\Options;
use Dompdf\Helpers;
use Dompdf\Exception\ImageException;

class PvController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(Request $request)
    {
        $request->user()->authorizeRoles(['ADMIN', 'AGENT DPM']);
        $is_dcm = $request->user()->hasRole('AGENT DCM');
        $is_dpm = $request->user()->hasRole('AGENT DPM');
        $_url = 'commande-protocoles';
        $pvs = CommandeProtocole::orderBy('created_at', 'desc')->with('bon')->get();
        foreach ($pvs as $pv) {
            //$bon = $pv->bon()->first();
            //$client = $bon->client()->first();
            //$pv->client = $client;
            //$pv->bon = $bon;
            //$pv->bon = $pv->bon()->first();
            //$pv->client = $pv->bon->client()->first();

            $pv->commissioningDate = (new DateTime($pv->commissioningDate))->format('d-m-Y');
        }

        //dd($pvs);

        return view('dashboard.pv.list', compact('pvs', 'is_dcm', 'is_dpm', '_url'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    /*public function create(Request $request)
    {
        $request->user()->authorizeRoles(['ADMIN', 'AGENT DPM']);
        $is_dcm = $request->user()->hasRole('AGENT DCM');
        $is_dpm = $request->user()->hasRole('AGENT DPM');
        $_url = 'capacity-commande-protocole';
        $bons = BonCommande::where('is_validate_by_dcm', 1)
            ->where('is_validate_by_dpm', 1)
            ->get();
        $offres_types = OffresType::all();

        return view('dashboard.pv.create', compact('bons', 'is_dcm', 'is_dpm', '_url', 'offres_types'));
    }*/
    public function create(Request $request)
    {
        $request->user()->authorizeRoles(['ADMIN', 'AGENT DPM']);
        $is_dcm = $request->user()->hasRole('AGENT DCM');
        $is_dpm = $request->user()->hasRole('AGENT DPM');
        $_url = 'capacity-commande-protocole';
        $bons = BonCommande::where('is_validate_by_dcm', 1)
            ->where('is_validate_by_dpm', 1)
            ->get();

        //$offres_types = OffresType::all();

         $sous_famille_service_transport    = Sfs::where('id_fs',1)->get();
         $sous_famille_access_service       = Sfs::where('id_fs',2)->get();
         $sous_famille_hebergement_service  = Sfs::where('id_fs',3)->get();
         $sous_famille_voice_service        = Sfs::where('id_fs',4)->get();
           // Sou sous famille de services
           $ssfs_transport      = Ssfs::where('id_sfs', 1 )->get();
           $ssfs_transit        = Ssfs::where('id_sfs', 4 )->get();
           $ssfs_pylones        = Ssfs::where('id_sfs', 10 )->get();

            //dd($sous_famille_hebergement_service);
        $sites = Zone::all();
        $capacites = Capacites::all();
        $distances = Distances::all();
        $volumes = Volume::all();
        $fs      =  Fs::all();
         // dd($ssfs_transport) ;
        return view('dashboard.pv.create', compact('bons', 'is_dcm', 'is_dpm', '_url','ssfs_transport','ssfs_transit','ssfs_pylones', 'sous_famille_access_service','sous_famille_service_transport', 'sous_famille_hebergement_service','sous_famille_voice_service', 'sites' , 'distances', 'capacites' , 'volumes','fs'));
    }

    public function detail(Request $request, $id)
    {
        $request->user()->authorizeRoles(['ADMIN', 'AGENT DPM', 'AGENT DCM']);
        $is_dcm = $request->user()->hasRole('AGENT DCM');
        $is_dpm = $request->user()->hasRole('AGENT DPM');
        $_url = 'commande-protocoles';
        $pv = CommandeProtocole::findOrFail($id);
        $pv->commissioningDate = (new DateTime($pv->commissioningDate))->format('d-m-Y');
        $pv->bon = $pv->bon()->first();
        $pv->client = $pv->bon->client()->first();
        /*$pv->type_offre = OffresType::find($pv->type_id)->name;*/
        /*$type_offre = $pv->type_id;

        $table_type_offre = explode(',' , $type_offre);
        $type_offre_id = $table_type_offre[0];
        $categorie = $table_type_offre[1];*/

        if ($pv->ssfs_id == null) {
            //dd($sous_famille_hebergement_service);
        $sites = Zone::all();
        $capacites = Capacites::all();
        $distances = Distances::all();
        $volumes = Volume::all();
        $fs      =  Fs::all();
        }
         // dd($ssfs_transport) ;
       // return view('dashboard.pv.create', compact('pv', 'is_dcm', 'is_dpm', '_url','ssfs_transport','ssfs_transit','ssfs_pylones', 'sous_famille_access_service','sous_famille_service_transport', 'sous_famille_hebergement_service','sous_famille_voice_service', 'sites' , 'distances', 'capacites' , 'volumes','fs'));
       return view('dashboard.pv.detail', compact('pv', 'is_dcm', 'is_dpm', '_url'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request    //     if($categorie == 0){

     * @return \Illuminate\Http\Response
     */

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->user()->authorizeRoles(['ADMIN', 'AGENT DPM']);
        $request->validate([
            'bon_id' => 'required|max:50',
            'reception_room' => 'required|max:50',
            'local_conditions' => 'required',
            'commissioningDate' => 'required|max:50',
            'guide_length' => 'required',
            'nbr_antenna' => 'required',
            'type_antenna' => 'required',
            'position_tma' => 'required',
            'ville' => 'required',


        ]);


        if ($request->type == 'capacite') {

            $request->validate([
                'work_done' => 'required|max:50',
            ]);

            $pv = new CommandeProtocole;
            //dd($pv);
            $pv->work_done = $request->work_done;
            $pv->type = 'capacite';

            } else if ($request->type == 'colocalisation') {

                $pv = new CommandeProtocole;
                $pv->intensity_consomme = $request->intensity_consomme;
                $pv->source_ac = $request->source_ac;
                $pv->source_cc = $request->source_cc;
                $pv->provider = $request->provider;
                $pv->energy_consomme = $request->energy_consomme;
                $pv->tension = $request->tension;
                $pv->power_consomme = $request->power_consomme;
                $pv->energy_consomme_per_month = $request->energy_consomme_per_month;
                $pv->type_equipment = $request->type_equipment;
                $pv->sol_capacity = $request->sol_capacity;
                $pv->height = $request->height;
                $pv->width = $request->width;
                $pv->length = $request->length;
                $pv->type = 'colocalisation';

            } else if ($request->type == 'trafique') { }

                    $pv->guide_length = $request->guide_length;
                    $pv->nbr_antenna = $request->nbr_antenna;
                    $pv->type_antenna = $request->type_antenna;
                    $pv->weight = $request->weight;
                    $pv->position_tma = $request->position_tma;
                    $pv->ville = $request->ville;
                    $pv->site = $request->site;

                    $pv->status = $request->status;

                    //$pv->type_id = $request->type_id;
                    $pv->bon_id = $request->bon_id;
                    $pv->reception_room = $request->reception_room;
                    $pv->local_conditions = $request->local_conditions;
                    $pv->commissioningDate = $request->commissioningDate;
                    $pv->support = $request->support == "1" ? true : false;
                    $pv->date = (new DateTime())->format('Y-m-d');
                    $pv->save();
                    $pv->num_commande_protocoles = 'BTP-' . (new DateTime)->format('Ymd') . $pv->id;
                    $pv->save();

                    $numero_Pv = 'BTP-' . (new DateTime)->format('Ymd') . $pv->id;
                    //dd($pv);
                    if ($pv) {
                        if ($request->status == 'ACTIVE') {
                            $num_pv = CommandeProtocole::where('num_commande_protocoles' , $numero_Pv)->first();
                             return redirect()->route('factures.create',$num_pv->id); # code...
                        }

                        else{
                            return redirect()->route('dashboard.pv');
                        }

                    }




    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, $id)
    {
        $request->user()->authorizeRoles(['ADMIN']);
        $is_dcm = $request->user()->hasRole('AGENT DCM');
        $is_dpm = $request->user()->hasRole('AGENT DPM');
        $_url = 'commande-protocole';
        $pv = CommandeProtocole::findOrFail($id);
        $bons = BonCommande::all();
        $offres_types = OffresType::all();

        return view('dashboard.pv.edit', compact('pv', 'bons', 'is_dcm', 'is_dpm', '_url', 'offres_types'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->user()->authorizeRoles(['ADMIN']);
        $request->validate([
            'bon_id' => 'required|max:50',
            'reception_room' => 'required|max:50',
            'local_conditions' => 'required',
            'commissioningDate' => 'required|max:50',
            'guide_length' => 'required',
            'nbr_antenna' => 'required',
            'type_antenna' => 'required',
            'weight' => 'required',
            'position_tma' => 'required',
            'ville' => 'required',
        ]);
        $getpv = CommandeProtocole::find($id);
        if ($getpv->type == 'capacite') {
            $request->validate([
                'work_done' => 'required|max:50',
            ]);

            $pv = CommandeProtocole::findOrFail($id);
            $pv->work_done = $request->work_done;
        } else if ($getpv->type == 'colocalisation') {

            $pv = CommandeProtocole::findOrFail($id);
            $pv->intensity_consomme = $request->intensity_consomme;
            $pv->source_ac = $request->source_ac;
            $pv->source_cc = $request->source_cc;
            $pv->provider = $request->provider;
            $pv->energy_consomme = $request->energy_consomme;
            $pv->tension = $request->tension;
            $pv->power_consomme = $request->power_consomme;
            $pv->energy_consomme_per_month = $request->energy_consomme_per_month;
            $pv->type_equipment = $request->type_equipment;
            $pv->sol_capacity = $request->sol_capacity;
            $pv->height = $request->height;
            $pv->width = $request->width;
            $pv->length = $request->length;
            $pv->capacity = $request->capacity;
        } else if ($getpv->type == 'trafique') { }

        $pv->guide_length = $request->guide_length;
        $pv->nbr_antenna = $request->nbr_antenna;
        $pv->type_antenna = $request->type_antenna;
        $pv->weight = $request->weight;
        $pv->position_tma = $request->position_tma;
        $pv->ville = $request->ville;
        $pv->site = $request->site;

        $pv->status = $request->status;
        $pv->status_date = $request->status_date;
        $pv->status_object = $request->status_object;
        $pv->bon_id = $request->bon_id;
        $pv->reception_room = $request->reception_room;
        $pv->local_conditions = $request->local_conditions;
        $pv->commissioningDate = $request->commissioningDate;
        $pv->support = $request->support == "1" ? true : false;
        $pv->save();

        return redirect()->route('dashboard.pv');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function delete(Request $request, $id)
    {
        $request->user()->authorizeRoles(['ADMIN']);
        //dd($id);
        $pv = CommandeProtocole::findOrFail($id);
        //dd($pv->facture->pv_id);

        if($pv){

            if ($pv->facture) {
                $facture = Facture::findOrFail($pv->id);
                $facture->delete();
            }

            $pv->delete();

        }

        return redirect()->route('dashboard.pv');
    }

    public function pdfPv()
    {
        $pvs = CommandeProtocole::all();

        foreach ($pvs as $pv) {
            $bon = $pv->bon()->first();
            $client = $bon->client()->first();
            $pv->client = $client;
            $pv->bon = $bon;
            $pv->commissioningDate = (new DateTime($pv->commissioningDate))->format('d-m-Y');
        }
        //gestion images
        $path = 'assets/images/btcom.png';
        asset('assets/images/btcom.png');
        $type = pathinfo($path, PATHINFO_EXTENSION);
        $data = file_get_contents($path);
        $base64 = 'data:image/' . $type . ';base64,' . base64_encode($data);
        $options = new Options();
        $options->set('isRemoteEnabled', true);
        $pdf = new PDF($options);

        $pdf = \PDF::loadView('dashboard.pv.pdfPv', compact('pvs', 'base64'));
        //return $pdf->download('dashboard.bons.pdfBons.pdf', compact('bons'));
        return $pdf->stream('Liste_des_procès_verbaux.pdf', compact('pvs', 'base64'));
    }

    public function pdfDetail($id)
    {
        $pv = CommandeProtocole::findOrFail($id);

        $pv = CommandeProtocole::findOrFail($id);
        $pv->commissioningDate = (new DateTime($pv->commissioningDate))->format('d-m-Y');
        $pv->bon = $pv->bon()->first();
        $pv->client = $pv->bon->client()->first();

        //gestion images
        $path = 'assets/images/btcom.png';
        asset('assets/images/btcom.png');
        $type = pathinfo($path, PATHINFO_EXTENSION);
        $data = file_get_contents($path);
        $base64 = 'data:image/' . $type . ';base64,' . base64_encode($data);
        $options = new Options();
        //dd($options);
        $options->set('isRemoteEnabled', true);
        $pdf = new PDF($options);

        if ($pv->type == 'capacite') {
            $pdf = \PDF::loadView('dashboard.pv.pdfDetail', compact('pv', 'base64'));
            return $pdf->stream('capacity_pv.pdf', compact('pv', 'base64'));
        } else if ($pv->type == 'colocalisation') {

            $pdf = \PDF::loadView('dashboard.pv.pdfColoc', compact('pv', 'base64'));
            //return $pdf->download('dashboard.bons.pdfBons.pdf', compact('bons'));

            return $pdf->stream('coloc_pv.pdf', compact('pv', 'base64'));
            //return view('dashboard.pv.pdfDetail', compact('pvs','base64'));
        }
    }

    public function trafique(Request $request)
    {
        $request->user()->authorizeRoles(['ADMIN', 'AGENT DPM']);
        $is_dcm = $request->user()->hasRole('AGENT DCM');
        $is_dpm = $request->user()->hasRole('AGENT DPM');
        $_url = 'trafique-commande-protocole';
        $bons = BonCommande::where('is_validate_by_dcm', 1)
            ->where('is_validate_by_dpm', 1)
            ->get();

        return view('dashboard.pv.trafique', compact('bons', 'is_dcm', 'is_dpm', '_url'));
    }

    public function colocalisation(Request $request)
    {
        $request->user()->authorizeRoles(['ADMIN', 'AGENT DPM']);
        $is_dcm = $request->user()->hasRole('AGENT DCM');
        $is_dpm = $request->user()->hasRole('AGENT DPM');
        $_url = 'colocalisation-commande-protocole';
        $bons = BonCommande::where('is_validate_by_dcm', 1)
            ->where('is_validate_by_dpm', 1)
            ->get();

        //$offres_types = OffresType::all();
        $offres_types = Sfs::with('ssfs')->get();
        $ssfs = Ssfs::all();
           //dd($offres_types);
       $sites = Zone::all();
       $capacites = Capacites::all();
       $distances = Distances::all();
       $volumes = Volume::all();

        return view('dashboard.pv.colocalisation', compact('bons', 'is_dcm', 'is_dpm', '_url', 'offres_types' , 'ssfs' , 'sites' , 'capacites' , 'distances' , 'volumes'));
    }

    public function gradeForm(Request $request, $id ){
        $request->user()->authorizeRoles(['ADMIN', 'AGENT DPM']);
        $is_dcm = $request->user()->hasRole('AGENT DCM');
        $is_dpm = $request->user()->hasRole('AGENT DPM');
        $_url = 'edit-commande-protocole';
        $capacites = Capacites::all();

        $pv = CommandeProtocole::where('id', $id)->with("bon")->first();
        // dd($pv->bon->num_ref);
        $new_num =  'BTP-' . (new DateTime)->format('Ymd') . $pv->id;
         //dd($new_num);
        return view('dashboard.pv.edit_grade',compact( 'is_dcm', 'is_dpm', '_url', 'pv','capacites','id'));
    }

    public function grade(Request $request , $id ){

        $pv = CommandeProtocole::find($id);
    //dd($request);
        //$bon = BonCommande::find($pv->bon->id);
        $bon = BonCommande::where('id' ,$pv->bon->id )->first();
       
            if($request->total_price == null){

                if ( $request->total_price_ht != 'Has not price' &&  $request->total_price_ht != NULL) {
                    $table = explode('F CFA' ,$request->total_price_ht );
                    $request->total_price_ht = $table[0];
                    $bon->update(['total_price_ht' => $request->total_price_ht]);
                }

            }else{
                $bon->update(['total_price_ht' => $request->total_price]); 
            }
            $bon->capacite_id = $request->capacite;
            $bon->save();


          $new_num =  'BTP-' . (new DateTime)->format('Ymd') . $id;
        //$pv->update(['number_links_attributed' => $request->lienAttribue, 'new_num_commande_protocoles' => $new_num ]);
        $pv->update([
                    'number_links_attributed' => $request->capacite,
                    'new_num_commande_protocoles' => $new_num
                    ]);



        $date = now();
        $table_date = explode(' ' , $date);
        $date = $table_date[0];
        $separateur_date = explode('-' , $date);
        $annee = $separateur_date[0];
        $mois = $separateur_date[1];
        //$code = sprintf('%04d' , 0001);
        $facture = Facture::all();
        $numero_facture = '' ;
        if ( $facture->isEmpty() ) {
            $numero_facture = $annee.''.$mois.'BTI0001';

        }else{
            $facture = Facture::latest('id')->first();
            //dd($facture);
            $ancien_Code = $facture->No_facture;

            $table_ancien_code = explode('BTI' , $ancien_Code);
            $code = $table_ancien_code[1] + 1;
            $code = sprintf('%04d' , $code);
            $numero_facture = $annee.''.$mois.'BTI'.$code;
        }


        if($request->total_price == null){
            $insert = Facture::create([
                'No_facture' => $numero_facture,
                'pv_id'     =>  $id,
                'montant_fact'   =>  $request->total_price_ht,
            ]);
        }else{
            $insert = Facture::create([
                'No_facture' => $numero_facture,
                'pv_id'     =>  $id,
                'montant_fact'   =>  $request->total_price,
            ]);
        }




            if ($pv) {
                return redirect()->route('dashboard.pv');
            }

    }


    public function ajaxRequest( Request $request){

        $ssfs = NULL;
        $montant = '';

        if ( isset($request->Ssfs_id) ) {
            $ssfs = $request->Ssfs_id;
        }

        $sfs = $request->Sfs_id;
        $capacite = $request->capacite;
        $distance = $request->distance;
        $site = $request->site;

        $tarifs = Tarifs::where( [
            ['sfs_id' , $sfs],
            ['ssfs_id', $ssfs],
            ['distance_id' ,'=',  $distance],
            ['capacite_id' ,'=',  $capacite],
            ['zone_id' ,'=',$site]
                                 ] )->first();
        if ($tarifs != NULL ) {
            $montant = $tarifs->tarifs_Or_redevances .' F CFA';
        }else{
            $montant = 'Has not price';
        }


    return response()->json(['message' => $montant ]);
}


}
