<?php

namespace App\Http\Controllers;

use App\BonCommande;
use App\Capacites;
use App\Client;
use App\CommandeDetail;
use App\CommandeProtocole;
use App\Contrat;
use App\Distances;
use App\Facture;
use App\Frais_acces;
use App\FraisActivation;
use App\Fs;
use App\NatureOffre;
use App\Sfs;
use App\Ssfs;
use App\Volume;
use App\Zone;
use App\Tarifs;
use Illuminate\Http\Request;
use DB;

use Barryvdh\DomPDF\Facade as PDF;
use Carbon\Carbon;
use DateTime;
use  Dompdf\Options;
use Dompdf\Helpers;
use Dompdf\Exception\ImageException;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Validator;

class BonController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(Request $request)
    {
        $request->user()->authorizeRoles(['ADMIN', 'AGENT DCM']);
        $is_dcm = $request->user()->hasRole('AGENT DCM');
        $is_dpm = $request->user()->hasRole('AGENT DPM');
        $_url = 'service-demande';
        $is_dcm_counter = DB::table('bon_commandes')->where('is_validate_by_dcm', '=', 0)->count();
        $is_dpm_counter = DB::table('bon_commandes')->where('is_validate_by_dpm', '=', 0)->count();
        $bons = BonCommande::orderBy('created_at', 'desc')->get();
        foreach ($bons as $bon) {
            $client = $bon->client()->first();
            $bon->client = $client;
            $bon->date = (new DateTime($bon->date))->format('d-m-Y');
        }

        return view('dashboard.bons.list', compact('bons', 'is_dcm', 'is_dpm', '_url','is_dcm_counter','is_dpm_counter'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $request->user()->authorizeRoles(['ADMIN', 'AGENT DCM']);
        $is_dcm = $request->user()->hasRole('AGENT DCM');
        $is_dpm = $request->user()->hasRole('AGENT DPM');
        $_url = 'new-service-demande';
        $clients = Client::all();

          // Famille de service
          $fs      =  Fs::all();

         //Sous famille de service
        $sous_famille_service_transport    = Sfs::where('id_fs',1)->get();
        $sous_famille_access_service       = Sfs::where('id_fs',2)->get();
        $sous_famille_hebergement_service  = Sfs::where('id_fs',3)->get();
        $sous_famille_voice_service        = Sfs::where('id_fs',4)->get();

        //dd($sous_famille_access_service);
         // Sous sous famille de service
        $ssfs_transport      = Ssfs::where('id_sfs', 1 )->get();
        $ssfs_transit        = Ssfs::where('id_sfs', 4 )->get();
        $ssfs_pylones        = Ssfs::where('id_sfs', 10 )->get();

        $sites = Zone::all();
        $capacites = Capacites::all();
        $distances = Distances::all();
        $volumes = Volume::all();
        $offres  = NatureOffre::all();
        return view('dashboard.bons.create', compact('sites','capacites','distances','volumes','clients', 'is_dcm', 'is_dpm', '_url','sous_famille_service_transport','sous_famille_access_service','sous_famille_hebergement_service','sous_famille_voice_service','ssfs_transport','ssfs_transit','ssfs_pylones','fs','offres'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
          
        $request->user()->authorizeRoles(['ADMIN', 'AGENT DCM']);
       
        $request->validate([
            'num_ref' => 'required|max:50|unique:bon_commandes',
            'client_id' => 'required|max:50',
            'object' => 'required|max:225',
            'Sfs_id'   => 'required',
            'fs'    => 'required'
        ]);


        if ( !isset($request->Ssfs_id) ) {
            $Ssfs_id = NULL;
        }else{

            $Ssfs_id = $request->Ssfs_id;
        }
        if ( !isset($request->capacite) ) {
            $capacite = NULL;
        }else{
            $capacite = $request->capacite;
        }
        if ( !isset($request->distance) ) {
            $distance = NULL;
        }else{
            $distance = $request->distance;
        }
        if ( !isset($request->site) ) {
            $site = NULL;
        }else{
            $site = $request->site;
        }

 
        
        if (!isset($request->total_price_ht)) {    
            return redirect()->back()->withErrors(['montant-invalid' => 'Veuillez afficher le montant avant l\'ajout'])->withInput();
        }elseif($request->total_price_ht=="Has not price"){
            
            $validator = Validator::make($request->all(),[
                'total_price' => 'required'
            ]); 
            if( $validator->fails() ){ 
               return redirect()->back()->withErrors(['montant-invalid' => 'Vous devez saisir le montant pour cette demande avant de l\'ajouter '])->withInput();
            }
        }else{
            
                $tarifs = Tarifs::where( [
                    ['sfs_id' , $request->Sfs_id],
                    ['ssfs_id', $Ssfs_id],
                    ['distance_id' ,'=',  $distance],
                    ['capacite_id' ,'=',  $capacite],
                    ['zone_id' ,'=',$site]
                                         ] )->first();
                
                
                if ($tarifs != NULL) {
                    $montant = $tarifs->tarifs_Or_redevances.' F CFA';
                    $montantTab = explode("\n F CFA",$montant);
                    $montant = $montantTab[0];
                   
                }else{
                    $montant = 'no montant';
                }

                $montantTab = explode(" F CFA",$request->total_price_ht);
                $montEnvoye = $montantTab[0];

                if($montEnvoye != $montant){
                    return redirect()->back()->withErrors(['montant-invalid' => 'Veuillez afficher le montant de cette offre modifier avant de l\'ajouter'])->withInput();
                }

        }

                    
                    $bon = new BonCommande();
                    $bon->num_ref = $request->num_ref;
                    $bon->total_price_ht = $request->total_price_ht;
                    $bon->client_id = $request->client_id;
                    $bon->date = $request->date;
                    $bon->object = $request->object;
                    $bon->fs_id     = $request->fs;
                    $bon->Sfs_id     = $request->Sfs_id;
                    if ($request->Ssfs_id != null ) {
                        $bon->Ssfs_id     = $request->Ssfs_id;
                    }
                    if ($request->capacite != null ) {
                        $bon->capacite_id     = $request->capacite;
                    }
                    if ($request->distance != null ) {
                        $bon->distances_id     = $request->distance;
                    }
                    if ($request->site != null ) {
                        $bon->zones_id    = $request->site;
                    }
                    if ($request->total_price_ht == 'Has not price') {
                        $bon->total_price_ht   = $request->total_price;
                    }
                    elseif($request->total_price_ht != 'Has not price') {
                        $bon->total_price_ht   = $request->total_price_ht;
                    }
                    $bon->zones_id    = $request->site;
                    
                    $bon->num_bon = 'BTB-' . (new DateTime)->format('Ymd') . $bon->id;







                    $frais = Frais_acces::where([
                        ['id_sfs' , $request->Sfs_id],
                        ['id_ssfs', $Ssfs_id],
                        ])->get();

                        if( $frais->all()!=[] ){ 
                            
                            $validator = Validator::make($request->all(),[
                                'capaciteActivation' => 'required'
                            ]);
                                
                            if( $validator->fails() ){ 
                                return redirect()->back()->withErrors(['montantActivation-invalid' => 'Le champs capacité activation est obligatoir si elle est afficher'])->withInput();
                            }else{
                               
                               

                                if ($request->total_priceActivation_ht == null) { 
                                    return redirect()->back()->withErrors(['montantActivation-invalid' => 'Veuillez afficher le frais d\'activation avant ajout'])->withInput();
                                }elseif($request->total_priceActivation_ht == "Has not price"){
                                    return redirect()->back()->withErrors(['montantActivation-invalid' => 'Veuillez verifier la capacité sélectionnée pour cette frais d\'activation. Elle n\'existe pas dans notre catalogue'])->withInput();
                                }else{

                                    $frais = Frais_acces::where([
                                        ['id_sfs' , $request->Sfs_id],
                                        ['id_ssfs', $Ssfs_id],
                                        ['id_capacite', $request->capaciteActivation],
                                        ])->first();
                                    $montantAct = $frais->frais.' F CFA';

                                        if ($montantAct == $request->total_priceActivation_ht) {

                                            if ($request->total_priceActivation_ht != "Has not price") {

                                                $total_ht_table = explode('F CFA' , $request->total_priceActivation_ht );
                                                $total_ht = $total_ht_table[0];



                                                $tva = intval($total_ht)*0.18;
                                                $ttc = $tva + intval($total_ht);

                                                $bon->save();
                                                $num_bons = BonCommande::where('num_ref' , $request->num_ref)->first();
                                                $num_bon = $num_bons->id;

                                                FraisActivation::create([
                                                    'bonCommandes_id'   => $num_bon ,
                                                    'capacites_id'      => $request->capaciteActivation,
                                                    'clients_id'        => $request->client_id,
                                                    'prix'              => intval($total_ht),
                                                    'TVA'               => $tva,
                                                    'TTC'               => $ttc,
                                                ]);
                                            }

                                        }else{
                                            return redirect()->back()->withErrors(['montantActivation-invalid' => 'Ce frais ne correspond pas à la demande '])->withInput();
                                        }
                                } 
                            }
                        }


                    if(!($bon->save()) ){
                        $bon->save();
                    }
                        


                    if ($request->has('description') && $request->has('capacity') && $request->has('mensuality')) {
                        $descriptions = $request->description;
                        $capacitys = $request->capacity;
                        $mensualities = $request->mensuality;
                        foreach ($descriptions as $key => $description) {
                            if ($description != null && $capacitys[$key] != null && $mensualities[$key] != null) {
                                CommandeDetail::create([
                                    'description' => $description,
                                    'capacity' => $capacitys[$key],
                                    'mensuality' => $mensualities[$key],
                                    'bon_id' => $bon->id
                                ]);
                            }
                        }
                    }

                    if (isset($request->add_contrat)) { 
                        $this->createContrat($request, $bon);
                    }

                    return redirect()->route('mail.send');
                
                    
}

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, $id)
    {
        $request->user()->authorizeRoles(['ADMIN']);
        $is_dcm = $request->user()->hasRole('AGENT DCM');
        $is_dpm = $request->user()->hasRole('AGENT DPM');
        $_url = 'service-demande';
        $bon = BonCommande::findOrFail($id);
        $bon->commande_details = CommandeDetail::where('bon_id', $bon->id)->get();
        $clients = Client::all();
        $bon->contrat = Contrat::where('bon_id', $bon->id)->first();


        $fs      =  Fs::all();

        //Sous famille de service
       $sous_famille_service_transport    = Sfs::where('id_fs',1)->get();
       $sous_famille_access_service       = Sfs::where('id_fs',2)->get();
       $sous_famille_hebergement_service  = Sfs::where('id_fs',3)->get();
       $sous_famille_voice_service        = Sfs::where('id_fs',4)->get();

       //dd($sous_famille_access_service);
        // Sous sous famille de service
       $ssfs_transport      = Ssfs::where('id_sfs', 1 )->get();
       $ssfs_transit        = Ssfs::where('id_sfs', 4 )->get();
       $ssfs_pylones        = Ssfs::where('id_sfs', 10 )->get();

       $sites = Zone::all();
       $capacites = Capacites::all();
       $distances = Distances::all();
       $volumes = Volume::all();
       $offres  = NatureOffre::all();



        return view('dashboard.bons.edit', compact('sites','capacites','distances','volumes','bon', 'clients', 'is_dcm', 'is_dpm', '_url','sous_famille_service_transport','sous_famille_access_service','sous_famille_hebergement_service','sous_famille_voice_service','ssfs_transport','ssfs_transit','ssfs_pylones','fs','offres'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

       
        $request->user()->authorizeRoles(['ADMIN']);

       $request->validate([
            'num_ref' => 'required|max:50',
            'client_id' => 'required|max:50',
            'date' => 'required|max:50',
            'object' => 'required|max:225',
            'Sfs_id'   => 'required',
            'fs'    => 'required'
        ]);


        if ( !isset($request->Ssfs_id) ) {
            $Ssfs_id = NULL;
        }else{
            $Ssfs_id = $request->Ssfs_id;
        }
        if ( !isset($request->capacite) ) {
            $capacite = NULL;
        }else{
            $capacite = $request->capacite;
        }
        if ( !isset($request->distance) ) {
            $distance = NULL;
        }else{
            $distance = $request->distance;
        }
        if ( !isset($request->site) ) {
            $site = NULL;
        }else{
            $site = $request->site;
        }

        /**************************************************************************** */
        if (!isset($request->total_price_ht)) {    
            return redirect()->back()->withErrors(['montant-invalid' => 'Veuillez afficher le montant avant l\'ajout']);
        }elseif($request->total_price_ht=="Has not price"){
            
            $validator = Validator::make($request->all(),[
                'total_price' => 'required'
            ]); 
            if( $validator->fails() ){ 
               return redirect()->back()->withErrors(['montant-invalid' => 'Vous devez saisir le montant pour cette demande avant de l\'ajouter ']);
            }
        }else{
            
                $tarifs = Tarifs::where( [
                    ['sfs_id' , $request->Sfs_id],
                    ['ssfs_id', $Ssfs_id],
                    ['distance_id' ,'=',  $distance],
                    ['capacite_id' ,'=',  $capacite],
                    ['zone_id' ,'=',$site]
                                         ] )->first();
                
                
                if ($tarifs != NULL) {
                    $montant = $tarifs->tarifs_Or_redevances.' F CFA';
                    $montantTab = explode("\n F CFA",$montant);
                    $montant = $montantTab[0];
                   
                }else{
                    $montant = 'no montant';
                }

                $montantTab = explode(" F CFA",$request->total_price_ht);
                $montEnvoye = $montantTab[0];

                    


                if($montEnvoye != $montant){
                    return redirect()->back()->withErrors(['montant-invalid' => 'Veuillez afficher le montant de cette offre modifier avant de l\'ajouter']);
                }
            }

                    
                    $bon = BonCommande::findOrFail($id);
                    $bon->num_ref = $request->num_ref;
                    $bon->total_price_ht = $request->total_price_ht;
                    $bon->client_id = $request->client_id;
                    $bon->date = $request->date;
                    $bon->object = $request->object;
                    $bon->fs_id     = $request->fs;
                    $bon->Sfs_id     = $request->Sfs_id;
                    if ($request->Ssfs_id != null ) {
                        $bon->Ssfs_id     = $request->Ssfs_id;
                    }
                    if ($request->capacite != null ) {
                        $bon->capacite_id     = $request->capacite;
                    }
                    if ($request->distance != null ) {
                        $bon->distances_id     = $request->distance;
                    }
                    if ($request->site != null ) {
                        $bon->zones_id    = $request->site;
                    }
                    if ($request->total_price_ht == 'Has not price') {
                        $bon->total_price_ht   = $request->total_price;
                    }
                    else if($request->total_price_ht != 'Has not price') {
                        $bon->total_price_ht   = $request->total_price_ht;
                    }


                    $frais = Frais_acces::where([
                        ['id_sfs' , $request->Sfs_id],
                        ['id_ssfs', $Ssfs_id],
                        ])->get();

                        if( $frais->all()!=[] ){ 
                            
                            $validator = Validator::make($request->all(),[
                                'capaciteActivation' => 'required'
                            ]);
                                
                            if( $validator->fails() ){ 
                                return redirect()->back()->withErrors(['montantActivation-invalid' => 'Le champs capacité activation est obligatoire si elle est afficher']);
                            }else{
                               
                               

                                if ($request->total_priceActivation_ht == null) { 
                                    return redirect()->back()->withErrors(['montantActivation-invalid' => 'Veuillez afficher le frais d\'activation avant ajout']);
                                }elseif($request->total_priceActivation_ht == "Has not price"){
                                    return redirect()->back()->withErrors(['montantActivation-invalid' => 'Veuillez verifier la capacité sélectionnée pour cette frais d\'activation. Elle n\'existe pas dans notre catalogue']);
                                }else{

                                    $frais = Frais_acces::where([
                                        ['id_sfs' , $request->Sfs_id],
                                        ['id_ssfs', $Ssfs_id],
                                        ['id_capacite', $request->capaciteActivation],
                                        ])->first();
                                    $montantAct = $frais->frais.' F CFA';

                                        if ($montantAct == $request->total_priceActivation_ht) {

                                            if ($request->total_priceActivation_ht != "Has not price") {

                                                $total_ht_table = explode('F CFA' , $request->total_priceActivation_ht );
                                                $total_ht = $total_ht_table[0];



                                                $tva = intval($total_ht)*0.18;
                                                $ttc = $tva + intval($total_ht);

                                                $bon->save();
                                                $num_bons = BonCommande::where('num_ref' , $request->num_ref)->first();
                                                $num_bon = $num_bons->id;

                                                FraisActivation::create([
                                                    'bonCommandes_id'   => $num_bon ,
                                                    'capacites_id'      => $request->capaciteActivation,
                                                    'clients_id'        => $request->client_id,
                                                    'prix'              => intval($total_ht),
                                                    'TVA'               => $tva,
                                                    'TTC'               => $ttc,
                                                ]);
                                            }

                                        }else{
                                            return redirect()->back()->withErrors(['montantActivation-invalid' => 'Ce frais ne correspond pas à la demande ']);
                                        }
                                } 
                            }
                        }


                    if(!($bon->save()) ){
                        $bon->save();
                    }
                        

        /**************************************************************************** */


      $bon_contrat = Contrat::where('bon_id', $bon->id)->first();
       
        if (!$bon_contrat) {
            if (isset($request->add_contrat)) {
                $this->createContrat($request, $bon);
            }
           
        } else {
           
            $validator = Validator::make($request->all(),[
                'begin_date' => 'required',
                'end_date' => 'required',
                'object_contrat' => 'required|max:255',
            ]);
           
           
            if( $validator->fails() ){
                return Redirect::back()->withErrors($validator)->withInput();
            }else{
                $begin_date = Carbon::create($request->begin_date);
                $end_date = Carbon::create($request->end_date);
    
                
                $bon_contrat->duree_contrat = $end_date->diffInYears($begin_date);
                $bon_contrat->begin_date = $request->begin_date;
                $bon_contrat->end_date = $request->end_date;
                $bon_contrat->object_contrat = $request->object_contrat;
              
                $bon_contrat->save();
            }
        }

        if ($request->has('description') && $request->has('capacity') && $request->has('mensuality') && $request->has('detail_id')) {
            $descriptions = $request->description;
            $capacitys = $request->capacity;
            $mensualities = $request->mensuality;
            $detail_id = $request->detail_id;
            foreach ($descriptions as $key => $description) {
                if (array_key_exists($key, $detail_id)) {
                    $detail = CommandeDetail::find($detail_id[$key]);
                    $detail->description = $description;
                    $detail->capacity = $capacitys[$key];
                    $detail->mensuality = $mensualities[$key];
                    $detail->save();
                } else {
                    if ($description != null && $capacitys[$key] != null && $mensualities[$key] != null) {
                        CommandeDetail::create([
                            'description' => $description,
                            'capacity' => $capacitys[$key],
                            'mensuality' => $mensualities[$key],
                            'bon_id' => $bon->id
                        ]);
                    }
                }
            }
        }

        return redirect()->route('dashboard.bons');

}

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function delete(Request $request, $id)
    {

        $request->user()->authorizeRoles(['ADMIN']);

        $bon = BonCommande::findOrFail($id);
        $bon_details = CommandeDetail::where('bon_id', $bon->id)->get();

        foreach ($bon_details as $bon_detail) {
            $bon_detail->delete();
        }
        $bon_pvs = CommandeProtocole::where('bon_id', $bon->id)->get();
        foreach ($bon_pvs as $bon_pv) {
            $bon_pv->delete();
            $pv_factures = Facture::where('pv_id' , $bon_pv->id)->first();
            if($pv_factures != null){
            $pv_factures->delete();
            }
        }
        $bon_contrat = Contrat::where('bon_id', $bon->id)->first();
        if ($bon_contrat) {
            $bon_contrat->delete();
        }
        $bon->delete();
        return redirect()->route('dashboard.bons');
    }

    public function pdfBon()
    {
        $bons = BonCommande::all();

        foreach ($bons as $bon) {
            $client = $bon->client()->first();
            $bon->client = $client;
        }
        //gestion images
        $path = 'assets/images/btcom.png';
        asset('assets/images/btcom.png');

        $type = pathinfo($path, PATHINFO_EXTENSION);
        $data = file_get_contents($path);
        $base64 = 'data:image/' . $type . ';base64,' . base64_encode($data);

        $options = new Options();
        $options->set('isRemoteEnabled', true);
        $pdf = new PDF($options);

        // $pdf = new DOMPDF($options);
        $pdf = \PDF::loadView('dashboard.bons.pdfBon', compact('bons', 'base64'));


        return $pdf->stream('liste_des_commandes.pdf');
        // return view('dashboard.bons.pdfBon', compact('bons'));


    }

    public function detailsBon(Request $request, $id)
    {
        $request->user()->authorizeRoles(['ADMIN', 'AGENT DCM']);
        $is_dcm = $request->user()->hasRole('AGENT DCM');
        $is_dpm = $request->user()->hasRole('AGENT DPM');
        $_url = 'service-demande';
        $bon = BonCommande::findOrFail($id);
        $bon->date = (new DateTime($bon->date))->format('d-m-Y');
        $bon->client = $bon->client()->first();
        $bon->details = CommandeDetail::where('bon_id', $bon->id)->get();
        $bon->contrat = Contrat::where('bon_id', $bon->id)->first();

        return view('dashboard.bons.detailsBon', compact('bon', '_url', 'is_dcm', 'is_dpm'));
    }

    public function pdfDetail()
    {
        $bons = BonCommande::all();

        foreach ($bons as $bon) {
            $client = $bon->client()->first();
            $bon->client = $client;
        }
        //gestion images
        $path = 'assets/images/btcom.png';
        asset('assets/images/btcom.png');
        $type = pathinfo($path, PATHINFO_EXTENSION);
        $data = file_get_contents($path);
        $base64 = 'data:image/' . $type . ';base64,' . base64_encode($data);

        $options = new Options();
        $options->set('isRemoteEnabled', true);
        $pdf = new PDF($options);

        // $pdf = new DOMPDF($options);
        $pdf = \PDF::loadView('dashboard.bons.pdfDetail', compact('bons', 'base64'));


        return $pdf->stream('liste_bons.pdf');
        // return view('dashboard.bons.pdfBon', compact('bons'));


    }

    public function createContrat(Request $request, BonCommande $bon)
    {
        $request->validate([
            'num_contrat' => 'required',
            'begin_date' => 'required',
            'end_date' => 'required',
            'object_contrat' => 'required|max:255',
        ]);

        $begin_date = Carbon::create($request->begin_date);
        $end_date = Carbon::create($request->end_date);

        $contrat = new Contrat();
        $contrat->num_contrat = $request->num_contrat;
        $contrat->duree_contrat = $end_date->diffInYears($begin_date);

        $contrat->begin_date = $request->begin_date;
        $contrat->end_date = $request->end_date;
        $contrat->object_contrat = $request->object_contrat;
        $contrat->bon_id = $bon->id;
        $contrat->save();

    }

    public function ajaxRequest( Request $request){

            $ssfs = NULL;
            $montant = '';

            if ( isset($request->Ssfs_id) ) {
                $ssfs = $request->Ssfs_id;
            }

            $sfs = $request->Sfs_id;
            $capacite = $request->capacite;
            $distance = $request->distance;
            $site = $request->site;
            //dd($request);
            $tarifs = Tarifs::where( [
                ['sfs_id' , $sfs],
                ['ssfs_id', $ssfs],
                ['distance_id' ,'=',  $distance],
                ['capacite_id' ,'=',  $capacite],
                ['zone_id' ,'=',$site]
                                     ] )->first();
            if ($tarifs != NULL ) {
                $montant = $tarifs->tarifs_Or_redevances .' F CFA';
            }else{
                $montant = 'Has not price';
            }



        return response()->json(['message' => $montant ]);
    }

    public function ajaxRequestAfficherForm(Request $request){

        $frais_table = Frais_acces::where([
                                        ['id_sfs' , $request->Sfs_id],
                                        ['id_ssfs', $request->Ssfs_id],
                                        ])->first();

        if($frais_table != null){
            $rep = true;
        }else{
            $rep = false;
        }

        return response()->json(['message' => $rep ]);
    }


    public function ajaxRequestAfficherMont(Request $request){

        $montant = "";

        $frais = Frais_acces::where([
            ['id_sfs' , $request->Sfs_id],
            ['id_ssfs', $request->Ssfs_id],
            ['id_capacite', $request->capaciteActivation_id],
            ])->first();

            if($frais != null){
                $montant = $frais->frais.' F CFA';
            }else{
                $montant = "Has not price";
            }

        return response()->json(['message' => $montant ]);
    }

}



