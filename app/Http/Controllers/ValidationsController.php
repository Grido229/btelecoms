<?php

namespace App\Http\Controllers;

use App\BonCommande;
use DateTime;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class ValidationsController extends Controller
{
    public function dcm(Request $request)
    {
        $request->user()->authorizeRoles(['ADMIN', 'AGENT DCM']);
        $is_dcm = $request->user()->hasRole('AGENT DCM');
        $is_dpm = $request->user()->hasRole('AGENT DPM');
        $_url = 'dcm-validation';
        $type = 'non_validate';

        $bons = BonCommande::where('is_validate_by_dcm', 0)->get();
        foreach($bons as $bon) {
            $client = $bon->client()->first();
            $bon->client = $client;
            $bon->date = (new DateTime($bon->date))->format('d-m-Y');
        }
        return view('dashboard.validations.dcm', compact('is_dcm', 'is_dpm', '_url', 'bons', 'type'));
    }

    public function validateDcmCommande(Request $request, $id)
    {
        $request->user()->authorizeRoles(['ADMIN', 'AGENT DCM']);
        $bon = BonCommande::findOrFail($id);
        $bon->is_validate_by_dcm = 1;
        $bon->dcm_user = Auth::user()->id;
        $bon->save();

        //return redirect()->route('dashboard.validations.dcm');
        return redirect()->route('mail.dpm.send');
    }

    public function filterDcm(Request $request)
    {
        $request->user()->authorizeRoles(['ADMIN', 'AGENT DCM']);
        $is_dcm = $request->user()->hasRole('AGENT DCM');
        $is_dpm = $request->user()->hasRole('AGENT DPM');
        $_url = 'dcm-validation';
        $type = 'non_validate';

        if($request->filter == 'non_validate') {
            $bons = BonCommande::where('is_validate_by_dcm', 0)->get();
            $type = 'non_validate';
        } else if($request->filter == 'already_validate') {
            $bons = BonCommande::where('is_validate_by_dcm', 1)->get();
            $type = 'already_validate';
        }

        foreach($bons as $bon) {
            $client = $bon->client()->first();
            $bon->date = (new DateTime($bon->date))->format('d-m-Y');
            $bon->client = $client;
        }
        return view('dashboard.validations.dcm', compact('is_dcm', 'is_dpm', '_url', 'bons', 'type'));
        //return redirect()->route('mail.dpm.send');
    }

    public function dpm(Request $request)
    {
        $request->user()->authorizeRoles(['ADMIN', 'AGENT DPM']);
        $is_dcm = $request->user()->hasRole('AGENT DCM');
        $is_dpm = $request->user()->hasRole('AGENT DPM');
        $_url = 'dpm-validation';
        $type = 'non_validate';

        $bons = BonCommande::where('is_validate_by_dcm', 1)
                            ->where('is_validate_by_dpm', 0)
                            ->get();
        foreach($bons as $bon) {
            $client = $bon->client()->first();
            $bon->date = (new DateTime($bon->date))->format('d-m-Y');
            $bon->client = $client;
        }
        return view('dashboard.validations.dpm', compact('is_dcm', 'is_dpm', '_url', 'bons', 'type'));
    }

    public function filterDpm(Request $request)
    {
        $request->user()->authorizeRoles(['ADMIN', 'AGENT DPM']);
        $is_dcm = $request->user()->hasRole('AGENT DCM');
        $is_dpm = $request->user()->hasRole('AGENT DPM');
        $_url = 'dpm-validation';

        if($request->filter == 'non_validate') {
            $bons = BonCommande::where('is_validate_by_dcm', 1)
                                ->where('is_validate_by_dpm', 0)
                                ->get();
            $type = 'non_validate';
        } else if($request->filter == 'already_validate') {
            $bons = BonCommande::where('is_validate_by_dcm', 1)
                                ->where('is_validate_by_dpm', 1)
                                ->get();
            $type = 'already_validate';
        }

        foreach($bons as $bon) {
            $client = $bon->client()->first();
            $bon->date = (new DateTime($bon->date))->format('d-m-Y');
            $bon->client = $client;
        }
        return view('dashboard.validations.dpm', compact('is_dcm', 'is_dpm', '_url', 'bons', 'type'));
    }

    public function validateDpmCommande(Request $request, $id)
    {
        $request->user()->authorizeRoles(['ADMIN', 'AGENT DPM']);
        $bon = BonCommande::findOrFail($id);
        $bon->is_validate_by_dpm = 1;
        $bon->dpm_user = Auth::user()->id;
        $bon->save();

        return redirect()->route('dashboard.validations.dpm');
        //return redirect()->route('mail.dpm.send');

    }

    public function validateDpmDetails(Request $request , $id){
        $request->user()->authorizeRoles(['ADMIN', 'AGENT DPM']);
        $is_dcm = $request->user()->hasRole('AGENT DCM');
        $is_dpm = $request->user()->hasRole('AGENT DPM');
        $_url = 'dpm-validation';

        $bon = BonCommande::where('id' , $id)->first();

        return view('dashboard.validations.dpmDetails' , compact('is_dcm', 'is_dpm', '_url', 'bon'));
    }

    public function validateDcmDetails(Request $request , $id){
        $request->user()->authorizeRoles(['ADMIN', 'AGENT DPM']);
        $is_dcm = $request->user()->hasRole('AGENT DCM');
        $is_dpm = $request->user()->hasRole('AGENT DPM');
        $_url = 'dcm-validation';

        $bon = BonCommande::where('id' , $id)->first();

        return view('dashboard.validations.dcmDetails' , compact('is_dcm', 'is_dpm', '_url', 'bon'));
    }


}
