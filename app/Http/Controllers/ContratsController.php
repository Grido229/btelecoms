<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class ContratsController extends Controller
{

    public function create(Request $request)
    {
        $request->user()->authorizeRoles(['ADMIN', 'AGENT DCM']);
        $is_dcm = $request->user()->hasRole('AGENT DCM');
        $is_dpm = $request->user()->hasRole('AGENT DPM');
        $_url = 'new-service-demande';

        return view('dashboard.contrats.create', compact('is_dcm', 'is_dpm', '_url'));
    }


}
