<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class TestController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $file = public_path('files/fraisAcces.csv');
        $readFile = fopen($file,'r');

            while (!feof($readFile)) {
                $ligne = fgets($readFile,4096);
                $table_ligne = explode(';' , $ligne);

                $id_sfs = (isset($table_ligne[0])) ? $table_ligne[0] : NULL;
                $id_ssfs = (isset($table_ligne[1])) ? $table_ligne[1] : NULL;
                $id_capacite = (isset($table_ligne[2])) ? $table_ligne[2] : NULL;
                $frais = (isset($table_ligne[3])) ? $table_ligne[3] : NULL;
                $frais_mod = (isset($table_ligne[4])) ? $table_ligne[4] : NULL;
                echo $id_ssfs; 


            }

            

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
