<?php

namespace App\Http\Controllers;

use App\BonCommande;
use App\Bordereau;
use App\CommandeProtocole;
use App\Sfs;
use App\Ssfs;
use App\Zone;
use App\Volume;
use App\Distances;
use App\Capacites;
use App\Client;
use App\Fs;
use App\Tarifs;
use App\Facture;
use App\FactureGlobale;
use App\Frais_acces;
use App\Signataire;
use App\FraisActivation;
use DateTime;
use PDF;
use  Dompdf\Options;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Date;
use Illuminate\Support\Facades\Redirect ;
use Illuminate\Support\Facades\Validator ;
use Illuminate\Support\Facades\Session ;
class InvoiceController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(Request $request)
    {
        $request->user()->authorizeRoles(['ADMIN']);
        $is_dcm = $request->user()->hasRole('AGENT DCM');
        $is_dpm = $request->user()->hasRole('AGENT DPM');
        $_url = 'list-invoices';
        $pvs = CommandeProtocole::orderBy('created_at', 'desc')->get();
        $factures = Facture::all();
        return view('dashboard.factures.list', compact('factures' , 'pvs','is_dcm', 'is_dpm', '_url'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request , $id)
    {

        $request->user()->authorizeRoles(['ADMIN']);
        $is_dcm = $request->user()->hasRole('AGENT DCM');
        $is_dpm = $request->user()->hasRole('AGENT DPM');
        $_url = 'add-invoice';
        $date = now();
        $table_date = explode(' ' , $date);
        $date = $table_date[0];
        $separateur_date = explode('-' , $date);
        $annee = $separateur_date[0];
        $mois = $separateur_date[1];
        //$code = sprintf('%04d' , 0001);
        $facture = Facture::all();
        $numero_facture = '' ;
        if ( $facture->isEmpty() ) {
            $numero_facture = $annee.''.$mois.'BTI0001';

        }else{
            $facture = Facture::latest('id')->first();
            //dd($facture);
            $ancien_Code = $facture->No_facture;

            $table_ancien_code = explode('BTI' , $ancien_Code);
            //dd( $table_ancien_code);
            $code = $table_ancien_code[1] + 1;
            $code = sprintf('%04d' , $code);
            $numero_facture = $annee.''.$mois.'BTI'.$code;
        }
            $bon = BonCommande::where('id' , $id)->first();
            $pv = CommandeProtocole::where('id' , $id)->first();
            $bon = BonCommande::where('id' , $pv->bon_id)->first();

                    $insert = Facture::create([
                        'No_facture' => $numero_facture,
                        'pv_id'     =>  $id,
                        'montant_fact'   =>  $bon->total_price_ht,
                    ]);


            if ($insert) {
                return redirect()->route('dashboard.pv');
            }else{
                echo 'facture creation failled ';
                return redirect()->route('dashboard.pv');
            }
    }


    public function store(Request $request){
        $request->user()->authorizeRoles(['ADMIN']);
        $is_dcm = $request->user()->hasRole('AGENT DCM');
        $is_dpm = $request->user()->hasRole('AGENT DPM');



    }
    public function bordereauForm(Request $request){
        $request->user()->authorizeRoles(['ADMIN']);
        $is_dcm = $request->user()->hasRole('AGENT DCM');
        $is_dpm = $request->user()->hasRole('AGENT DPM');
        $_url = 'bordereau';
        return view('dashboard.factures.create_bordereau' , compact('is_dcm' , 'is_dpm' , '_url'));
    }

    public function bordereau(Request $request){

        $validator = Validator::make($request->all(),[
            'num_poste'  => 'required',
            'type_bord'  => 'required',
            'resp_maj'  => 'required',
            'num_device'  => 'required'

        ]);

        if ($validator->fails()) {
            # code...
            return Redirect::back()->withErrors($validator)->withInput();
        } else{
        $months     = date('m');
        $years      = date('Y');
        $date       = date('mdy');
        $type_bord  = $request->type_bord;
        $n°_bord    =$type_bord.''.$months.''.$years.'OS';
        $bordereau  = Bordereau::create([
            'num_bordereau'       => $n°_bord ,
            'num_poste'           => $request->num_poste,
            'type_bordereau'      => $request->type_bord,
            'resp_maj'            => $request->resp_maj,
            'num_device'      => $request->num_device
        ]);

        if ($bordereau) {
            # code...
            return redirect()->route('bordereau.list') ;
        }

    }
    }
    public function details($id , Request $request){
        $request->user()->authorizeRoles(['ADMIN']);
        $is_dcm = $request->user()->hasRole('AGENT DCM');
        $is_dpm = $request->user()->hasRole('AGENT DPM');
        $_url = 'list-invoices';

        $facture = Facture::where('id' , $id)->with('pv','bordereau')->first();

        $pv = CommandeProtocole::with('ssfs', 'sfs', 'volume', 'distance' , 'capacite','zone')->first();
        //$pv = CommandeProtocole::where('id' , 1)->with('ssfs' , 'sfs')->first();

        //dd($pv->ssfs_id);

        return view('dashboard.factures.details_facture' , compact('facture','pv','is_dcm' , 'is_dpm' , '_url'));
    }
    public function bordereauList(Request $request){
        $request->user()->authorizeRoles(['ADMIN']);
        $is_dcm = $request->user()->hasRole('AGENT DCM');
        $is_dpm = $request->user()->hasRole('AGENT DPM');
        $_url = 'list_bordereau';
        $bordereaux = Bordereau::all();
        return view('dashboard.factures.list_bordereau',compact('is_dcm' , 'is_dpm' , '_url','bordereaux'));
    }

    public function generateForm(Request $request , $id){
        $request->user()->authorizeRoles(['ADMIN']);
        $is_dcm = $request->user()->hasRole('AGENT DCM');
        $is_dpm = $request->user()->hasRole('AGENT DPM');
        $_url = 'list_bordereau';
         $signataires = Signataire::all();
         $bordereaux  = Bordereau::all();
        return view('dashboard.factures.form_generate',compact('is_dcm' , 'is_dpm' , '_url','bordereaux','id','signataires'));
    }

    public function factureUpdate(Request $request, $id){

        /*$signataire = $request->name_signataire;
        $signataire_infos = Signataire::where("fullname" , $signataire)->first();
        $signataire_function = $signataire_infos->status;*/


        $validator = Validator::make($request->all(),[
            'date_limite'  => 'required',
            'signataires_id'=> 'required',
            'num_bord'     => 'required',
            'facture_type' => 'required',
            'start_date'   =>  'required',
            'end_date'     =>  'required',
        ]);
        //dd($validator->fails());
        if ($validator->fails()) {
            # code...
            return Redirect::back()->withErrors($validator)->withInput();
        } else{

            $facture = Facture::find($id);

                    $facture_montant = $facture->montant_fact ;
                    $facture_montant = intval($facture_montant);

                    if ($request->facture_type == 'Trimestrielle') {
                        $montant = $facture_montant * 3 ;
                     }
                     elseif ($request->facture_type == 'Semestrielle') {
                        $montant = $facture_montant * 6 ;
                     }

                     elseif ($request->facture_type == 'Mensuelle') {

                        $montant = $facture_montant ;
                     }

                     elseif ($request->facture_type == 'Annuelle') {

                        $montant = $facture_montant * 12 ;
                     }


                        $date_table = explode('-',$request->start_date);
                        $month = $date_table[1];
                        $month = sprintf('%01d' , $month);
                        $year = $date_table[0];
                        $month_table = array('Janvier','Fevrier','Mars','Avril','Mai','Juin','Juillet','Aout','Septembre','Octobre','novembre','Decembre');
                        $month = $month_table[$month-1];
                        $periode = $month.''.$year;

                    $montant_TVA = $montant * 0.18 ;

                    $montantTTC  = $montant + $montant_TVA ;
                    $montantTTC  = intval($montantTTC);

                    //Calcul sur le nombre de jours facturés
                    $start_date = new DateTime($request->start_date);
                    $end_date   = new DateTime($request->end_date);
                    $timeElapsed = $end_date->diff($start_date); // différence de temps entre les deux datess

                    // Mise à jour de la table facture

                    if($request->facture_type == "Mensuel"){
                        $request->facture_type = 'Mensuel '.date("Y");
                    }else{
                        $request->facture_type = $request->facture_type.' '.date("Y");
                    }
                    $facture = $facture->update([
                        'date_limite' => $request->date_limite,
                        'type_facture' => $request->facture_type,
                        'bordereau_id'=> $request->num_bord,'TVA' => $montant_TVA  ,
                        'montant_TTC' => $montantTTC ,
                        'signataires_id' => $request->signataires_id,
                        'montant_fact' => $montant,
                        'nbr_jours_fact'  => $timeElapsed->d,
                        'periode_fact'    => $periode
                        ]);

        //}



        return redirect()->route('facture.generate.generate' , $id);
    }
    }


    public function generateFacture(Request $request , $id ){

        $facture = Facture::find($id);


        if($facture){
            $facture = Facture::where('id' , $id)->with('pv')->first();
            $pv = CommandeProtocole::with('bon')->get();
           // dd($facture);
            $date_limite = new DateTime($facture->date_limite);
            $date_limite = $date_limite->format('d-m-Y');
             //dd();
            $options = new Options();
            $options->set('isRemoteEnabled', true);
            $pdf = new PDF($options);
            $formatter = \NumberFormatter::create('fr_FR', \NumberFormatter::SPELLOUT);
            $formatter->setAttribute(\NumberFormatter::FRACTION_DIGITS, 0);
            $formatter->setAttribute(\NumberFormatter::ROUNDING_MODE, \NumberFormatter::ROUND_HALFUP);
            $montant_lettre = $formatter->format($facture->montant_TTC);


            if ($facture->pv->type == 'capacite') {
                $pdf = \PDF::loadView('dashboard.factures.capacite_facture' , compact('facture','montant_lettre','date_limite'));
                return $pdf->stream('capacity_pv.pdf' , compact('facture'));
            }

            else if ($facture->pv->type == 'colocalisation') {
                $pdf = \PDF::loadView('dashboard.factures.colocalisation_facture' , compact('facture','montant_lettre','date_limite'));
                return $pdf->stream('capacity_pv.pdf' , compact('facture'));

            }

        }


    }

    public function signataireForm(Request $request){
        $request->user()->authorizeRoles(['ADMIN']);
        $is_dcm = $request->user()->hasRole('AGENT DCM');
        $is_dpm = $request->user()->hasRole('AGENT DPM');
        $_url = 'create_signataire';
        return view('dashboard.factures.create_signataire',compact('is_dcm' , 'is_dpm' , '_url'));
    }

    public function signataire(Request $request){
        $request->validate([
            'name'      => 'required',
            'function'  => 'required'
        ]);
        $signataire = new Signataire();
        $signataire->fullname = $request->name ;
        $signataire->status   = $request->function ;
        $signataire = $signataire->save();
         if ($signataire) {
             return redirect()->route('signataire.list');
         }
    }

    public function signataireList(Request $request){
        $request->user()->authorizeRoles(['ADMIN']);
        $is_dcm = $request->user()->hasRole('AGENT DCM');
        $is_dpm = $request->user()->hasRole('AGENT DPM');
        $_url = 'list_signataire';
        $signataires = Signataire::all();
        return view('dashboard.factures.signataire_list',compact('is_dcm' , 'is_dpm' , '_url','signataires'));
    }

    public function factureGlobale(Request $request){
        $request->user()->authorizeRoles(['ADMIN']);
        $is_dcm = $request->user()->hasRole('AGENT DCM');
        $is_dpm = $request->user()->hasRole('AGENT DPM');
        $_url = 'list_global';
        $factures_globale = FactureGlobale::all();

        return view('dashboard.factures.list_factures_globale',compact('is_dcm' , 'is_dpm' , '_url','factures_globale'));

    }

    public function createFactureGlobale(Request $request){
        $request->user()->authorizeRoles(['ADMIN']);
        $is_dcm = $request->user()->hasRole('AGENT DCM');
        $is_dpm = $request->user()->hasRole('AGENT DPM');
        $_url = '';
        $signataire = Signataire::all();
        $clients    = Client::all();
        return view('dashboard.factures.create_facture_globale',compact('is_dcm' , 'is_dpm' , '_url','clients','signataire'));

    }

    public function factureGlobaleStore(Request $request){
        $client_exist = array();
         $client_id = $request->client ;
        $date = now();
        $table_date = explode(' ' , $date);
        $date = $table_date[0];
        $separateur_date = explode('-' , $date);
        $annee = $separateur_date[0];
        $mois = $separateur_date[1];
        $numero_facture = '' ;
        $facture_old = FactureGlobale::all();
        if ( $facture_old->isEmpty() ) {
            $numero_facture = $annee.''.$mois.'FSBTI0001';

        }else{
            $facture = FactureGlobale::latest('id')->first();
            $ancien_Code = $facture->No_facture;
            $table_ancien_code = explode('BTI' , $ancien_Code);
            $code = $table_ancien_code[1] + 1;
            $code = sprintf('%04d' , $code);
            $numero_facture = $annee.''.$mois.'FSBTI'.$code;
        }
        $client   = Client::find($client_id);
        $factures = Facture::all();
            foreach ($factures as $key => $facture) {
                if ($client->enterprise_name == $facture->pv->bon->client->enterprise_name) {

                            if ($facture->pv->type == 'capacite') {
                                $factures = new FactureGlobale();
                                $factures->No_facture = $numero_facture ;
                                $factures->factures_capacite_id = $facture->id ;
                                $factures->type_capacite        = 1 ; 
                                $factures->signataires_id  = $request->signataire ;
                                
                                    
                            } 
                            if ($facture->pv->type == 'colocalisation') {
                                $factures->pv_id = $facture->pv->id ; 
                                $factures->type_colocalisation = 1;
                                if(isset($factures->type_capacite)){
                                    if ($factures->type_capacite == 1){
                                        $factures = $factures->save();
                                    }else{
                                        Session::flash('error suscription');
                                        
                                    }
                                } else{
                                    Session::flash('error suscription');
                                }
                                
                            } 

                
                } 
                

                
                    array_push($client_exist , $facture->pv->bon->client->enterprise_name);
        } 
        if (!in_array($client->enterprise_name , $client_exist)) {
           Session::flash('any suscription');
        }
        return redirect()->route('factureGlobale.list');

    }

    public function detailFactureGlobale(Request $request , $id ){
        $request->user()->authorizeRoles(['ADMIN']);
        $is_dcm = $request->user()->hasRole('AGENT DCM');
        $is_dpm = $request->user()->hasRole('AGENT DPM');
        $_url = 'detail_global';
        $facture = FactureGlobale::find($id);
        return view('dashboard.factures.details_facture_globale' , compact('is_dcm' , 'is_dpm' , '_url','facture'));
    }
    public function generateFactureGlobale(Request $request , $id){
         // Récupération des montants TTC pour chaque type de facture
        $facture = FactureGlobale::find($id);
         $montant_TTC_capacite = $facture->facture->montant_TTC ;
         $montant_TTC_colocalisation = $facture->pv->facture->montant_TTC ;
         if ($montant_TTC_capacite == null) {
            Session::flash('Error Facture Capacite');
            return Redirect::back();
         } else if ($montant_TTC_colocalisation == null) {
             Session::flash('Error Facture Colocalisation') ;
             return Redirect::back();
         }
         $montant_global = $montant_TTC_capacite + $montant_TTC_colocalisation ;
        $options = new Options();
        $options->set('isRemoteEnabled', true);
        $pdf = new PDF($options);
        $formatter = \NumberFormatter::create('fr_FR', \NumberFormatter::SPELLOUT);
        $formatter->setAttribute(\NumberFormatter::FRACTION_DIGITS, 0);
        $formatter->setAttribute(\NumberFormatter::ROUNDING_MODE, \NumberFormatter::ROUND_HALFUP);
        $montant_lettre = $formatter->format($montant_global);
         if ($montant_TTC_capacite != null  ) {
            $pdf = \PDF::loadView('dashboard.factures.factures_globale' , compact('facture','montant_lettre','montant_lettre','montant_global'));
            return $pdf->stream('globale_facture.pdf' , compact('facture'));
         }

         else{
             // Message pour indiquer que des factures périodiques ne sont pas générés
         }

    }

    //=======================================Frais Activation=====================================================

        public function frais_activation(Request $request){
            $request->user()->authorizeRoles(['ADMIN']);
            $is_dcm = $request->user()->hasRole('AGENT DCM');
            $is_dpm = $request->user()->hasRole('AGENT DPM');
            $_url = 'list-invoices-fraisActivation';

            $fraisActivations = FraisActivation::all();

        return view('dashboard.factures.fraisActivation',compact('fraisActivations' , 'is_dcm' , 'is_dpm' , '_url'));
        }


        public function frais_activation_create(Request $request , $id){
            $request->user()->authorizeRoles(['ADMIN']);
            $is_dcm = $request->user()->hasRole('AGENT DCM');
            $is_dpm = $request->user()->hasRole('AGENT DPM');
            $_url = 'list-invoices-fraisActivation';

            $ligne = FraisActivation::find($id);
            $id = $ligne->id;
            $signataires = Signataire::all();
            /*$bons = BonCommande::all();
            $clients = Client::all();
            $capacites = Capacites::all();


            $fs      =  Fs::all();

            $sous_famille_service_transport    = Sfs::where('id_fs',1)->get();
            $sous_famille_access_service       = Sfs::where('id_fs',2)->get();
            $sous_famille_hebergement_service  = Sfs::where('id_fs',3)->get();
            $sous_famille_voice_service        = Sfs::where('id_fs',4)->get();

             // Sous sous famille de service
            $ssfs_transport      = Ssfs::where('id_sfs', 1 )->get();
            $ssfs_transit        = Ssfs::where('id_sfs', 4 )->get();
            $ssfs_pylones        = Ssfs::where('id_sfs', 10 )->get();*/

        return view('dashboard.factures.fraisActivation_create',compact('id' , 'signataires' , 'is_dcm' , 'is_dpm' , '_url'));
        }



        public function ajaxRequest( Request $request){

            $ssfs = NULL;
            $montant = '';

            if ( isset($request->Ssfs_id) ) {
                $ssfs = $request->Ssfs_id;
            }

            $sfs = $request->Sfs_id;
            $capacite = $request->capacite;

            $frais = Frais_acces::where( [
                ['id_sfs' , $sfs],
                ['id_ssfs', $ssfs],
                ['id_capacite' ,'=',  $capacite],
                                    ] )->first();
            if ($frais != NULL ) {
                $montant = $frais->frais .' F CFA';
            }else{
                $montant = 'Has not price';
            }


        return response()->json(['message' => $montant ]);
    }




        public function frais_activation_store( Request $request , $id){
            $request->user()->authorizeRoles(['ADMIN']);
            $is_dcm = $request->user()->hasRole('AGENT DCM');
            $is_dpm = $request->user()->hasRole('AGENT DPM');
            $_url = 'list-invoices-fraisActivation';

            //dd($request);

            $validator = Validator::make($request->all(),[
                'num_facture'       =>  'required|unique:frais_activation',
                'object'            =>  'required',
                'signataire_id'     =>  'required',
                'designation'       =>  'required'
            ]);
                
            if ($validator->fails()) {
                
                return Redirect::back()->withErrors($validator)->withInput();
            }else{

                $month = date("m");
                $month = sprintf('%01d' , $month);
                $year = date("Y");
                $monthTable = array('Janvier' , 'Fevrier' , 'Mars' , 'Avril' , 'Mai' , 'Juin' , 'Juillet' , 'Aout' , 'Septembre' , 'Octobre' , 'Novembre', 'Decembre');
                $month = $monthTable[$month - 1];
                $periode = $month.''.$year;

                $fraisActivation = FraisActivation::find($id);

                $fraisActivation->signataires_id = $request->signataire_id;
                $fraisActivation->periode        = $periode;
                $fraisActivation->objet          = $request->object;
                $fraisActivation->designation    = $request->designation;
                $fraisActivation->num_facture    = $request->num_facture;

                $fraisActivation->save();

                if($fraisActivation){
                    return redirect()->route('facture.frais_activation.pdf',$fraisActivation->id);
                }else{
                    return Redirect::back();
                }
            }


               


        }

        public function frais_activation_details(Request $request , $id){

        $request->user()->authorizeRoles(['ADMIN']);
        $is_dcm = $request->user()->hasRole('AGENT DCM');
        $is_dpm = $request->user()->hasRole('AGENT DPM');
        $_url = 'list-invoices-fraisActivation';

            $fraisActivation = FraisActivation::where('id' , $id)->first();

        return view('dashboard.factures.fraisActivationDetails',compact('fraisActivation' , 'is_dcm' , 'is_dpm' , '_url'));


        }

        public function frais_activation_pdf(Request $request , $id){

        $request->user()->authorizeRoles(['ADMIN']);
        $is_dcm = $request->user()->hasRole('AGENT DCM');
        $is_dpm = $request->user()->hasRole('AGENT DPM');
        $_url = 'list-invoices-fraisActivation';

            $fraisActivation = FraisActivation::where('id' , $id)->with('signataire')->first();

            //dd($fraisActivation);
        //return view('dashboard.factures.fraisActivation',compact('fraisActivation' , 'is_dcm' , 'is_dpm' , '_url'));
        //$date_limite = new DateTime($facture->date_limite);
        //$date_limite = $date_limite->format('d-m-Y');
        //dd();
        $options = new Options();
        $options->set('isRemoteEnabled', true);
        $pdf = new PDF($options);
        $formatter = \NumberFormatter::create('fr_FR', \NumberFormatter::SPELLOUT);
        $formatter->setAttribute(\NumberFormatter::FRACTION_DIGITS, 0);
        $formatter->setAttribute(\NumberFormatter::ROUNDING_MODE, \NumberFormatter::ROUND_HALFUP);
        $montant_lettre = $formatter->format($fraisActivation->TTC);
        $pdf = \PDF::loadView('dashboard.factures.fraisAcces_facture' , compact('fraisActivation','montant_lettre' , 'is_dcm' , 'is_dpm' , '_url'));

        return $pdf->stream('fraisAcces_facture.pdf' , compact('fraisActivation','montant_lettre' , 'is_dcm' , 'is_dpm' , '_url'));
        }


}
