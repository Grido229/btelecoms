<?php

namespace App\Http\Controllers;

use App\BonCommande;
use App\Client;
use App\ClientsType;
use Illuminate\Http\Request;
use Barryvdh\DomPDF\Facade as PDF;
use DateTime;
use  Dompdf\Options;
use Dompdf\Helpers;
use Dompdf\Exception\ImageException;
use Illuminate\Support\Facades\Redirect;

class ClientController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(Request $request)
    {
        $request->user()->authorizeRoles(['ADMIN', 'AGENT DCM']);
        $is_dcm = $request->user()->hasRole('AGENT DCM');
        $is_dpm = $request->user()->hasRole('AGENT DPM');
        $_url = 'customers';
        $clients = Client::all();
        $ref = array();
        foreach ($clients as $key =>$client) {
            $ref [$key]  = $client->num_client ;
        }
        return view('dashboard.clients.list', compact('clients', 'is_dcm', 'is_dpm', '_url','ref'));
    }


    public function pdfClients()
    {
        $path = 'assets/images/btcom.png';
        asset('assets/images/btcom.png');
        $type = pathinfo($path, PATHINFO_EXTENSION);
        $data = file_get_contents($path);
        $base64 = 'data:image/' . $type . ';base64,' . base64_encode($data);

        $clients = Client::all();
        $options = new Options();
        $options->set('isRemoteEnabled', true);
        $pdf = new PDF($options);

        $pdf = \PDF::loadView('dashboard.clients.pdfClients', compact('clients', 'base64'));
        return $pdf->stream('Liste_des_clients.pdf', compact('clients', 'base64'));
        //return view('dashboard.clients.pdfclients', compact('clients'));


    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $request->user()->authorizeRoles(['ADMIN', 'AGENT DCM']);
        $is_dcm = $request->user()->hasRole('AGENT DCM');
        $is_dpm = $request->user()->hasRole('AGENT DPM');
        $_url = 'new-customer';
        $client_types = ClientsType::all();
        return view('dashboard.clients.create', compact('is_dcm', 'is_dpm', '_url', 'client_types'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->user()->authorizeRoles(['ADMIN', 'AGENT DCM']);
        $request->validate([
            'enterprise_name' => 'required|string|max:255|unique:clients',
            'client_type'     => 'required|string|max:255',
            'tel'             => 'max:12',
        ]);

        $client = new Client();
        $client->email = $request->email;
        $client->tel = $request->tel;
        $client->city = $request->city;
        $client->adress = $request->adress;
        $client->enterprise_name = $request->enterprise_name;
        $client->client_type = $request->client_type;
        $client->save();
        $client->num_client = 'BTI-' . (new DateTime)->format('Ymd') . $client->id;
        $client->save();

        return redirect()->route('dashboard.clients');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, $id)
    {
        $request->user()->authorizeRoles(['ADMIN']);
        $is_dcm = $request->user()->hasRole('AGENT DCM');
        $is_dpm = $request->user()->hasRole('AGENT DPM');
        $_url = 'customers';
        $client = Client::findOrFail($id);
        $client_types = ClientsType::all();

        return view('dashboard.clients.edit', compact('client', 'is_dcm', 'is_dpm', '_url', 'client_types'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->user()->authorizeRoles(['ADMIN']);
        $request->validate([
            'email' => 'required|string|email|max:100',

        ]);

        $other_client = Client::where('email', $request->email)
            ->where('id', '!=', $id)->first();

        if ($other_client) {
            return redirect()->back()->withErrors(['email' => 'Cet adresse email est déjà utilisé']);
        }

        $other_client = Client::where('enterprise_name', $request->enterprise_name)
            ->where('id', '!=', $id)->first();
        if ($other_client) {
            return redirect()->back()->withErrors(['enterprise_name' => 'Ce nom d\'entreprise existe déjà']);
        }

        $client = Client::findOrFail($id);

        if ($request->client_type) {
            $client->client_type = $request->client_type;
        }

        if ($request->email) {
            $client->email = $request->email;
        }

        if ($request->tel) {
            $client->tel = $request->tel;
        }

        if ($request->city) {
            $client->city = $request->city;
        }

        if ($request->adress) {
            $client->adress = $request->adress;
        }

        if ($request->enterprise_name) {
            $client->enterprise_name = $request->enterprise_name;
        }

        $client->save();
        return redirect()->route('dashboard.clients');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function delete(Request $request, $id)
    {
        $request->user()->authorizeRoles(['ADMIN']);
        $client = Client::findOrFail($id);
        $client_bon = BonCommande::where('client_id', $client->id)->first();

        if ($client_bon) {
            return redirect()->back()->withErrors(['delete-invalid' => 'Vous ne pouvez pas supprimer un client ayant une demande de service en cours...']);
        }else{

        $client->delete();
        return redirect()->route('dashboard.clients');
        }

    }
}
