<?php

namespace App\Http\Middleware;

use App\Role;
use App\UserRole;
use Closure;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;

class CheckStatusDCM
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
         $current_users_id   = Auth::id();
         $current_users_role = UserRole::where('user_id' , $current_users_id)->pluck('id');

         if ($current_users_role != 3 | $current_users_role != 1) {
            return Redirect::back();
         }
        return $next($request);
    }
}
