<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class NatureOffre extends Model
{
    //
    protected $table = 'offres_natures';
    protected $guarded = ['id'];
}
