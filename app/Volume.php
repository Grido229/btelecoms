<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Volume extends Model
{
    protected $table = 'volume';
    protected $guarded = ['id'];

    public function tarifs(){
        return $this->belongsTo('App\Tarifs' , 'id');
    }

    public function pv(){
        return $this->belongsTo('App\CommandeProtocole' , 'id');
    }

}
