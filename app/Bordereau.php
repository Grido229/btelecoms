<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Bordereau extends Model
{
    //
    protected $table = 'bord_av_valide' ;
    protected $guarded = ['id'] ;

    public function facture(){
        return $this->hasMany('App\Facture','id','bordereau_id');
    }

}
