<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Facture extends Model
{
    protected $table = 'factures';
    protected $guarded = ['id'];

    public function pv(){
        return $this->hasOne('App\CommandeProtocole' , 'id' , 'pv_id');
    }

    public function frais_acces(){
        return $this->hasOne('App\Frais_acces' , 'id' , 'frais_acces_id');
    }


    public function facture_globale(){
        return $this->belongsTo('App\FactureGlobale' , 'id');
    }

    public function signataire(){
        return $this->hasOne('App\Signataire','id','signataires_id');
    }

    public function bordereau(){
        return $this->belongsTo('App\Bordereau');

    }

}
