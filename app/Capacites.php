<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Capacites extends Model
{
    protected $table = 'capacites';
    protected $guarded = ['id'];

    public function tarifs(){
        return $this->belongsTo('App\Tarifs' , 'id');
    }

    public function faris_activation(){
        return $this->belongsTo('App\FraisActivation' , 'id');
    }

    public function bon(){
        return $this->belongsTo('App\BonCommande' , 'id');
    }



}
