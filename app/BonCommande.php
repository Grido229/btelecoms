<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BonCommande extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'num_ref',
        'num_bon',
        'total_price_ht',
        'client_id',
        'date',
    ];

    /**
     * Get the details for the BonCommande.
     */
    public function details()
    {
        return $this->hasMany('App\CommandeDetail');
    }

    /**
     * Get the Client that owns the BonCommande.
     */
    public function client()
    {
        return $this->belongsTo('App\Client' , 'client_id', 'id');
    }

    /**
     * Get the CommandeProtocole for the BonCommande.
     */
    public function pv()
    {

        return $this->hasOne('App\CommandeProtocole' , 'bon_id' , 'id');

        return $this->hasOne('App\CommandeProtocole');

    }

    public function contrat(){
        return $this->belongsTo('App\Contrat' , 'id');
    }



    public function faris_activation(){
        return $this->belongsTo('App\FraisActivation' , 'id');
    }

    public function capacite(){
        return $this->hasOne('App\Capacites' , 'id' , 'capacite_id');
    }

    public function zone(){
        return $this->hasOne('App\Zone' , 'id' , 'zones_id' );
    }

    public function distance(){
        return $this->hasOne('App\Distances'  , 'id', 'distances_id');
    }

    public function sfs(){
        return $this->hasOne('App\Sfs' , 'id' , 'sfs_id');
    }

    public function ssfs(){
        return $this->hasOne('App\Ssfs' , 'id' , 'ssfs_id');
    }

}
