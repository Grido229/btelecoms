<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Ssfs extends Model
{
    protected $table = 'ssfs';
    protected $guarded = ['id' , 'code'];

    /*public function sfs(){
        return $this->hasOne('App\Sfs' , 'id' , 'id_sfs');
    }*/

    public function sfs()
    {
        return $this->belongsTo('App\Sfs' , 'id');
    }

    public function tarifs(){
        return $this->belongsTo('App\Tarifs' , 'id');
    }

    public function frais_acces(){
        return $this->belongsTo('App\Frais_acces' , 'id');
    }

    public function pv(){
        return $this->belongsTo('App\CommandeProtocole' , 'id');
    }

    public function bon()
    {
        return $this->belongsTo('App\BonCommande' , 'id');
    }

}
