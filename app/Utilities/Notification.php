<?php

namespace App\Utilities;

use App\BonCommande;

class Notification
{
    public function commandes_without_dpm_validation()
    {
        return BonCommande::where('is_validate_by_dpm', 0)->count();
    }

    public function commandes_without_dcm_validation()
    {
        return BonCommande::where('is_validate_by_dcm', 0)->count();
    }

    public function all_commandes_without_validation()
    {
        return BonCommande::where('is_validate_by_dcm', 0)->count() + BonCommande::where('is_validate_by_dpm', 0)->count();
    }
}
