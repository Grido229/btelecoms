<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Contrat extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'num_contrat',
        'duree_contrat',
        'begin_date',
        'end_date',
        'object_contrat',
        'bon_id',
    ];


    public function bon(){
        return $this->hasOne('App\BonCommande' , 'id' , 'bon_id');
    }
}
