<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Distances extends Model
{
    protected $table = 'distance';
    protected $guarded = ['id'];

    public function tarifs(){
        return $this->belongsTo('App\Tarifs' , 'id');
    }

    public function bon(){
        return $this->belongsTo('App\BonCommande' , 'id');
    }

}
