<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Zone extends Model
{
    protected $table = 'zone';
    protected $guarded = ['id'];

    public function tarifs(){
        return $this->belongsTo('App\Tarifs' , 'id');
    }

    public function pv(){
        return $this->belongsTo('App\CommandeProtocole' , 'id');
    }

    public function bon(){
        return $this->belongsTo('App\BonCommande' , 'id');
    }

}
