<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ClientsType extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
    ];

    /**
     * Get the CommandeProtocole for the BonCommande.
     */
    public function client()
    {
        return $this->belongsTo('App\Client');
    }
}
