<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class FraisActivation extends Model
{
    protected $table = 'frais_activation';

    protected $guarded = ['id'];

    public function bon(){
        return $this->hasOne('App\BonCommande' , 'id' , 'bonCommandes_id');
    }

    public function client(){
        return $this->hasOne('App\Client' , 'id' , 'clients_id');
    }

    public function capacite(){
        return $this->hasOne('App\Capacites' , 'id' , 'capacites_id');
    }

    public function signataire(){
        return $this->hasOne('App\Signataire','id','signataires_id');
    }
}
