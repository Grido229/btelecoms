<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Sfs extends Model
{
    protected $table = 'sfs';
    protected $guarded = ['id' , 'code'];

    public function fs(){
        return $this->hasOne('App\Fs' , 'id' , 'id_fs');
    }

    /*public function ssfs(){
        return $this->belongsTo('App\Ssfs' , 'id');
    }*/
    public function ssfs(){
        return $this->hasMany('App\Ssfs' , 'id_sfs' , 'id');
    }

    public function tarifs(){
        return $this->belongsTo('App\Tarifs' , 'id');
    }

    public function frais_acces(){
        return $this->belongsTo('App\Frais_acces' , 'id');
    }

    public function pv(){
        return $this->belongsTo('App\CommandeProtocole' , 'id');
    }

    public function bon()
    {
        return $this->belongsTo('App\BonCommande' , 'id');
    }

}
