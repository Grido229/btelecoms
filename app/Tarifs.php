<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Tarifs extends Model
{
    protected $table = 'tarifs';
    protected $guarded = ['id'];

    public function sfs(){
        return $this->hasOne('App\Sfs'  , 'id' , 'id_sfs');
    }

    public function ssfs(){
        return $this->hasOne('App\Ssfs'  , 'id' , 'id_ssfs');
    }

    public function zone(){
        return $this->hasOne('App\Zone' , 'id' , 'id_zone' );
    }

    public function volume(){
        return $this->hasOne('App\Volume'  , 'id' , 'id_volume');
    }

    public function Distance(){
        return $this->hasOne('App\Distances'  , 'id', 'id_distance');
    }

    public function capacite(){
        return $this->hasOne('App\Capacites'  , 'id' , 'id_capacite');
    }

}
