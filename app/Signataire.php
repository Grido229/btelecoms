<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Signataire extends Model
{
    //
    protected $table = 'signataire';
    protected $guarded = ['id'];

    public function fraisActivation(){
        return $this->belongsTo('App\FraisActivation' , 'id');
    }

    public function facture(){
        return $this->belongsTo('App\Facture' , 'id');
    }

    public function factureGlobale(){
        return $this->belongsTo('App\FactureGlobale' , 'id');
    }

    public function deserte(){
        return $this->belongsTo('App\Desertes', 'id');
    }



}
