<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Frais_acces extends Model
{
    protected $table = 'frais_acces';
    protected $guarded = ['id'];

    public function sfs(){
        return $this->hasOne('App\Sfs' , 'id' , 'id_sfs');
    }

    public function ssfs(){
        return $this->hasOne('App\Ssfs' , 'id' , 'id_ssfs');
    }

    public function facture(){
        return $this->belongsTo('App\Facture' , 'id');
    }

}
