# Btelecoms

système intégré de recouvrement et de la facturation

=====================MODIFICATION RECENTES==============================
	- Gestion des vatidations pour les vues où il faut afficher le montant d'abord.


====================FICHIERS DE POPULATIONS=============================
	- CapacitesSeeder
	- ClientTypeSeeder
	- DatabaseSeeder
	- DeserteSeeder
	- DistanceSeeder
	- EtatTableSeeder
	- FamilleSeeder
	- FraisAccesSeeder
	- NatureOffreTableSeeder
	- OffresTypesSeeder
	- RoleTableSeeder
	- SousFamilleSeeder
	- SousSousFamilleSeeder
	- TarifsSeeder
	- UserTableSeeder
	- VolumeSeeder
	- ZoneSeeder
===================FICHIERS AVEC DU CODE AJAX===========================
	- auth/login.blade.php   
	- dashboard/bons/create.blade.php
	- dashboard/bons/edit.blade.php
	- dashboard/pv/edit_grade.blade.php


=========================================================================
		- Routes avec du code ajax nommé. 
		- Plus de modifications de routes pour les prochaines mises à jour 
		- 

==========================================================================

# Corrections 23/03/2020
  - La connexion se fera désormais avec des codes utilisateurs et non leur noms.
  - Seul l'administrateur principal est créé par le seeder des users. Les autres       		utilisateurs devront être créés par l'admin . 
  - Leur codes utilisateurs leur seront communiqué une fois leur compte crée.
  - Ils définiront eux memes leur mot de passe à leur première connexion . 
  # Les mises à jour  .
   - La table des rôles a été mis à jour .
   - La table users a été mis à jour .
   - Le menu a été mis à jour . Les agents DPM avaient le menu Demande de service. 
     Mais plus maintenant.
   - La liste des utilisateurs a été mis à jour .
