<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSsfsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ssfs', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('id_sfs')->unsigned()->nullable();
            $table->foreign('id_sfs')
                    ->references('id')
                    ->on('sfs')
                    ->onDelete('cascade')
                    ->upDate('cascade');
            $table->string('code',100);
            $table->string('libelle',150);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ssfs');
    }
}
