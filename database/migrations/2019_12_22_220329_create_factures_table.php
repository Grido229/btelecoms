<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFacturesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('factures', function (Blueprint $table) {

            $table->increments('id');
            $table->string('No_facture' , 100)->unique();
            $table->integer('pv_id')->unsigned();
            $table->integer('signataires_id')->unsigned()->nullable();
            $table->foreign('signataires_id')
                  ->references('id')
                  ->on('signataire')
                  ->onDelete('cascade')
                  ->onUpdate('cascade');
            $table->foreign('pv_id')
                  ->references('id')
                  ->on('commande_protocoles')
                  ->onDelete('cascade')
                  ->onUpdate('cascade');
            $table->integer('bordereau_id')->unsigned()->nullable();
            $table->foreign('bordereau_id')
                  ->references('id')
                  ->on('bord_av_valide')
                  ->onDelete('cascade')
                  ->onUpdate('cascade') ;
            $table->integer('frais_acces_id')->unsigned()->nullable();
            $table->foreign('frais_acces_id')
                  ->references('id')
                  ->on('frais_acces')
                  ->onDelete('cascade')
                  ->onUpdate('cascade') ;
            $table->string('type_facture' , 100)->nullable();
            $table->string('TVA' , 100)->nullable();
            $table->string('montant_TTC' , 100)->nullable();
            $table->string('montant_fact' , 100)->nullable();
            $table->string('nbr_jours_fact' , 100)->nullable();
            $table->string('periode_fact' , 100)->nullable();
            $table->date('date_limite')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('references');
    }
}
