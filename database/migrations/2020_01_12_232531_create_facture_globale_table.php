<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFactureGlobaleTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('facture_globale', function (Blueprint $table) {
            $table->increments('id');
            $table->string('No_facture' , 100)->unique();
            $table->string('nb_jours_fact')->nullable();
            $table->integer('factures_capacite_id')->unsigned()->nullable();
            $table->foreign('factures_capacite_id')
                  ->references('id')
                  ->on('factures')
                  ->onDelete('cascade')
                  ->onUpdate('cascade');
            $table->integer('pv_id')->unsigned()->nullable();
            $table->foreign('pv_id')
                ->references('id')
                ->on('commande_protocoles')
                ->onDelete('cascade')
            ->onUpdate('cascade');
            $table->integer('signataires_id')->unsigned()->nullable();
            $table->foreign('signataires_id')
                ->references('id')
                ->on('signataire')
                ->onDelete('cascade')
                ->onUpdate('cascade');
            $table->boolean('type_capacite')->default(0)->nullable();
            $table->boolean('type_colocalisation')->default(0)->nullable();
            $table->date('date_limite')->nullable();
            $table->string('periode' , 100)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.id
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('facture_globale');
    }
}

