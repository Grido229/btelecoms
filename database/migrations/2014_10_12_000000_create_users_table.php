<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->string('fullname' , 100);
            $table->string('adresse' , 100)->nullable();
            $table->string('email')->nullable();
            $table->string('tel' , 100)->nullable();
            $table->string('service' , 100)->nullable();
            $table->string('password' , 100)->nullable();
            $table->string('code_users' , 100)->nullable();   
            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
