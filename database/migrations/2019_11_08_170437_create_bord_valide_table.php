<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBordValideTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('bord_valide', function (Blueprint $table) {
            $table->increments('id');
            $table->string('num_bordereau' , 100);
            $table->string('num_bureau' , 100);
            $table->string('type_bureau' , 100);
            $table->string('resp_maj' , 100);
            $table->string('num_device' , 100);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('bord_valide');
    }
}
