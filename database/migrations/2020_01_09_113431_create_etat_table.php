<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEtatTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('etat', function (Blueprint $table) {
            $table->increments('id');

            $table->string('ref_pv',100)->nullable();
            $table->text('nom_service')->nullable();
            $table->string('type_service',100)->nullable();
            $table->string('date_activation' , 100)->nullable();
            $table->string('frais' , 100)->nullable();
            $table->string('redevance' , 100)->nullable();
            $table->string('frequence_redevance' , 100)->nullable();
            $table->string('date_premiere_facturation' , 100)->nullable();
            $table->string('statut' , 100)->nullable();
            $table->string('date_statut' , 100)->nullable();
            $table->string('raison' , 100)->nullable();
            $table->string('ref_contrat' , 100)->nullable();
            $table->string('ref_demande' , 100)->nullable();
            $table->string('nom_operateur' , 100)->nullable();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('etat');
    }
}
