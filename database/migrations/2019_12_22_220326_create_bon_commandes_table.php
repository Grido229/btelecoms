<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBonCommandesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('bon_commandes', function (Blueprint $table) {
            $table->increments('id');
            $table->string('num_bon',20)->nullable();
            $table->string('num_ref',50)->unique();
            $table->string('total_price_ht' , 100);
            $table->string('object' , 100);
            $table->date('date');
            $table->boolean('is_validate_by_dcm')->default(0);
            $table->integer('dcm_user')->nullable();
            $table->boolean('is_validate_by_dpm')->default(0);
            $table->integer('dpm_user')->nullable();
            $table->integer('client_id')->unsigned();
            $table->foreign('client_id')
                                    ->references('id')
                                    ->on('clients')
                                    ->onDelete('cascade')
                                    ->onUpdate('cascade');
            $table->integer('fs_id')->unsigned();
            $table->foreign('fs_id')->references('id')
                                    ->on('fs')
                                    ->onDelete('cascade')
                                    ->onUpdate('cascade');
            $table->integer('sfs_id')->unsigned();
            $table->foreign('sfs_id')->references('id')
                                    ->on('sfs')
                                    ->onDelete('cascade')
                                    ->onUpdate('cascade');
            $table->integer('ssfs_id')->unsigned()->nullable();
            $table->foreign('ssfs_id')->references('id')
                                    ->on('ssfs')
                                    ->onDelete('cascade')
                                    ->onUpdate('cascade');
            $table->integer('distances_id')->unsigned()->nullable();
            $table->foreign('distances_id')->references('id')
                                    ->on('distance')
                                    ->onDelete('cascade')
                                    ->onUpdate('cascade');
            $table->integer('capacite_id')->unsigned()->nullable();
            $table->foreign('capacite_id')->references('id')
                                    ->on('capacites')
                                    ->onDelete('cascade')
                                    ->onUpdate('cascade');
            $table->integer('zones_id')->unsigned()->nullable();
            $table->foreign('zones_id')->references('id')
                    ->on('zone')
                    ->onDelete('cascade')
                    ->onUpdate('cascade');
            $table->integer('natures_offres_id')->unsigned()->nullable();
            $table->foreign('natures_offres_id')->references('id')
                    ->onDelete('cascade')
                    ->on('offres_natures')
                    ->onDelete('cascade')
                    ->onUpdate('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('bon_commandes');
    }
}
