<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateContratsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('contrats', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('num_contrat', 20)->nullable();
            $table->string('duree_contrat' , 100)->nullable();
            $table->date('begin_date');
            $table->date('end_date');
            $table->longText('object_contrat')->nullable();
            $table->string('nature_contrat')->nullable();
            $table->integer('bon_id')->unsigned();
            $table->foreign('bon_id')->references('id')->on('bon_commandes');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('contrats');
    }
}
