<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDeserteTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('deserte', function (Blueprint $table) {
            $table->increments('id');
            $table->string('ref_devis' , 100);
            $table->string('ref_facture' , 100)->nullable();
            $table->integer('clients_id')->unsigned()->nullable();
            $table->foreign('clients_id')->references('id')->on('clients')->onDelete('cascade')->onUpdate('cascade');
            $table->integer('signataires_id')->unsigned()->nullable();
            $table->foreign('signataires_id')->references('id')->on('signataire')->onDelete('cascade')->onUpdate('cascade');
            $table->text('intitule');
            $table->string('type_devis' , 100);


            $table->string('pU_fourniture_1',100)->default('0');
            $table->string('pU_fourniture_2',100)->default('0');
            $table->string('pU_fourniture_3',100)->default('0');
            $table->string('pU_fourniture_4',100)->default('0');
            $table->string('pU_fourniture_5',100)->default('0');
            $table->string('pU_fourniture_6',100)->default('0');
            $table->string('pU_fourniture_7',100)->default('0');
            $table->string('pU_fourniture_8',100)->default('0');
            $table->string('pU_fourniture_9',100)->default('0');
            $table->string('pU_fourniture_10',100)->default('0');

            $table->string('pU_prestation_1',100)->default('0');
            $table->string('pU_prestation_2',100)->default('0');
            $table->string('pU_prestation_3',100)->default('0');
            $table->string('pU_prestation_4',100)->default('0');
            $table->string('pU_prestation_5',100)->default('0');
            $table->string('pU_prestation_6',100)->default('0');
            $table->string('pU_prestation_7',100)->default('0');
            $table->string('pU_prestation_8',100)->default('0');
            $table->string('pU_prestation_9',100)->default('0');
            $table->string('pU_prestation_10',100)->default('0');


            $table->string('fourniture_1',100)->default('0');
            $table->string('fourniture_2',100)->default('0');
            $table->string('fourniture_3',100)->default('0');
            $table->string('fourniture_4',100)->default('0');
            $table->string('fourniture_5',100)->default('0');
            $table->string('fourniture_6',100)->default('0');
            $table->string('fourniture_7',100)->default('0');
            $table->string('fourniture_8',100)->default('0');
            $table->string('fourniture_9',100)->default('0');
            $table->string('fourniture_10',100)->default('0');

            $table->string('prestation_1',100)->default('0');
            $table->string('prestation_2',100)->default('0');
            $table->string('prestation_3',100)->default('0');
            $table->string('prestation_4',100)->default('0');
            $table->string('prestation_5',100)->default('0');
            $table->string('prestation_6',100)->default('0');
            $table->string('prestation_7',100)->default('0');
            $table->string('prestation_8',100)->default('0');
            $table->string('prestation_9',100)->default('0');
            $table->string('prestation_10',100)->default('0');


            $table->string('quantite_1',100)->default('0');
            $table->string('quantite_2',100)->default('0');
            $table->string('quantite_3',100)->default('0');
            $table->string('quantite_4',100)->default('0');
            $table->string('quantite_5',100)->default('0');
            $table->string('quantite_6',100)->default('0');
            $table->string('quantite_7',100)->default('0');
            $table->string('quantite_8',100)->default('0');
            $table->string('quantite_9',100)->default('0');
            $table->string('quantite_10',100)->default('0');


            $table->string('montant_1',100)->default('0');
            $table->string('montant_2',100)->default('0');
            $table->string('montant_3',100)->default('0');
            $table->string('montant_4',100)->default('0');
            $table->string('montant_5',100)->default('0');
            $table->string('montant_6',100)->default('0');
            $table->string('montant_7',100)->default('0');
            $table->string('montant_8',100)->default('0');
            $table->string('montant_9',100)->default('0');
            $table->string('montant_10',100)->default('0');

            $table->string('sous_total_1' , 100)->default('0');
            $table->string('sous_total_2' , 100)->default('0');

            $table->string('total_HT' , 100)->default('0');
            $table->string('total_TVA' , 100)->default('0');
            $table->string('montant_facture' , 100)->default('0');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('deserte');
    }
}
