<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTarifsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tarifs', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('sfs_id')->unsigned();
            $table->foreign('sfs_id')
                  ->references('id')
                  ->on('sfs')
                  ->onDelete('cascade')
                  ->onUpdate('cascade');
            $table->integer('ssfs_id')->unsigned()->nullable();
            $table->foreign('ssfs_id')
                ->references('id')
                ->on('ssfs')
                ->onDelete('cascade')
                ->onUpdate('cascade');
            $table->integer('zone_id')->unsigned()->nullable();
            $table->foreign('zone_id')
                ->references('id')
                ->on('zone')
                ->onDelete('cascade')
                ->onUpdate('cascade');
            $table->integer('volume_id')->unsigned()->nullable();
            $table->foreign('volume_id')
                ->references('id')
                ->on('volume')
                ->onDelete('cascade')
                ->onUpdate('cascade');
            $table->integer('distance_id')->unsigned()->nullable();
            $table->foreign('distance_id')
                ->references('id')
                ->on('distance')
                ->onDelete('cascade')
                ->onUpdate('cascade');
            $table->integer('capacite_id')->unsigned()->nullable();
            $table->foreign('capacite_id')
                ->references('id')
                ->on('capacites')
                ->onDelete('cascade')
                ->onUpdate('cascade');
            $table->string('frais_installation' , 100)->nullable();
            $table->string('surface' , 100 )->nullable();
            $table->string('tarifs_Or_redevances' , 100);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tarifs');
    }
}
