<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOffresTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('offres', function (Blueprint $table) {
            $table->increments('id');
            $table->string('No_offre',100);
            $table->string('type_offre',100);
            $table->string('No_region',100);
            $table->string('statut_offre',100);
            $table->integer('taux_abo_jour');
            $table->date('date_mise_service');
            $table->integer('mt_abo_jour');
            $table->date('date_maj');
            $table->string('nom_abo',100);
            $table->string('prenom_abo',100);
            $table->string('resp_maj',100);
            $table->integer('debit');
            $table->date('date_debut_susp');
            $table->date('date_fin_susp');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('offres');
    }
}
