<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use phpDocumentor\Reflection\DocBlock\Tags\Reference\Reference;

class CreateFraisActivationTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('frais_activation', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('bonCommandes_id')->unsigned();
            $table->foreign('bonCommandes_id')->references('id')->on('bon_commandes')->onDelete('cascade')->onUpdate('cascade');
            $table->integer('signataires_id')->unsigned()->nullable();
            $table->foreign('signataires_id')->references('id')->on('signataire')->onDelete('cascade')->onUpdate('cascade');
            $table->integer('clients_id')->unsigned();
            $table->foreign('clients_id')->references('id')->on('clients')->onDelete('cascade')->onUpdate('cascade');
            $table->integer('capacites_id')->unsigned();
            $table->foreign('capacites_id')->references('id')->on('capacites')->onDelete('cascade')->onUpdate('cascade');
            $table->string('objet' , 100)->nullable();
            $table->string('periode' , 100)->nullable();
            $table->string('designation' , 100)->nullable();
            $table->string('num_facture' , 100)->unique()->nullable();
            $table->string('prix' , 100);
            $table->string('TVA' , 100);
            $table->string('TTC' , 100);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('frais_activation');
    }
}
