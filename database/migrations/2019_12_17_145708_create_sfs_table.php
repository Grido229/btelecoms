<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSfsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sfs', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('id_fs')->unsigned()->nullable();
            $table->foreign('id_fs')
                    ->references('id')
                    ->on('fs')
                    ->onDelete('cascade')
                    ->upDate('cascade');
            $table->string('code' , 100);
            $table->string('libelle' , 150);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sfs');
    }
}
