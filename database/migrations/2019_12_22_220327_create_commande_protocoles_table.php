<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCommandeProtocolesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('commande_protocoles', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('volume_id')->unsigned()->nullable();
            $table->foreign('volume_id')->references('id')
                                    ->on('volume')
                                    ->onDelete('cascade')
                                    ->onUpdate('cascade');
            $table->string('surface' , 100)->nullable();
            $table->string('frais_installation' , 100)->nullable();

            $table->string('work_done' , 100)->nullable();
            $table->boolean('support')->nullable();
            $table->string('number_links_attributed' , 100)->nullable();


            $table->string('status' , 100 )->nullable();
            $table->date('status_date')->nullable();
            $table->string('status_object' , 100)->nullable();

            $table->string('provider' , 100)->nullable();
            $table->string('tension' , 100)->nullable();
            $table->string('energy_consomme' , 100)->nullable();
            $table->string('weight' , 100)->nullable();
            $table->string('position_tma' , 100)->nullable();
            $table->string('height' , 100)->nullable();
            $table->string('width' , 100)->nullable();
            $table->string('length' , 100)->nullable();
            $table->string('guide_length' , 100)->nullable();
            $table->string('type_antenna' , 100)->nullable();

            $table->string('intensity_consomme' , 100)->nullable();
            $table->string('source_ac' , 100)->nullable();
            $table->string('source_cc' , 100)->nullable();
            $table->string('type_equipment' , 100)->nullable();
            $table->string('sol_capacity' , 100)->nullable();
            $table->string('power_consomme' , 100)->nullable();
            $table->string('energy_consomme_per_month' , 100)->nullable();
            $table->string('nbr_antenna' , 100)->nullable();
            $table->string('ville' , 100)->nullable();
            $table->string('site' , 100)->nullable();

            $table->date('commissioningDate');
            $table->string('local_conditions' ,100);
            $table->string('type' , 100);
            $table->string('reception_room' , 100);
            $table->integer('bon_id')->unsigned();
            $table->foreign('bon_id')->references('id')->on('bon_commandes');
            $table->date('date');
            $table->string('num_commande_protocoles', 20)->nullable();
            $table->string('new_num_commande_protocoles', 20)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('commande_protocoles');
    }
}
