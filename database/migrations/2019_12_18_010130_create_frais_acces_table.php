<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFraisAccesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('frais_acces', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('id_sfs')->unsigned()->nullable();
            $table->foreign('id_sfs')
                  ->references('id')
                  ->on('sfs')
                  ->onDelete('cascade')
                  ->onUpdate('cascade');
            $table->integer('id_ssfs')->unsigned()->nullable();
            $table->foreign('id_ssfs')
                ->references('id')
                ->on('ssfs')
                ->onDelete('cascade')
                ->onUpdate('cascade');
            $table->integer('id_capacite')->unsigned();
            $table->foreign('id_capacite')
                  ->references('id')
                  ->on('capacites')
                  ->onDelete('cascade')
                  ->onUpdate('cascade');
            $table->string('frais_mod' , 100 );
            $table->string('frais' , 100);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('frais_acces');
    }
}
