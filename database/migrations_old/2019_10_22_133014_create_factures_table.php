<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFacturesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('factures', function (Blueprint $table) {
            $table->bigIncrements('id');
            $$table->integer('No_facture');
            $table->integer('offres_types_id');
            $table->foreign('offres_types')
                   ->references('id')
                   ->on('offres_types')
                   ->onDelete('restrict')
                   ->onUpdate('restrict');
            $table->integer('clients_types_id');
            $table->foreign('clients_types')
                    ->references('id')
                    ->on('clients_types')
                    ->onDelete('restrict')
                    ->onUpdate('restrict');
            $table->string('intitule_offre',100);
            $table->string('nom',100);
            $table->string('prenom',100);
            $table->string('Mont_TVA',100);
            $table->string('MTotalFacture_TTC',100); 
            $table->integer('No_client');
            $table->string('Nb_jours',100);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('factures');
    }
}
