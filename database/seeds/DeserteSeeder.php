<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;

class DeserteSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $file = public_path('files/deserte.csv');
        $readFile = fopen($file,'r');

            while (!feof($readFile)) {
                $ligne = fgets($readFile,4096);
                $table_ligne = explode(';' , $ligne);

                if ($ligne != NULL) {

                $ref_fact =  $table_ligne[3];
                //$date_fact = $table_ligne[4];
                //$date_devis = $table_ligne[6];
                $ref_devis = $table_ligne[0];
                $intitule = $table_ligne[1];
                $type_devis = $table_ligne[2];
                $montant_fact = $table_ligne[5];



                            # code...
                            DB::table('deserte')->insert([
                                'ref_devis' => $ref_devis,
                                'intitule' => $intitule,
                                'type_devis' => $type_devis,
                                'ref_facture' =>  $ref_fact,
                                //'Date_Facture'    =>  $date_fact,
                                'montant_facture' => $montant_fact,
                                //'Date_Devis' => $date_devis,

                                'created_at'  => Carbon::now(),
                                'updated_at'  => Carbon::now()
                            ]);



                    }


            }

    }
}
