<?php

use App\ClientsType;
use Illuminate\Database\Seeder;

class ClientTypeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $type_ong = new ClientsType();
        $type_ong->name = 'ONG';
        $type_ong->save();

        $type_embassade = new ClientsType();
        $type_embassade->name = 'EMBASSADE';
        $type_embassade->save();

        $type_colect_locale = new ClientsType();
        $type_colect_locale->name = 'COLLECTIVITE LOCALE';
        $type_colect_locale->save();

        $type_admin = new ClientsType();
        $type_admin->name = 'ADMINISTRATION';
        $type_admin->save();

        $type_societe_etat = new ClientsType();
        $type_societe_etat->name = 'SOCIETE ETAT';
        $type_societe_etat->save();

        $type_societe_prive = new ClientsType();
        $type_societe_prive->name = 'SOCIETE PRIVE';
        $type_societe_prive->save();

        $type_prive_resident = new ClientsType();
        $type_prive_resident->name = 'PRIVE RESIDENTIEL';
        $type_prive_resident->save();

        $type_fai = new ClientsType();
        $type_fai->name = 'FAI';
        $type_fai->save();

        $type_scsm = new ClientsType();
        $type_scsm->name = 'SCSM';
        $type_scsm->save();

        $type_mobile = new ClientsType();
        $type_mobile->name = 'MOBILE';
        $type_mobile->save();

        $type_internation = new ClientsType();
        $type_internation->name = 'INTERNATIONAL';
        $type_internation->save();

        $type_partenaire = new ClientsType();
        $type_partenaire->name = 'PARTENAIRE';
        $type_partenaire->save();

        $type_partenaire = new ClientsType();
        $type_partenaire->name = 'FIXE';
        $type_partenaire->save();

        $type_partenaire = new ClientsType();
        $type_partenaire->name = 'CLS';
        $type_partenaire->save();

        $type_partenaire = new ClientsType();
        $type_partenaire->name = 'Autres';
        $type_partenaire->save();

    }
}
