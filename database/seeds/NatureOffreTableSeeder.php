<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;
class NatureOffreTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('offres_natures')->insert([
            'libelle' => 'Capacites Nationales',
            'created_at'  => Carbon::now(),
            'updated_at'  => Carbon::now()
        ]);
        DB::table('offres_natures')->insert([
            'libelle' => 'Capacites Internationales',
            'created_at'  => Carbon::now(),
            'updated_at'  => Carbon::now()
        ]);
        DB::table('offres_natures')->insert([
            'libelle' => 'Colocalisation',
            'created_at'  => Carbon::now(),
            'updated_at'  => Carbon::now()
        ]);
        DB::table('offres_natures')->insert([
            'libelle' => 'Hubbing',
            'created_at'  => Carbon::now(),
            'updated_at'  => Carbon::now()
        ]);
        DB::table('offres_natures')->insert([
            'libelle' => 'Internationales Sortant',
            'created_at'  => Carbon::now(),
            'updated_at'  => Carbon::now()
        ]);
        DB::table('offres_natures')->insert([
            'libelle' => 'Internationales Entrant',
            'created_at'  => Carbon::now(),
            'updated_at'  => Carbon::now()
        ]);
    }
}
