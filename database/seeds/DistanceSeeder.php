<?php

use Carbon\Carbon;
use Illuminate\Database\Seeder;

class DistanceSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        $file = public_path('files/distance.csv');
			$readFile = fopen($file,'r');

				while (!feof($readFile)) {
					$ligne = fgets($readFile,4096);
					$distance = $ligne;
						if ($ligne != NULL) {

                            DB::table('distance')->insert([
                                'distance' => $distance,
                                'created_at'  => Carbon::now(),
                                'updated_at'  => Carbon::now()
                            ]);

						}


                }


    }
}
