<?php

use App\Role;
use Illuminate\Database\Seeder;

class RoleTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $role_admin = new Role();
        $role_admin->name = 'ADMIN';
        $role_admin->save();

        $role_agent_dpm = new Role();
        $role_agent_dpm->name = 'AGENT DPM';
        $role_agent_dpm->save();

        $role_agent_dcm = new Role();
        $role_agent_dcm->name = 'AGENT DCM';
        $role_agent_dcm->save();

        $role_agent_dcm = new Role();
        $role_agent_dcm->name = 'DC';
        $role_agent_dcm->save();

        $role_agent_dcm = new Role();
        $role_agent_dcm->name = 'DPM';
        $role_agent_dcm->save();

        $role_agent_dcm = new Role();
        $role_agent_dcm->name = 'DSI';
        $role_agent_dcm->save();
    }
}
