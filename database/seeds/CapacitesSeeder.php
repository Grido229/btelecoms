<?php

use Carbon\Carbon;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class CapacitesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        $file = public_path('files/capacites.csv');
			$readFile = fopen($file,'r');

				while (!feof($readFile)) {
					$ligne = fgets($readFile,4096);
					$capacite = $ligne;
						if ($ligne != NULL) {

                            DB::table('capacites')->insert([
                                'capacite' => $capacite,
                                'created_at'  => Carbon::now(),
                                'updated_at'  => Carbon::now()
                            ]);

						}


                }


    }
}
