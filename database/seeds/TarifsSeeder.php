<?php

use Carbon\Carbon;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class TarifsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $file = public_path('files/tarifs2.csv');
        $readFile = fopen($file,'r');

            while (!feof($readFile)) {
                $ligne = fgets($readFile,4096);
                $table_ligne = explode(';' , $ligne);

                if ($ligne != NULL) {


                if ($ligne != NULL) {

                $id_sfs =  ($table_ligne[0] != '') ? $table_ligne[0] : NULL;
                $id_ssfs = ($table_ligne[1] != '') ? $table_ligne[1] : NULL;
                $id_zone = ($table_ligne[2] != '') ? $table_ligne[2] : NULL;
                $id_volume = ($table_ligne[3] != '') ? $table_ligne[3] : NULL;
                $id_distance = ($table_ligne[4] != '') ? $table_ligne[4] : NULL;
                $id_capacite = ($table_ligne[5] != '') ? $table_ligne[5] : NULL;
                $frais_instation = ($table_ligne[6] != '') ? $table_ligne[6] : NULL;
                $surface = ($table_ligne[7] != '') ? $table_ligne[7] : NULL;
                $tarifs = ($table_ligne[8] != '') ? $table_ligne[8] : NULL;




                            # code...
                            DB::table('tarifs')->insert([
                                'sfs_id' => $id_sfs,
                                'ssfs_id' => $id_ssfs,
                                'volume_id' => $id_volume,
                                'distance_id' =>  $id_distance,
                                'capacite_id'    =>  $id_capacite,
                                'zone_id' => $id_zone,
                                'frais_installation' => $frais_instation,
                                'surface'           => $surface,
                                'tarifs_Or_redevances'  => $tarifs,
                                'created_at'  => Carbon::now(),
                                'updated_at'  => Carbon::now()
                            ]);



                    }


            }
        }
    }
}
