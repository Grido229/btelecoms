<?php

use App\Role;
use App\User;
use Illuminate\Database\Seeder;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $role_admin = Role::where('name', 'ADMIN')->first();
        $role_agent_dpm = Role::where('name', 'AGENT DPM')->first();
        $role_agent_dcm = Role::where('name', 'AGENT DCM')->first();
        $code_users = uniqid();
        $admin = new User();
        $admin->fullname   = 'ADMIN';
        $admin->password   = bcrypt('administrateur');
        $admin->code_users = $code_users ; 
        $admin->save();
        $admin->roles()->attach($role_admin);
    }
}
