<?php

use Carbon\Carbon;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class SousSousFamilleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('ssfs')->insert([
            'code' => 'AAA',
            'id_sfs' => '1',
            'libelle' => 'Locations de MICs',
            'created_at'  => Carbon::now(),
            'updated_at'  => Carbon::now()
        ]);

        DB::table('ssfs')->insert([
            'code' => 'AAB',
            'id_sfs' => '1',
            'libelle' => 'Interconnexion par fibre optique',
            'created_at'  => Carbon::now(),
            'updated_at'  => Carbon::now()
        ]);

        DB::table('ssfs')->insert([
            'code' => 'AAC',
            'id_sfs' => '1',
            'libelle' => 'Capacité nationale point à point',
            'created_at'  => Carbon::now(),
            'updated_at'  => Carbon::now()
        ]);

        DB::table('ssfs')->insert([
            'code' => 'AAD',
            'id_sfs' => '1',
            'libelle' => 'Capacité nationale point à point incluant la colocalisation',
            'created_at'  => Carbon::now(),
            'updated_at'  => Carbon::now()
        ]);

        DB::table('ssfs')->insert([
            'code' => 'AAE',
            'id_sfs' => '1',
            'libelle' => 'Capacité nationale point-multipoints',
            'created_at'  => Carbon::now(),
            'updated_at'  => Carbon::now()
        ]);

        DB::table('ssfs')->insert([
            'code' => 'AAF',
            'id_sfs' => '1',
            'libelle' => 'Les offres secourues',
            'created_at'  => Carbon::now(),
            'updated_at'  => Carbon::now()
        ]);

        DB::table('ssfs')->insert([
            'code' => 'ADA',
            'id_sfs' => '4',
            'libelle' => 'Offre de transit IP au niveau de backbone IP',
            'created_at'  => Carbon::now(),
            'updated_at'  => Carbon::now()
        ]);

        DB::table('ssfs')->insert([
            'code' => 'ADB',
            'id_sfs' => '4',
            'libelle' => 'Offre de transit IP au niveau des POP',
            'created_at'  => Carbon::now(),
            'updated_at'  => Carbon::now()
        ]);

        DB::table('ssfs')->insert([
            'code' => 'CCA',
            'id_sfs' => '10',
            'libelle' => 'Location de pylône par les opérateurs mobiles',
            'created_at'  => Carbon::now(),
            'updated_at'  => Carbon::now()
        ]);

        DB::table('ssfs')->insert([
            'code' => 'CCB',
            'id_sfs' => '10',
            'libelle' => 'Location de pylône par les FAI et autres',
            'created_at'  => Carbon::now(),
            'updated_at'  => Carbon::now()
        ]);


    }
}
