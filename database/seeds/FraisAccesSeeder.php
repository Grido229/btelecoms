<?php

use Carbon\Carbon;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class FraisAccesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $file = public_path('files/fraisAcces.csv');
        $readFile = fopen($file,'r');

            while (!feof($readFile)) {
                $ligne = fgets($readFile,4096);
                $table_ligne = explode(';' , $ligne);


                if ($ligne != NULL) {

                    $id_sfs = $table_ligne[0];
                    $id_ssfs = $table_ligne[1];
                    $id_capacite = $table_ligne[2];
                    $frais = $table_ligne[3];
                    $frais_mod = $table_ligne[4];

                        if($id_ssfs != 'null'){


                            if($id_ssfs != ''){
                                DB::table('frais_acces')->insert([
                                    'id_sfs' => $id_sfs,
                                    'id_ssfs' => $id_ssfs,
                                    'id_capacite' => $id_capacite,
                                    'frais_mod' =>  $frais_mod,
                                    'frais'    =>  $frais,
                                    'created_at'  => Carbon::now(),
                                    'updated_at'  => Carbon::now()
                                ]);
                            }else{

                                DB::table('frais_acces')->insert([
                                    'id_sfs' => $id_sfs,
                                    'id_capacite' => $id_capacite,
                                    'frais_mod' =>  $frais_mod,
                                    'frais'    =>  $frais,
                                    'created_at'  => Carbon::now(),
                                    'updated_at'  => Carbon::now()
                                ]);
                            }

                        }


                }

        }
    }
}
