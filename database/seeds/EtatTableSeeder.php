<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;

class EtatTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
                $file = public_path('files/etat.csv');
                $readFile = fopen($file,'r');

                    while (!feof($readFile)) {
                        $ligne = fgets($readFile,4096);
                        $table_ligne = explode(';' , $ligne);

                        if ($ligne != NULL) {

                        $ref_pv =  ($table_ligne[0] != '') ? $table_ligne[0] : NULL;
                        $nom_service =  ($table_ligne[1] != '') ? $table_ligne[1] : NULL;
                        $type_service =  ($table_ligne[2] != '') ? $table_ligne[2] : NULL;
                        $date_activation =  ($table_ligne[3] != '') ? $table_ligne[3] : NULL;
                        $frais =  ($table_ligne[4] != '') ? $table_ligne[4] : NULL;
                        $redevance =  ($table_ligne[5] != '') ? $table_ligne[5] : NULL;
                        $frequence_redevance =  ($table_ligne[6] != '') ? $table_ligne[6] : NULL;
                        $date_premiere_facturation =  ($table_ligne[7] != '') ? $table_ligne[7] : NULL;
                        $statut =  ($table_ligne[8] != '') ? $table_ligne[8] : NULL;
                        $date_statut =  ($table_ligne[9] != '') ? $table_ligne[9] : NULL;
                        $raison =  ($table_ligne[10] != '') ? $table_ligne[10] : NULL;
                        $ref_contrat =  ($table_ligne[11] != '') ? $table_ligne[11] : NULL;
                        $ref_demande =  ($table_ligne[12] != '') ? $table_ligne[12]: NULL;
                        $nom_operateur =  ($table_ligne[13] != '') ? $table_ligne[13]:NULL;






                                    # code...
                                    DB::table('etat')->insert([

                                        'ref_pv' => $ref_pv,
                                        'nom_service' => $nom_service,
                                        'type_service' => $type_service,
                                        'date_activation' =>  $date_activation,
                                        'frais'    =>  $frais,
                                        'redevance' => $redevance,
                                        'frequence_redevance' => $frequence_redevance,
                                        'date_premiere_facturation' => $date_premiere_facturation,
                                        'statut'  => $statut,
                                        'date_statut' => $date_statut,
                                        'raison' => $raison,
                                        'ref_contrat' => $ref_contrat,
                                        'ref_demande' => $ref_demande,
                                        'nom_operateur' => $nom_operateur,

                                        'created_at'  => Carbon::now(),
                                        'updated_at'  => Carbon::now()
                                    ]);


                        }
                }

            }
}
