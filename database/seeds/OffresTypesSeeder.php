<?php

use App\OffresType;
use Illuminate\Database\Seeder;

class OffresTypesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $type_bande_passante = new OffresType();
        $type_bande_passante->name = 'BANDE PASSANTE';
        $type_bande_passante->save();

        $type_fibre_terre= new OffresType();
        $type_fibre_terre->name = 'FIBRE OPTIQUE TERRESTRE';
        $type_fibre_terre->save();

        $type_fibre_noire= new OffresType();
        $type_fibre_noire->name = 'FIBRE OPTIQUE NOIRE';
        $type_fibre_noire->save();

        $type_location_site = new OffresType();
        $type_location_site->name = 'LOCATION DE SITE';
        $type_location_site->save();

        $type_intersites = new OffresType();
        $type_intersites->name = 'MICS INTERSITES';
        $type_intersites->save();

        $type_sat3 = new OffresType();
        $type_sat3->name = 'SAT3';
        $type_sat3->save();

        $type_sat3 = new OffresType();
        $type_sat3->name = 'DESSERTE';
        $type_sat3->save();
    }
}
