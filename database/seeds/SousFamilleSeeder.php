<?php

use Carbon\Carbon;
use Illuminate\Database\Seeder;

class SousFamilleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('sfs')->insert([
            'code' => 'AA',
            'id_fs' => '1',
            'libelle' => 'Location de capacités nationales',
            'created_at'  => Carbon::now(),
            'updated_at'  => Carbon::now()
        ]);

        DB::table('sfs')->insert([
            'code' => 'AB',
            'id_fs' => '1',
            'libelle' => 'Location de capacités sur câbles sous-marins',
            'created_at'  => Carbon::now(),
            'updated_at'  => Carbon::now()
        ]);

        DB::table('sfs')->insert([
            'code' => 'AC',
            'id_fs' => '1',
            'libelle' => 'Complément terrestre pour l’accès aux câbles sous-marins',
            'created_at'  => Carbon::now(),
            'updated_at'  => Carbon::now()
        ]);

        DB::table('sfs')->insert([
            'code' => 'AD',
            'id_fs' => '1',
            'libelle' => 'Offre  de capacité de transit IP',
            'created_at'  => Carbon::now(),
            'updated_at'  => Carbon::now()
        ]);

        DB::table('sfs')->insert([
            'code' => 'AE',
            'id_fs' => '1',
            'libelle' => 'Offre de liaisons de roaming international (64k)',
            'created_at'  => Carbon::now(),
            'updated_at'  => Carbon::now()
        ]);

        DB::table('sfs')->insert([
            'code' => 'AF',
            'id_fs' => '1',
            'libelle' => 'Offre Fibre Optique Noire',
            'created_at'  => Carbon::now(),
            'updated_at'  => Carbon::now()
        ]);

        DB::table('sfs')->insert([
            'code' => 'BA',
            'id_fs' => '2',
            'libelle' => 'Cross connect',
            'created_at'  => Carbon::now(),
            'updated_at'  => Carbon::now()
        ]);


        DB::table('sfs')->insert([
            'code' => 'CA',
            'id_fs' => '3',
            'libelle' => 'Location de terrains nus, de terrasses et de locaux administratifs ou techniques',
            'created_at'  => Carbon::now(),
            'updated_at'  => Carbon::now()
        ]);

        DB::table('sfs')->insert([
            'code' => 'CB',
            'id_fs' => '3',
            'libelle' => 'Colocalisation en salle',
            'created_at'  => Carbon::now(),
            'updated_at'  => Carbon::now()
        ]);

        DB::table('sfs')->insert([
            'code' => 'CC',
            'id_fs' => '3',
            'libelle' => 'Location de pylônes ',
            'created_at'  => Carbon::now(),
            'updated_at'  => Carbon::now()
        ]);

        DB::table('sfs')->insert([
            'code' => 'CD',
            'id_fs' => '3',
            'libelle' => 'Location d’énergie',
            'created_at'  => Carbon::now(),
            'updated_at'  => Carbon::now()
        ]);

        DB::table('sfs')->insert([
            'code' => 'CE',
            'id_fs' => '3',
            'libelle' => 'Génie civil et fourreaux',
            'created_at'  => Carbon::now(),
            'updated_at'  => Carbon::now()
        ]);

        DB::table('sfs')->insert([
            'code' => 'DA',
            'id_fs' => '4',
            'libelle' => 'Raccordement au centre de transit national',
            'created_at'  => Carbon::now(),
            'updated_at'  => Carbon::now()
        ]);

        DB::table('sfs')->insert([
            'code' => 'DB',
            'id_fs' => '4',
            'libelle' => 'Raccordement au centre de transit international',
            'created_at'  => Carbon::now(),
            'updated_at'  => Carbon::now()
        ]);




    }
}
