<?php

use Carbon\Carbon;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class FamilleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('fs')->insert([
            'code' => 'A',
            'libelle' => 'Service Transport',
            'created_at'  => Carbon::now(),
            'updated_at'  => Carbon::now()
        ]);

        DB::table('fs')->insert([
            'code' => 'B',
            'libelle' => 'Service D\'Acces',
            'created_at'  => Carbon::now(),
            'updated_at'  => Carbon::now()
        ]);

        DB::table('fs')->insert([
            'code' => 'C',
            'libelle' => 'Service D\'Hebergement',
            'created_at'  => Carbon::now(),
            'updated_at'  => Carbon::now()
        ]);

        DB::table('fs')->insert([
            'code' => 'D',
            'libelle' => 'Service Trafic Voix',
            'created_at'  => Carbon::now(),
            'updated_at'  => Carbon::now()
        ]);

    }
}
